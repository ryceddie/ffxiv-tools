package zc.games.ffxiv.tools.data.init;

import java.io.File;

import lombok.Value;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.BaseData.Ingredients;
import zc.games.ffxiv.tools.data.BaseDataParser;
import zc.games.ffxiv.tools.data.BaseDataParserV5;
import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.xivapi.GameContent;
import zc.games.ffxiv.tools.data.xivapi.GameContentProvider;

/**
 * 
 * @author Zhang Chi
 * @since 2020-03-03
 *
 */
public class BaseDataInitialization {

	public BaseData init(Config config) {
		GameContent gameContent = GameContentProvider.builder()
				.defaultClient()
				.cachingAt(new File(config.getWebResourceCacheDir()))
				.build().provide();
		
		
		Ingredients ingredients = new BaseDataParserV5()
				.withJobCategoryIdResolver(
						jobId -> gameContent.getClassJob(Integer.parseInt(jobId)).getCategoryId())
				.withJobParentIdResolver(jobId -> {
					int parentId = gameContent.getClassJob(Integer.parseInt(jobId)).getParentId();
					return String.valueOf(parentId);
				})
				.parse()
				.withItemCategories(new BaseDataParser().parse().getItemCategories());
		
		//validation
		ingredients.getJobCategories().stream()
		.filter(category -> category.getCode() != 0)
		.filter(category -> category.getJobs().isEmpty())
		.findAny().ifPresent(category -> {
			throw new DataException("can not find any class/job included by category: " + category);
		});
		
		return ingredients.compose();
	}
	
	@Value
	public static class Config {
		
		private String webResourceCacheDir;
	}
}
