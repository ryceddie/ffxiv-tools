package zc.games.ffxiv.tools.view;

import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

import lombok.Value;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.Material;

@Value
public class CompositeBom {

	private Map<String, Bom> components;
	
	public CompositeBom(Map<String, Bom> components) {
		this.components = ImmutableSortedMap.copyOf(components);
	}

	public static CompositeBom from(Bom original, Function<Material, String> grouper) {
		Map<String, Bom> components = Maps.transformValues(
				Multimaps.index(original, grouper).asMap(),
				mc -> new Bom(mc));
		return new CompositeBom(components);
	}
	
	public String toString() {
		String repr = this.components.entrySet().stream()
				.map(e -> String.format("- %s:%s", e.getKey(), e.getValue().toList()))
				.collect(Collectors.joining("\n"));
		return String.format("BOM:%n%s", repr);
	}
}
