package zc.games.ffxiv.tools.model;

public interface ItemRecipe {

	Material getProduct();
	
	default int countFor(int productNum) {
		int craftingCount = productNum / this.getProduct().getCount();
		if (productNum % this.getProduct().getCount() > 0) {
			craftingCount++;
		}
		return craftingCount;
	}

	/**
	 * 
	 * @param target
	 * @return
	 * @deprecated use {@link #countFor(int)}
	 */
	@Deprecated
	default int countFor(Material target) {
		return this.countFor(target.getCount());
	}

	Iterable<Material> getBom();
	
	default Bom getBom(int productNum) {
		return new Bom(this.getBom()).multiply(this.countFor(productNum));
	}
}
