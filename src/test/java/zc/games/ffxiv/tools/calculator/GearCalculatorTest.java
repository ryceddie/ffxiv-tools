package zc.games.ffxiv.tools.calculator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Multimap;

import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.service.PlayerCharacterService;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.JobState;
import zc.games.ffxiv.tools.model.PlayerCharacter;
import zc.games.ffxiv.tools.model.PlayerCharacter.JobStates;

public class GearCalculatorTest {
	
	private GearCalculator calculator;
	
	private PlayerCharacterService pcService;
	
	private ItemRepository itemRepo;
	
	private BaseData baseData;
	
	@Before
	public void setup() {
		this.itemRepo = mock(ItemRepository.class);
		this.baseData = mock(BaseData.class);
		this.pcService = mock(PlayerCharacterService.class);
		this.calculator = new GearCalculator(this.pcService, itemRepo, baseData);
	}

	@Test
	public void trivial_case() {
		Job job = Job.builder().id("job001").build();
		PlayerCharacter pc = PlayerCharacter.builder()
				.currentJob(job)
				.build();
		Multimap<GearSlot, Item> items = this.calculator.upgradeSuggestion(pc, Optional.empty(), GearCalculator.Options.NONE);
		
		assertThat(items.values()).isEmpty();
	}
	
	@Test
	public void simple_case() {
		Job job = Job.builder().id("job001").build();
		
		Item.Category itemCategory = new Item.Category("1", "mock-cat-01", Item.CategoryGroup.ARM);
		Item item1 = Item.builder()
				.id("1")
				.name("mock-item01")
				.category(itemCategory)
				.level(50)
				.requiredLevel(50)
				.jobs(Arrays.asList(job))
				.build();
		Item item2 = Item.builder()
				.id("2")
				.name("mock-item02")
				.category(itemCategory)
				.level(55)
				.requiredLevel(55)
				.jobs(Arrays.asList(job))
				.build();
		when(this.itemRepo.getAllGears())
		.thenReturn(Arrays.asList(item1, item2));
		when(this.baseData.getItemCategory("1"))
		.thenReturn(itemCategory);
		
		PlayerCharacter pc = PlayerCharacter.builder()
				.currentJob(job)
				.jobStates(new JobStates(Arrays.asList(new JobState(job, 50)), Collections.emptyList()))
				.build();
		Multimap<GearSlot, Item> items = this.calculator.upgradeSuggestion(pc, Optional.empty(), GearCalculator.Options.NONE);
		
		assertThat(items.get(GearSlot.MAIN_HAND)).containsExactly(item1);		
	}
}
