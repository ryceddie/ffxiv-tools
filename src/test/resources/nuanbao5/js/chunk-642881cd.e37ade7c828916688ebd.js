(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chunk-642881cd"], {
    "0a49": function(t, e, s) {
        var a = s("9b43")
          , i = s("626a")
          , n = s("4bf8")
          , o = s("9def")
          , l = s("cd1c");
        t.exports = function(t, e) {
            var s = 1 == t
              , r = 2 == t
              , c = 3 == t
              , u = 4 == t
              , d = 6 == t
              , h = 5 == t || d
              , p = e || l;
            return function(e, l, m) {
                for (var f, g, b = n(e), v = i(b), y = a(l, m, 3), k = o(v.length), C = 0, w = s ? p(e, k) : r ? p(e, 0) : void 0; k > C; C++)
                    if ((h || C in v) && (f = v[C],
                    g = y(f, C, b),
                    t))
                        if (s)
                            w[C] = g;
                        else if (g)
                            switch (t) {
                            case 3:
                                return !0;
                            case 5:
                                return f;
                            case 6:
                                return C;
                            case 2:
                                w.push(f)
                            }
                        else if (u)
                            return !1;
                return d ? -1 : c || u ? u : w
            }
        }
    },
    1169: function(t, e, s) {
        var a = s("2d95");
        t.exports = Array.isArray || function(t) {
            return "Array" == a(t)
        }
    },
    "386d": function(t, e, s) {
        "use strict";
        var a = s("cb7c")
          , i = s("83a1")
          , n = s("5f1b");
        s("214f")("search", 1, function(t, e, s, o) {
            return [function(s) {
                var a = t(this)
                  , i = void 0 == s ? void 0 : s[e];
                return void 0 !== i ? i.call(s, a) : new RegExp(s)[e](String(a))
            }
            , function(t) {
                var e = o(s, t, this);
                if (e.done)
                    return e.value;
                var l = a(t)
                  , r = String(this)
                  , c = l.lastIndex;
                i(c, 0) || (l.lastIndex = 0);
                var u = n(l, r);
                return i(l.lastIndex, c) || (l.lastIndex = c),
                null === u ? -1 : u.index
            }
            ]
        })
    },
    6070: function(t) {
        t.exports = {
            toolbar: [{
                type: "job",
                jid: 0,
                label: "刻木匠",
                icon: 62108,
                i18n: "job.CRP"
            }, {
                type: "job",
                jid: 1,
                label: "锻铁匠",
                icon: 62109,
                i18n: "job.BSM"
            }, {
                type: "job",
                jid: 2,
                label: "铸甲匠",
                icon: 62110,
                i18n: "job.ARM"
            }, {
                type: "job",
                jid: 3,
                label: "雕金匠",
                icon: 62111,
                i18n: "job.GSM"
            }, {
                type: "job",
                jid: 4,
                label: "制革匠",
                icon: 62112,
                i18n: "job.LTW"
            }, {
                type: "job",
                jid: 5,
                label: "裁衣匠",
                icon: 62113,
                i18n: "job.WVR"
            }, {
                type: "job",
                jid: 6,
                label: "炼金术士",
                icon: 62114,
                i18n: "job.ALC"
            }, {
                type: "job",
                jid: 7,
                label: "烹调师",
                icon: 62115,
                i18n: "job.CUL"
            }, {
                type: "meau",
                label: "收藏",
                icon: 61830,
                i18n: "toolbar.lis_mine"
            }, {
                type: "meau",
                label: "推荐",
                icon: 61831,
                i18n: "toolbar.lis_hot"
            }, {
                type: "meau",
                label: "反查历史",
                icon: 61826,
                i18n: "toolbar.his_back"
            }],
            v5pwsbar: [{
                type: "meau",
                label: "潜水艇",
                icon: 61821,
                value: "submersible"
            }, {
                type: "meau",
                label: "飞空艇",
                icon: 61821,
                value: "airship"
            }, {
                type: "meau",
                label: "外墙",
                icon: 61821,
                value: "wall"
            }, {
                type: "meau",
                label: "转轮",
                icon: 61821,
                value: "wheel"
            }],
            blessedbar: [{
                type: "job",
                jid: 0,
                label: "刻木匠",
                icon: 62502,
                i18n: "job.CRP"
            }, {
                type: "job",
                jid: 1,
                label: "锻铁匠",
                icon: 62503,
                i18n: "job.BSM"
            }, {
                type: "job",
                jid: 2,
                label: "铸甲匠",
                icon: 62504,
                i18n: "job.ARM"
            }, {
                type: "job",
                jid: 3,
                label: "雕金匠",
                icon: 62505,
                i18n: "job.GSM"
            }, {
                type: "job",
                jid: 4,
                label: "制革匠",
                icon: 62506,
                i18n: "job.LTW"
            }, {
                type: "job",
                jid: 5,
                label: "裁衣匠",
                icon: 62507,
                i18n: "job.WVR"
            }, {
                type: "job",
                jid: 6,
                label: "炼金术士",
                icon: 62508,
                i18n: "job.ALC"
            }, {
                type: "job",
                jid: 7,
                label: "烹调师",
                icon: 62509,
                i18n: "job.CUL"
            }],
            job_note_nomel: [["76 - 80", 15], ["71 - 75", 14], ["66 - 70", 13], ["60 - 65", 12], ["56 - 60", 11], ["50 - 55", 10], ["46 - 50", 9], ["40 - 45", 8], ["36 - 40", 7], ["30 - 35", 6], ["26 - 30", 5], ["20 - 25", 4], ["16 - 20", 3], ["10 - 15", 2], ["06 - 10", 1], ["01 - 05", 0]],
            job_note_special: [["秘籍第一卷", 1e3], ["秘籍第二卷", 1001], ["秘籍第三卷", 1002], ["秘籍第四卷", 1003], ["秘籍第五卷", 1004], ["秘籍第六卷", 1005], ["秘籍第七卷", 1006], ["秘籍·其他", 1007], ["房屋1", 1008], ["房屋2", 1009], ["染剂", 1010], ["其他(pvp)", 1011], ["职业任务", 1012], ["工艺馆", 1013], ["工艺馆", 1014], ["工艺馆", 1015], ["鸟人族蛮族", 1016], ["莫古力蛮族", 1017], ["鲶鱼精蛮族", 1018], ["老主顾·熙洛", 1019], ["老主顾·梅", 1020], ["老主顾·红", 1021], ["老主顾·波奇", 1022]],
            blessed: [["01 - 05", 0], ["06 - 10", 1], ["10 - 15", 2], ["16 - 20", 3], ["20 - 25", 4], ["26 - 30", 5], ["30 - 35", 6], ["36 - 40", 7], ["40 - 45", 8], ["46 - 50", 9], ["50 - 55", 10], ["56 - 60", 11], ["60 - 65", 12], ["66 - 70", 13], ["71 - 75", 14], ["76 - 80", 15], ["房屋1", 1008], ["房屋2", 1009]],
            job_note_hash: {
                0: [[0, 8], [40, 13], [80, 7], [120, 7], [160, 15], [200, 21], [240, 8], [280, 15]],
                1: [[1, 10], [41, 16], [81, 8], [121, 11], [161, 17], [201, 18], [241, 8], [281, 13]],
                2: [[2, 11], [42, 18], [82, 14], [122, 17], [162, 22], [202, 22], [242, 8], [282, 13]],
                3: [[3, 11], [43, 24], [83, 12], [123, 18], [163, 16], [203, 20], [243, 11], [283, 15]],
                4: [[4, 12], [44, 24], [84, 16], [124, 28], [164, 18], [204, 21], [244, 12], [284, 13]],
                5: [[5, 10], [45, 25], [85, 19], [125, 33], [165, 18], [205, 21], [245, 12], [285, 16]],
                6: [[6, 11], [46, 26], [86, 21], [126, 39], [166, 21], [206, 28], [246, 20], [286, 11]],
                7: [[7, 11], [47, 24], [87, 19], [127, 41], [167, 29], [207, 33], [247, 11], [287, 11]],
                8: [[8, 12], [48, 37], [88, 34], [128, 51], [168, 40], [208, 55], [248, 12], [288, 9]],
                9: [[9, 22], [49, 39], [89, 54], [129, 74], [169, 88], [209, 91], [249, 51], [289, 27]],
                10: [[10, 18], [50, 48], [90, 35], [130, 77], [170, 84], [210, 78], [250, 63], [290, 15]],
                11: [[11, 19], [51, 40], [91, 36], [131, 59], [171, 38], [211, 34], [251, 13], [291, 15]],
                12: [[12, 24], [52, 60], [92, 34], [132, 86], [172, 76], [212, 61], [252, 18], [292, 18]],
                13: [[13, 22], [53, 50], [93, 30], [133, 73], [173, 64], [213, 44], [253, 69], [293, 18]],
                14: [[14, 32], [54, 56], [94, 52], [134, 71], [174, 69], [214, 61], [254, 18], [294, 19]],
                15: [[15, 20], [55, 51], [95, 33], [135, 57], [175, 45], [215, 40], [255, 14], [295, 17]],
                1000: [[1e3, 17], [1001, 15], [1002, 18], [1003, 16], [1004, 25], [1005, 19], [1006, 47], [1007, 11]],
                1001: [[1008, 9], [1009, 14], [1010, 11], [1011, 27], [1012, 22], [1013, 18], [1014, 18], [1015, 4]],
                1002: [[1016, 32], [1017, 31], [1018, 40], [1019, 46], [1020, 112], [1021, 93], [1022, 37], [1023, 11]],
                1003: [[1024, 61], [1025, 87], [1026, 91], [1027, 87], [1028, 105], [1029, 89], [1030, 47], [1031, 17]],
                1004: [[1032, 17], [1033, 15], [1034, 12], [1035, 20], [1036, 23], [1037, 26], [1038, 13], [1039, 5]],
                1005: [[1040, 47], [1041, 35], [1042, 28], [1043, 53], [1044, 46], [1045, 69], [1046, 39], [1047, 18]],
                1006: [[1048, 9], [1049, 25], [1050, 19], [1051, 28], [1052, 22], [1053, 23], [1054, 14], [1055, 12]],
                1007: [[1056, 7], [1057, 22], [1058, 3], [1059, 41], [1060, 17], [1061, 14], [1062, 26], [1063, 0]],
                1008: [[1064, 87], [1065, 29], [1066, 30], [1067, 79], [1068, 6], [1069, 27], [1070, 65], [1071, 4]],
                1009: [[1072, 137], [1073, 32], [1074, 28], [1075, 54], [1076, 26], [1077, 51], [1078, 24], [1079, 33]],
                1010: [[1080, 25], [1081, 25], [1082, 25], [1083, 25], [1084, 25], [1085, 25], [1086, 25], [1087, 25]],
                1011: [[1088, 3], [1089, 6], [1090, 12], [1091, 26], [1092, 11], [1093, 14], [1094, 4], [1095, 0]],
                1012: [[1096, 4], [1097, 4], [1098, 4], [1099, 4], [1100, 4], [1101, 4], [1102, 4], [1103, 4]],
                1013: [[1104, 0], [1105, 6], [1106, 6], [1107, 6], [1108, 0], [1109, 0], [1110, 0], [1111, 0]],
                1014: [[1112, 6], [1113, 0], [1114, 0], [1115, 0], [1116, 6], [1117, 6], [1118, 0], [1119, 0]],
                1015: [[1120, 0], [1121, 0], [1122, 0], [1123, 0], [1124, 0], [1125, 0], [1126, 6], [1127, 6]],
                1016: [[1128, 39], [1129, 39], [1130, 39], [1131, 39], [1132, 39], [1133, 39], [1134, 39], [1135, 39]],
                1017: [[1136, 26], [1137, 26], [1138, 26], [1139, 26], [1140, 26], [1141, 26], [1142, 26], [1143, 26]],
                1018: [[1144, 27], [1145, 27], [1146, 27], [1147, 27], [1148, 27], [1149, 27], [1150, 27], [1151, 27]],
                1019: [[1152, 10], [1153, 10], [1154, 10], [1155, 10], [1156, 10], [1157, 10], [1158, 10], [1159, 10]],
                1020: [[1160, 10], [1161, 10], [1162, 10], [1163, 10], [1164, 10], [1165, 10], [1166, 10], [1167, 10]],
                1021: [[1168, 10], [1169, 10], [1170, 10], [1171, 10], [1172, 10], [1173, 10], [1174, 10], [1175, 10]],
                1022: [[1176, 10], [1177, 10], [1178, 10], [1179, 10], [1180, 10], [1181, 10], [1182, 10], [1183, 10]]
            }
        }
    },
    "6ec0": function(t, e, s) {},
    7303: function(t, e, s) {
        "use strict";
        var a = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", [s("div", {
                staticClass: "list-img",
                attrs: {
                    title: t.item.title
                }
            }, [s("img", {
                staticClass: "img",
                attrs: {
                    src: t.getImg(t.item.icon, t.item.hq)
                }
            }), s("div", {
                staticClass: "bd"
            })]), s("div", {
                staticClass: "name2"
            }, [s("span", {
                class: t.item.dye ? "dye" : "hide",
                attrs: {
                    title: "可染色、can dye"
                }
            }), s("span", {
                staticClass: "t1"
            }, [s("copyName", {
                attrs: {
                    title: t.item.title,
                    message: t.item.name
                }
            })], 1), s("span", {
                staticClass: "t2"
            }, [s("img", {
                staticClass: "img3",
                attrs: {
                    title: t.item.ucTitle,
                    src: t.getImg(t.item.ucIcon)
                }
            }), s("span", {
                attrs: {
                    title: t.item.jobTitle
                }
            }, [s("img", {
                staticClass: "img2",
                staticStyle: {
                    "margin-left": "3px"
                },
                attrs: {
                    src: t.getImg(t.item.jobIcon)
                }
            }), s("span", {
                staticClass: "t4"
            }, [t._v(" rLv" + t._s(t.item.rlv) + t._s(t.item.rs) + " ")])])])]), s("div", {
                staticClass: "act2",
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.stop(e)
                    }
                }
            }, [s("InputNumber", {
                attrs: {
                    "active-change": !1,
                    size: "small",
                    max: 999,
                    min: t.min
                },
                on: {
                    "on-change": t.changeNum
                },
                model: {
                    value: t.times,
                    callback: function(e) {
                        t.times = e
                    },
                    expression: "times"
                }
            })], 1)])
        }
          , i = []
          , n = (s("c5f6"),
        s("c276"))
          , o = s("c1b4")
          , l = {
            name: "calitem",
            components: {
                copyName: o["a"]
            },
            props: {
                item: {
                    type: Object
                },
                num: {
                    type: Number
                },
                min: {
                    type: Number,
                    default: 0
                },
                commit: {
                    type: Boolean,
                    default: function() {
                        return !0
                    }
                }
            },
            data: function() {
                return {
                    times: null
                }
            },
            watch: {
                num: function(t) {
                    this.times = t
                }
            },
            methods: {
                changeNum: function(t) {
                    this.commit ? this.$store.commit("changeCalList", {
                        id: this.item.id,
                        val: t
                    }) : this.$emit("on-handle", {
                        id: this.item.id,
                        val: t
                    })
                },
                getImg: function(t, e) {
                    return Object(n["b"])(t, e)
                },
                stop: function(t) {
                    try {
                        t.stopPropagation()
                    } catch (e) {
                        window.event.cancelBubble = !0
                    }
                }
            },
            mounted: function() {
                this.times = this.item.num
            }
        }
          , r = l
          , c = s("2877")
          , u = Object(c["a"])(r, a, i, !1, null, null, null);
        e["a"] = u.exports
    },
    7514: function(t, e, s) {
        "use strict";
        var a = s("5ca1")
          , i = s("0a49")(5)
          , n = "find"
          , o = !0;
        n in [] && Array(1)[n](function() {
            o = !1
        }),
        a(a.P + a.F * o, "Array", {
            find: function(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0)
            }
        }),
        s("9c6c")(n)
    },
    "80d9": function(t, e, s) {},
    "83a1": function(t, e) {
        t.exports = Object.is || function(t, e) {
            return t === e ? 0 !== t || 1 / t === 1 / e : t != t && e != e
        }
    },
    "98e6": function(t, e, s) {
        "use strict";
        var a = s("6ec0")
          , i = s.n(a);
        i.a
    },
    9967: function(t, e, s) {},
    b315: function(t, e, s) {
        "use strict";
        var a = s("9967")
          , i = s.n(a);
        i.a
    },
    cd1c: function(t, e, s) {
        var a = s("e853");
        t.exports = function(t, e) {
            return new (a(t))(e)
        }
    },
    d8f4: function(t, e, s) {},
    e853: function(t, e, s) {
        var a = s("d3f4")
          , i = s("1169")
          , n = s("2b4c")("species");
        t.exports = function(t) {
            var e;
            return i(t) && (e = t.constructor,
            "function" != typeof e || e !== Array && !i(e.prototype) || (e = void 0),
            a(e) && (e = e[n],
            null === e && (e = void 0))),
            void 0 === e ? Array : e
        }
    },
    f0c6: function(t, e, s) {
        "use strict";
        var a = s("d8f4")
          , i = s.n(a);
        i.a
    },
    f36b: function(t, e, s) {
        "use strict";
        var a = s("80d9")
          , i = s.n(a);
        i.a
    },
    f476: function(t, e, s) {
        "use strict";
        s.r(e);
        var a = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", [s("div", {
                staticClass: "m-Content"
            }, [s("Card", {
                staticClass: "m-card"
            }, [s("p", {
                staticClass: "p1",
                attrs: {
                    slot: "title"
                },
                slot: "title"
            }, [t._v("\n        V5+ Alpha\n      ")]), s("a", {
                attrs: {
                    slot: "extra",
                    href: "javascript:void(0)"
                },
                slot: "extra"
            }, [s("Icon", {
                attrs: {
                    type: "ios-contact",
                    size: "20",
                    title: "个人中心"
                },
                on: {
                    click: t.showUserInfo
                }
            }), s("Icon", {
                attrs: {
                    type: "ios-settings",
                    size: "20",
                    title: "系统设置"
                },
                on: {
                    click: function(e) {
                        return t.$refs.setting.show()
                    }
                }
            }), s("Icon", {
                attrs: {
                    type: "md-refresh",
                    size: "20",
                    title: "强制刷新缓存"
                },
                on: {
                    click: t.reload
                }
            }), s("Icon", {
                attrs: {
                    type: "ios-help-circle-outline",
                    size: "20",
                    title: "声明"
                },
                on: {
                    click: function(e) {
                        return t.$refs.sysdialog.show()
                    }
                }
            })], 1), s("Row", {
                staticStyle: {
                    padding: "5px",
                    "margin-left": "5px",
                    "border-bottom": "solid 1px rgba(50, 50, 50, 0.15)"
                },
                attrs: {
                    type: "flex"
                }
            }, [t._l(t.toolbar, function(e) {
                return s("a", {
                    key: e.icon,
                    staticClass: "toolbar",
                    attrs: {
                        title: e.label
                    },
                    on: {
                        click: function(s) {
                            return t.onToolbarClick(e.icon, e.jid)
                        }
                    }
                }, [s("div", {
                    class: e.icon == t.jobiconid ? "active" : ""
                }, [s("div", {
                    class: t.getIconCss(e.icon)
                })])])
            }), s("Input", {
                staticStyle: {
                    width: "260px",
                    "margin-top": "4px",
                    "margin-left": "116px"
                },
                attrs: {
                    size: "small",
                    placeholder: "输入关键字、或设置高级筛选条件",
                    search: "",
                    "enter-button": ""
                },
                on: {
                    "on-search": t.doSearch
                },
                model: {
                    value: t.searchKey,
                    callback: function(e) {
                        t.searchKey = e
                    },
                    expression: "searchKey"
                }
            }), s("Icon", {
                staticStyle: {
                    "margin-top": "6px",
                    "margin-left": "5px",
                    cursor: "pointer"
                },
                attrs: {
                    type: "ios-settings-outline",
                    size: "20",
                    title: "检索设置"
                },
                on: {
                    click: function(e) {
                        t.showSeModal = !t.showSeModal
                    }
                }
            }), s("Card", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.showSeModal,
                    expression: "showSeModal"
                }],
                staticStyle: {
                    width: "283px",
                    height: "420px",
                    display: "block",
                    position: "absolute",
                    "z-index": "100",
                    "background-color": "rgba(255, 255, 255, 0.9)",
                    left: "509px",
                    top: "73px",
                    overflow: "initial"
                }
            }, [s("p", {
                attrs: {
                    slot: "title"
                },
                slot: "title"
            }, [t._v("高级检索条件")]), s("a", {
                attrs: {
                    slot: "extra",
                    href: "#"
                },
                slot: "extra"
            }, [s("Icon", {
                attrs: {
                    type: "md-close-circle"
                },
                on: {
                    click: function(e) {
                        t.showSeModal = !t.showSeModal
                    }
                }
            })], 1), s("div", {
                staticClass: "seConf",
                staticStyle: {
                    "padding-right": "7px"
                }
            }, [s("Form", {
                attrs: {
                    "label-position": "right",
                    "label-width": 110
                }
            }, [s("FormItem", {
                attrs: {
                    label: "是否有配方："
                }
            }, [s("i-switch", {
                model: {
                    value: t.seConf.hazRid,
                    callback: function(e) {
                        t.$set(t.seConf, "hazRid", e)
                    },
                    expression: "seConf.hazRid"
                }
            }, [s("Icon", {
                attrs: {
                    slot: "open",
                    type: "md-checkmark"
                },
                slot: "open"
            }), s("Icon", {
                attrs: {
                    slot: "close",
                    type: "md-close"
                },
                slot: "close"
            })], 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.eRid,
                    callback: function(e) {
                        t.$set(t.seConf, "eRid", e)
                    },
                    expression: "seConf.eRid"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "是否可收藏品："
                }
            }, [s("i-switch", {
                model: {
                    value: t.seConf.hazBon,
                    callback: function(e) {
                        t.$set(t.seConf, "hazBon", e)
                    },
                    expression: "seConf.hazBon"
                }
            }, [s("Icon", {
                attrs: {
                    slot: "open",
                    type: "md-checkmark"
                },
                slot: "open"
            }), s("Icon", {
                attrs: {
                    slot: "close",
                    type: "md-close"
                },
                slot: "close"
            })], 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.ebon,
                    callback: function(e) {
                        t.$set(t.seConf, "ebon", e)
                    },
                    expression: "seConf.ebon"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "是否可精选："
                }
            }, [s("i-switch", {
                model: {
                    value: t.seConf.canReduce,
                    callback: function(e) {
                        t.$set(t.seConf, "canReduce", e)
                    },
                    expression: "seConf.canReduce"
                }
            }, [s("Icon", {
                attrs: {
                    slot: "open",
                    type: "md-checkmark"
                },
                slot: "open"
            }), s("Icon", {
                attrs: {
                    slot: "close",
                    type: "md-close"
                },
                slot: "close"
            })], 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.eReduce,
                    callback: function(e) {
                        t.$set(t.seConf, "eReduce", e)
                    },
                    expression: "seConf.eReduce"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "是否可分解："
                }
            }, [s("i-switch", {
                model: {
                    value: t.seConf.canDesy,
                    callback: function(e) {
                        t.$set(t.seConf, "canDesy", e)
                    },
                    expression: "seConf.canDesy"
                }
            }, [s("Icon", {
                attrs: {
                    slot: "open",
                    type: "md-checkmark"
                },
                slot: "open"
            }), s("Icon", {
                attrs: {
                    slot: "close",
                    type: "md-close"
                },
                slot: "close"
            })], 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.eDesy,
                    callback: function(e) {
                        t.$set(t.seConf, "eDesy", e)
                    },
                    expression: "seConf.eDesy"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "是否可染色："
                }
            }, [s("i-switch", {
                model: {
                    value: t.seConf.canDye,
                    callback: function(e) {
                        t.$set(t.seConf, "canDye", e)
                    },
                    expression: "seConf.canDye"
                }
            }, [s("Icon", {
                attrs: {
                    slot: "open",
                    type: "md-checkmark"
                },
                slot: "open"
            }), s("Icon", {
                attrs: {
                    slot: "close",
                    type: "md-close"
                },
                slot: "close"
            })], 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.eDye,
                    callback: function(e) {
                        t.$set(t.seConf, "eDye", e)
                    },
                    expression: "seConf.eDye"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "版本起始："
                }
            }, [s("InputNumber", {
                staticClass: "inputnumber",
                attrs: {
                    max: 10,
                    min: 1,
                    size: "small",
                    step: .05
                },
                model: {
                    value: t.seConf.p1,
                    callback: function(e) {
                        t.$set(t.seConf, "p1", e)
                    },
                    expression: "seConf.p1"
                }
            }), t._v("~\n                "), s("InputNumber", {
                staticClass: "inputnumber",
                attrs: {
                    max: 10,
                    min: 1,
                    size: "small",
                    step: .05
                },
                model: {
                    value: t.seConf.p2,
                    callback: function(e) {
                        t.$set(t.seConf, "p2", e)
                    },
                    expression: "seConf.p2"
                }
            }), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.ep,
                    callback: function(e) {
                        t.$set(t.seConf, "ep", e)
                    },
                    expression: "seConf.ep"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "物品(装)等级："
                }
            }, [s("InputNumber", {
                staticClass: "inputnumber",
                attrs: {
                    max: 600,
                    min: 1,
                    size: "small",
                    step: 5
                },
                model: {
                    value: t.seConf.ilv1,
                    callback: function(e) {
                        t.$set(t.seConf, "ilv1", e)
                    },
                    expression: "seConf.ilv1"
                }
            }), t._v("~\n                "), s("InputNumber", {
                staticClass: "inputnumber",
                attrs: {
                    max: 600,
                    min: 1,
                    size: "small",
                    step: 5
                },
                model: {
                    value: t.seConf.ilv2,
                    callback: function(e) {
                        t.$set(t.seConf, "ilv2", e)
                    },
                    expression: "seConf.ilv2"
                }
            }), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.eilv,
                    callback: function(e) {
                        t.$set(t.seConf, "eilv", e)
                    },
                    expression: "seConf.eilv"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "(可)装备等级："
                }
            }, [s("InputNumber", {
                staticClass: "inputnumber",
                attrs: {
                    max: 80,
                    min: 1,
                    size: "small",
                    step: 5
                },
                model: {
                    value: t.seConf.lv1,
                    callback: function(e) {
                        t.$set(t.seConf, "lv1", e)
                    },
                    expression: "seConf.lv1"
                }
            }), t._v("~\n                "), s("InputNumber", {
                staticClass: "inputnumber",
                attrs: {
                    max: 80,
                    min: 1,
                    size: "small",
                    step: 5
                },
                model: {
                    value: t.seConf.lv2,
                    callback: function(e) {
                        t.$set(t.seConf, "lv2", e)
                    },
                    expression: "seConf.lv2"
                }
            }), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.elv,
                    callback: function(e) {
                        t.$set(t.seConf, "elv", e)
                    },
                    expression: "seConf.elv"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "物品(UI)分类："
                }
            }, [s("Select", {
                staticStyle: {
                    width: "129px"
                },
                attrs: {
                    size: "small",
                    clearable: ""
                },
                model: {
                    value: t.seConf.uc,
                    callback: function(e) {
                        t.$set(t.seConf, "uc", e)
                    },
                    expression: "seConf.uc"
                }
            }, t._l(t.itemUC, function(e) {
                return s("Option", {
                    key: e.id,
                    attrs: {
                        value: e.id
                    }
                }, [t._v(t._s(e.lang.join(" / ")))])
            }), 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                model: {
                    value: t.seConf.euc,
                    callback: function(e) {
                        t.$set(t.seConf, "euc", e)
                    },
                    expression: "seConf.euc"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "(可)装备职业："
                }
            }, [s("Select", {
                staticStyle: {
                    width: "129px"
                },
                attrs: {
                    size: "small",
                    clearable: ""
                },
                model: {
                    value: t.seConf.job,
                    callback: function(e) {
                        t.$set(t.seConf, "job", e)
                    },
                    expression: "seConf.job"
                }
            }, t._l(t.classJob, function(e) {
                return s("Option", {
                    key: e.id,
                    attrs: {
                        value: e.id
                    }
                }, [t._v(t._s(e.lang.join(" / ")))])
            }), 1), s("Checkbox", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    disabled: "",
                    title: "暂不可用"
                },
                model: {
                    value: t.seConf.ejob,
                    callback: function(e) {
                        t.$set(t.seConf, "ejob", e)
                    },
                    expression: "seConf.ejob"
                }
            })], 1)], 1)], 1)]), s("div", {
                staticStyle: {
                    display: "block",
                    position: "absolute",
                    top: "43px",
                    right: "13px"
                }
            }, [null != t.curJob ? s("img", {
                staticClass: "curJobIcon",
                attrs: {
                    title: "当前职业",
                    src: t.getCurJobIcon
                }
            }) : t._e()])], 2), s("Row", {
                attrs: {
                    type: "flex",
                    align: "top"
                }
            }, [s("i-col", {
                staticStyle: {
                    height: "735px",
                    overflow: "auto",
                    width: "226px"
                },
                attrs: {
                    span: "5"
                }
            }, [s("left", {
                attrs: {
                    jobiconid: t.jobiconid,
                    "v-if": !t.loading,
                    userConf: t.userConf,
                    starlist: t.starlist,
                    usrlist: t.usrlist
                },
                on: {
                    "on-star-list-click": t.loadStar,
                    rsync: t.loadUsrList,
                    rsyncStar: t.loadStarList,
                    "on-recipe-hash-click": t.loadList,
                    "on-back-list-click": t.loadBack
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "735px",
                    "border-left": "solid 1px rgba(50, 50, 50, 0.15)",
                    overflow: "auto",
                    width: "283px"
                },
                attrs: {
                    span: "6"
                }
            }, [s("itemList", {
                ref: "itemList",
                attrs: {
                    jobiconid: t.jobiconid,
                    curJob: t.curJob
                },
                on: {
                    "on-item-click": t.loadItem
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "735px",
                    "border-left": "solid 1px rgba(50, 50, 50, 0.15)",
                    overflow: "auto",
                    width: "340px"
                },
                attrs: {
                    span: "6"
                }
            }, [s("itemInfo", {
                ref: "itemInfo",
                attrs: {
                    jobiconid: t.jobiconid,
                    curJob: t.curJob
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "735px",
                    "border-left": "solid 1px rgba(50, 50, 50, 0.15)",
                    overflow: "auto",
                    width: "348px"
                },
                attrs: {
                    span: "6"
                }
            }, [s("calList", {
                ref: "calList",
                attrs: {
                    jobiconid: t.jobiconid,
                    curJob: t.curJob
                },
                on: {
                    "on-item-click": t.loadItem,
                    "on-show-rep": t.showRep,
                    rsync: t.loadUsrList,
                    "on-do-cal": t.doCal
                }
            })], 1)], 1)], 1)], 1), s("setting", {
                ref: "setting"
            }), s("sysdialog", {
                ref: "sysdialog",
                on: {
                    "on-handle": t.onSwitchLang
                }
            }), s("calRep", {
                ref: "calRep",
                attrs: {
                    jobiconid: t.jobiconid,
                    curJob: t.curJob,
                    loading: t.loading
                }
            }), s("sign", {
                ref: "sgin",
                on: {
                    login: t.setLogin
                }
            }), s("usrInfo", {
                ref: "usrInfo",
                attrs: {
                    userInfo: t.userInfo
                },
                on: {
                    rsync: t.loadUsrList
                }
            })], 1)
        }
          , i = []
          , n = (s("7514"),
        s("386d"),
        s("c276"))
          , o = s("c24f")
          , l = s("6070")
          , r = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                staticStyle: {
                    hight: "100%"
                }
            }, [s("Row", {
                staticStyle: {
                    heigh: "28px",
                    margin: "5px 0px"
                }
            }, [s("a", {
                staticClass: "left-tab",
                class: t.tabActive(0),
                on: {
                    click: function(e) {
                        t.tab = 0
                    }
                }
            }, [s("Icon", {
                attrs: {
                    type: "md-trending-up"
                }
            })], 1), s("a", {
                staticClass: "left-tab",
                class: t.tabActive(1),
                on: {
                    click: function(e) {
                        t.tab = 1
                    }
                }
            }, [s("Icon", {
                attrs: {
                    type: "md-star-outline"
                }
            })], 1)]), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 0 == t.tab && t.jobiconid >= 62108,
                    expression: "tab==0 && jobiconid >= 62108 ? true : false "
                }],
                key: "row1",
                staticStyle: {
                    margin: "10px 4px 10px 10px"
                }
            }, [s("transition-group", {
                staticClass: "left-list",
                attrs: {
                    name: "fade",
                    tag: "ul"
                }
            }, t._l(t.job_note_nomel, function(e) {
                return s("li", {
                    key: e[1],
                    class: t.userConf.curId == e[1] ? "active" : "",
                    on: {
                        click: function(s) {
                            return t.onRecipeHashClick(e[1])
                        }
                    }
                }, [s("Icon", {
                    attrs: {
                        type: "md-trending-up"
                    }
                }), t._v("\n        " + t._s(e[0]) + "\n        "), s("Tag", {
                    staticClass: "count",
                    attrs: {
                        color: "success"
                    }
                }, [t._v(t._s(t.getCount(e[1])))])], 1)
            }), 0)], 1), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 1 == t.tab && t.jobiconid >= 62108,
                    expression: "tab==1 && jobiconid >= 62108  ? true : false "
                }],
                key: "row2",
                staticStyle: {
                    margin: "10px 4px 10px 10px"
                }
            }, [s("transition-group", {
                staticClass: "left-list",
                attrs: {
                    name: "fade",
                    tag: "ul"
                }
            }, t._l(t.job_note_special, function(e) {
                return s("li", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.getCount(e[1]) > 0,
                        expression: "getCount(item[1]) > 0"
                    }],
                    key: e[1],
                    class: t.userConf.curId == e[1] ? "active" : "",
                    on: {
                        click: function(s) {
                            return t.onRecipeHashClick(e[1])
                        }
                    }
                }, [s("Icon", {
                    attrs: {
                        type: "ios-ribbon"
                    }
                }), t._v("\n        " + t._s(e[0]) + "\n        "), s("Tag", {
                    staticClass: "count",
                    attrs: {
                        color: "success"
                    }
                }, [t._v(t._s(t.getCount(e[1])))])], 1)
            }), 0)], 1), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 61821 == t.jobiconid,
                    expression: "jobiconid == 61821  ? true : false "
                }],
                key: "row3",
                staticStyle: {
                    margin: "0px 4px 10px 10px"
                }
            }, [s("Alert", {
                attrs: {
                    color: "blue"
                }
            }, [t._v("地下工坊")])], 1), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 61830 == t.jobiconid,
                    expression: "jobiconid == 61830  ? true : false "
                }],
                key: "row4",
                staticStyle: {
                    margin: "0px 4px 10px 10px"
                }
            }, [s("Alert", {
                attrs: {
                    color: "blue"
                }
            }, [t._v("收藏队列\n      "), s("Icon", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    type: "md-refresh",
                    size: "16"
                },
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.$emit("rsync", !0)
                    }
                }
            })], 1), s("transition-group", {
                staticClass: "left-list",
                attrs: {
                    name: "fade",
                    tag: "ul"
                }
            }, t._l(t.usrlist, function(e) {
                return s("li", {
                    key: e.id,
                    staticClass: "usrlist",
                    attrs: {
                        title: e.desc
                    },
                    on: {
                        click: function(s) {
                            return t.onUsrListClick(e.id)
                        }
                    }
                }, [s("label", {
                    staticClass: "name"
                }, [t._v(t._s(e.desc))]), s("Icon", {
                    staticClass: "opt2",
                    attrs: {
                        type: "md-calculator",
                        title: "加入计算队列"
                    },
                    on: {
                        click: function(s) {
                            return s.stopPropagation(),
                            t.onCalUsrListClick(e.id)
                        }
                    }
                }), s("Icon", {
                    staticClass: "opt1",
                    attrs: {
                        type: "ios-trash",
                        title: "删除"
                    },
                    on: {
                        click: function(s) {
                            return s.stopPropagation(),
                            t.onCalUsrListDelClick(e.id)
                        }
                    }
                })], 1)
            }), 0)], 1), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 61831 == t.jobiconid,
                    expression: "jobiconid == 61831 ? true : false "
                }],
                key: "row5",
                staticStyle: {
                    margin: "0px 4px 10px 10px"
                }
            }, [s("Alert", {
                attrs: {
                    color: "blue"
                }
            }, [t._v("推荐队列\n      "), s("Icon", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    type: "md-refresh",
                    size: "16"
                },
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.$emit("rsyncStar", !0)
                    }
                }
            })], 1), s("transition-group", {
                staticClass: "left-list",
                attrs: {
                    name: "fade",
                    tag: "ul"
                }
            }, t._l(t.starlist, function(e) {
                return s("li", {
                    key: e.id,
                    staticClass: "usrlist",
                    attrs: {
                        title: e.desc
                    },
                    on: {
                        click: function(s) {
                            return t.onStarListClick(e.id)
                        }
                    }
                }, [s("label", {
                    staticClass: "name"
                }, [t._v(t._s(e.desc))]), s("Icon", {
                    staticClass: "opt1",
                    attrs: {
                        type: "md-calculator",
                        title: "加入计算队列"
                    },
                    on: {
                        click: function(s) {
                            return s.stopPropagation(),
                            t.onCalStarListClick(e.id)
                        }
                    }
                })], 1)
            }), 0)], 1), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 61826 == t.jobiconid,
                    expression: "jobiconid == 61826  ? true : false "
                }],
                key: "row6",
                staticStyle: {
                    margin: "0px 4px 10px 10px"
                }
            }, [s("Alert", {
                attrs: {
                    color: "blue"
                }
            }, [t._v("反查历史")]), s("transition-group", {
                staticClass: "left-list",
                attrs: {
                    name: "fade",
                    tag: "ul"
                }
            }, t._l(t.backList, function(e) {
                return s("li", {
                    key: e.id,
                    on: {
                        click: function(s) {
                            return t.onBackListClick(e.it)
                        }
                    }
                }, [s("Icon", {
                    attrs: {
                        type: "ios-redo"
                    }
                }), t._v("\n        " + t._s(e.it.name) + "\n        "), s("Tag", {
                    staticClass: "count",
                    attrs: {
                        color: "success"
                    }
                }, [t._v(t._s(e.hash.length))])], 1)
            }), 0)], 1), s("Row", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 61829 == t.jobiconid,
                    expression: "jobiconid == 61829  ? true : false "
                }],
                key: "row7",
                staticStyle: {
                    margin: "10px 4px 10px 10px"
                }
            }, [s("Alert", {
                attrs: {
                    color: "blue"
                }
            }, [t._v("搜索历史")])], 1), t.loading ? s("Spin", {
                staticStyle: {
                    "background-color": "rgba(255, 255, 255, 0)"
                },
                attrs: {
                    size: "large",
                    fix: ""
                }
            }) : t._e()], 1)
        }
          , c = []
          , u = (s("c5f6"),
        {
            name: "leftMeau",
            props: {
                jobiconid: {
                    type: Number
                },
                userConf: {
                    type: Object
                },
                loading: {
                    type: Boolean
                },
                curJob: {
                    type: Number
                },
                starlist: {
                    type: Array,
                    default: function() {
                        return []
                    }
                },
                usrlist: {
                    type: Array,
                    default: function() {
                        return []
                    }
                }
            },
            data: function() {
                return {
                    tab: 0,
                    job_note_nomel: l.job_note_nomel,
                    job_note_special: l.job_note_special,
                    job_note_hash: l.job_note_hash,
                    backList: []
                }
            },
            computed: {
                getBackList: function() {
                    return this.$store.state.app.backList
                }
            },
            watch: {
                jobiconid: function(t) {
                    null != t && this.onRecipeHashClick(this.userConf.curId)
                },
                getBackList: function(t, e) {
                    console.log("watch.getBackList", t),
                    this.backList = t
                },
                deep: !0
            },
            mounted: function() {
                console.log(this.$cal.noteHash)
            },
            methods: {
                tabActive: function(t) {
                    var e = [];
                    return this.tab === t && e.push("active"),
                    this.jobiconid < 62108 && e.push("hide"),
                    e
                },
                onBackListClick: function(t) {
                    this.$emit("on-back-list-click", t)
                },
                getCount: function(t) {
                    if (this.jobiconid < 62108)
                        return 0;
                    var e = this.jobiconid - 62108
                      , s = this.job_note_hash[t];
                    return isNaN(e) || void 0 === s ? 0 : s[e][1]
                },
                onRecipeHashClick: function(t) {
                    if (!(this.jobiconid < 62108)) {
                        var e = this.jobiconid - 62108
                          , s = this.job_note_hash[t][e];
                        console.log("onRecipeHashClick:".concat(s)),
                        void 0 !== s && 0 !== s[1] ? this.$emit("on-recipe-hash-click", [t, s]) : this.$Message.warning("没有配方！")
                    }
                },
                onStarListClick: function(t) {
                    this.$emit("on-star-list-click", t)
                },
                onCalStarListClick: function(t) {
                    var e = this.starlist.find(function(e) {
                        return e.id === t
                    });
                    this.redayCal(e, !0)
                },
                onUsrListClick: function(t) {
                    this.$emit("on-start-list-click", t)
                },
                onCalUsrListClick: function(t) {
                    var e = this.usrlist.find(function(e) {
                        return e.id === t
                    });
                    this.redayCal(e, !1)
                },
                onCalUsrListDelClick: function(t) {
                    var e = this;
                    this.$Modal.confirm({
                        title: "提示",
                        content: "是否删除队列".concat(t),
                        onOk: function() {
                            Object(o["b"])("/cloud/dellist", {
                                id: t
                            }).then(function(t) {
                                0 === t.data.errno ? (e.$Message.success("保存成功!"),
                                e.$emit("rsync", !0)) : e.$Message.warning("保存失败! msg:".concat(t.data.errmsg))
                            })
                        }
                    })
                },
                redayCal: function(t, e) {
                    var s = JSON.parse(t.content)
                      , a = {};
                    for (var i in s) {
                        var n = JSON.parse(JSON.stringify(this.$cal.item[i]));
                        if (void 0 !== n.rid && n.rid.length > 0) {
                            var o = n.rid[0];
                            a[i] = [i, s[i], o, !1]
                        }
                    }
                    t.star = e,
                    this.$store.commit("initCalList", a),
                    this.$store.commit("initCalListInfo", t)
                }
            }
        })
          , d = u
          , h = s("2877")
          , p = Object(h["a"])(d, r, c, !1, null, null, null)
          , m = p.exports
          , f = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%"
                }
            }, [s("nav", {
                staticStyle: {
                    display: "block",
                    position: "absolute",
                    width: "100%",
                    padding: "5px"
                }
            }, [s("Select", {
                staticStyle: {
                    width: "100%"
                },
                attrs: {
                    clearable: "",
                    size: "small",
                    filterable: ""
                },
                model: {
                    value: t.itemtype,
                    callback: function(e) {
                        t.itemtype = e
                    },
                    expression: "itemtype"
                }
            }, t._l(t.itemtypes, function(e) {
                return s("Option", {
                    key: e.id,
                    attrs: {
                        value: e.id
                    }
                }, [t._v(t._s(e.label) + "(" + t._s(e.num) + ")")])
            }), 1)], 1), s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%",
                    padding: "35px 5px 5px 5px"
                }
            }, [s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%",
                    overflow: "scroll",
                    "overflow-x": "hidden"
                }
            }, [s("transition-group", {
                staticClass: "item-list",
                attrs: {
                    name: "component-fade",
                    mode: "out-in",
                    "leave-active-class": "animated bounceOutUp"
                }
            }, t._l(t.listData, function(e) {
                return s("li", {
                    key: e.id,
                    class: null == t.itemtype || t.itemtype == e.uc ? "" : "hiden",
                    on: {
                        click: function(s) {
                            return t.itemClick(e)
                        }
                    }
                }, [s("div", {
                    staticClass: "list-img",
                    attrs: {
                        title: e.title
                    }
                }, [s("img", {
                    staticClass: "img",
                    attrs: {
                        src: t.getImg(e.icon, e.hq)
                    }
                }), s("div", {
                    staticClass: "bd"
                })]), s("div", {
                    staticClass: "name",
                    class: t.curItemId == e.id ? "active" : ""
                }, [s("span", {
                    class: e.dye ? "dye" : "hide",
                    attrs: {
                        title: "可染色、can dye"
                    }
                }), s("span", {
                    staticClass: "t1"
                }, [s("copyName", {
                    attrs: {
                        title: e.title,
                        message: e.name
                    }
                })], 1), s("span", {
                    staticClass: "t2"
                }, [s("img", {
                    staticClass: "img3",
                    attrs: {
                        title: e.ucTitle,
                        src: t.getImg(e.ucIcon)
                    }
                }), s("span", {
                    staticClass: "t3"
                }, [t._v("iLv" + t._s(e.ilv))]), s("span", {
                    staticClass: "t5"
                }, [t._v("p" + t._s(e.p ? e.p : "?"))]), e.rid ? s("span", {
                    attrs: {
                        title: e.jobTitle
                    }
                }, [e.showJobIcon ? s("img", {
                    staticClass: "img2",
                    attrs: {
                        src: t.getImg(e.jobIcon)
                    }
                }) : t._e(), s("span", {
                    staticClass: "t4"
                }, [t._v("Lv" + t._s(e.rlv) + t._s(e.rs))])]) : t._e()])]), s("div", {
                    staticClass: "act",
                    class: t.curItemId == e.id ? "active" : ""
                }, [s("Icon", {
                    attrs: {
                        type: "md-search",
                        title: "反查"
                    },
                    on: {
                        click: function(s) {
                            return s.stopPropagation(),
                            t.searchBack(e)
                        }
                    }
                }), s("Icon", {
                    attrs: {
                        type: "ios-share-alt",
                        title: "加入计算队列"
                    },
                    on: {
                        click: function(s) {
                            return s.stopPropagation(),
                            t.addToList(e.id, e.rid)
                        }
                    }
                })], 1)])
            }), 0)], 1)])])
        }
          , g = []
          , b = (s("7f7f"),
        s("ac6a"),
        s("c1b4"))
          , v = {
            name: "itemList",
            components: {
                copyName: b["a"]
            },
            props: {
                jobiconid: {
                    type: Number
                },
                curJob: {
                    type: Number
                }
            },
            data: function() {
                return {
                    curItemId: 0,
                    listData: [],
                    langHsh: {
                        "en-US": 1,
                        "ja-JP": 0,
                        "zh-CN": 2
                    },
                    typeList: [],
                    itemtypes: [],
                    itemtype: null,
                    lastLoad: null
                }
            },
            computed: {
                getLang: function() {
                    return this.$store.state.lang
                },
                getLang2: function() {
                    return this.$store.state.lang2
                }
            },
            watch: {
                typeList: function(t, e) {
                    if (t.length > 0) {
                        var s = {}
                          , a = [];
                        t.forEach(function(t) {
                            void 0 !== s[t] ? s[t] = s[t] + 1 : s[t] = 1
                        });
                        var i = this.langHsh[this.$store.state.lang]
                          , n = this.langHsh[this.$store.state.lang2];
                        for (var o in s) {
                            var l = this.$cal.itemUC[o]
                              , r = "" !== l.lang[i] ? l.lang[i] : l.lang[n];
                            a.push({
                                id: o,
                                label: r,
                                num: s[o]
                            })
                        }
                        this.itemtypes = a
                    } else
                        this.itemtypes = [];
                    this.itemtype = null
                },
                getLang: function() {
                    this.loadList(this.lastLoad)
                },
                getLang2: function() {
                    this.loadList(this.lastLoad)
                }
            },
            methods: {
                itemClick: function(t) {
                    this.curItemId = t.id,
                    this.$emit("on-item-click", t.id)
                },
                searchBack: function(t) {
                    var e = t.id
                      , s = this.$store.state.app.backList
                      , a = [];
                    if (void 0 !== s[e])
                        return a = s[e].hash,
                        this.$Message.success("反查【{0}】成功,共找到【{1}】个配方".format(t.name, a.length)),
                        this.mkList(a, !0);
                    a = this.$cal.ser.searchBack(t),
                    this.$Message.success("反查【{0}】成功,共找到【{1}】个配方".format(t.name, a.length)),
                    a.length > 0 && (a.unshift(a.length),
                    this.$store.commit("addToBackList", {
                        id: e,
                        hash: a,
                        it: t
                    }),
                    this.mkList(a, !0))
                },
                findJob: function(t) {
                    var e = this;
                    if (1 === t.length) {
                        var s = this.$cal.recipe[t[0]];
                        return s.job
                    }
                    var a = [];
                    if (t.forEach(function(s) {
                        a.push(e.$cal.recipe[t[0]])
                    }),
                    this.lastJob <= 0)
                        return a[0].job;
                    var i = 0;
                    return a.forEach(function(t) {
                        t.job === e.lastJob && (i = t.job)
                    }),
                    0 === i ? a[0].job : i
                },
                findR: function() {},
                addToList: function(t, e) {
                    void 0 !== e && 0 !== e ? this.$store.commit("addToCalList", [t, 1, e, !1]) : this.$Message.warning("没有配方！")
                },
                loadList: function(t) {
                    this.lastLoad = t,
                    this.lastJob = this.curJob;
                    var e = this.$cal.recipeNote[t[0]];
                    void 0 !== e && this.mkList(e, !1)
                },
                mkList: function(t, e) {
                    for (var s = [], a = this.langHsh[this.$store.state.lang], i = this.langHsh[this.$store.state.lang2], o = [], l = 1; l < t.length; l++) {
                        var r = t[l]
                          , c = this.$cal.recipe[r]
                          , u = JSON.parse(JSON.stringify(this.$cal.item[c.it]))
                          , d = this.$cal.itemUC[u.uc]
                          , h = c.job
                          , p = this.$cal.classJob[h + 8];
                        u.name = "" !== u.lang[a] ? u.lang[a] : u.lang[i],
                        u.title = u.lang.join("\n"),
                        u.ucTitle = d.lang.join("\n"),
                        u.ucIcon = d.icon,
                        u.jobIcon = p.icon,
                        u.jobTitle = p.lang.join("\n"),
                        u.showJobIcon = e,
                        u.rlv = c.bp[2],
                        u.rid = r,
                        u.rs = Object(n["c"])(c.bp[3]),
                        o.push(u.uc),
                        s.push(u)
                    }
                    this.typeList = o,
                    this.listData = s
                },
                mkList2: function(t, e) {
                    var s = []
                      , a = this.langHsh[this.$store.state.lang]
                      , i = this.langHsh[this.$store.state.lang2]
                      , o = [];
                    for (var l in t) {
                        var r = JSON.parse(JSON.stringify(this.$cal.item[l]))
                          , c = this.$cal.itemUC[r.uc];
                        if (r.name = "" !== r.lang[a] ? r.lang[a] : r.lang[i],
                        r.title = r.lang.join("\n"),
                        r.ucTitle = c.lang.join("\n"),
                        r.ucIcon = c.icon,
                        r.showJobIcon = e,
                        void 0 !== r.rid && r.rid.length > 0) {
                            var u = this.$cal.recipe[r.rid[0]]
                              , d = u.job
                              , h = this.$cal.classJob[d + 8];
                            r.jobIcon = h.icon,
                            r.jobTitle = h.lang.join("\n"),
                            r.rlv = u.bp[2],
                            r.rid = r.rid[0],
                            r.rs = Object(n["c"])(u.bp[3])
                        }
                        o.push(r.uc),
                        s.push(r)
                    }
                    this.typeList = o,
                    this.listData = s
                },
                loadSe: function(t) {
                    var e = this;
                    console.log("mkList.its", t);
                    var s = this.langHsh[this.$store.state.lang]
                      , a = this.langHsh[this.$store.state.lang2]
                      , i = [];
                    this.listData = t.map(function(t) {
                        var o = e.$cal.itemUC[t.uc];
                        if (t.name = "" !== t.lang[s] ? t.lang[s] : t.lang[a],
                        t.title = t.lang.join("\n"),
                        t.ucTitle = o.lang.join("\n"),
                        t.ucIcon = o.icon,
                        t.showJobIcon = !0,
                        void 0 !== t.rid && t.rid.length > 0) {
                            var l = e.$cal.recipe[t.rid[0]]
                              , r = l.job
                              , c = e.$cal.classJob[r + 8];
                            t.jobIcon = c.icon,
                            t.jobTitle = c.lang.join("\n"),
                            t.rlv = l.bp[2],
                            t.rid = t.rid[0],
                            t.rs = Object(n["c"])(l.bp[3])
                        }
                        return i.push(t.uc),
                        t
                    }),
                    this.typeList = i
                },
                getImg: function(t, e) {
                    return Object(n["b"])(t, e)
                }
            }
        }
          , y = v
          , k = Object(h["a"])(y, f, g, !1, null, null, null)
          , C = k.exports
          , w = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%"
                }
            }, [s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%",
                    padding: "15px 5px 150px 5px"
                }
            }, [null != t.curIt ? s("div", {
                staticClass: "itemInfo"
            }, [s("div", {
                staticClass: "info"
            }, [s("div", {
                staticClass: "item-img",
                attrs: {
                    title: t.curIt.title
                }
            }, [s("div", [s("img", {
                staticClass: "img",
                attrs: {
                    src: t.getImg(t.curIt.icon, t.curIt.hq)
                }
            }), s("div", {
                staticClass: "bd"
            })])]), s("div", {
                staticClass: "name"
            }, t._l(t.curIt.lang, function(e, a) {
                return s("span", {
                    key: a
                }, ["" !== e ? s("Tag", {
                    attrs: {
                        size: "small"
                    }
                }, [t._v(t._s(e))]) : t._e()], 1)
            }), 0)]), s("div"), s("Collapse", {
                attrs: {
                    simple: !0,
                    value: [1, 2, 3, 4, 5, 6, 7, "actParm", "spm"]
                }
            }, [s("Panel", {
                attrs: {
                    name: "1"
                }
            }, [s("span", [t._v("基本信息")]), s("div", {
                attrs: {
                    slot: "content"
                },
                slot: "content"
            }, [s("div", {
                staticStyle: {
                    display: "flex"
                }
            }, [s("img", {
                staticClass: "img3",
                attrs: {
                    title: t.curIt.ucTitle,
                    src: t.getImg(t.curIt.ucIcon)
                }
            }), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v(t._s(t.curIt.ucTitle))])], 1), s("div", {
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("ID: " + t._s(t.curIt.id))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("patch: " + t._s(t.curIt.p))])], 1), s("div", {
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("使用等级: " + t._s(t.curIt.elv))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("物品等级: " + t._s(t.curIt.ilv))])], 1), s("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.curIt.jobs > 0,
                    expression: "curIt.jobs > 0"
                }],
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v(t._s(t.curIt.ejob1))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v(t._s(t.curIt.ejob2))])], 1)])]), s("Panel", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: void 0 !== t.curIt.actParm && t.curIt.actParm.length > 0,
                    expression: "curIt.actParm !== undefined && curIt.actParm.length > 0"
                }],
                attrs: {
                    name: "actParm"
                }
            }, [t._v("\n          特殊属性\n          "), s("p", {
                attrs: {
                    slot: "content"
                },
                slot: "content"
            }, [void 0 !== t.curIt.actParm && t.curIt.actParm.length > 0 ? s("label", t._l(t.curIt.actParm, function(e) {
                return s("actParm", {
                    key: e[0],
                    attrs: {
                        lang1: t.hash1,
                        lang2: t.hash2,
                        actParm: e
                    }
                })
            }), 1) : t._e()])]), s("Panel", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: void 0 !== t.curIt.spm && t.curIt.spm.length > 0,
                    expression: "curIt.spm !== undefined && curIt.spm.length > 0"
                }],
                attrs: {
                    name: "spm"
                }
            }, [t._v("\n          特殊属性\n          "), s("p", {
                attrs: {
                    slot: "content"
                },
                slot: "content"
            }, [void 0 !== t.curIt.spm && t.curIt.spm.length > 0 ? s("label", t._l(t.curIt.spm, function(e) {
                return s("sParm", {
                    key: e[0],
                    attrs: {
                        lang1: t.hash1,
                        lang2: t.hash2,
                        sParm: e
                    }
                })
            }), 1) : t._e()])]), s("Panel", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: null !== t.curRe,
                    expression: "curRe !== null"
                }],
                attrs: {
                    name: "5"
                }
            }, [t._v("\n          配方\n          "), s("div", {
                attrs: {
                    slot: "content"
                },
                slot: "content"
            }, [null !== t.curRe ? s("dd", [s("div", {
                staticStyle: {
                    display: "flex"
                }
            }, [t.curIt.rid ? s("img", {
                staticClass: "img3",
                attrs: {
                    title: t.curIt.jobTitle,
                    src: t.getImg(t.curIt.jobIcon)
                }
            }) : t._e(), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v(t._s(t.curIt.jobTitle))])], 1), null !== t.curRe ? s("div", {
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("rid: " + t._s(t.curIt.rid[0]))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("产量: " + t._s(t.curRe.bp[1]))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("Lv" + t._s(t.curIt.rlv) + " " + t._s(t.curIt.rs))])], 1) : t._e(), s("div", {
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("耐久: " + t._s(t.curIt.elv))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("难度: " + t._s(t.curRe.sp2[0]))]), s("Tag", {
                attrs: {
                    size: "small",
                    color: "success"
                }
            }, [t._v("品质: " + t._s(t.curRe.sp2[1]))])], 1), null !== t.curRe ? s("div", {
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "warning"
                }
            }, [t._v("制作条件：作业精度: " + t._s(t.curRe.sp2[0]))])], 1) : t._e(), null !== t.curRe ? s("div", {
                staticStyle: {
                    padding: "0 23px"
                }
            }, [s("Tag", {
                attrs: {
                    size: "small",
                    color: "warning"
                }
            }, [t._v("制作条件：加工精度: " + t._s(t.curRe.sp2[1]))])], 1) : t._e(), s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data0
                }
            })], 1) : t._e()])]), s("Panel", {
                attrs: {
                    name: "7"
                }
            }, [t._v("\n          链接\n          "), s("p", {
                attrs: {
                    slot: "content"
                },
                slot: "content"
            }, [t.curIt.lsid ? s("a", {
                staticClass: "link",
                attrs: {
                    target: "_blank",
                    href: "http://jp.finalfantasyxiv.com/lodestone/playguide/db/item/" + t.curIt.lsid + "/?hq=1"
                }
            }, [t._v("LodeStone")]) : t._e(), t.curIt.lang[2].length > 0 ? s("a", {
                staticClass: "link",
                attrs: {
                    target: "_blank",
                    href: "https://ff14.huijiwiki.com/wiki/%E7%89%A9%E5%93%81:" + t.curIt.lang[2]
                }
            }, [t._v("灰机wiki")]) : t._e(), t.curIt.lang[0].length > 0 ? s("a", {
                staticClass: "link",
                attrs: {
                    target: "_blank",
                    href: "http://www.garlandtools.org/db/#item/" + t.curIt.id
                }
            }, [t._v("garlandtools")]) : t._e(), t.curIt.lang[2].length > 0 ? s("a", {
                staticClass: "link",
                attrs: {
                    target: "_blank",
                    href: "https://www.ffxivsc.cn/#/search?text=" + t.curIt.lang[2] + "&type=armor"
                }
            }, [t._v("光之收藏家")]) : t._e()])])], 1)], 1) : t._e()])])
        }
          , I = []
          , x = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                staticStyle: {
                    "padding-left": "25px"
                }
            }, [s("Tag", {
                attrs: {
                    color: "success",
                    title: t.title
                }
            }, [t._v(t._s(t.bm))]), 100 !== t.actParm[2] && 0 !== t.actParm[3] ? s("Tag", {
                attrs: {
                    border: "",
                    color: "orange"
                }
            }, [t._v(t._s(t.actParm[2]) + "% Max" + t._s(t.actParm[3]))]) : t._e(), 100 !== t.actParm[2] && 0 !== t.actParm[3] && t.actParm[1] ? s("Tag", {
                attrs: {
                    border: "",
                    color: "red"
                }
            }, [t._v("HQ:" + t._s(t.actParm[4]) + "% Max" + t._s(t.actParm[5]))]) : t._e(), 100 === t.actParm[2] && 0 !== t.actParm[3] ? s("Tag", {
                attrs: {
                    border: "",
                    color: "orange"
                }
            }, [t._v(t._s(t.actParm[3]))]) : t._e(), 100 === t.actParm[2] && 0 !== t.actParm[3] && t.actParm[1] ? s("Tag", {
                attrs: {
                    border: "",
                    color: "red"
                }
            }, [t._v("HQ:" + t._s(t.actParm[5]))]) : t._e(), 0 !== t.actParm[3] || t.actParm[1] ? t._e() : s("Tag", {
                attrs: {
                    border: "",
                    color: "red"
                }
            }, [t._v(t._s(t.actParm[2]))])], 1)
        }
          , _ = []
          , $ = {
            props: {
                actParm: {
                    type: Array,
                    default: function() {
                        return []
                    }
                },
                lang1: {
                    type: Number
                },
                lang2: {
                    type: Number
                }
            },
            data: function() {
                return {
                    bm: "",
                    title: ""
                }
            },
            created: function() {},
            mounted: function() {
                var t = this.$cal.BaseParam[this.actParm[0]];
                this.bm = "" !== t.lang[this.lang1] ? t.lang[this.lang1] : t.lang[this.lang2],
                this.title = t.lang.join("\n")
            },
            methods: {}
        }
          , j = $
          , S = Object(h["a"])(j, x, _, !1, null, null, null)
          , L = S.exports
          , M = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                staticStyle: {
                    "padding-left": "25px"
                }
            }, [s("Tag", {
                attrs: {
                    color: "success",
                    title: t.title
                }
            }, [t._v(t._s(t.bm))]), s("Tag", {
                attrs: {
                    border: "",
                    color: "orange"
                }
            }, [t._v(t._s(t.sParm[1]))]), t.sParm[2] > 0 ? s("Tag", {
                attrs: {
                    border: "",
                    color: "red"
                }
            }, [t._v("HQ:" + t._s(t.sParm[1] + t.sParm[2]))]) : t._e()], 1)
        }
          , J = []
          , N = {
            props: {
                sParm: {
                    type: Array,
                    default: function() {
                        return []
                    }
                },
                lang1: {
                    type: Number
                },
                lang2: {
                    type: Number
                }
            },
            data: function() {
                return {
                    bm: "",
                    title: ""
                }
            },
            created: function() {},
            mounted: function() {
                var t = this.$cal.BaseParam[this.sParm[0]];
                this.bm = "" !== t.lang[this.lang1] ? t.lang[this.lang1] : t.lang[this.lang2],
                this.title = t.lang.join("\n")
            }
        }
          , T = N
          , O = Object(h["a"])(T, M, J, !1, null, null, null)
          , R = O.exports
          , z = {
            name: "itemInfo",
            components: {
                copyName: b["a"],
                actParm: L,
                sParm: R
            },
            props: {
                jobiconid: {
                    type: Number
                },
                curJob: {
                    type: Number
                },
                item: {
                    type: Object
                },
                itemUC: {
                    type: Object
                },
                recipe: {
                    type: Object
                },
                classJob: {
                    type: Object
                },
                recipeNote: {
                    type: Object
                }
            },
            data: function() {
                return {
                    curItemId: 0,
                    listData: [],
                    langHsh: {
                        "en-US": 1,
                        "ja-JP": 0,
                        "zh-CN": 2
                    },
                    hash1: 0,
                    hash2: 0,
                    curIt: null,
                    curRe: null,
                    data0: [{
                        title: "配方详情",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }]
                }
            },
            computed: {
                getLang: function() {
                    return this.$store.state.lang
                },
                getLang2: function() {
                    return this.$store.state.lang2
                }
            },
            mounted: function() {
                this.hash1 = this.langHsh[this.$store.state.lang],
                this.hash2 = this.langHsh[this.$store.state.lang2]
            },
            watch: {
                getLang: function() {
                    this.hash1 = this.langHsh[this.$store.state.lang],
                    this.laodItem(this.lastLoad)
                },
                getLang2: function() {
                    this.hash2 = this.langHsh[this.$store.state.lang2],
                    this.laodItem(this.lastLoad)
                }
            },
            methods: {
                laodItem: function(t) {
                    this.lastLoad = t;
                    var e = JSON.parse(JSON.stringify(this.$cal.item[t]))
                      , s = this.$cal.itemUC[e.uc]
                      , a = e.jobs;
                    e.name = "" !== e.lang[this.hash1] ? e.lang[this.hash1] : e.lang[this.hash2],
                    e.title = e.lang.join("\n"),
                    e.ucTitle = s.lang.join("\n"),
                    e.ucIcon = s.icon;
                    var i = e.rid;
                    if (i && i.length > 0) {
                        var o = 1 === i.length ? i[0] : this.findRid(i);
                        this.$set(this.data0, "title", e.name),
                        this.data0[0].children = this.toArray(this.findRC(e.id, 1, o));
                        var l = this.$cal.recipe[o]
                          , r = l.job
                          , c = this.$cal.classJob[r + 8];
                        e.jobIcon = c.icon,
                        e.jobTitle = c.lang.join("\n"),
                        e.rlv = l.bp[2],
                        e.rs = Object(n["c"])(l.bp[3]),
                        this.curRe = l
                    } else
                        this.data0[0].children = [],
                        this.curRe = null;
                    if (a > 0) {
                        var u = JSON.parse(JSON.stringify(this.$cal.ClassJobCategory[a]));
                        e.ejob1 = u.lang[0],
                        e.ejob2 = u.lang[1]
                    }
                    this.curIt = e
                },
                findRid: function(t) {
                    var e = this
                      , s = this.$cal.recipe
                      , a = 0;
                    return t.forEach(function(t) {
                        var i = s[t];
                        i.job === e.curJob && (a = t)
                    }),
                    0 === a && (a = t[0]),
                    a
                },
                findRC: function(t, e, s) {
                    var a = {}
                      , i = JSON.parse(JSON.stringify(this.$cal.item[t]))
                      , n = i.rid;
                    if (n && n.length > 0) {
                        for (var o = s || n[0], l = this.$cal.recipe[o], r = l.m, c = l.s, u = 0; u < 4; u += 2) {
                            var d = c[u];
                            if (!(d <= 0)) {
                                var h = this.$cal.item[d]
                                  , p = c[u + 1];
                                if (a[d]) {
                                    var m = parseInt(a[d]["need"]) + p * e;
                                    a[d].need = m
                                } else
                                    a[d] = {
                                        id: d,
                                        rid: [],
                                        icon: h.icon,
                                        name: h.lang,
                                        need: p,
                                        mkc: 0
                                    }
                            }
                        }
                        for (var f = 0; f < r.length; f += 2) {
                            var g = r[f]
                              , b = this.$cal.item[g]
                              , v = e * parseInt(r[f + 1]);
                            if (a[g]) {
                                var y = parseInt(a[g]["need"]) + v;
                                a[g].need = y
                            } else {
                                var k = this.findRC(g, e);
                                a[g] = {
                                    id: g,
                                    rid: b.rid,
                                    name: b.lang,
                                    icon: b.icon,
                                    need: v,
                                    mkc: e,
                                    children: this.toArray(k)
                                }
                            }
                        }
                    }
                    return a
                },
                toArray: function(t) {
                    var e = [];
                    for (var s in t) {
                        var a = t[s];
                        a.render = this.renderItem,
                        e.push(a)
                    }
                    return e
                },
                renderItem: function(t, e) {
                    e.root,
                    e.node;
                    var s = e.data;
                    if (void 0 === s)
                        return "";
                    var a = this.langHsh[this.$store.state.lang]
                      , i = this.langHsh[this.$store.state.lang2]
                      , o = s.name ? "" !== s.name[a] ? s.name[a] : s.name[i] : s.title
                      , l = []
                      , r = s.rid && s.rid.length > 0;
                    return l.push(t("img", {
                        attrs: {
                            src: Object(n["b"])(s.icon, r)
                        },
                        style: {
                            width: "20px",
                            height: "20px",
                            position: "absolute"
                        }
                    })),
                    l.push(t("span", {
                        style: {
                            paddingLeft: "22px"
                        }
                    }, [t(b["a"], {
                        props: {
                            message: o,
                            title: s.name.join("\n")
                        }
                    })])),
                    s.need > 1 && l.push(t("span", {
                        style: {
                            color: "#19be6b"
                        }
                    }, " x " + s.need)),
                    t("span", {
                        style: {
                            display: "inline-block"
                        }
                    }, [t("span", l)])
                },
                renderTitle: function(t, e) {
                    e.root,
                    e.node;
                    var s = e.data;
                    return t("span", {
                        style: {
                            display: "inline-block"
                        }
                    }, [t("span", [t("span", s.title)])])
                },
                getImg: function(t, e) {
                    return Object(n["b"])(t, e)
                }
            }
        }
          , P = z
          , A = (s("f0c6"),
        Object(h["a"])(P, w, I, !1, null, null, null))
          , D = A.exports
          , U = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%"
                }
            }, [s("nav", {
                staticStyle: {
                    display: "block",
                    position: "absolute",
                    width: "100%",
                    padding: "5px"
                }
            }, [s("Select", {
                staticStyle: {
                    width: "100%"
                },
                attrs: {
                    clearable: "",
                    size: "small",
                    filterable: ""
                },
                model: {
                    value: t.itemtype,
                    callback: function(e) {
                        t.itemtype = e
                    },
                    expression: "itemtype"
                }
            }, t._l(t.itemtypes, function(e) {
                return s("Option", {
                    key: e.id,
                    attrs: {
                        value: e.id
                    }
                }, [t._v(t._s(e.label) + "(" + t._s(e.num) + ")")])
            }), 1)], 1), s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%",
                    padding: "35px 5px 150px 5px"
                }
            }, [s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%",
                    overflow: "scroll",
                    "overflow-x": "hidden"
                }
            }, [s("transition-group", {
                staticClass: "item-list",
                attrs: {
                    name: "fade",
                    "leave-active-class": "animated bounceOutRight"
                }
            }, t._l(t.listData, function(e) {
                return s("li", {
                    key: e.id,
                    class: null == t.itemtype || t.itemtype == e.uc ? "" : "hiden",
                    on: {
                        click: function(s) {
                            return t.itemClick(e.id)
                        }
                    }
                }, [s("calItem", {
                    attrs: {
                        item: e,
                        num: e.num
                    }
                })], 1)
            }), 0)], 1)]), s("nav", {
                staticStyle: {
                    display: "block",
                    position: "absolute",
                    width: "100%",
                    padding: "5px",
                    bottom: "0",
                    height: "145px"
                }
            }, [s("div", {
                staticStyle: {
                    height: "100%",
                    width: "100%",
                    "border-top": "solid 1px rgba(50, 50, 50, 0.15)",
                    "padding-top": "5px"
                }
            }, [s("Button", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    type: "primary",
                    size: "small",
                    ghost: ""
                },
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.calItemList(e)
                    }
                }
            }, [t._v("计算")]), s("Button", {
                staticStyle: {
                    float: "right",
                    "margin-right": "5px"
                },
                attrs: {
                    type: "primary",
                    size: "small",
                    ghost: ""
                },
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.showRep(e)
                    }
                }
            }, [t._v("报表")]), s("Button", {
                staticStyle: {
                    float: "right",
                    "margin-right": "5px"
                },
                attrs: {
                    type: "primary",
                    size: "small",
                    ghost: ""
                },
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.clearCalList(e)
                    }
                }
            }, [t._v("清空")]), s("Button", {
                staticStyle: {
                    float: "right",
                    "margin-right": "5px"
                },
                attrs: {
                    type: "primary",
                    size: "small",
                    ghost: ""
                },
                on: {
                    click: function(e) {
                        return t.saveList(!0)
                    }
                }
            }, [t._v("另存")]), s("Button", {
                staticStyle: {
                    float: "right",
                    "margin-right": "5px"
                },
                attrs: {
                    type: "primary",
                    size: "small",
                    ghost: "",
                    disabled: t.calListInfo.star
                },
                on: {
                    click: function(e) {
                        return t.saveList(!1)
                    }
                }
            }, [t._v("保存")]), s("div", {
                staticClass: "seConf",
                staticStyle: {
                    "margin-top": "28px"
                }
            }, [s("Form", {
                attrs: {
                    "label-position": "right",
                    "label-width": 80
                }
            }, [s("FormItem", {
                attrs: {
                    label: "描述:"
                }
            }, [s("Input", {
                attrs: {
                    size: "small"
                },
                model: {
                    value: t.calListInfo.desc,
                    callback: function(e) {
                        t.$set(t.calListInfo, "desc", e)
                    },
                    expression: "calListInfo.desc"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "Hash:"
                }
            }, [s("Input", {
                attrs: {
                    readonly: "",
                    size: "small"
                },
                model: {
                    value: t.calListInfo.hash,
                    callback: function(e) {
                        t.$set(t.calListInfo, "hash", e)
                    },
                    expression: "calListInfo.hash"
                }
            })], 1), s("FormItem", {
                attrs: {
                    label: "时间戳:"
                }
            }, [s("Input", {
                attrs: {
                    readonly: "",
                    size: "small"
                },
                model: {
                    value: t.calListInfo.time,
                    callback: function(e) {
                        t.$set(t.calListInfo, "time", e)
                    },
                    expression: "calListInfo.time"
                }
            })], 1)], 1)], 1)], 1)])])
        }
          , B = []
          , H = (s("456d"),
        s("7303"))
          , F = {
            name: "itemList",
            components: {
                calItem: H["a"]
            },
            props: {
                jobiconid: {
                    type: Number
                },
                curJob: {
                    type: Number
                }
            },
            data: function() {
                return {
                    curItemId: 0,
                    listData: [],
                    langHsh: {
                        "en-US": 1,
                        "ja-JP": 0,
                        "zh-CN": 2
                    },
                    typeList: [],
                    itemtypes: [],
                    itemtype: null,
                    lastLoad: null,
                    nums: {},
                    calListInfo: {
                        desc: "",
                        time: 0,
                        id: "",
                        star: !1,
                        hash: ""
                    }
                }
            },
            computed: {
                getCalLsC: function() {
                    return this.$store.state.app.calListC
                },
                getCalLsD: function() {
                    return this.$store.state.app.calListD
                },
                getLang: function() {
                    return this.$store.state.lang
                },
                getLang2: function() {
                    return this.$store.state.lang2
                }
            },
            watch: {
                typeList: function(t, e) {
                    if (t.length > 0) {
                        var s = {}
                          , a = [];
                        t.forEach(function(t) {
                            void 0 !== s[t] ? s[t] = s[t] + 1 : s[t] = 1
                        });
                        var i = this.langHsh[this.$store.state.lang]
                          , n = this.langHsh[this.$store.state.lang2];
                        for (var o in s) {
                            var l = this.$cal.itemUC[o]
                              , r = "" !== l.lang[i] ? l.lang[i] : l.lang[n];
                            a.push({
                                id: o,
                                label: r,
                                num: s[o]
                            })
                        }
                        this.itemtypes = a
                    } else
                        this.itemtypes = [];
                    this.itemtype = null
                },
                getCalLsC: function() {
                    this.loadList(this.$store.state.app.calList)
                },
                getCalLsD: function() {
                    this.calListInfo = this.$store.state.app.calListInfo
                },
                getLang: function() {
                    this.loadList(this.lastLoad)
                },
                getLang2: function() {
                    this.loadList(this.lastLoad)
                },
                deep: !0
            },
            methods: {
                saveList: function(t) {
                    var e = this
                      , s = {}
                      , a = "/cloud/addlist";
                    for (var i in this.listData) {
                        var n = this.listData[i];
                        s[n.id] = n.num
                    }
                    if (Object.keys(s) <= 0)
                        this.$Message.warning("无法保存空队列");
                    else if ("" !== this.calListInfo.desc.trim()) {
                        var l = {
                            desc: this.calListInfo.desc,
                            content: JSON.stringify(s)
                        };
                        "" === this.calListInfo.id || this.calListInfo.star || (a = "/cloud/uplist",
                        l.id = this.calListInfo.id),
                        t && (a = "/cloud/addlist"),
                        this.$Modal.confirm({
                            title: "提示",
                            content: "是否保存队列",
                            onOk: function() {
                                Object(o["b"])(a, l).then(function(t) {
                                    0 === t.data.errno ? (e.$Message.success("保存成功!"),
                                    e.$set(e.calListInfo, "hash", t.data.data.hash),
                                    e.$emit("rsync", !0)) : e.$Message.warning("保存失败! msg:".concat(t.data.errmsg))
                                })
                            }
                        })
                    } else
                        this.$Message.warning("描述不能为空")
                },
                clearCalList: function() {
                    this.$store.commit("initCalList", {}),
                    this.$store.commit("initCalListInfo", {
                        desc: "",
                        time: 0,
                        id: "",
                        star: !1,
                        hash: ""
                    })
                },
                showRep: function() {
                    this.$emit("on-show-rep")
                },
                calItemList: function() {
                    this.$emit("on-do-cal")
                },
                itemClick: function(t) {
                    console.log("itemClick id=", t),
                    this.$emit("on-item-click", t)
                },
                findJob: function(t) {
                    var e = this;
                    if (1 === t.length) {
                        var s = this.$cal.recipe[t[0]];
                        return s.job
                    }
                    var a = [];
                    if (t.forEach(function(s) {
                        a.push(e.$cal.recipe[t[0]])
                    }),
                    this.curJob <= 0)
                        return a[0].job;
                    var i = 0;
                    return a.forEach(function(t) {
                        t.job === e.curJob && (i = t.job)
                    }),
                    0 === i ? a[0].job : i
                },
                loadListInfo: function(t) {},
                loadList: function(t) {
                    this.lastLoad = t;
                    var e = []
                      , s = this.langHsh[this.$store.state.lang]
                      , a = this.langHsh[this.$store.state.lang2]
                      , i = [];
                    for (var o in t)
                        if (!isNaN(o)) {
                            var l = t[o][2]
                              , r = this.$cal.recipe[l]
                              , c = JSON.parse(JSON.stringify(this.$cal.item[o]))
                              , u = this.$cal.itemUC[c.uc]
                              , d = r.job
                              , h = this.$cal.classJob[d + 8];
                            c.name = "" !== c.lang[s] ? c.lang[s] : c.lang[a],
                            c.title = c.lang.join("\n"),
                            c.ucTitle = u.lang.join("\n"),
                            c.ucIcon = u.icon,
                            c.jobIcon = h.icon,
                            c.jobTitle = h.lang.join("\n"),
                            c.showJobIcon = !1,
                            c.rlv = r.bp[2],
                            c.rs = Object(n["c"])(r.bp[3]),
                            c.num = t[o][1],
                            this.$set(this.nums, o, c.num),
                            i.push(c.uc),
                            e.push(c)
                        }
                    this.typeList = i,
                    this.listData = e
                },
                getImg: function(t, e) {
                    return Object(n["b"])(t, e)
                }
            }
        }
          , E = F
          , q = Object(h["a"])(E, U, B, !1, null, null, null)
          , Z = q.exports
          , K = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("Modal", {
                attrs: {
                    "class-name": "m-model model-bodel"
                },
                model: {
                    value: t.showModel,
                    callback: function(e) {
                        t.showModel = e
                    },
                    expression: "showModel"
                }
            }, [s("Row", {
                staticClass: "bigTitles",
                staticStyle: {
                    "margin-bottom": "10px"
                }
            }, [s("a", {
                staticClass: "title"
            }, [t._v("系统设置")])]), s("Form", [s("FormItem", {
                attrs: {
                    label: "程序版本："
                }
            }, [t._v("\n      " + t._s(t.webVer.ver) + "\n      "), s("a", {
                attrs: {
                    href: "javascript:void(0)"
                },
                on: {
                    click: t.reload
                }
            }, [s("Icon", {
                staticStyle: {
                    color: "#FFFFFF",
                    "font-size": "14px",
                    "margin-left": "10px"
                },
                attrs: {
                    type: "refresh",
                    title: "强制刷新缓存"
                }
            }), t._v("强制刷新缓存\n      ")], 1)]), s("FormItem", {
                attrs: {
                    label: "首选语言："
                }
            }, [s("RadioGroup", {
                on: {
                    "on-change": t.onSwitchLang
                },
                model: {
                    value: t.pageLangs.lang,
                    callback: function(e) {
                        t.$set(t.pageLangs, "lang", e)
                    },
                    expression: "pageLangs.lang"
                }
            }, [s("Radio", {
                attrs: {
                    label: "zh-CN"
                }
            }, [t._v("简中")]), s("Radio", {
                attrs: {
                    label: "ja-JP"
                }
            }, [t._v("日語")]), s("Radio", {
                attrs: {
                    label: "en-US"
                }
            }, [t._v("English")])], 1)], 1), s("FormItem", {
                attrs: {
                    label: "备用语言："
                }
            }, [s("RadioGroup", {
                on: {
                    "on-change": t.onSwitchLang2
                },
                model: {
                    value: t.pageLangs.lang2,
                    callback: function(e) {
                        t.$set(t.pageLangs, "lang2", e)
                    },
                    expression: "pageLangs.lang2"
                }
            }, [s("Radio", {
                attrs: {
                    label: "ja-JP"
                }
            }, [t._v("日語")]), s("Radio", {
                attrs: {
                    label: "en-US"
                }
            }, [t._v("English")])], 1)], 1)], 1), s("div", {
                attrs: {
                    slot: "footer"
                },
                slot: "footer"
            })], 1)
        }
          , V = []
          , Q = {
            name: "syssetting",
            data: function() {
                return {
                    webVer: {},
                    showModel: !1,
                    pageLangs: {
                        lang: "",
                        lang2: ""
                    },
                    langs: [{
                        value: "zh-CN",
                        lable: "汉"
                    }, {
                        value: "en-US",
                        lable: "En"
                    }, {
                        value: "ja-JP",
                        lable: "Ja"
                    }]
                }
            },
            mounted: function() {
                this.pageLangs.lang = this.$store.state.lang,
                this.pageLangs.lang2 = this.$store.state.lang2
            },
            methods: {
                show: function() {
                    this.showModel = !0
                },
                reload: function() {
                    window.location.reload(),
                    localStorage.clear(),
                    this.$router.go(0)
                },
                onSwitchLang: function(t) {
                    this.pageLangs.lang = t,
                    this.$store.commit("switchLang", t),
                    this.$emit("on-handle", this.pageLangs)
                },
                onSwitchLang2: function(t) {
                    this.pageLangs.lang2 = t,
                    this.$store.commit("switchLang2", t),
                    this.$emit("on-handle", this.pageLangs)
                }
            }
        }
          , W = Q
          , G = Object(h["a"])(W, K, V, !1, null, null, null)
          , X = G.exports
          , Y = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("Modal", {
                attrs: {
                    "class-name": "m-model model-bodel"
                },
                model: {
                    value: t.showModel,
                    callback: function(e) {
                        t.showModel = e
                    },
                    expression: "showModel"
                }
            }, [s("Row", {
                staticClass: "bigTitles",
                staticStyle: {
                    "margin-bottom": "10px"
                }
            }, [s("a", {
                staticClass: "title"
            }, [t._v("\n            声明\n        ")])]), s("Form", [s("FormItem", {
                attrs: {
                    label: "使用说明："
                }
            }, [s("br"), t._v(" 1.本项目是实验性项目，有时候可能会有稳定性问题\n        ")]), s("FormItem", {
                attrs: {
                    label: "使用帮助："
                }
            }, [s("br"), t._v(" 1.备用语言设定之在首选语言下对象的多语言不存在时候生效（比如国际服最新版本配方，国服没有实装，这时候这个道具是没有汉语资料的）\n        ")])], 1), s("div", {
                attrs: {
                    slot: "footer"
                },
                slot: "footer"
            })], 1)
        }
          , tt = []
          , et = {
            name: "sysdialog",
            data: function() {
                return {
                    showModel: !1
                }
            },
            props: {},
            methods: {
                show: function() {
                    this.showModel = !0
                }
            }
        }
          , st = et
          , at = Object(h["a"])(st, Y, tt, !1, null, null, null)
          , it = at.exports
          , nt = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("Modal", {
                attrs: {
                    "footer-hide": "",
                    "class-name": "model-bodel",
                    width: "400px"
                },
                model: {
                    value: t.showModel,
                    callback: function(e) {
                        t.showModel = e
                    },
                    expression: "showModel"
                }
            }, [0 == t.mo ? s("login", {
                on: {
                    change: function(e) {
                        t.mo = 1
                    },
                    hide: function(e) {
                        t.showModel = !1
                    },
                    login: t.setLogin
                }
            }) : s("reg", {
                attrs: {
                    closable: !1
                },
                on: {
                    change: function(e) {
                        t.mo = 0
                    },
                    hide: function(e) {
                        t.showModel = !1
                    }
                }
            })], 1)
        }
          , ot = []
          , lt = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                attrs: {
                    id: "reg"
                }
            }, [s("Row", {
                staticClass: "bigTitles",
                staticStyle: {
                    "margin-bottom": "10px"
                }
            }, [s("a", {
                staticClass: "title"
            }, [t._v("登录")])]), s("Form", {
                ref: "formInline",
                attrs: {
                    model: t.formInline,
                    rules: t.ruleInline
                }
            }, [s("FormItem", {
                attrs: {
                    prop: "user"
                }
            }, [s("Input", {
                attrs: {
                    type: "text",
                    placeholder: "登录名，数字英文"
                },
                model: {
                    value: t.formInline.name,
                    callback: function(e) {
                        t.$set(t.formInline, "name", e)
                    },
                    expression: "formInline.name"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-person"
                },
                slot: "prepend"
            })], 1)], 1), s("FormItem", {
                attrs: {
                    prop: "password"
                }
            }, [s("Input", {
                attrs: {
                    type: "password",
                    placeholder: "password"
                },
                model: {
                    value: t.formInline.password,
                    callback: function(e) {
                        t.$set(t.formInline, "password", e)
                    },
                    expression: "formInline.password"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-lock"
                },
                slot: "prepend"
            })], 1)], 1), s("div", {
                staticStyle: {
                    "adding-top": "10px"
                }
            }, [s("Button", {
                attrs: {
                    size: "small"
                },
                on: {
                    click: t.toReg
                }
            }, [t._v("注册")]), s("Button", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    size: "small",
                    type: "primary"
                },
                on: {
                    click: function(e) {
                        return t.handleSubmit("formInline")
                    }
                }
            }, [t._v("登录")])], 1)], 1)], 1)
        }
          , rt = []
          , ct = "0123456789ABCDEF";
        function ut(t) {
            for (var e = "", s = 0; s <= 3; s++)
                e += ct.charAt(t >> 8 * s + 4 & 15) + ct.charAt(t >> 8 * s & 15);
            return e
        }
        function dt(t) {
            for (var e = 1 + (t.length + 8 >> 6), s = new Array(16 * e), a = 0; a < 16 * e; a++)
                s[a] = 0;
            for (a = 0; a < t.length; a++)
                s[a >> 2] |= t.charCodeAt(a) << a % 4 * 8;
            return s[a >> 2] |= 128 << a % 4 * 8,
            s[16 * e - 2] = 8 * t.length,
            s
        }
        function ht(t, e) {
            var s = (65535 & t) + (65535 & e)
              , a = (t >> 16) + (e >> 16) + (s >> 16);
            return a << 16 | 65535 & s
        }
        function pt(t, e) {
            return t << e | t >>> 32 - e
        }
        function mt(t, e, s, a, i, n) {
            return ht(pt(ht(ht(e, t), ht(a, n)), i), s)
        }
        function ft(t, e, s, a, i, n, o) {
            return mt(e & s | ~e & a, t, e, i, n, o)
        }
        function gt(t, e, s, a, i, n, o) {
            return mt(e & a | s & ~a, t, e, i, n, o)
        }
        function bt(t, e, s, a, i, n, o) {
            return mt(e ^ s ^ a, t, e, i, n, o)
        }
        function vt(t, e, s, a, i, n, o) {
            return mt(s ^ (e | ~a), t, e, i, n, o)
        }
        var yt = function(t) {
            for (var e = dt(t), s = 1732584193, a = -271733879, i = -1732584194, n = 271733878, o = 0; o < e.length; o += 16) {
                var l = s
                  , r = a
                  , c = i
                  , u = n;
                s = ft(s, a, i, n, e[o + 0], 7, -680876936),
                n = ft(n, s, a, i, e[o + 1], 12, -389564586),
                i = ft(i, n, s, a, e[o + 2], 17, 606105819),
                a = ft(a, i, n, s, e[o + 3], 22, -1044525330),
                s = ft(s, a, i, n, e[o + 4], 7, -176418897),
                n = ft(n, s, a, i, e[o + 5], 12, 1200080426),
                i = ft(i, n, s, a, e[o + 6], 17, -1473231341),
                a = ft(a, i, n, s, e[o + 7], 22, -45705983),
                s = ft(s, a, i, n, e[o + 8], 7, 1770035416),
                n = ft(n, s, a, i, e[o + 9], 12, -1958414417),
                i = ft(i, n, s, a, e[o + 10], 17, -42063),
                a = ft(a, i, n, s, e[o + 11], 22, -1990404162),
                s = ft(s, a, i, n, e[o + 12], 7, 1804603682),
                n = ft(n, s, a, i, e[o + 13], 12, -40341101),
                i = ft(i, n, s, a, e[o + 14], 17, -1502002290),
                a = ft(a, i, n, s, e[o + 15], 22, 1236535329),
                s = gt(s, a, i, n, e[o + 1], 5, -165796510),
                n = gt(n, s, a, i, e[o + 6], 9, -1069501632),
                i = gt(i, n, s, a, e[o + 11], 14, 643717713),
                a = gt(a, i, n, s, e[o + 0], 20, -373897302),
                s = gt(s, a, i, n, e[o + 5], 5, -701558691),
                n = gt(n, s, a, i, e[o + 10], 9, 38016083),
                i = gt(i, n, s, a, e[o + 15], 14, -660478335),
                a = gt(a, i, n, s, e[o + 4], 20, -405537848),
                s = gt(s, a, i, n, e[o + 9], 5, 568446438),
                n = gt(n, s, a, i, e[o + 14], 9, -1019803690),
                i = gt(i, n, s, a, e[o + 3], 14, -187363961),
                a = gt(a, i, n, s, e[o + 8], 20, 1163531501),
                s = gt(s, a, i, n, e[o + 13], 5, -1444681467),
                n = gt(n, s, a, i, e[o + 2], 9, -51403784),
                i = gt(i, n, s, a, e[o + 7], 14, 1735328473),
                a = gt(a, i, n, s, e[o + 12], 20, -1926607734),
                s = bt(s, a, i, n, e[o + 5], 4, -378558),
                n = bt(n, s, a, i, e[o + 8], 11, -2022574463),
                i = bt(i, n, s, a, e[o + 11], 16, 1839030562),
                a = bt(a, i, n, s, e[o + 14], 23, -35309556),
                s = bt(s, a, i, n, e[o + 1], 4, -1530992060),
                n = bt(n, s, a, i, e[o + 4], 11, 1272893353),
                i = bt(i, n, s, a, e[o + 7], 16, -155497632),
                a = bt(a, i, n, s, e[o + 10], 23, -1094730640),
                s = bt(s, a, i, n, e[o + 13], 4, 681279174),
                n = bt(n, s, a, i, e[o + 0], 11, -358537222),
                i = bt(i, n, s, a, e[o + 3], 16, -722521979),
                a = bt(a, i, n, s, e[o + 6], 23, 76029189),
                s = bt(s, a, i, n, e[o + 9], 4, -640364487),
                n = bt(n, s, a, i, e[o + 12], 11, -421815835),
                i = bt(i, n, s, a, e[o + 15], 16, 530742520),
                a = bt(a, i, n, s, e[o + 2], 23, -995338651),
                s = vt(s, a, i, n, e[o + 0], 6, -198630844),
                n = vt(n, s, a, i, e[o + 7], 10, 1126891415),
                i = vt(i, n, s, a, e[o + 14], 15, -1416354905),
                a = vt(a, i, n, s, e[o + 5], 21, -57434055),
                s = vt(s, a, i, n, e[o + 12], 6, 1700485571),
                n = vt(n, s, a, i, e[o + 3], 10, -1894986606),
                i = vt(i, n, s, a, e[o + 10], 15, -1051523),
                a = vt(a, i, n, s, e[o + 1], 21, -2054922799),
                s = vt(s, a, i, n, e[o + 8], 6, 1873313359),
                n = vt(n, s, a, i, e[o + 15], 10, -30611744),
                i = vt(i, n, s, a, e[o + 6], 15, -1560198380),
                a = vt(a, i, n, s, e[o + 13], 21, 1309151649),
                s = vt(s, a, i, n, e[o + 4], 6, -145523070),
                n = vt(n, s, a, i, e[o + 11], 10, -1120210379),
                i = vt(i, n, s, a, e[o + 2], 15, 718787259),
                a = vt(a, i, n, s, e[o + 9], 21, -343485551),
                s = ht(s, l),
                a = ht(a, r),
                i = ht(i, c),
                n = ht(n, u)
            }
            return ut(s) + ut(a) + ut(i) + ut(n)
        }
          , kt = {
            name: "login",
            data: function() {
                return {
                    showModel: !0,
                    formInline: {
                        name: "",
                        password: ""
                    },
                    ruleInline: {
                        loginname: [{
                            required: !0,
                            message: "请输入登录名",
                            trigger: "blur"
                        }, {
                            type: "string",
                            min: 4,
                            message: "至少4位",
                            trigger: "blur"
                        }],
                        password: [{
                            required: !0,
                            message: "请输入密码",
                            trigger: "blur"
                        }, {
                            type: "string",
                            min: 6,
                            message: "密码至少6位",
                            trigger: "blur"
                        }]
                    }
                }
            },
            props: {},
            methods: {
                toReg: function() {
                    this.$emit("change")
                },
                hide: function() {
                    this.$emit("hide")
                },
                show: function() {
                    this.showModel = !0
                },
                handleSubmit: function(t) {
                    var e = this;
                    this.$refs[t].validate(function(t) {
                        t && e.login()
                    })
                },
                forgetpwd: function() {
                    var t = this
                      , e = {};
                    e.name = this.formInline.name;
                    var s = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
                    s.test(e.name) ? Object(o["a"])("/readypwd", e).then(function(e) {
                        200 === e.status ? t.$Modal.info({
                            title: "提示",
                            content: "已提交修改密码请求，请留意您的邮箱。"
                        }) : t.$Modal.warning({
                            title: "提示",
                            content: "请求失败！"
                        })
                    }) : this.$Message.error("找回密码必须提供邮箱！")
                },
                login: function() {
                    var t = this
                      , e = {};
                    e.name = this.formInline.name,
                    e.password = yt(this.formInline.password),
                    Object(o["b"])("/user/login", e).then(function(e) {
                        if (200 === e.status)
                            if (0 === e.data.errno) {
                                var s = e.data.data.token;
                                t.$Modal.info({
                                    title: "提示",
                                    content: "登录成功!"
                                }),
                                localStorage.NBB_token = s,
                                t.$store.commit("setLogin", e.data),
                                t.$emit("login", e.data),
                                t.hide()
                            } else
                                t.$Modal.warning({
                                    title: "提示",
                                    content: e.data.errmsg
                                });
                        else
                            t.$Modal.warning({
                                title: "提示",
                                content: "请求失败！"
                            })
                    }).catch(function(t) {
                        console.log(t)
                    })
                }
            }
        }
          , Ct = kt
          , wt = Object(h["a"])(Ct, lt, rt, !1, null, null, null)
          , It = wt.exports
          , xt = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("div", {
                attrs: {
                    id: "reg"
                }
            }, [s("Row", {
                staticClass: "bigTitles",
                staticStyle: {
                    "margin-bottom": "10px"
                }
            }, [s("a", {
                staticClass: "title"
            }, [t._v("注册")])]), s("Form", {
                ref: "formInline",
                attrs: {
                    model: t.formInline,
                    rules: t.ruleInline
                }
            }, [s("FormItem", {
                attrs: {
                    prop: "user"
                }
            }, [s("Input", {
                attrs: {
                    type: "text",
                    placeholder: "登录名，数字英文"
                },
                model: {
                    value: t.formInline.loginname,
                    callback: function(e) {
                        t.$set(t.formInline, "loginname", e)
                    },
                    expression: "formInline.loginname"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-person"
                },
                slot: "prepend"
            })], 1)], 1), s("FormItem", {
                attrs: {
                    prop: "nickname"
                }
            }, [s("Input", {
                attrs: {
                    type: "text",
                    placeholder: "昵称"
                },
                model: {
                    value: t.formInline.nickname,
                    callback: function(e) {
                        t.$set(t.formInline, "nickname", e)
                    },
                    expression: "formInline.nickname"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-person"
                },
                slot: "prepend"
            })], 1)], 1), s("FormItem", {
                attrs: {
                    prop: "email"
                }
            }, [s("Input", {
                attrs: {
                    type: "text",
                    placeholder: "邮箱"
                },
                model: {
                    value: t.formInline.email,
                    callback: function(e) {
                        t.$set(t.formInline, "email", e)
                    },
                    expression: "formInline.email"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-mail"
                },
                slot: "prepend"
            }), s("i-button", {
                attrs: {
                    slot: "append",
                    type: "primary"
                },
                on: {
                    click: t.handle
                },
                slot: "append"
            }, [t._v("验证邮箱")])], 1)], 1), s("FormItem", {
                attrs: {
                    prop: "password"
                }
            }, [s("Input", {
                attrs: {
                    type: "password",
                    placeholder: "password"
                },
                model: {
                    value: t.formInline.password,
                    callback: function(e) {
                        t.$set(t.formInline, "password", e)
                    },
                    expression: "formInline.password"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-lock"
                },
                slot: "prepend"
            })], 1)], 1), s("FormItem", {
                attrs: {
                    prop: "code"
                }
            }, [s("Input", {
                attrs: {
                    type: "text",
                    placeholder: "邀请码(邮箱验证通过后自动发往你邮箱)"
                },
                model: {
                    value: t.formInline.code,
                    callback: function(e) {
                        t.$set(t.formInline, "code", e)
                    },
                    expression: "formInline.code"
                }
            }, [s("icon", {
                attrs: {
                    slot: "prepend",
                    type: "md-code"
                },
                slot: "prepend"
            })], 1)], 1), s("div", {
                staticStyle: {
                    "adding-top": "10px"
                }
            }, [s("i-button", {
                attrs: {
                    size: "small"
                },
                on: {
                    click: t.toLogin
                }
            }, [t._v("登录")]), s("i-button", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    size: "small",
                    type: "primary"
                },
                on: {
                    click: function(e) {
                        return t.handleSubmit("formInline")
                    }
                }
            }, [t._v("注册")])], 1)], 1)], 1)
        }
          , _t = []
          , $t = {
            name: "reg",
            data: function() {
                return {
                    showModel: !0,
                    formInline: {
                        nickname: "",
                        loginname: "",
                        email: "",
                        password: "",
                        code: ""
                    },
                    ruleInline: {
                        nickname: [{
                            required: !0,
                            message: "请输入昵称",
                            trigger: "blur"
                        }, {
                            type: "string",
                            min: 3,
                            message: "至少3位",
                            trigger: "blur"
                        }],
                        loginname: [{
                            required: !0,
                            message: "请输入登录名",
                            trigger: "blur"
                        }, {
                            type: "string",
                            min: 4,
                            message: "至少4位",
                            trigger: "blur"
                        }],
                        email: [{
                            required: !0,
                            message: "请输入邮箱",
                            trigger: "blur"
                        }, {
                            type: "email",
                            message: "邮箱格式错误",
                            trigger: "blur"
                        }],
                        password: [{
                            required: !0,
                            message: "请输入密码",
                            trigger: "blur"
                        }, {
                            type: "string",
                            min: 6,
                            message: "密码至少6位",
                            trigger: "blur"
                        }],
                        code: [{
                            required: !0,
                            message: "请输入邀请码",
                            trigger: "blur"
                        }]
                    }
                }
            },
            props: {},
            methods: {
                toLogin: function() {
                    this.$emit("change")
                },
                handleSubmit: function(t) {
                    var e = this;
                    this.$refs[t].validate(function(t) {
                        t && e.reg()
                    })
                },
                handle: function() {
                    var t = this.formInline.email
                      , e = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
                      , s = {
                        email: t
                    }
                      , a = this;
                    e.test(t) ? Object(o["b"])("/user/pycode", s).then(function(t) {
                        a.$Modal.info({
                            title: "提示",
                            content: "已发送，请查收邮件"
                        })
                    }) : a.$Modal.info({
                        title: "提示",
                        content: "无效的邮箱"
                    })
                },
                reg: function() {
                    var t = this
                      , e = JSON.parse(JSON.stringify(this.formInline));
                    e.password = yt(e.password),
                    Object(o["b"])("/user/reg", e).then(function(e) {
                        200 === e.status ? 0 === e.data.errno ? (t.$store.commit("setLogin", e.data),
                        t.$emit("login", e.data),
                        t.$emit("hide"),
                        t.$Modal.info({
                            title: "提示",
                            content: "注册成功，Do enjoy it!"
                        })) : t.$Modal.info({
                            title: "提示",
                            content: e.data.msg
                        }) : t.$Modal.warning({
                            title: "提示",
                            content: "注册失败!"
                        })
                    }).catch(function(t) {
                        console.log(t)
                    })
                }
            }
        }
          , jt = $t
          , St = Object(h["a"])(jt, xt, _t, !1, null, null, null)
          , Lt = St.exports
          , Mt = {
            name: "sysdialog",
            components: {
                login: It,
                reg: Lt
            },
            data: function() {
                return {
                    showModel: !1,
                    mo: 0
                }
            },
            props: {},
            methods: {
                show: function() {
                    this.showModel = !0
                },
                setLogin: function(t) {
                    this.$emit("login", t)
                }
            }
        }
          , Jt = Mt
          , Nt = Object(h["a"])(Jt, nt, ot, !1, null, null, null)
          , Tt = Nt.exports
          , Ot = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("Modal", {
                staticStyle: {
                    "padding-bottom": "30px"
                },
                attrs: {
                    "footer-hide": "",
                    "class-name": "model-bodel",
                    width: "400px"
                },
                model: {
                    value: t.showModel,
                    callback: function(e) {
                        t.showModel = e
                    },
                    expression: "showModel"
                }
            }, [s("Row", {
                staticClass: "bigTitles",
                staticStyle: {
                    "margin-bottom": "10px"
                }
            }, [s("a", {
                staticClass: "title"
            }, [t._v("个人中心")])]), null != t.userInfo ? s("div", {
                staticStyle: {
                    "padding-bottom": "22px"
                }
            }, [s("p", {
                staticClass: "txt-w"
            }, [s("Tag", {
                staticClass: "usr-info-tag",
                attrs: {
                    color: "primary"
                }
            }, [t._v("昵称")]), t._v(" " + t._s(t.userInfo.nickname))], 1), s("p", {
                staticClass: "txt-w"
            }, [s("Tag", {
                staticClass: "usr-info-tag",
                attrs: {
                    color: "primary"
                }
            }, [t._v("登录名")]), t._v(" " + t._s(t.userInfo.loginname))], 1), s("p", {
                staticClass: "txt-w"
            }, [s("Tag", {
                staticClass: "usr-info-tag",
                attrs: {
                    color: "primary"
                }
            }, [t._v("邮箱")]), t._v(" " + t._s(t.userInfo.email))], 1), s("Button", {
                staticStyle: {
                    float: "right"
                },
                attrs: {
                    icon: "md-refresh",
                    size: "small"
                },
                on: {
                    click: t.rsync
                }
            }, [t._v("同步列表")]), s("Button", {
                staticStyle: {
                    float: "right",
                    "margin-right": "10px"
                },
                attrs: {
                    size: "small"
                },
                on: {
                    click: t.loginout
                }
            }, [t._v("注销")])], 1) : t._e()], 1)
        }
          , Rt = []
          , zt = {
            name: "userInfo",
            props: {
                userInfo: {
                    type: Object
                }
            },
            data: function() {
                return {
                    showModel: !1,
                    mo: 0
                }
            },
            methods: {
                show: function() {
                    this.showModel = !0
                },
                rsync: function() {
                    this.$emit("rsync", !0)
                },
                loginout: function() {
                    localStorage.removeItem("NBB_usr"),
                    localStorage.removeItem("NBB_token"),
                    window.location.reload()
                }
            }
        }
          , Pt = zt
          , At = (s("98e6"),
        Object(h["a"])(Pt, Ot, Rt, !1, null, null, null))
          , Dt = At.exports
          , Ut = function() {
            var t = this
              , e = t.$createElement
              , s = t._self._c || e;
            return s("Modal", {
                attrs: {
                    width: "1350",
                    styles: {
                        top: "30px"
                    },
                    "footer-hide": "",
                    "class-name": "m-model model-bodel",
                    "mask-closable": !1
                },
                model: {
                    value: t.show,
                    callback: function(e) {
                        t.show = e
                    },
                    expression: "show"
                }
            }, [s("Row", {
                staticClass: "bigTitles",
                staticStyle: {
                    "margin-bottom": "10px"
                }
            }, [s("a", {
                staticClass: "title"
            }, [t._v("\n      统计报表\n    ")]), s("Button", {
                staticStyle: {
                    float: "right",
                    "margin-right": "35px"
                },
                attrs: {
                    type: "primary",
                    size: "small",
                    ghost: ""
                },
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                        t.doCal(e)
                    }
                }
            }, [t._v("立刻计算")])], 1), s("div", {
                staticStyle: {
                    display: "block",
                    height: "3px",
                    "border-bottom": "solid 1px #464646"
                }
            }), s("div", {
                staticStyle: {
                    display: "block",
                    height: "730px"
                }
            }, [s("Row", {
                staticStyle: {
                    height: "100%"
                },
                attrs: {
                    type: "flex",
                    align: "top",
                    gutter: 4
                }
            }, [s("i-col", {
                staticStyle: {
                    height: "100%",
                    overflow: "auto",
                    width: "19.9%"
                },
                attrs: {
                    span: "5"
                }
            }, [s("span", {
                staticStyle: {
                    float: "right",
                    color: "white",
                    padding: "6px 6px 0 0"
                }
            }, [t._v("\n          流程模式:\n          "), s("i-switch", {
                attrs: {
                    size: "large"
                },
                model: {
                    value: t.workModal0,
                    callback: function(e) {
                        t.workModal0 = e
                    },
                    expression: "workModal0"
                }
            }, [s("span", {
                attrs: {
                    slot: "open"
                },
                slot: "open"
            }, [t._v("开启")]), s("span", {
                attrs: {
                    slot: "close"
                },
                slot: "close"
            }, [t._v("关闭")])])], 1), s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data0,
                    "show-checkbox": t.workModal0 || t.checkModal
                },
                on: {
                    "on-check-change": t.checkChange0
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "100%",
                    overflow: "auto",
                    "border-left": "solid 1px #464646",
                    width: "19.9%"
                },
                attrs: {
                    span: "5"
                }
            }, [s("span", {
                staticStyle: {
                    float: "right",
                    color: "white",
                    padding: "6px 6px 0 0"
                }
            }, [t._v("\n          流程模式:\n          "), s("i-switch", {
                attrs: {
                    size: "large"
                },
                model: {
                    value: t.workModal1,
                    callback: function(e) {
                        t.workModal1 = e
                    },
                    expression: "workModal1"
                }
            }, [s("span", {
                attrs: {
                    slot: "open"
                },
                slot: "open"
            }, [t._v("开启")]), s("span", {
                attrs: {
                    slot: "close"
                },
                slot: "close"
            }, [t._v("关闭")])])], 1), s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data1,
                    "show-checkbox": t.workModal1 || t.checkModal
                },
                on: {
                    "on-check-change": t.checkChange1
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "100%",
                    overflow: "auto",
                    "border-left": "solid 1px #464646",
                    width: "19.9%"
                },
                attrs: {
                    span: "5"
                }
            }, [s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data2,
                    "show-checkbox": t.checkModal
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "100%",
                    overflow: "auto",
                    "border-left": "solid 1px #464646",
                    width: "19.9%"
                },
                attrs: {
                    span: "5"
                }
            }, [s("span", {
                staticStyle: {
                    float: "right",
                    color: "white",
                    padding: "6px 6px 0 0"
                }
            }, [t._v("\n          流程模式:\n          "), s("i-switch", {
                attrs: {
                    size: "large"
                },
                model: {
                    value: t.workModal3,
                    callback: function(e) {
                        t.workModal3 = e
                    },
                    expression: "workModal3"
                }
            }, [s("span", {
                attrs: {
                    slot: "open"
                },
                slot: "open"
            }, [t._v("开启")]), s("span", {
                attrs: {
                    slot: "close"
                },
                slot: "close"
            }, [t._v("关闭")])])], 1), s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data3,
                    "show-checkbox": t.workModal3 || t.checkModal
                },
                on: {
                    "on-check-change": t.checkChange3
                }
            })], 1), s("i-col", {
                staticStyle: {
                    height: "100%",
                    overflow: "auto",
                    "border-left": "solid 1px #464646",
                    width: "19.9%"
                },
                attrs: {
                    span: "5"
                }
            }, [s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data4,
                    "show-checkbox": t.checkModal
                }
            }), s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data5,
                    "show-checkbox": t.checkModal
                }
            }), s("Tree", {
                staticClass: "treeview",
                attrs: {
                    data: t.data6,
                    "show-checkbox": t.checkModal
                }
            })], 1)], 1)], 1)], 1)
        }
          , Bt = []
          , Ht = {
            name: "calRep",
            props: {
                curJob: {
                    type: Number
                }
            },
            computed: {
                getCalLsC: function() {
                    return this.$store.state.app.calListC
                }
            },
            data: function() {
                return {
                    CalObj: null,
                    show: !1,
                    workModal0: !1,
                    workModal1: !1,
                    workModal3: !1,
                    checkModal: !1,
                    data0: [{
                        title: "计算队列",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    data1: [{
                        title: "直接素材",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    data2: [{
                        title: "基础素材",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    data3: [{
                        title: "二级素材",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    data4: [{
                        title: "三级素材",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    data5: [{
                        title: "四级素材",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    data6: [{
                        title: "五级素材",
                        expand: !0,
                        checked: !1,
                        render: this.renderTitle,
                        children: []
                    }],
                    lastCal: {},
                    langHsh: {
                        "en-US": 1,
                        "ja-JP": 0,
                        "zh-CN": 2
                    }
                }
            },
            watch: {
                workModal0: function(t) {
                    t && (this.checkModal = !1)
                },
                workModal1: function(t) {
                    t && (this.checkModal = !1)
                },
                workModal3: function(t) {
                    t && (this.checkModal = !1)
                },
                checkModal: function(t) {
                    t && (this.workModal0 = !1,
                    this.workModal1 = !1,
                    this.workModal3 = !1)
                },
                getCalLsC: function() {
                    var t = this.$cal.ser.calHelperTop(this.$store.state.app.calList);
                    this.data0[0].children = this.toArray(t),
                    this.checkModal = !1
                }
            },
            methods: {
                initCal: function() {},
                showRep: function() {
                    this.show = !0
                },
                initDatas: function() {
                    this.data0[0].checked = !1,
                    this.data1[0].checked = !1,
                    this.data2[0].checked = !1,
                    this.data3[0].checked = !1,
                    this.data4[0].checked = !1,
                    this.data5[0].checked = !1,
                    this.data6[0].checked = !1
                },
                toArray: function(t) {
                    var e = [];
                    for (var s in t) {
                        var a = t[s];
                        a.render = this.renderItem,
                        e.push(a)
                    }
                    return e
                },
                doCal: function() {
                    this.show = !0,
                    this.initDatas();
                    var t = this.$cal.ser.doCal(this.$store.state.app.calList);
                    this.lastCal = JSON.parse(JSON.stringify(t)),
                    console.log("结果：", t),
                    this.data0[0].children = this.toArray(t.ls),
                    this.data1[0].children = this.toArray(t.lv1),
                    this.data2[0].children = this.toArray(t.lvBase),
                    this.data3[0].children = this.toArray(t.lv2),
                    this.data4[0].children = this.toArray(t.lv3),
                    this.data5[0].children = this.toArray(t.lv4),
                    this.data6[0].children = this.toArray(t.lv5)
                },
                renderItem: function(t, e) {
                    e.root,
                    e.node;
                    var s = e.data;
                    if (void 0 === s)
                        return "";
                    var a = this.langHsh[this.$store.state.lang]
                      , i = this.langHsh[this.$store.state.lang2]
                      , o = s.name ? "" !== s.name[a] ? s.name[a] : s.name[i] : s.title
                      , l = []
                      , r = s.rid && s.rid.length > 0;
                    return l.push(t("img", {
                        attrs: {
                            src: Object(n["b"])(s.icon, r)
                        },
                        style: {
                            width: "20px",
                            height: "20px",
                            position: "absolute"
                        }
                    })),
                    l.push(t("span", {
                        style: {
                            paddingLeft: "22px"
                        }
                    }, [t(b["a"], {
                        props: {
                            message: o,
                            title: s.name.join("\n")
                        }
                    })])),
                    s.need > 1 && l.push(t("span", {
                        style: {
                            color: "#19be6b"
                        }
                    }, " x " + s.need)),
                    t("span", {
                        style: {
                            display: "inline-block"
                        }
                    }, [t("span", l)])
                },
                renderTitle: function(t, e) {
                    e.root,
                    e.node;
                    var s = e.data;
                    return t("span", {
                        style: {
                            display: "inline-block"
                        }
                    }, [t("span", [t("span", s.title)])])
                },
                checkChange0: function(t) {
                    console.log("checkChange0", t);
                    var e = JSON.parse(JSON.stringify(this.$store.state.app.calList));
                    for (var s in e) {
                        var a = !1;
                        t.forEach(function(t) {
                            t.id === Number(s) && (a = !0)
                        }),
                        e[s][3] = a
                    }
                    console.log("calList", e);
                    var i = this.$cal.ser.doCal(e);
                    this.lastCal = JSON.parse(JSON.stringify(i)),
                    this.data1[0].children = this.toArray(i.lv1),
                    this.data2[0].children = this.toArray(i.lvBase),
                    this.data3[0].children = this.toArray(i.lv2),
                    this.data4[0].children = this.toArray(i.lv3),
                    this.data5[0].children = this.toArray(i.lv4),
                    this.data6[0].children = this.toArray(i.lv5)
                },
                checkChange1: function(t) {
                    var e = JSON.parse(JSON.stringify(this.lastCal.lv1))
                      , s = {}
                      , a = {}
                      , i = {}
                      , n = {}
                      , o = {};
                    for (var l in e) {
                        var r = !1;
                        t.forEach(function(t) {
                            t.id === Number(l) && (r = !0)
                        }),
                        e[l].checked = r
                    }
                    s = this.$cal.ser.docalHelper(e),
                    a = this.$cal.ser.docalHelper(s),
                    i = this.$cal.ser.docalHelper(a),
                    n = this.$cal.ser.docalHelper(i),
                    o = this.$cal.ser.dusum(e, o),
                    o = this.$cal.ser.dusum(s, o),
                    o = this.$cal.ser.dusum(a, o),
                    o = this.$cal.ser.dusum(i, o),
                    o = this.$cal.ser.dusum(n, o),
                    this.data2[0].children = this.toArray(o),
                    this.data3[0].children = this.toArray(s),
                    this.data4[0].children = this.toArray(a),
                    this.data5[0].children = this.toArray(i),
                    this.data6[0].children = this.toArray(n)
                },
                checkChange3: function(t) {
                    var e = JSON.parse(JSON.stringify(this.lastCal.lv2))
                      , s = {}
                      , a = {}
                      , i = {}
                      , n = {};
                    for (var o in e) {
                        var l = !1;
                        t.forEach(function(t) {
                            t.id === Number(o) && (l = !0)
                        }),
                        e[o].checked = l
                    }
                    s = this.$cal.ser.docalHelper(e),
                    a = this.$cal.ser.docalHelper(s),
                    i = this.$cal.ser.docalHelper(a),
                    n = this.$cal.ser.dusum(e, n),
                    n = this.$cal.ser.dusum(s, n),
                    n = this.$cal.ser.dusum(a, n),
                    n = this.$cal.ser.dusum(i, n),
                    this.data2[0].children = this.toArray(n),
                    this.data4[0].children = this.toArray(s),
                    this.data5[0].children = this.toArray(a),
                    this.data6[0].children = this.toArray(i)
                }
            }
        }
          , Ft = Ht
          , Et = (s("f36b"),
        Object(h["a"])(Ft, Ut, Bt, !1, null, null, null))
          , qt = Et.exports
          , Zt = {
            name: "home",
            components: {
                left: m,
                itemList: C,
                setting: X,
                sysdialog: it,
                calList: Z,
                calRep: qt,
                itemInfo: D,
                sign: Tt,
                usrInfo: Dt
            },
            props: {},
            data: function() {
                return {
                    jobiconid: 62108,
                    curJob: null,
                    toolbar: l.toolbar,
                    userInfo: null,
                    userToken: "",
                    userConf: {},
                    starlist: null,
                    usrlist: null,
                    pageLangs2: {
                        lang: "",
                        lang2: ""
                    },
                    showSys: !1,
                    showDialog: !1,
                    action: "n",
                    webVer: {},
                    curPage: "web.home",
                    clientHeight: document.body.clientWidth,
                    isCollapsed: !1,
                    loading: !0,
                    calList: {},
                    showRepModal: !1,
                    showSeModal: !1,
                    searchKey: "",
                    itemUC: [],
                    classJob: [],
                    fromUrl: !1,
                    seConf: {
                        hazRid: !1,
                        eRid: !1,
                        hazBon: !1,
                        ebon: !1,
                        p1: 5,
                        p2: 5.1,
                        ep: !1,
                        lv1: 70,
                        lv2: 70,
                        elv: !1,
                        ilv1: 430,
                        ilv2: 470,
                        eilv: !1,
                        euc: !1,
                        uc: 0,
                        canDye: !1,
                        eDye: !1,
                        canReduce: !1,
                        eReduce: !1,
                        canDesy: !1,
                        eDesy: !1,
                        job: null,
                        ejob: !1,
                        id: null
                    }
                }
            },
            computed: {
                getCurJobIcon: function() {
                    return Object(n["b"])(this.$cal.classJob[this.curJob + 8].icon + 302)
                }
            },
            mounted: function() {
                var t = this;
                this.$cal.init().then(function() {
                    t.ready(),
                    t.itemUC = t.mkData(t.$cal.itemUC),
                    t.classJob = t.mkData(t.$cal.classJob)
                })
            },
            watch: {},
            created: function() {
                this.userToken = this.$store.state.user.token,
                this.userInfo = this.$store.state.user.userInfo;
                var t = !1
                  , e = this.$route.query;
                void 0 !== e.item && (t = !0),
                void 0 !== e.itemID && (t = !0),
                this.fromUrl = t
            },
            methods: {
                doSearch: function() {
                    var t = this.$cal.ser.search(this.searchKey, this.seConf);
                    this.$Message.info(t.msg),
                    t.its.length > 1e3 ? this.$Message.warning("基于性能考虑，大于1000的结果集不予显示，清添加搜索条件！") : t.its.length > 0 && this.$refs.itemList.loadSe(t.its)
                },
                mkData: function(t) {
                    var e = [];
                    for (var s in t)
                        e.push(t[s]);
                    return e
                },
                ready: function() {
                    var t = this;
                    localStorage.userInfo && (this.userInfo = JSON.parse(localStorage.userInfo)),
                    localStorage.userConf ? this.userConf = JSON.parse(localStorage.userConf) : this.userConf = {
                        curId: 0,
                        curMeau: 62108,
                        curJob: 0
                    },
                    localStorage.userToken && (this.userToken = localStorage.userToken),
                    setTimeout(function() {
                        t.jobiconid = t.userConf.curMeau,
                        t.curJob = t.userConf.curJob;
                        var e = localStorage.temCalList;
                        void 0 !== e && t.$store.commit("initCalList", JSON.parse(e));
                        var s = localStorage.temCalListInfo;
                        void 0 !== s && t.$store.commit("initCalListInfo", JSON.parse(s)),
                        t.lodData(),
                        t.loading = !1,
                        setTimeout(function() {
                            t.checkUrlQuery()
                        }, 1e3)
                    }, 500),
                    console.log("loading", Date.now())
                },
                setLogin: function(t) {
                    console.log("home.setLogin", t),
                    this.userToken = t.data.token,
                    this.userInfo = t.data,
                    this.loadUsrList(!0)
                },
                checkUrlQuery: function() {
                    if (this.fromUrl) {
                        var t = this.$route.query;
                        void 0 !== t.item && (this.searchKey = t.item,
                        this.doSearch())
                    }
                },
                lodData: function() {
                    "" === this.userToken || null === this.userToken || void 0 === this.userToken || this.loadUsrList(!1),
                    this.loadStarList()
                },
                loadUsrList: function(t) {
                    var e = this;
                    t || void 0 === localStorage.usrlist ? Object(o["b"])("/cloud/synclist", {}).then(function(t) {
                        200 === t.status && 0 === t.data.errno ? (e.usrlist = t.data.data,
                        localStorage.usrlist = JSON.stringify(t.data.data),
                        e.$Message.success("拉取用户列表成功！tatol:".concat(t.data.data.length))) : e.$Message.error("拉取用户列表失败！".concat(t.data.msg))
                    }) : this.usrlist = JSON.parse(localStorage.usrlist)
                },
                loadStarList: function(t) {
                    var e = this;
                    t ? Object(o["a"])("/user/starlist", {}).then(function(t) {
                        0 === t.data.errno ? (localStorage.starlist = JSON.stringify(t.data.data),
                        e.starlist = t.data.data,
                        e.$Message.success("拉取推荐队列成功！tatol:".concat(t.data.data.length))) : e.$Message.error("拉取推荐列表失败！".concat(t.data.errmsg))
                    }) : void 0 !== localStorage.starlist && (this.starlist = JSON.parse(localStorage.starlist))
                },
                loadStar: function(t) {
                    var e = this.starlist.find(function(e) {
                        return e._id === t
                    });
                    if (void 0 !== e) {
                        var s = JSON.parse(e.content);
                        this.$refs.itemList.mkList2(s, !0)
                    }
                },
                showUserInfo: function() {
                    "" === this.userToken || null === this.userToken || void 0 === this.userToken ? this.$refs.sgin.show() : this.$refs.usrInfo.show()
                },
                showRep: function() {
                    this.$refs.calRep.showRep()
                },
                doCal: function() {
                    this.$refs.calRep.doCal()
                },
                loadItem: function(t) {
                    console.log("loadItem id=", t),
                    this.$refs.itemInfo.laodItem(t)
                },
                loadBack: function(t) {
                    this.$refs.itemList.searchBack(t)
                },
                saveUserConf: function() {
                    localStorage.userConf = JSON.stringify(this.userConf)
                },
                onToolbarClick: function(t, e) {
                    this.userConf.curMeau = t,
                    this.jobiconid = t,
                    void 0 !== e && (this.curJob = e,
                    this.userConf.curJob = e),
                    this.saveUserConf()
                },
                getIconCss: function(t) {
                    return Object(n["a"])(t, 32)
                },
                loadList: function(t) {
                    this.$refs.itemList.loadList(t[1]),
                    this.userConf.curId = t[0],
                    this.saveUserConf()
                },
                cacheUser: function(t) {
                    localStorage.userInfo = JSON.stringify(t.data),
                    localStorage.userToken = t.data.token
                },
                reload: function() {
                    this.$IDB.clear(),
                    localStorage.removeItem("ver"),
                    localStorage.removeItem("userConf"),
                    window.location.reload(),
                    this.$router.go(0)
                },
                onSwitchLang: function(t) {
                    console.log("onSwitchLang", t)
                }
            }
        }
          , Kt = Zt
          , Vt = (s("b315"),
        Object(h["a"])(Kt, a, i, !1, null, null, null));
        e["default"] = Vt.exports
    }
}]);
