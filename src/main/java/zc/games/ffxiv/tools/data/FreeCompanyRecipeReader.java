package zc.games.ffxiv.tools.data;

import java.io.IOException;
import java.util.List;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

import zc.games.ffxiv.tools.model.FreeCompanyRecipe;

public class FreeCompanyRecipeReader {
	
	private ObjectMapper jsonMapper = new Jackson2ObjectMapperBuilder()
			.findModulesViaServiceLoader(true)
			.createXmlMapper(false)
			.build();
	
	public Iterable<FreeCompanyRecipe> read() {
		try {
			return this.jsonMapper.readValue(Resources.getResource(DataSpecs.FC_RECIPE_DATA), new TypeReference<List<FreeCompanyRecipe>>() {});
		} catch (IOException e) {
			throw new DataException("failed to read free company recipe data", e);
		}
	}
}
