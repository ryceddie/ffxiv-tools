package zc.games.ffxiv.tools.calculator;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableList;

import lombok.Builder;
import lombok.Value;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.view.CompositeBom;

@Value
public class CraftingChecklist {
	
	private Bom fromInventory;

	private Bom raw;
	
	private CraftTable craftTable;
	
	private List<CraftingStep> procedure;
	
	private Bom redundant;
	
	@Builder
	public CraftingChecklist(Bom fromInventory, Bom raw, CraftTable craftTable, List<CraftingStep> procedure, Bom redundant) {
		this.fromInventory = fromInventory;
		this.raw = raw;
		this.craftTable = craftTable;
		this.procedure = procedure;
		this.redundant = redundant;
	}
	
	public String toString() {
		return String.format(
				"crafting checklist:%nfrom-inventory %s%nraw %s%nprocedure %s%nredundant %s",
				this.fromInventory, this.raw, this.craftTable, this.redundant);
	}

	@Value
	public static class CraftTable {
		
		private List<CompositeBom> rows;
		
		private CraftTable(List<CompositeBom> rows) {
			this.rows = rows;
		}

		public static CraftTable from(Iterable<Bom> steps, Function<String, String> item2Job) {
			return new CraftTable(ImmutableList.copyOf(steps).stream()
					.map(b -> CompositeBom.from(b, m -> item2Job.apply(m.getItemId())))
					.collect(Collectors.toList()));
		}
		
		public String toString() {
			String repr = IntStream.range(0, this.rows.size()).boxed()
					.map(i -> String.format("- %s %s", i + 1, rows.get(i)))
					.map(s -> s.replaceAll("\n", "\n\t"))
					.collect(Collectors.joining("\n"));
			return String.format("craft table:%n%s", repr);
		}
	}

	public static class BomList {
		
		private List<Bom> steps;
		
		private BomList(List<Bom> steps) {
			this.steps = steps;
		}

		public String toString() {
			String stepsRepr = IntStream.range(0, this.steps.size()).boxed()
					.map(i -> String.format("- %s: %s", i + 1, steps.get(i).toList()))
					.collect(Collectors.joining("\n"));
			return String.format("BOM list:%n%s", stepsRepr);
		}
	}
}
