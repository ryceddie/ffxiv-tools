package zc.games.ffxiv.tools.model;

import lombok.Value;

@Value
public class ConcreteItem {

	private Item spec;
	
	private boolean hq;
	
//	private int duration;
	
	public boolean isNq() {
		return !this.hq;
	}
}
