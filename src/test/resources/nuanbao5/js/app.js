(function(t) {
    function e(e) {
        for (var i, a, s = e[0], c = e[1], l = e[2], u = 0, h = []; u < s.length; u++)
            a = s[u],
            o[a] && h.push(o[a][0]),
            o[a] = 0;
        for (i in c)
            Object.prototype.hasOwnProperty.call(c, i) && (t[i] = c[i]);
        d && d(e);
        while (h.length)
            h.shift()();
        return r.push.apply(r, l || []),
        n()
    }
    function n() {
        for (var t, e = 0; e < r.length; e++) {
            for (var n = r[e], i = !0, a = 1; a < n.length; a++) {
                var s = n[a];
                0 !== o[s] && (i = !1)
            }
            i && (r.splice(e--, 1),
            t = c(c.s = n[0]))
        }
        return t
    }
    var i = {}
      , a = {
        app: 0
    }
      , o = {
        app: 0
    }
      , r = [];
    function s(t) {
        return c.p + "js/" + ({}[t] || t) + ".e37ade7c828916688ebd.js"
    }
    function c(e) {
        if (i[e])
            return i[e].exports;
        var n = i[e] = {
            i: e,
            l: !1,
            exports: {}
        };
        return t[e].call(n.exports, n, n.exports, c),
        n.l = !0,
        n.exports
    }
    c.e = function(t) {
        var e = []
          , n = {
            "chunk-033a30ee": 1,
            "chunk-3583721c": 1,
            "chunk-64107e79": 1,
            "chunk-642881cd": 1,
            "chunk-7f05ee89": 1,
            "chunk-07a554d1": 1,
            "chunk-09e0c16d": 1,
            "chunk-eb67f20c": 1
        };
        a[t] ? e.push(a[t]) : 0 !== a[t] && n[t] && e.push(a[t] = new Promise(function(e, n) {
            for (var i = "css/" + ({}[t] || t) + "." + {
                "chunk-033a30ee": "68ec753e",
                "chunk-3583721c": "071935bd",
                "chunk-64107e79": "ae4c2988",
                "chunk-642881cd": "fc8179e7",
                "chunk-7f05ee89": "2bf27dbb",
                "chunk-07a554d1": "d430b095",
                "chunk-09e0c16d": "d430b095",
                "chunk-2d0b37e3": "31d6cfe0",
                "chunk-2d0c78c6": "31d6cfe0",
                "chunk-2d20ee40": "31d6cfe0",
                "chunk-2d2290cc": "31d6cfe0",
                "chunk-2d238a06": "31d6cfe0",
                "chunk-eb67f20c": "d430b095"
            }[t] + ".css", o = c.p + i, r = document.getElementsByTagName("link"), s = 0; s < r.length; s++) {
                var l = r[s]
                  , u = l.getAttribute("data-href") || l.getAttribute("href");
                if ("stylesheet" === l.rel && (u === i || u === o))
                    return e()
            }
            var h = document.getElementsByTagName("style");
            for (s = 0; s < h.length; s++) {
                l = h[s],
                u = l.getAttribute("data-href");
                if (u === i || u === o)
                    return e()
            }
            var d = document.createElement("link");
            d.rel = "stylesheet",
            d.type = "text/css",
            d.onload = e,
            d.onerror = function(e) {
                var i = e && e.target && e.target.src || o
                  , r = new Error("Loading CSS chunk " + t + " failed.\n(" + i + ")");
                r.code = "CSS_CHUNK_LOAD_FAILED",
                r.request = i,
                delete a[t],
                d.parentNode.removeChild(d),
                n(r)
            }
            ,
            d.href = o;
            var p = document.getElementsByTagName("head")[0];
            p.appendChild(d)
        }
        ).then(function() {
            a[t] = 0
        }));
        var i = o[t];
        if (0 !== i)
            if (i)
                e.push(i[2]);
            else {
                var r = new Promise(function(e, n) {
                    i = o[t] = [e, n]
                }
                );
                e.push(i[2] = r);
                var l, u = document.createElement("script");
                u.charset = "utf-8",
                u.timeout = 120,
                c.nc && u.setAttribute("nonce", c.nc),
                u.src = s(t),
                l = function(e) {
                    u.onerror = u.onload = null,
                    clearTimeout(h);
                    var n = o[t];
                    if (0 !== n) {
                        if (n) {
                            var i = e && ("load" === e.type ? "missing" : e.type)
                              , a = e && e.target && e.target.src
                              , r = new Error("Loading chunk " + t + " failed.\n(" + i + ": " + a + ")");
                            r.type = i,
                            r.request = a,
                            n[1](r)
                        }
                        o[t] = void 0
                    }
                }
                ;
                var h = setTimeout(function() {
                    l({
                        type: "timeout",
                        target: u
                    })
                }, 12e4);
                u.onerror = u.onload = l,
                document.head.appendChild(u)
            }
        return Promise.all(e)
    }
    ,
    c.m = t,
    c.c = i,
    c.d = function(t, e, n) {
        c.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: n
        })
    }
    ,
    c.r = function(t) {
        "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
        }),
        Object.defineProperty(t, "__esModule", {
            value: !0
        })
    }
    ,
    c.t = function(t, e) {
        if (1 & e && (t = c(t)),
        8 & e)
            return t;
        if (4 & e && "object" === typeof t && t && t.__esModule)
            return t;
        var n = Object.create(null);
        if (c.r(n),
        Object.defineProperty(n, "default", {
            enumerable: !0,
            value: t
        }),
        2 & e && "string" != typeof t)
            for (var i in t)
                c.d(n, i, function(e) {
                    return t[e]
                }
                .bind(null, i));
        return n
    }
    ,
    c.n = function(t) {
        var e = t && t.__esModule ? function() {
            return t["default"]
        }
        : function() {
            return t
        }
        ;
        return c.d(e, "a", e),
        e
    }
    ,
    c.o = function(t, e) {
        return Object.prototype.hasOwnProperty.call(t, e)
    }
    ,
    c.p = "/",
    c.oe = function(t) {
        throw console.error(t),
        t
    }
    ;
    var l = window["webpackJsonp"] = window["webpackJsonp"] || []
      , u = l.push.bind(l);
    l.push = e,
    l = l.slice();
    for (var h = 0; h < l.length; h++)
        e(l[h]);
    var d = u;
    r.push([0, "chunk-vendors"]),
    n()
}
)({
    0: function(t, e, n) {
        t.exports = n("56d7")
    },
    1303: function(t, e, n) {},
    1745: function(t, e, n) {},
    "193c": function(t, e, n) {
        "use strict";
        var i = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("nbbIcon", {
                staticClass: "nbb-job-icon",
                attrs: {
                    icon: t.icon,
                    size: t.size
                }
            })
        }
          , a = []
          , o = (n("c5f6"),
        {
            line: [62001, 62002, 62003, 62004, 62005, 62006, 62007, 62008, 62009, 62010, 62011, 62012, 62013, 62014, 62015, 62016, 62017, 62018, 62019, 62020, 62021, 62022, 62023, 62024, 62025, 62026, 62027, 62028, 62029, 62030, 62031, 62032, 62033, 62034, 62035, 62036, 62037, 62038],
            b32: [62101, 62102, 62103, 62104, 62105, 62106, 62107, 62108, 62109, 62110, 62111, 62112, 62113, 62114, 62115, 62116, 62117, 62118, 62119, 62120, 62121, 62122, 62123, 62124, 62125, 62126, 62127, 62128, 62129, 62130, 62131, 62132, 62133, 62134, 62135, 62136, 62137, 62138],
            light: [62301, 62302, 62303, 62304, 62305, 62306, 62307, 62310, 62311, 62312, 62313, 62314, 62315, 62316, 62317, 62318, 62319, 62320, 62401, 62402, 62403, 62404, 62405, 62406, 62407, 62308, 62408, 62409, 62309, 62410, 62411, 62412, 62413, 62414, 62415, 62416, 62417, 62418]
        })
          , r = {
            name: "nbbJobIcon",
            props: {
                job: {
                    type: Number,
                    default: 0
                },
                size: {
                    type: Number,
                    default: 20
                },
                type: {
                    type: String,
                    default: "line"
                }
            },
            data: function() {
                return {
                    icon: 0
                }
            },
            watch: {
                job: function(t) {
                    this.ready()
                }
            },
            methods: {
                ready: function() {
                    var t = 0;
                    void 0 !== o[this.type] && (t = o[this.type][this.job]),
                    this.icon = t
                }
            },
            mounted: function() {
                this.ready()
            }
        }
          , s = r
          , c = n("2877")
          , l = Object(c["a"])(s, i, a, !1, null, null, null);
        e["a"] = l.exports
    },
    "33ec": function(t, e, n) {},
    5062: function(t, e, n) {},
    "56d7": function(t, e, n) {
        "use strict";
        n.r(e);
        n("3b2b"),
        n("a481"),
        n("ac6a"),
        n("5df3"),
        n("cadf"),
        n("551c"),
        n("f751"),
        n("097d");
        var i = n("bc3a")
          , a = n.n(i)
          , o = n("2b0e")
          , r = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("div", {
                staticClass: "nbb",
                class: t.style,
                attrs: {
                    id: "app"
                }
            }, [n("nav", [t.$store.state.app.loading ? t._e() : n("div", {
                attrs: {
                    id: "top"
                }
            }, [n("router-link", {
                attrs: {
                    to: "/staging"
                }
            }, [t._v("工作台模拟器 V1")]), t._v("|\n      "), n("router-link", {
                attrs: {
                    to: "/cal"
                }
            }, [t._v("配方计算器 V5+")]), t._v("|\n      "), n("router-link", {
                attrs: {
                    to: "/blessed"
                }
            }, [t._v("赐福计算器 v1")]), t._v("|\n      "), n("router-link", {
                attrs: {
                    to: "/workshop"
                }
            }, [t._v("工坊模拟器 v1")]), t._v("|\n      "), n("span", {
                staticClass: "spanStyle1",
                staticStyle: {
                    float: "right"
                }
            }, [t._v("Data:国际服5.1 国服5.0 App:V" + t._s(t.$webVer))]), n("span", {
                staticStyle: {
                    float: "right"
                }
            }, [t._v("\n        配方版本：\n        "), n("RadioGroup", {
                on: {
                    "on-change": t.change
                },
                model: {
                    value: t.recipe,
                    callback: function(e) {
                        t.recipe = e
                    },
                    expression: "recipe"
                }
            }, [n("Radio", {
                attrs: {
                    label: "ja"
                }
            }, [n("span", [t._v("国际服")])]), n("Radio", {
                attrs: {
                    label: "chs"
                }
            }, [n("span", [t._v("国服")])])], 1)], 1)], 1)]), n("keep-alive", [t.$store.state.app.loading ? n("loading") : n("router-view")], 1), t.$store.state.app.loading ? t._e() : n("div", {
                staticClass: "m-footer"
            }, [t._m(0)])], 1)
        }
          , s = [function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("p", [n("span", {
                staticClass: "spanStyle1"
            }, [t._v("\n        DC: mana - Nbb Strife\n        群：345744982 微博：@NbbJack\n      ")]), t._v("\n      Copyright 2016-2019 N.B.B All rights reserved.\n      Data,image FINAL FANTASY XIV©2010 - 2018 SQUARE ENIX CO., LTD. All Rights Reserved.\n    ")])
        }
        ]
          , c = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("div", {
                staticClass: "loading-bg"
            }, [n("div", {
                staticClass: "loading-div"
            }, [n("span", {
                staticClass: "loading-p"
            }, [t._v(t._s(t.$store.state.app.loadingP) + "%")]), n("Icon", {
                staticClass: "loading icon-spin",
                attrs: {
                    type: "ios-loading"
                }
            })], 1)])
        }
          , l = []
          , u = {
            name: "fadeOut",
            computed: {}
        }
          , h = u
          , d = (n("8e69"),
        n("2877"))
          , p = Object(d["a"])(h, c, l, !1, null, null, null)
          , f = p.exports
          , m = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("canvas", {
                attrs: {
                    id: "ribbonsCanvas001"
                }
            })
        }
          , b = []
          , v = (n("6c7b"),
        n("6b54"),
        n("7618"))
          , g = n("d225")
          , y = n("b0b4")
          , _ = function t() {
            if (1 === arguments.length) {
                if (Array.isArray(arguments[0])) {
                    var e = Math.round(t(0, arguments[0].length - 1));
                    return arguments[0][e]
                }
                return t(0, arguments[0])
            }
            return 2 === arguments.length ? Math.random() * (arguments[1] - arguments[0]) + arguments[0] : 0
        }
          , k = function() {
            var t = document.body
              , e = document.documentElement
              , n = Math.max(0, window.innerWidth || t.clientWidth || e.clientWidth || 0)
              , i = Math.max(0, window.innerHeight || t.clientHeight || e.clientHeight || 0);
            return {
                width: n,
                height: i,
                ratio: n / i,
                centerx: n / 2,
                centery: i / 2
            }
        }
          , x = function() {
            function t(e, n) {
                Object(g["a"])(this, t),
                this.x = 0,
                this.y = 0,
                this.set(e, n)
            }
            return Object(y["a"])(t, [{
                key: "set",
                value: function(t, e) {
                    this.x = t || 0,
                    this.y = e || 0
                }
            }, {
                key: "copy",
                value: function(t) {
                    return this.x = t.x || 0,
                    this.y = t.y || 0,
                    this
                }
            }, {
                key: "multiply",
                value: function(t, e) {
                    return this.x *= t || 1,
                    this.y *= e || 1,
                    this
                }
            }, {
                key: "divide",
                value: function(t, e) {
                    return this.x /= t || 1,
                    this.y /= e || 1,
                    this
                }
            }, {
                key: "add",
                value: function(t, e) {
                    return this.x += t || 0,
                    this.y += e || 0,
                    this
                }
            }, {
                key: "subtract",
                value: function(t, e) {
                    return this.x -= t || 0,
                    this.y -= e || 0,
                    this
                }
            }, {
                key: "clampX",
                value: function(t, e) {
                    return this.x = Math.max(t, Math.min(this.x, e)),
                    this
                }
            }, {
                key: "clampY",
                value: function(t, e) {
                    return this.y = Math.max(t, Math.min(this.y, e)),
                    this
                }
            }, {
                key: "flipX",
                value: function() {
                    return this.x *= -1,
                    this
                }
            }, {
                key: "flipY",
                value: function() {
                    return this.y *= -1,
                    this
                }
            }]),
            t
        }()
          , w = function() {
            function t(e, n) {
                Object(g["a"])(this, t),
                this._canvas = null,
                this._context = null,
                this._sto = null,
                this._width = 0,
                this._height = 0,
                this._scroll = 0,
                this._ribbons = [],
                this._options = {
                    colorSaturation: "80%",
                    colorBrightness: "60%",
                    colorAlpha: .65,
                    colorCycleSpeed: 6,
                    verticalPosition: "center",
                    horizontalSpeed: 150,
                    ribbonCount: 3,
                    strokeSize: 0,
                    parallaxAmount: -.5,
                    animateSections: !0
                },
                this._onDraw = this._onDraw.bind(this),
                this._onResize = this._onResize.bind(this),
                this._onScroll = this._onScroll.bind(this),
                this.setOptions(e),
                this.init(n),
                console.log(this)
            }
            return Object(y["a"])(t, [{
                key: "setOptions",
                value: function(t) {
                    if ("object" === Object(v["a"])(t))
                        for (var e in t)
                            t.hasOwnProperty(e) && (this._options[e] = t[e])
                }
            }, {
                key: "init",
                value: function(t) {
                    try {
                        this._canvas = t,
                        this._canvas.style["display"] = "block",
                        this._canvas.style["position"] = "fixed",
                        this._canvas.style["margin"] = "0",
                        this._canvas.style["padding"] = "0",
                        this._canvas.style["border"] = "0",
                        this._canvas.style["outline"] = "0",
                        this._canvas.style["left"] = "0",
                        this._canvas.style["top"] = "0",
                        this._canvas.style["width"] = "100%",
                        this._canvas.style["height"] = "100%",
                        this._canvas.style["z-index"] = "-1",
                        this._onResize(),
                        this._context = this._canvas.getContext("2d"),
                        this._context.clearRect(0, 0, this._width, this._height),
                        this._context.globalAlpha = this._options.colorAlpha,
                        window.addEventListener("resize", this._onResize),
                        window.addEventListener("scroll", this._onScroll),
                        document.body.appendChild(this._canvas)
                    } catch (e) {
                        return void console.log("Canvas Context Error: " + e.toString())
                    }
                    this._onDraw()
                }
            }, {
                key: "addRibbon",
                value: function() {
                    var t = Math.round(_(1, 9)) > 5 ? "right" : "left"
                      , e = 1e3
                      , n = 200
                      , i = 0 - n
                      , a = this._width + n
                      , o = 0
                      , r = 0
                      , s = "right" === t ? i : a
                      , c = Math.round(_(0, this._height));
                    /^(top|min)$/i.test(this._options.verticalPosition) ? c = 0 + n : /^(middle|center)$/i.test(this._options.verticalPosition) ? c = this._height / 2 : /^(bottom|max)$/i.test(this._options.verticalPosition) && (c = this._height - n);
                    var l = []
                      , u = new x(s,c)
                      , h = new x(s,c)
                      , d = null
                      , p = Math.round(_(0, 360))
                      , f = 0;
                    while (1) {
                        if (e <= 0)
                            break;
                        if (e--,
                        o = Math.round((1 * Math.random() - .2) * this._options.horizontalSpeed),
                        r = Math.round((1 * Math.random() - .5) * (.25 * this._height)),
                        d = new x,
                        d.copy(h),
                        "right" === t) {
                            if (d.add(o, r),
                            h.x >= a)
                                break
                        } else if ("left" === t && (d.subtract(o, r),
                        h.x <= i))
                            break;
                        l.push({
                            point1: new x(u.x,u.y),
                            point2: new x(h.x,h.y),
                            point3: d,
                            color: p,
                            delay: f,
                            dir: t,
                            alpha: 0,
                            phase: 0
                        }),
                        u.copy(h),
                        h.copy(d),
                        f += 4,
                        p += this._options.colorCycleSpeed
                    }
                    this._ribbons.push(l)
                }
            }, {
                key: "_drawRibbonSection",
                value: function(t) {
                    if (t) {
                        if (t.phase >= 1 && t.alpha <= 0)
                            return !0;
                        if (t.delay <= 0) {
                            if (t.phase += .02,
                            t.alpha = 1 * Math.sin(t.phase),
                            t.alpha = t.alpha <= 0 ? 0 : t.alpha,
                            t.alpha = t.alpha >= 1 ? 1 : t.alpha,
                            this._options.animateSections) {
                                var e = .1 * Math.sin(1 + t.phase * Math.PI / 2);
                                "right" === t.dir ? (t.point1.add(e, 0),
                                t.point2.add(e, 0),
                                t.point3.add(e, 0)) : (t.point1.subtract(e, 0),
                                t.point2.subtract(e, 0),
                                t.point3.subtract(e, 0)),
                                t.point1.add(0, e),
                                t.point2.add(0, e),
                                t.point3.add(0, e)
                            }
                        } else
                            t.delay -= .5;
                        var n = this._options.colorSaturation
                          , i = this._options.colorBrightness
                          , a = "hsla(" + t.color + ", " + n + ", " + i + ", " + t.alpha + " )";
                        this._context.save(),
                        0 !== this._options.parallaxAmount && this._context.translate(0, this._scroll * this._options.parallaxAmount),
                        this._context.beginPath(),
                        this._context.moveTo(t.point1.x, t.point1.y),
                        this._context.lineTo(t.point2.x, t.point2.y),
                        this._context.lineTo(t.point3.x, t.point3.y),
                        this._context.fillStyle = a,
                        this._context.fill(),
                        this._options.strokeSize > 0 && (this._context.lineWidth = this._options.strokeSize,
                        this._context.strokeStyle = a,
                        this._context.lineCap = "round",
                        this._context.stroke()),
                        this._context.restore()
                    }
                    return !1
                }
            }, {
                key: "_onDraw",
                value: function() {
                    for (var t = 0, e = this._ribbons.length; t < e; ++t)
                        this._ribbons[t] || this._ribbons.splice(t, 1);
                    this._context.clearRect(0, 0, this._width, this._height);
                    for (var n = 0; n < this._ribbons.length; ++n) {
                        var i = this._ribbons[n]
                          , a = 0;
                        try {
                            a = i.length
                        } catch (s) {
                            console.log(this._ribbons)
                        }
                        for (var o = 0, r = 0; r < a; ++r)
                            this._drawRibbonSection(i[r]) && o++;
                        o >= a && (this._ribbons[n] = null)
                    }
                    this._ribbons.length < this._options.ribbonCount && this.addRibbon(),
                    requestAnimationFrame(this._onDraw)
                }
            }, {
                key: "_onResize",
                value: function(t) {
                    var e = k(t);
                    this._width = e.width,
                    this._height = e.height,
                    this._canvas && (this._canvas.width = this._width,
                    this._canvas.height = this._height,
                    this._context && (this._context.globalAlpha = this._options.colorAlpha))
                }
            }, {
                key: "_onScroll",
                value: function(t) {
                    var e = k(t);
                    this._scroll = e.scrolly
                }
            }]),
            t
        }()
          , j = {
            data: function() {
                return {
                    ribbons: null
                }
            },
            mounted: function() {
                var t = {
                    animateSections: !0,
                    colorAlpha: .5,
                    colorBrightness: "50%",
                    colorCycleSpeed: 5,
                    colorSaturation: "60%",
                    horizontalSpeed: 200,
                    parallaxAmount: -.2,
                    ribbonCount: 5,
                    strokeSize: 0,
                    verticalPosition: "center"
                };
                this.ribbons = new w(t,document.getElementById("ribbonsCanvas001"))
            }
        }
          , S = j
          , C = (n("f488"),
        Object(d["a"])(S, m, b, !1, null, null, null))
          , N = C.exports
          , I = n("c24f")
          , L = {
            name: "App",
            components: {
                loading: f,
                ribbons: N
            },
            data: function() {
                return {
                    recipe: "",
                    style: "light"
                }
            },
            methods: {
                change: function(t) {
                    localStorage.nbb_recipe_patch = t,
                    setTimeout(function() {
                        window.location.reload()
                    }, 1e3)
                },
                changeStyle: function(t) {
                    localStorage.nbb_style = t
                },
                loadNotice: function() {
                    var t = this;
                    Object(I["a"])("/user/notice").then(function(e) {
                        if (200 === e.status && 0 === e.data.errno) {
                            var n = e.data.data
                              , i = {
                                duration: 30,
                                title: "系统公告",
                                render: function(t) {
                                    var e = [];
                                    return n.map(function(n) {
                                        e.push(t("li", n.content))
                                    }),
                                    t("ul", e)
                                }
                            };
                            n.length > 0 && t.$Notice.info(i)
                        }
                    })
                }
            },
            mounted: function() {
                this.loadNotice(),
                this.recipe = localStorage.nbb_recipe_patch || "ja",
                this.$store.commit("upRecipe", this.recipe)
            }
        }
          , O = L
          , B = (n("7c55"),
        Object(d["a"])(O, r, s, !1, null, null, null))
          , D = B.exports
          , P = n("e069")
          , T = n.n(P)
          , $ = n("8c4f")
          , E = [{
            path: "/",
            redirect: "/staging"
        }, {
            path: "/tst",
            name: "tst",
            component: function() {
                return n.e("chunk-2d238a06").then(n.bind(null, "ffd4"))
            }
        }, {
            path: "/staging",
            name: "staging",
            component: function() {
                return Promise.all([n.e("chunk-033a30ee"), n.e("chunk-7f05ee89")]).then(n.bind(null, "266e"))
            }
        }, {
            path: "/cal",
            name: "cal5p",
            component: function() {
                return Promise.all([n.e("chunk-033a30ee"), n.e("chunk-642881cd")]).then(n.bind(null, "f476"))
            }
        }, {
            path: "/blessed",
            name: "blessed",
            component: function() {
                return Promise.all([n.e("chunk-033a30ee"), n.e("chunk-3583721c")]).then(n.bind(null, "717f"))
            }
        }, {
            path: "/naicha",
            name: "naicha",
            component: function() {
                return n.e("chunk-2d20ee40").then(n.bind(null, "b0fb"))
            }
        }, {
            path: "/workshop",
            name: "workshop",
            component: function() {
                return Promise.all([n.e("chunk-033a30ee"), n.e("chunk-64107e79")]).then(n.bind(null, "a20b"))
            }
        }, {
            path: "/about",
            name: "about",
            component: function() {
                return n.e("chunk-2d0c78c6").then(n.bind(null, "50a6"))
            }
        }, {
            path: "/share",
            name: "about",
            component: function() {
                return n.e("chunk-2d0b37e3").then(n.bind(null, "2939"))
            }
        }, {
            path: "/idea",
            name: "about",
            component: function() {
                return n.e("chunk-2d2290cc").then(n.bind(null, "dc90"))
            }
        }, {
            path: "/401",
            name: "error_401",
            meta: {
                hideInMenu: !0
            },
            component: function() {
                return n.e("chunk-07a554d1").then(n.bind(null, "24e2"))
            }
        }, {
            path: "/500",
            name: "error_500",
            meta: {
                hideInMenu: !0
            },
            component: function() {
                return n.e("chunk-09e0c16d").then(n.bind(null, "721c"))
            }
        }, {
            path: "*",
            name: "error_404",
            meta: {
                hideInMenu: !0
            },
            component: function() {
                return n.e("chunk-eb67f20c").then(n.bind(null, "1db4"))
            }
        }];
        o["default"].use($["a"]);
        var z = new $["a"]({
            routes: E,
            mode: "hash"
        });
        z.beforeEach(function(t, e, n) {
            n()
        }),
        z.afterEach(function(t) {
            T.a.LoadingBar.finish(),
            window.scrollTo(0, 0)
        });
        var M = z
          , A = (n("7f7f"),
        n("2f62"))
          , R = {
            state: {
                access: "",
                userInfo: {},
                token: ""
            },
            mutations: {
                setLogin: function(t, e) {
                    t.userInfo = e.data,
                    t.token = e.data.token,
                    localStorage.setItem("NBB_usr", JSON.stringify(e.data)),
                    localStorage.NBB_token = e.data.token
                },
                loginout: function(t) {
                    t.userInfo = null,
                    t.token = "",
                    localStorage.removeItem("NBB_usr"),
                    localStorage.removeItem("NBB_token")
                },
                intState: function(t) {
                    var e = localStorage.getItem("NBB_usr")
                      , n = localStorage.getItem("NBB_token");
                    void 0 !== n && (t.token = n,
                    t.userInfo = JSON.parse(e))
                }
            },
            actions: {}
        }
          , J = {
            state: {
                dataSet: {},
                loading: !0,
                loadingP: 0,
                calList: {},
                calListInfo: {},
                calListC: 0,
                calListD: 0,
                recipe: "ja",
                backList: []
            },
            getters: {},
            mutations: {
                endLoading: function(t) {
                    t.loading = !1,
                    t.loadingP = 0
                },
                uploadingP: function(t, e) {
                    e > 1 && (e = 1),
                    t.loadingP = 100 * e
                },
                upRecipe: function(t, e) {
                    t.recipe = e
                },
                addToBackList: function(t, e) {
                    var n = t.backList
                      , i = !1;
                    n.forEach(function(t) {
                        t.id === e.id && (i = !0)
                    }),
                    i || t.backList.push(e)
                },
                addToCalList: function(t, e) {
                    t.calListC = Math.random(),
                    void 0 !== t.calList[e[0]] ? t.calList[e[0]][1] = t.calList[e[0]][1] + e[1] : t.calList[e[0]] = e,
                    localStorage.temCalList = JSON.stringify(t.calList)
                },
                changeCalList: function(t, e) {
                    console.log("changeCalList", e),
                    void 0 !== t.calList[e.id] && (0 === e.val ? delete t.calList[e.id] : t.calList[e.id][1] = e.val,
                    t.calListC = Math.random(),
                    localStorage.temCalList = JSON.stringify(t.calList))
                },
                initCalList: function(t, e) {
                    console.log("initCalList", e),
                    t.calList = e,
                    t.calListC = Math.random()
                },
                initCalListInfo: function(t, e) {
                    console.log("initCalListInfo", e),
                    t.calListInfo.desc = e.desc,
                    t.calListInfo.time = e.time,
                    t.calListInfo.id = e.id,
                    t.calListInfo.hash = e.hash,
                    t.calListInfo.star = e.star,
                    localStorage.temCalListInfo = JSON.stringify(t.calListInfo),
                    t.calListD = Math.random()
                }
            }
        };
        o["default"].use(A["a"]);
        var H = new A["a"].Store({
            state: {
                lang: "",
                lang2: ""
            },
            mutations: {
                switchLang: function(t, e) {
                    t.lang = e,
                    o["default"].config.lang = e,
                    localStorage.lang = e
                },
                switchLang2: function(t, e) {
                    t.lang2 = e,
                    localStorage.lang2 = e
                },
                endLoading: function(t) {
                    t.loading = !1
                },
                saveDataSet: function(t, e) {
                    t.dataSet[e.name] = e.data,
                    "update" === e.name && (localStorage.ver = e.data.ver)
                },
                initlangs: function(t) {
                    localStorage.lang ? t.lang = localStorage.lang : (localStorage.lang = "zh-CN",
                    t.lang = "zh-CN"),
                    localStorage.lang2 ? t.lang2 = localStorage.lang2 : (localStorage.lang2 = "ja-JP",
                    t.lang2 = "ja-JP")
                }
            },
            actions: {},
            modules: {
                user: R,
                app: J
            }
        })
          , q = n("f121")
          , U = {
            DB_Config: {
                zip: {
                    ver: 2,
                    k: "k",
                    Index: {}
                }
            },
            cacheTable: "zip",
            name: "calv7",
            getTime: function() {
                return (new Date).getTime()
            },
            checkPromise: function() {
                "function" != typeof window.Promise && (window.Promise = function(t) {
                    return this._f = t,
                    this._a = null,
                    this
                }
                ,
                window.Promise.prototype.then = function(t) {
                    var e = this;
                    return e._f(function(e) {
                        t(e)
                    }, function(t) {
                        e._a = t
                    }),
                    this
                }
                ,
                window.Promise.prototype.catche = function(t) {
                    return t(this._a)
                }
                )
            },
            DB_Init: function(t) {
                U.checkPromise();
                var e = U.DB_Config[t] || U.DB_Config[U.cacheTable]
                  , n = !window.msIndexedDB;
                if (window.indexedDB || (n = !1),
                n && e) {
                    var i = window.indexedDB.open(U.name)
                      , a = U.getTime();
                    return new Promise(function(n, r) {
                        i.onerror = function(t) {
                            r("OPen Error!")
                        }
                        ,
                        i.onsuccess = function(t) {
                            console.log("打开数据库成功!耗时" + (U.getTime() - a) + "ms"),
                            n(o(i.result))
                        }
                        ,
                        i.onupgradeneeded = function(n) {
                            if (console.log("version is " + i.result.version + "upgradeed"),
                            !i.result.objectStoreNames.contains(t)) {
                                var a = i.result.createObjectStore(t, {
                                    keyPath: e.k || "id"
                                });
                                if (e["Index"])
                                    for (var o in e["Index"])
                                        a.createIndex(o, o, {
                                            unique: e["Index"][o]
                                        })
                            }
                        }
                    }
                    )
                }
                function o(n) {
                    var i = n.transaction(t, "readwrite").objectStore(t);
                    return i.keyPath != e.k && (console.log("objectStore.keyPath=%s 与配置文件 DB_Config.%s.k = %s 不一致！", i.keyPath, t, e.k),
                    e.k = i.keyPath),
                    {
                        del: function(t) {
                            var e = this
                              , n = this.DB.transaction(e.n, "readwrite").objectStore(e.n);
                            n.delete(t)
                        },
                        set: function(t, e) {
                            var n = this
                              , i = n.getTime();
                            "string" == typeof t && alert("indexDB.set 不支持纯字符储存。"),
                            e && (t[n.k] = e),
                            t[n.k] || alert("indexDB.set " + n.k + "不能为空。"),
                            n.del(t[n.k]);
                            var a = n.DB.transaction(n.n, "readwrite").objectStore(n.n);
                            return a = a.add(t),
                            a.onsuccess = function(e) {
                                console.log(t[n.k] + " 保存成功,耗时" + (n.getTime() - i) + "ms")
                            }
                            ,
                            a.onerro = function(e) {
                                console.log(t[n.k] + " 已经存在，并且不能覆盖,耗时" + (n.getTime() - i) + "ms")
                            }
                            ,
                            a
                        },
                        get: function(t) {
                            var e = this
                              , n = e.DB.transaction([e.n], "readwrite").objectStore(e.n)
                              , i = e.getTime();
                            return n = n.get(t),
                            new Promise(function(a, o) {
                                n.onsuccess = function(o) {
                                    console.log("获取" + t + "耗时" + (e.getTime() - i) + "ms"),
                                    a(n.result)
                                }
                                ,
                                n.onerro = function(t) {
                                    console.log("读取失败")
                                }
                            }
                            )
                        },
                        clear: function() {
                            var t = this
                              , e = t.DB.transaction([t.n], "readwrite").objectStore(t.n);
                            return e = e.clear(),
                            new Promise(function(t, n) {
                                e.onsuccess = function(t) {
                                    console.log("清空成功")
                                }
                                ,
                                e.onerro = function(t) {
                                    console.log("清空失败")
                                }
                            }
                            )
                        },
                        store: function() {
                            var t = this
                              , e = t.DB.transaction([t.n], "readwrite").objectStore(t.n);
                            return e
                        },
                        getTime: U.getTime,
                        n: t,
                        k: i.keyPath,
                        DB: n
                    }
                }
                alert("不支持 indexedDB,请换更换能完全支持indexedDB的【现代浏览器】以保证程序正常使用")
            }
        }
          , V = U
          , F = n("8355")
          , Q = n.n(F)
          , W = n("4eb5")
          , X = n.n(W)
          , Y = (n("c5f6"),
        function() {
            function t(e, n) {
                Object(g["a"])(this, t),
                this.itemData = e,
                this.recipeData = n
            }
            return Object(y["a"])(t, [{
                key: "doCal",
                value: function(t) {
                    var e = {}
                      , n = {}
                      , i = {}
                      , a = {}
                      , o = {}
                      , r = {}
                      , s = {};
                    e = this.calHelperTop(t),
                    n = this.docalHelper(e),
                    i = this.docalHelper(n),
                    a = this.docalHelper(i),
                    o = this.docalHelper(a),
                    r = this.docalHelper(o),
                    s = this.dusum(n, s),
                    s = this.dusum(i, s),
                    s = this.dusum(a, s),
                    s = this.dusum(o, s),
                    s = this.dusum(r, s);
                    var c = {
                        ls: e,
                        lv1: n,
                        lv2: i,
                        lv3: a,
                        lv4: o,
                        lv5: r,
                        lvBase: s
                    };
                    return JSON.parse(JSON.stringify(c))
                }
            }, {
                key: "calHelperTopArr",
                value: function(t) {
                    for (var e = {}, n = 0; n < t.length; n++) {
                        var i = 1
                          , a = 0
                          , o = t[n]
                          , r = this.recipeData[o]
                          , s = r.it
                          , c = this.itemData[s];
                        if (r) {
                            var l = parseInt(r.bp[1]);
                            a = parseInt(i / l),
                            a += i % l > 0 ? 1 : 0,
                            e[s] = {
                                id: s,
                                checked: !1,
                                rid: o,
                                job: r.job,
                                name: c.lang,
                                icon: c.icon,
                                need: i,
                                mkc: a,
                                pc: l
                            }
                        }
                    }
                    return e
                }
            }, {
                key: "calHelperTop",
                value: function(t) {
                    var e = {};
                    if (void 0 !== t.length)
                        return this.calHelperTopArr(t);
                    for (var n in t)
                        if (5 !== t[n].length || !t[n][4]) {
                            var i = t[n][1]
                              , a = 0
                              , o = this.recipeData[t[n][2]]
                              , r = this.itemData[n];
                            if (o) {
                                var s = parseInt(o.bp[1]);
                                a = parseInt(i / s),
                                a += i % s > 0 ? 1 : 0,
                                e[n] = {
                                    id: Number(n),
                                    checked: t[n][3],
                                    rid: t[n][2],
                                    job: o.job,
                                    name: r.lang,
                                    icon: r.icon,
                                    need: i,
                                    mkc: a,
                                    pc: s
                                }
                            } else
                                e[n] = {
                                    id: Number(n),
                                    checked: t[n][3],
                                    rid: [],
                                    name: r.lang,
                                    icon: r.icon,
                                    need: i,
                                    mkc: a
                                }
                        }
                    return e
                }
            }, {
                key: "docalHelper",
                value: function(t, e) {
                    var n = {};
                    for (var i in t)
                        if (!0 !== t[i].checked) {
                            var a = t[i].rid;
                            if (void 0 !== a && 0 !== a.length) {
                                "object" === Object(v["a"])(a) && void 0 !== a.length && (a = a[0]);
                                for (var o = this.recipeData[a], r = o.m, s = o.s, c = t[i].mkc, l = 0; l < 4; l += 2) {
                                    var u = s[l];
                                    if (!(u <= 0)) {
                                        var h = this.itemData[u]
                                          , d = s[l + 1];
                                        if (n[u]) {
                                            var p = parseInt(n[u]["need"]) + d;
                                            n[u].need = p
                                        } else
                                            n[u] = {
                                                id: u,
                                                rid: [],
                                                icon: h.icon,
                                                name: h.lang,
                                                need: d,
                                                mkc: 0,
                                                pc: 1
                                            }
                                    }
                                }
                                for (var f = 0; f < r.length; f += 2) {
                                    var m = r[f]
                                      , b = this.itemData[m]
                                      , g = c * parseInt(r[f + 1]);
                                    if (n[m]) {
                                        var y = parseInt(n[m]["need"]) + g;
                                        n[m].need = y
                                    } else
                                        n[m] = {
                                            id: m,
                                            rid: b.rid,
                                            name: b.lang,
                                            icon: b.icon,
                                            need: g,
                                            mkc: 0,
                                            pc: 1
                                        }
                                }
                            }
                        }
                    for (var _ in n) {
                        var k = n[_]
                          , x = 0
                          , w = parseInt(k["need"])
                          , j = 0;
                        if (k.rid && k.rid.length > 0) {
                            var S = this.recipeData[k.rid[0]];
                            j = parseInt(S.bp[1]),
                            x = parseInt(w / j),
                            x += w % j > 0 ? 1 : 0
                        }
                        n[_].mkc = x,
                        n[_].pc = j,
                        e && console.log(" 需求数量  = ", w, " 产量 = ", j, " 生产次数 = ", x)
                    }
                    return n
                }
            }, {
                key: "dusum",
                value: function(t, e) {
                    for (var n in t) {
                        var i = n;
                        if (!0 !== t[i].checked) {
                            var a = t[i]["need"]
                              , o = t[i]["name"];
                            void 0 !== t[i].rid && 0 !== t[i].rid.length || (e[i] ? (a = parseInt(e[i]["need"]) + parseInt(a),
                            e[i].need = a) : e[i] = {
                                id: i,
                                name: o,
                                icon: t[i].icon,
                                need: a,
                                mkc: 0
                            })
                        }
                    }
                    return e
                }
            }, {
                key: "searchBack",
                value: function(t) {
                    var e = t.id
                      , n = [];
                    for (var i in this.recipeData)
                        for (var a = this.recipeData[i].m, o = 0; o < a.length; o += 2)
                            a[o] === Number(e) && n.push(Number(i));
                    return n
                }
            }, {
                key: "search",
                value: function(t, e) {
                    var n = {
                        msg: "",
                        its: []
                    };
                    if ("" === t.trim() && !e.eRid && !e.ep && !e.elv && !e.eilv && !e.euc && !e.eDye && !e.eReduce && !e.eDesy && !e.ejob && !e.ebon)
                        return n.msg = "若是不设定高级搜索条件，关键字不能留空！",
                        n;
                    var i = [];
                    for (var a in this.itemData)
                        if (!(i.length >= 2e3)) {
                            var o = this.itemData[a];
                            if (!this.checkName(t, o) && !this.checkRid(e, o) && !this.checkPatch(e, o) && !this.checkLv(e, o) && !this.checkILv(e, o) && !this.checkUC(e, o) && !this.checkDye(e, o) && !this.checkReduce(e, o) && !this.checkDesy(e, o) && !this.checkBon(e, o)) {
                                var r = JSON.parse(JSON.stringify(o));
                                r.id = a,
                                i.push(r)
                            }
                        }
                    return n.conf = e,
                    n.its = i,
                    n.msg = "输入的关键字是【".concat(t, "】,检索出物品总数【").concat(i.length, "】"),
                    n
                }
            }, {
                key: "checkName",
                value: function(t, e) {
                    return "" !== t.trim() && !(e.lang[0].indexOf(t.trim()) > -1 || e.lang[1].indexOf(t.trim()) > -1 || e.lang[2].indexOf(t.trim()) > -1)
                }
            }, {
                key: "checkUC",
                value: function(t, e) {
                    return !!t.euc && Number(e.uc) !== Number(t.uc)
                }
            }, {
                key: "checkReduce",
                value: function(t, e) {
                    return !!t.eReduce && !(e.reduce > 0)
                }
            }, {
                key: "checkDesy",
                value: function(t, e) {
                    return !!t.eDesy && !(e.desy > 0)
                }
            }, {
                key: "checkBon",
                value: function(t, e) {
                    return !!t.ebon && e.bon !== t.hazBon
                }
            }, {
                key: "checkDye",
                value: function(t, e) {
                    return !!t.eDye && e.dye !== t.canDye
                }
            }, {
                key: "checkRid",
                value: function(t, e) {
                    return !!t.eRid && (void 0 === e.rid || !(e.rid.length > 0))
                }
            }, {
                key: "checkPatch",
                value: function(t, e) {
                    if (!t.ep)
                        return !1;
                    if (void 0 === e.p)
                        return !0;
                    var n = Number(e.p);
                    return !!isNaN(n) || !(t.p1 <= n && t.p2 >= n)
                }
            }, {
                key: "checkLv",
                value: function(t, e) {
                    if (!t.elv)
                        return !1;
                    if (void 0 === e.elv)
                        return !0;
                    var n = Number(e.elv);
                    return !!isNaN(n) || !(t.lv1 <= n && t.lv2 >= n)
                }
            }, {
                key: "checkILv",
                value: function(t, e) {
                    if (!t.eilv)
                        return !1;
                    if (void 0 === e.ilv)
                        return !0;
                    var n = Number(e.ilv);
                    return !!isNaN(n) || !(t.ilv1 <= n && t.ilv2 >= n)
                }
            }]),
            t
        }())
          , Z = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("div", [n("div", {
                staticClass: "craft-item"
            }, [t.showExpand ? n("Icon", {
                staticClass: "craft-item-expand",
                attrs: {
                    size: 15,
                    type: t.expand ? "md-expand" : "md-contract"
                },
                on: {
                    click: function(e) {
                        t.expand = !t.expand
                    }
                }
            }) : t._e(), n("nbbIcon", {
                staticClass: "craft-item-icon",
                attrs: {
                    icon: t.icon,
                    size: 20
                }
            }), n("span", {
                directives: [{
                    name: "clipboard",
                    rawName: "v-clipboard:copy",
                    value: t.name,
                    expression: "name",
                    arg: "copy"
                }, {
                    name: "clipboard",
                    rawName: "v-clipboard:success",
                    value: t.onCopy,
                    expression: "onCopy",
                    arg: "success"
                }, {
                    name: "clipboard",
                    rawName: "v-clipboard:error",
                    value: t.onError,
                    expression: "onError",
                    arg: "error"
                }],
                staticClass: "craft-item-name",
                attrs: {
                    title: t.title + "\n点击复制:" + t.name
                }
            }, [t._v(t._s(t.name))]), n("span", {
                staticClass: "craft-item-count"
            }, [t._t("count")], 2), n("span", {
                staticClass: "craft-item-need"
            }, [t._t("need")], 2), n("span", {
                staticClass: "craft-item-pc"
            }, [t._t("pc")], 2), n("span", {
                staticClass: "craft-item-mkc"
            }, [t._t("mkc")], 2), n("span", {
                staticClass: "craft-item-getter"
            }, [t._t("getter")], 2)], 1), t.expand ? n("div", {
                staticClass: "craft-item-child"
            }, [n("nbbCraftChildren", {
                attrs: {
                    recipe: t.recipe,
                    mkc: t.mkc,
                    jobType: t.jobType
                }
            })], 1) : t._e()])
        }
          , G = []
          , K = "http://cal.ffxiv.xin/"
          , tt = {
            "en-US": 1,
            "ja-JP": 0,
            "zh-CN": 2
        }
          , et = function(t, e) {
            var n = e ? "hq/" : ""
              , i = t + "";
            return 5 === i.length && (i = "0" + i),
            4 === i.length && (i = "00" + i),
            3 === i.length && (i = "000" + i),
            2 === i.length && (i = "0000" + i),
            1 === i.length && (i = "00000" + i),
            "".concat(K).concat(i.substring(0, 3), "000/").concat(n).concat(i, ".png")
        }
          , nt = {
            name: "nbbCraftItem",
            props: {
                item: {
                    type: Number,
                    defult: 0
                },
                mkc: {
                    type: Number,
                    defult: 0
                },
                showExpand: {
                    type: Boolean,
                    defult: !0
                },
                expanded: {
                    type: Boolean,
                    defult: !1
                },
                jobType: {
                    type: String,
                    default: "line"
                }
            },
            data: function() {
                return {
                    recipe: null,
                    icon: 0,
                    name: "",
                    title: "",
                    expand: !1
                }
            },
            watch: {
                item: function(t) {
                    this.ready()
                }
            },
            methods: {
                ready: function() {
                    if (!(this.item <= 0 || void 0 === this.item)) {
                        var t = Object.assign({}, this.$cal.item[this.item])
                          , e = tt[this.$store.state.lang]
                          , n = tt[this.$store.state.lang2];
                        this.name = "" !== t.lang[e] ? t.lang[e] : t.lang[n],
                        this.title = t.lang.join("\n"),
                        this.icon = t.icon,
                        this.expand = this.expanded;
                        var i = t.rid;
                        void 0 !== i && i.length > 0 && (this.recipe = Object.assign({}, this.$cal.recipe[i[0]]))
                    }
                },
                onCopy: function(t) {
                    this.$Message.success("复制成功:".concat(this.name))
                },
                onError: function(t) {
                    console.log(t)
                }
            },
            mounted: function() {
                this.ready()
            }
        }
          , it = nt
          , at = Object(d["a"])(it, Z, G, !1, null, null, null)
          , ot = at.exports
          , rt = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("div", [n("ul", t._l(t.items, function(e) {
                return n("li", {
                    key: e.id
                }, [e.id > 0 && void 0 !== e.bp ? n("nbbCraftItem", {
                    attrs: {
                        item: e.id,
                        mkc: e.mkc,
                        showExpand: e.showExpand,
                        jobType: t.jobType
                    }
                }, [n("span", {
                    attrs: {
                        slot: "count",
                        title: "配方数量"
                    },
                    slot: "count"
                }, [t._v("x " + t._s(e.count))]), n("span", {
                    attrs: {
                        slot: "need",
                        title: "需求数量"
                    },
                    slot: "need"
                }, [t._v("[ x " + t._s(e.need) + " ]")]), e.bp[1] > 1 ? n("span", {
                    attrs: {
                        slot: "pc",
                        title: "产量"
                    },
                    slot: "pc"
                }, [t._v("( " + t._s(e.bp[1]) + " )")]) : t._e(), e.mkc > 1 ? n("span", {
                    attrs: {
                        slot: "mkc",
                        title: "生产次数"
                    },
                    slot: "mkc"
                }, [t._v("[ " + t._s(e.mkc) + " ]")]) : t._e(), e.job > 0 ? n("span", {
                    attrs: {
                        slot: "getter",
                        title: "配方"
                    },
                    slot: "getter"
                }, [t._v("\n          [\n          "), n("nbbJobIcon", {
                    attrs: {
                        job: e.job,
                        size: 20,
                        type: t.jobType
                    }
                }), t._v("\n          Lv" + t._s(e.bp[2]) + " ]\n        ")], 1) : t._e()]) : t._e()], 1)
            }), 0)])
        }
          , st = []
          , ct = {
            name: "nbbCraftChildren",
            props: {
                item: {
                    type: Number
                },
                mkc: {
                    type: Number
                },
                recipe: {
                    type: Object
                },
                jobType: {
                    type: String,
                    default: "line"
                }
            },
            data: function() {
                return {
                    items: [],
                    rids: {
                        type: Array,
                        default: function() {
                            return []
                        }
                    }
                }
            },
            watch: {
                recipe: function(t) {
                    this.ready()
                }
            },
            methods: {
                ready: function() {
                    if (void 0 !== this.recipe && null !== this.recipe && void 0 !== this.recipe.id) {
                        var t = [];
                        t.push({
                            id: this.recipe.s[0],
                            count: this.recipe.s[1],
                            bp: [0, 0, 0, 0],
                            job: 0,
                            showExpand: !1,
                            need: this.mkc * this.recipe.s[1]
                        }),
                        this.recipe.s.length > 2 && t.push({
                            id: this.recipe.s[2],
                            count: this.recipe.s[3],
                            bp: [0, 0, 0, 0],
                            job: 0,
                            showExpand: !1,
                            need: this.mkc * this.recipe.s[1]
                        });
                        for (var e = this.recipe.m, n = 0; n < e.length; n += 2)
                            if (Number(e[n]) >= 0) {
                                var i = Object.assign({}, this.$cal.item[e[n]])
                                  , a = i.rid
                                  , o = void 0 !== a && a.length > 0 ? Object.assign({}, this.$cal.recipe[a[0]]) : void 0
                                  , r = void 0 !== o ? o.bp : [0, 0, 0, 0]
                                  , s = void 0 !== o ? o.job + 7 : 0
                                  , c = e[n + 1] * this.mkc
                                  , l = 0 === r[1] ? 1 : c / r[1];
                                t.push({
                                    id: e[n],
                                    count: e[n + 1],
                                    bp: r,
                                    showExpand: void 0 !== a,
                                    job: s,
                                    need: c,
                                    mkc: Number(l.toFixed(0))
                                })
                            }
                        this.items = t
                    }
                }
            },
            mounted: function() {
                this.ready()
            }
        }
          , lt = ct
          , ut = Object(d["a"])(lt, rt, st, !1, null, null, null)
          , ht = ut.exports
          , dt = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("nbbCraftItem", {
                attrs: {
                    item: t.item.id,
                    fist: !0,
                    showExpand: !0,
                    mkc: t.item.mkc,
                    expanded: !0,
                    jobType: t.jobType
                }
            }, [t.item.count > 1 ? n("span", {
                attrs: {
                    slot: "count",
                    title: "配方数量"
                },
                slot: "count"
            }, [t._v("x " + t._s(t.item.count))]) : t._e(), n("span", {
                attrs: {
                    slot: "need",
                    title: "需求数量"
                },
                slot: "need"
            }, [t._v("[ x " + t._s(t.item.need) + " ]")]), t.item.bp[1] > 1 ? n("span", {
                attrs: {
                    slot: "pc",
                    title: "产量"
                },
                slot: "pc"
            }, [t._v("( " + t._s(t.item.bp[1]) + " )")]) : t._e(), t.item.mkc > 1 ? n("span", {
                attrs: {
                    slot: "mkc",
                    title: "生产次数"
                },
                slot: "mkc"
            }, [t._v("[ " + t._s(t.item.mkc) + " ]")]) : t._e(), t.item.job > 0 ? n("span", {
                attrs: {
                    slot: "getter",
                    title: "配方"
                },
                slot: "getter"
            }, [t._v("\n    [\n    "), n("nbbJobIcon", {
                attrs: {
                    job: t.item.job,
                    size: 18,
                    type: t.jobType
                }
            }), t._v("\n    Lv" + t._s(t.item.bp[2]) + "\n    ]\n  ")], 1) : t._e()])
        }
          , pt = []
          , ft = {
            name: "nbbCraftList",
            props: {
                id: {
                    type: Number,
                    default: 0
                },
                rid: {
                    type: Number,
                    default: 0
                },
                need: {
                    type: Number,
                    default: 1
                },
                jobType: {
                    type: String,
                    default: "line"
                }
            },
            data: function() {
                return {
                    item: {
                        bp: [0, 0, 0, 0],
                        mkc: 1
                    },
                    recipe: {}
                }
            },
            watch: {
                rid: function() {
                    this.ready()
                },
                id: function() {
                    this.ready()
                }
            },
            methods: {
                ready: function() {
                    if (this.rid > 0) {
                        this.recipe = Object.assign({}, this.$cal.recipe[this.rid]);
                        var t = void 0 !== this.recipe ? this.recipe.bp : [0, 0, 0, 0]
                          , e = void 0 !== this.recipe ? this.recipe.job + 7 : 0
                          , n = this.need
                          , i = n / t[1];
                        this.item = {
                            id: this.recipe.it,
                            count: this.count,
                            bp: t,
                            showExpand: !0,
                            job: e,
                            need: n,
                            mkc: Number(i.toFixed(0))
                        }
                    }
                    if (this.id > 0) {
                        var a = Object.assign({}, this.$cal.item[this.id])
                          , o = a.rid
                          , r = [0, 0, 0, 0]
                          , s = 0;
                        if (void 0 !== o && o.length > 0) {
                            var c = Object.assign({}, this.$cal.recipe[o[0]]);
                            r = void 0 !== c ? c.bp : [0, 0, 0, 0],
                            s = void 0 !== c ? c.job + 7 : 0,
                            this.recipe = void 0 !== c ? c : {}
                        }
                        var l = this.need
                          , u = l / r[1];
                        this.item = {
                            id: this.id,
                            count: this.count,
                            bp: r,
                            showExpand: !0,
                            job: s,
                            need: l,
                            mkc: Number(u.toFixed(0))
                        }
                    }
                }
            },
            mounted: function() {
                this.ready()
            }
        }
          , mt = ft
          , bt = Object(d["a"])(mt, dt, pt, !1, null, null, null)
          , vt = bt.exports
          , gt = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("img", {
                class: t.sizeStr,
                attrs: {
                    src: t.href
                }
            })
        }
          , yt = []
          , _t = {
            name: "nbbIcon",
            props: {
                icon: {
                    type: Number,
                    default: 0
                },
                size: {
                    type: Number,
                    default: 20
                }
            },
            data: function() {
                return {
                    sizeStr: "",
                    href: ""
                }
            },
            watch: {
                icon: function(t) {
                    this.ready()
                }
            },
            methods: {
                ready: function() {
                    this.sizeStr = "size-" + this.size,
                    this.href = et(this.icon)
                }
            },
            mounted: function() {
                this.ready()
            }
        }
          , kt = _t
          , xt = Object(d["a"])(kt, gt, yt, !1, null, null, null)
          , wt = xt.exports
          , jt = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("div", [n("div", [t._v("nbbItem:" + t._s(t.item))]), t._t("default")], 2)
        }
          , St = []
          , Ct = {
            name: "nbbItem",
            props: {
                item: {
                    type: Number,
                    default: 0
                }
            },
            data: function() {
                return {}
            },
            watch: {},
            methods: {
                ready: function() {}
            },
            mounted: function() {
                this.ready()
            }
        }
          , Nt = Ct
          , It = Object(d["a"])(Nt, jt, St, !1, null, null, null)
          , Lt = It.exports
          , Ot = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("div")
        }
          , Bt = []
          , Dt = {}
          , Pt = Dt
          , Tt = Object(d["a"])(Pt, Ot, Bt, !1, null, null, null)
          , $t = Tt.exports
          , Et = n("193c")
          , zt = function() {
            var t = this
              , e = t.$createElement
              , n = t._self._c || e;
            return n("ul", {
                staticClass: "nbb-tree"
            }, [t._l(t.rids, function(e, i) {
                return n("li", {
                    key: i
                }, [n("nbbCraftList", {
                    staticClass: "craft-list",
                    attrs: {
                        rid: Number(i),
                        need: e,
                        jobType: t.jobType
                    }
                })], 1)
            }), t._l(t.ids, function(e, i) {
                return n("li", {
                    key: i
                }, [n("nbbCraftList", {
                    staticClass: "craft-list",
                    attrs: {
                        id: Number(i),
                        need: e,
                        jobType: t.jobType
                    }
                })], 1)
            })], 2)
        }
          , Mt = []
          , At = {
            name: "nbbTree",
            props: {
                rids: {
                    type: Object,
                    default: function() {}
                },
                ids: {
                    type: Object,
                    default: function() {}
                },
                jobType: {
                    type: String,
                    default: "line"
                }
            },
            data: function() {
                return {}
            },
            watch: {
                rids: function(t) {
                    this.ready()
                }
            },
            methods: {
                ready: function() {}
            },
            mounted: function() {
                this.ready()
            }
        }
          , Rt = At
          , Jt = Object(d["a"])(Rt, zt, Mt, !1, null, null, null)
          , Ht = Jt.exports;
        o["default"].component("nbbIcon", wt),
        o["default"].component("nbbJobIcon", Et["a"]),
        o["default"].component("nbbCraftItem", ot),
        o["default"].component("nbbCraftChildren", ht),
        o["default"].component("nbbCraftList", vt),
        o["default"].component("nbbTree", Ht),
        o["default"].component("nbbItem", Lt),
        o["default"].component("nbbItemList", $t);
        n("9ddc"),
        n("d310"),
        n("7ebb"),
        n("33ec"),
        n("5062"),
        n("1745"),
        n("76ff"),
        n("daa3");
        o["default"].use(T.a),
        o["default"].use(X.a),
        o["default"].config.productionTip = !1,
        o["default"].prototype.$JSZIP = Q.a,
        o["default"].prototype.$config = q["a"],
        o["default"].prototype.$webVer = q["a"].proVer,
        o["default"].prototype.$getBin = function(t) {
            return a.a.get(t, {
                responseType: "arraybuffer"
            })
        }
        ,
        o["default"].prototype.$cal = {
            item: null,
            recipe_ja: null,
            recipe_chs: null,
            recipeNote: null,
            itemSC: null,
            itemUC: null,
            classJob: null,
            workshop: null,
            ClassJobCategory: null,
            inited: !1,
            init: function(t) {
                var e = o["default"].prototype.$IDB
                  , n = o["default"].prototype.$cal;
                return new Promise(function(t, i) {
                    if (n.inited)
                        t();
                    else {
                        var a = localStorage.nbb_recipe_patch || "ja"
                          , o = [];
                        o.push(e.get("item")),
                        o.push(e.get("recipe_ja")),
                        o.push(e.get("recipe_chs")),
                        o.push(e.get("recipeNote")),
                        o.push(e.get("itemSC")),
                        o.push(e.get("itemUC")),
                        o.push(e.get("classJob")),
                        o.push(e.get("workshop")),
                        o.push(e.get("RecipeLevel")),
                        o.push(e.get("BaseParam")),
                        o.push(e.get("noteHash")),
                        o.push(e.get("ClassJobCategory")),
                        Promise.all(o).then(function(e) {
                            e.forEach(function(t) {
                                "recipe_ja" === t.k && "ja" === a ? n.recipe = t.data : "recipe_chs" === t.k && "chs" === a ? n.recipe = t.data : n[t.k] = t.data
                            }),
                            n.ser = new Y(n.item,n.recipe),
                            n.inited = !0,
                            t()
                        }).catch(function(t) {
                            i(t)
                        })
                    }
                }
                )
            },
            reload: function(t) {
                var e = o["default"].prototype.$IDB
                  , n = o["default"].prototype.$cal;
                return new Promise(function(i, a) {
                    var o = t || "ja"
                      , r = [];
                    r.push(e.get("recipe_ja")),
                    r.push(e.get("recipe_chs")),
                    Promise.all(r).then(function(t) {
                        t.forEach(function(t) {
                            "recipe_ja" === t.k && "ja" === o ? n.recipe = t.data : "recipe_chs" === t.k && "chs" === o && (n.recipe = t.data)
                        }),
                        i()
                    }).catch(function(t) {
                        a(t)
                    })
                }
                )
            },
            filterParm: function(t, e, n) {
                var i = this.BaseParam
                  , a = i[t[0]]
                  , o = "" !== a.lang[e] ? a.lang[e] : a.lang[n]
                  , r = '<Tag color="success">'.concat(o, "</tag> NQ:").concat(t[2], "% (Max ", 3, ") HQ:").concat(t[4], "% (Max ").concat(t[3] + t[5], ")")
                  , s = '<Tag color="success">'.concat(o, "</tag> ").concat(t[2], "% (Max ", 3, ") ")
                  , c = '<Tag color="success">'.concat(o, "</tag> NQ:").concat(t[3], " HQ:").concat(t[3] + t[5])
                  , l = '<Tag color="success">'.concat(o, "</tag> ").concat(t[3], " ");
                return t[1] ? 100 === t[2] ? c : r : 100 === t[2] ? l : s
            }
        },
        V.DB_Init("zip").then(function(t) {
            o["default"].prototype.$IDB = t
        }),
        String.prototype.format = function() {
            if (0 === arguments.length)
                return this;
            for (var t = this, e = 0; e < arguments.length; e++)
                t = t.replace(new RegExp("\\{" + e + "\\}","g"), arguments[e]);
            return t
        }
        ,
        Array.prototype.removeByValue = function(t) {
            for (var e = 0; e < this.length; e++)
                if (this[e] === t) {
                    this.splice(e, 1);
                    break
                }
        }
        ,
        new o["default"]({
            router: M,
            store: H,
            render: function(t) {
                return t(D)
            },
            created: function() {
                var t = this;
                this.$store.commit("initlangs"),
                this.$store.commit("intState"),
                console.log("webver=", this.$webVer),
                void 0 === localStorage.ver || this.$webVer !== localStorage.ver ? (console.log("从远程加载"),
                this.loadStar(),
                this.loadDatas()) : (console.log("从localStorage加载"),
                this.$store.commit("uploadingP", 100),
                setTimeout(function() {
                    t.$store.commit("endLoading")
                }, 1455))
            },
            methods: {
                loadDatas: function() {
                    var t = this;
                    this.$store.commit("uploadingP", .1),
                    this.$getBin("/statics/statics.json").then(function(e) {
                        t.$store.commit("uploadingP", .5),
                        t.unzip(e.data)
                    })
                },
                loadStar: function() {
                    var t = this;
                    Object(I["a"])("/user/starlist", {}).then(function(e) {
                        0 === e.data.errno ? localStorage.starlist = JSON.stringify(e.data.data) : t.$Message.error("拉取推荐列表失败！".concat(e.data.errmsg))
                    })
                },
                unzip: function(t) {
                    var e = this
                      , n = new e.$JSZIP;
                    n.loadAsync(t).then(function(t) {
                        var n = [];
                        for (var i in t.files)
                            n.push(i);
                        var a = 0;
                        n.forEach(function(i) {
                            t.file(i).async("string").then(function(t) {
                                a++;
                                try {
                                    var o = JSON.parse(t);
                                    e.$IDB.set({
                                        data: o
                                    }, i),
                                    e.$store.commit("uploadingP", .5 * (a / n.length).toFixed(2) + .5),
                                    a === n.length && (setTimeout(function() {
                                        e.$store.commit("endLoading")
                                    }, 1455),
                                    localStorage.ver = e.$webVer)
                                } catch (r) {
                                    console.log(r)
                                }
                            })
                        })
                    })
                }
            }
        }).$mount("#app")
    },
    "76ff": function(t, e, n) {},
    "7c55": function(t, e, n) {
        "use strict";
        var i = n("cba8")
          , a = n.n(i);
        a.a
    },
    "7ebb": function(t, e, n) {},
    "8e69": function(t, e, n) {
        "use strict";
        var i = n("1303")
          , a = n.n(i);
        a.a
    },
    ba50: function(t, e, n) {},
    c24f: function(t, e, n) {
        "use strict";
        n("ac6a"),
        n("456d");
        var i = n("d225")
          , a = n("b0b4")
          , o = n("bc3a")
          , r = n.n(o)
          , s = n("4328")
          , c = n.n(s)
          , l = n("e069")
          , u = function() {
            function t() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : baseURL;
                Object(i["a"])(this, t),
                this.baseUrl = e,
                this.queue = {}
            }
            return Object(a["a"])(t, [{
                key: "getInsideConfig",
                value: function() {
                    var t = {
                        baseURL: this.baseUrl,
                        headers: {}
                    };
                    return t
                }
            }, {
                key: "destroy",
                value: function(t) {
                    delete this.queue[t],
                    Object.keys(this.queue).length
                }
            }, {
                key: "interceptors",
                value: function(t, e) {
                    var n = this;
                    t.interceptors.request.use(function(t) {
                        return t.data = c.a.stringify(t.data),
                        n.queue[e] = !0,
                        t
                    }, function(t) {
                        return Promise.reject(t)
                    }),
                    t.interceptors.response.use(function(t) {
                        n.destroy(e);
                        var i = t.data
                          , a = t.status;
                        return -3 === t.data.errno && (localStorage.removeItem("NBB_usr"),
                        localStorage.removeItem("NBB_token"),
                        l["Modal"].error({
                            content: "登录失效,请重新登录"
                        }),
                        setTimeout(function() {
                            window.location.reload()
                        }, 3e3),
                        i = []),
                        {
                            data: i,
                            status: a
                        }
                    }, function(t) {
                        return n.destroy(e),
                        Promise.reject(t)
                    })
                }
            }, {
                key: "request",
                value: function(t) {
                    var e = r.a.create();
                    return t = Object.assign(this.getInsideConfig(), t),
                    "/user/login" !== t.url && (t.headers.Authorization = localStorage.NBB_token),
                    this.interceptors(e, t.url),
                    e(t)
                }
            }]),
            t
        }()
          , h = u
          , d = n("f121")
          , p = d["a"].baseUrl.pro
          , f = new h(p)
          , m = f;
        n.d(e, "b", function() {
            return b
        }),
        n.d(e, "a", function() {
            return v
        });
        var b = function(t, e) {
            return m.request({
                url: t,
                data: e,
                method: "post"
            })
        }
          , v = function(t, e) {
            return m.request({
                url: t,
                params: e,
                method: "get"
            })
        }
    },
    cba8: function(t, e, n) {},
    d310: function(t, e, n) {},
    daa3: function(t, e, n) {},
    f121: function(t, e, n) {
        "use strict";
        var i = "19.11.12.1663";
        e["a"] = {
            proVer: i,
            baseUrl: {
                dev: "http://cloud.nbb.ffxiv.cn",
                pro: "http://cloud.nbb.ffxiv.cn"
            }
        }
    },
    f488: function(t, e, n) {
        "use strict";
        var i = n("ba50")
          , a = n.n(i);
        a.a
    }
});
