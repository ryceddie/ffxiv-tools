package zc.games.ffxiv.tools.web;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import zc.games.ffxiv.tools.data.ActorSnapshot;
import zc.games.ffxiv.tools.data.service.ActorService;

@RestController
@RequestMapping("/api/actors")
@AllArgsConstructor
public class ActorController {
	
	private ActorService service;

	@PutMapping()
	public void updateActorsState(@RequestBody ActorSnapshot snapshot) {
		this.service.updateStates(snapshot);
	}
}
