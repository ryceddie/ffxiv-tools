package zc.games.ffxiv.tools.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;

import lombok.AllArgsConstructor;
import lombok.Value;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.service.ItemService;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Item;

@RestController
@RequestMapping("/api/items")
@AllArgsConstructor
public class ItemController {
	
	private static final Splitter CSL_SPLITTER = Splitter.on(',');

	private ItemService service;
	
	private ItemRepository itemRepo;

	@GetMapping("/search")
	public Items search(@RequestParam String key) {
		List<Item> listing = ImmutableList.copyOf(this.service.search(key));
		return new Items(listing);
	}
	
	@GetMapping(params = "ids")
	public Items batchGet(@RequestParam String ids) {
		List<Item> listing = CSL_SPLITTER.splitToList(ids)
				.stream()
				.map(id -> this.itemRepo.findById(id).orElse(null))
				.filter(it -> it != null)
				.collect(Collectors.toList());
		return new Items(listing);
	}
	
	@GetMapping(path = "/subsets/glamour")
	public Items findGlamourItems(
			@RequestParam GearSlot slot,
			@RequestParam boolean craftable) {
		List<Item> listing = this.service.findGlamourItems(slot, ItemService.SearchCriteria.builder()
				.craftableOnly(craftable)
				.build());
		return new Items(listing);
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleException(IllegalArgumentException e) {
		return ResponseEntity.badRequest().body(e.getMessage());
	}
	
	@Value
	public static class Items {
		
		private List<Item> listing;
		
		public static Items from(Iterable<Item> items) {
			return new Items(ImmutableList.copyOf(items));
		}
	}
}
