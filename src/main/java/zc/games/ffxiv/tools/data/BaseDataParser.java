package zc.games.ffxiv.tools.data;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;
import com.google.common.io.Resources;

import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Item.Category;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.Recipe;
import zc.games.ffxiv.tools.model.Recipe.Spec;

public class BaseDataParser {
	
	public BaseData.Ingredients parse() {
		List<Job> jobs = parseJobs();
		List<Job.Category> jobCategories = parseJobCategories(jobs);
		List<Item.Category> categories = parseItemCategories();
		Collection<Recipe.Spec> recipeSpecs = parseRecipeSpecs();
		Collection<Recipe.NotebookCategory> recipeNotebookCategories = parseRecipeNotebookCategories();
		return new BaseData.Ingredients(jobs, jobCategories, categories, recipeSpecs, recipeNotebookCategories);
	}

	private Collection<Recipe.NotebookCategory> parseRecipeNotebookCategories() {
		Map<Integer, List<String>> raw = new JsonDataReaderV6().readNotebookCategories();
		RecipeNotebookDataResolver recipeNotebookDataResolver = new RecipeNotebookDataResolver();
		return raw.entrySet().stream()
				.<Recipe.NotebookCategory>map(entry -> {
					Integer categoryId = entry.getKey();
					Range<Integer> notebookIdRange = recipeNotebookDataResolver.resolveNotebookIdRange(categoryId);
					List<String> nameList = entry.getValue();
					return new Recipe.NotebookCategory(categoryId, nameList.get(0), notebookIdRange);
				})
				.collect(Collectors.toList());
	}

	private Collection<Spec> parseRecipeSpecs() {
		List<List<Integer>> rawData = new JsonDataReaderV6().readRecipeSpecs();
		return rawData.stream()
				.map(entry -> {
					return Recipe.Spec.builder()
							.id(entry.get(0))
							.level(entry.get(1))
							.stars(entry.get(2))
							.difficulty(entry.get(3))
							.quality(entry.get(4))
							.durability(entry.get(5))
							.build();
				})
				.collect(Collectors.toList());
	}

	private List<Job.Category> parseJobCategories(List<Job> jobs) {
		List<List<Object>> dataList = new JsonDataReaderV6().readJobCategories();
		
		JobCategoryDataResolver jobCategoryDataResolver = new JobCategoryDataResolver(jobs);
		Function<List<Object>, List<Job>> jobsResolver = data -> {
			int id = (int) data.get(0);
			String jLabel = (String) data.get(1);
			String eLabel = (String) data.get(2);
			return jobCategoryDataResolver.resolve(id, jLabel, eLabel);
		};
		return dataList.stream()
				.map(catData -> this.buildJobCategory(catData, jobsResolver))
				.collect(Collectors.toList());
	}

	private List<Category> parseItemCategories() {
		List<List<Object>> data = new JsonDataReaderV6().readItemCategories();
		
		return data.stream()
				.map(this::buildItemCategory)
				.collect(Collectors.toList());
	}

	private List<Job> parseJobs() {
		List<List<Object>> jobData = new JsonDataReaderV6().readJobs();
		
		Map<String, Job> indexed = jobData.stream()
				.collect(
						HashMap::new,
						(index, data) -> {
							Job job = this.buildJob(data, index::get);
							index.put(job.getId(), job);
						},
						HashMap::putAll);
		return ImmutableList.copyOf(indexed.values());
	}
	
	private Job buildJob(List<Object> data, Function<String, Job> jobIdResolver) {
		String id = String.valueOf(data.get(0));
		String name = String.valueOf(data.get(1));
		String abbr = String.valueOf(data.get(2));
		int category = (int) data.get(4);
		String parentJobId = String.valueOf(data.get(24));
		Job parentJob = jobIdResolver.apply(parentJobId);
		String englishName = String.valueOf(data.get(25));
		return Job.builder().id(id)
				.name(name)
				.englishName(englishName)
				.englishAbbr(abbr)
				.parent(parentJob)
				.primaryCategoryId(category)
				.build();
	}
	
	private Job.Category buildJobCategory(List<Object> data, Function<List<Object>, List<Job>> jobResolver) {
		int code = (int) data.get(0);
		String jLabel = (String) data.get(1);
		String eLabel = (String) data.get(2);
		return Job.Category.builder()
				.code(code)
				.label(jLabel)
				.englishLabel(eLabel)
				.jobs(jobResolver.apply(data))
				.build();
	}
	
	private Item.Category buildItemCategory(List<Object> data) {
		String code = String.valueOf(data.get(0));
		String name = String.valueOf(data.get(1));
		int groupOrdinal = (int) data.get(4);
		return new Item.Category(code, name, Item.CategoryGroup.VALUES[groupOrdinal]);
	}
	
	/**
	 * Utility for reading json data from V6 app.
	 * 
	 * @author Zhang Chi
	 * @since 2020-01-02
	 *
	 */
	private static class JsonDataReaderV6 {
		
		private String rootPath = "game-data/nuanbao";
		
		private ObjectMapper jsonMapper = new ObjectMapper();
		
		List<List<Object>> readJobs() {
			List<List<Object>> jobData;
			try {
				jobData = jsonMapper.readValue(Resources.getResource(this.rootPath + "/jobs.json"), 
						new TypeReference<List<List<Object>>>() {});
			} catch (IOException e) {
				throw new DataException("failed to parse job data", e);
			}
			return jobData;
		}
		
		List<List<Object>> readItemCategories() {
			List<List<Object>> data;
			try {
				data = jsonMapper.readValue(
						Resources.getResource(this.rootPath + "/item-category.json"), 
						new TypeReference<List<List<Object>>>() {});
			} catch (IOException e) {
				throw new DataException("failed to parse item category", e);
			}
			return data;
		}
		
		List<List<Object>> readJobCategories() {
			List<List<Object>> dataList;
			try {
				dataList = jsonMapper.readValue(
						Resources.getResource(this.rootPath + "/job-category.json"), 
						new TypeReference<List<List<Object>>>() {});
			} catch (IOException e) {
				throw new DataException("failed to parse job category", e);
			}
			return dataList;
		}

		List<List<Integer>> readRecipeSpecs() {
			List<List<Integer>> rawData;
			try {
				rawData = this.jsonMapper.readValue(Resources.getResource(this.rootPath + "/recipe-spec.json"), new TypeReference<List<List<Integer>>>() {});
			} catch (IOException e) {
				throw new DataException("failed to parse recipe spec", e);
			}
			return rawData;
		}

		Map<Integer, List<String>> readNotebookCategories() {
			Map<Integer, List<String>> raw;
			try {
				raw = this.jsonMapper.readValue(Resources.getResource(this.rootPath + "/recipe-notebook-category.json"), new TypeReference<Map<Integer, List<String>>>() {});
			} catch (IOException e) {
				throw new DataException("failed to parse recipe notebook category", e);
			}
			return raw;
		}
		
		//TODO add more readXX methods
	}
}