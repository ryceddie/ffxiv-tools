package zc.games.ffxiv.tools.web;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;
import lombok.Value;
import zc.games.ffxiv.tools.calculator.CraftingCalculator;
import zc.games.ffxiv.tools.calculator.CraftingChecklist;
import zc.games.ffxiv.tools.data.service.InventoryService;
import zc.games.ffxiv.tools.data.service.InventoryService.StockExclusion;
import zc.games.ffxiv.tools.data.service.InventoryService.StockFilter;
import zc.games.ffxiv.tools.model.Material;

@RestController
@RequestMapping("/api/crafting")
@AllArgsConstructor
public final class CraftingController {
	
	private CraftingCalculator calculator;
	
	private InventoryService inventoryService;

	@PostMapping(path = "/calculation", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public CraftingChecklist calculate(@RequestBody CraftingCalculationRequest calculationRequest) {
		List<StockFilter> filters = calculationRequest.getStockExclusion().stream()
				.map(StockExclusion::toFilter).collect(Collectors.toList());
		List<Material> inventoryMaterials = this.inventoryService.getAll(filters);
		CraftingCalculator.CraftingContext context = CraftingCalculator.CraftingContext.builder()
				.inventoryMaterials(inventoryMaterials)
				.unacquirableItems(calculationRequest.getUnacquirableItems())
				.build();

		return this.calculator.calculate(calculationRequest.getCraftingListing(), context);
	}
	
	@Value
	@Builder
	public static class CraftingCalculationRequest {
		
		@Singular("craft")
		@NonNull
		private List<Material> craftingListing;
		
		private List<StockExclusion> stockExclusion = Collections.emptyList();
		
		/**
		 * Id list of items which can not be acquired, indicates that these items should not appear in raw BOM.
		 */
		private List<String> unacquirableItems = Collections.emptyList();

		@Deprecated
		private boolean inventoryDisabled;
	}
}
