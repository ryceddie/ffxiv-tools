package zc.games.ffxiv.tools.data.service;

/**
 * 
 * @author Administrator
 *
 */
public class PlayerCharacterMissingException extends RuntimeException {

	public PlayerCharacterMissingException(String characterKey) {
		super("Can not find player character: " + characterKey);
	}
}
