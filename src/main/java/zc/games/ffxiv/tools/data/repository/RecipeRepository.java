package zc.games.ffxiv.tools.data.repository;

import java.util.Collection;
import java.util.Optional;

import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.RecipeParser;
import zc.games.ffxiv.tools.model.Recipe;

public class RecipeRepository {
	
	private Multimap<String, Recipe> index;
	
	public RecipeRepository(Iterable<Recipe> recipes, ItemRepository itemRepo) {
		this.index = Multimaps.index(recipes, r -> r.getProduct().getItemId());
	}
	
	/**
	 * 
	 * @param itemRepo
	 * @param baseData
	 * @deprecated use {@link #RecipeRepository(Iterable, ItemRepository)}
	 */
	public RecipeRepository(ItemRepository itemRepo, BaseData baseData) {
		this(new RecipeParser(itemRepo, baseData).parse(), itemRepo);
	}
	
	public Recipe byItem(String itemId) {
		return this.findByItemId(itemId)
				.orElseThrow(() -> new DataException("failed to find recipe for item: " + itemId));
	}

	public Optional<Recipe> findByItemId(String itemId) {
		Collection<Recipe> matched = this.index.get(itemId);
		return Optional.ofNullable(Iterables.getFirst(matched, null));
	}

}
