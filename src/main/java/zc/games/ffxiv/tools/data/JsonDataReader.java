package zc.games.ffxiv.tools.data;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

/**
 * Provides methods for reading commonly structured JSON data.
 * 
 * @author Zhang Chi
 * @since 2020-02-15
 *
 */
public class JsonDataReader {
	
	private String rootPath = "game-data/nuanbao5";
	private ObjectMapper jsonMapper = new ObjectMapper();

	public Map<String, Map<String, Object>> readIndexedObject(String resourceName, String dataDesc) {
		return this.readMap(resourceName, dataDesc, new TypeReference<Map<String, Object>>() {});
	}

	public <T> Map<String, T> readMap(String resourceName, String dataDesc, TypeReference<T> typeRef) {
		Map<String, T> result;
		try {
			result = jsonMapper.readValue(Resources.getResource(
					String.format("%s/%s.json", this.rootPath, resourceName)), new TypeReference<Map<String, T>>() {});
		} catch (IOException e) {
			throw new DataException("failed to read " + dataDesc + " data", e);
		}
		return result;
	}
}
