package zc.games.ffxiv.tools.data.repository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import zc.games.ffxiv.tools.data.ActorState;
import zc.games.ffxiv.tools.data.PlayerCharacterState;
import zc.games.ffxiv.tools.model.Chocobo;
import zc.games.ffxiv.tools.model.Inventories;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.JobState;
import zc.games.ffxiv.tools.model.PlayerCharacter;
import zc.games.ffxiv.tools.model.Retainer;
import zc.games.ffxiv.tools.model.SoulCrystal;

public class SinglePlayerCharacterDataRepository implements PlayerCharacterRepository {
	
	private PlayerCharacter data;
	
	private PlayerCharacterState state;
	
	private List<Inventory> inventories;

	@Override
	public void updateState(PlayerCharacterState state) {
		String characterKey = state.getName();
		synchronized (this) {
			this.data = Optional.ofNullable(this.data)
					.map(lastSnapshot -> lastSnapshot.toBuilder())
					.orElse(PlayerCharacter.builder())
					.name(characterKey)
					.currentJob(state.getCurrentJob())
//					.jobInfo(state.getJobInfo().entrySet().stream()
//							.map(entry -> new JobState(entry.getKey(), entry.getValue()))
//							.collect(Collectors.toList()))
					.build();
		}

	}

	@Override
	public void updateInventoryData(String characterKey, List<Inventory> inventoryData) {
		Preconditions.checkState(this.data != null, String.format("character '%s' does not exist", characterKey));
		
		synchronized(this) {
			
			PlayerCharacter player = this.data;
			
			List<Inventory> characterInventories = inventoryData.stream()
					.filter(inv -> inv.getType().isPlayers())
					.collect(Collectors.toList());
			List<Inventory> retainerInventories = inventoryData.stream()
					.filter(inv -> inv.getType().isRetainers())
					.filter(inv -> inv.getCells().size() > 0)
					.collect(Collectors.toList());
			List<Retainer> retainers = resolveRetainers(Optional.of(player), retainerInventories)
					.stream().collect(Collectors.toList());
			List<Inventory> chocoboInventories = inventoryData.stream()
					.filter(inv -> inv.getType().isChocobos())
					.collect(Collectors.toList());
			
			player = player.toBuilder()
					.inventories(characterInventories)
					.retainers(retainers)
					.chocobo(new Chocobo(chocoboInventories))
					.build();
//			player = player.toBuilder()
//					.allJobStates(resolveJobStates(player))
//					.build();
			
			this.data = player;
			
//			this.updateSuiteCache(
//					player, player.getCurrentJob(), player.getCurrentEquipment());
		}
	}

	@Override
	public Optional<PlayerCharacterState> stateOf(String characterKey) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<PlayerCharacterState> allStates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateActorState(ActorState state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<ActorState> actorStateOf(Predicate<String> keyPredicate) {
		// TODO Auto-generated method stub
		return null;
	}

	private Collection<Retainer> resolveRetainers(Optional<PlayerCharacter> player, List<Inventory> retainerInventories) {
		
		List<Retainer> currentRetainers = player
				.map(PlayerCharacter::getRetainers)
				.orElse(Collections.emptyList());
		if (retainerInventories.isEmpty()) {
			return currentRetainers;
		}
		
		Map<String, Retainer> index = currentRetainers.stream()
				.collect(Collectors.toMap(Retainer::getName, it -> it));
		String name = matchRetainer(index, retainerInventories)
				.orElse(String.format("retainer#%s", index.size() + 1));
		Retainer current = new Retainer(name, retainerInventories);
		index.put(name, current);
		//XXX warning is more appropriate
//		if (index.size() > Retainer.MAX_PER_CHARACTER) {
//			throw new DataException("retainer resolution by inventories failed");
//		}
		return index.values();
	}

	private Collection<JobState> resolveJobStates(PlayerCharacter pc) {
		List<Job> extended = pc.soulCrystals().stream()
				.map(SoulCrystal::from)
				.filter(sc -> sc.job().parent().isPresent())
				.map(SoulCrystal::job)
//				.map(job -> new JobState(job, pc.getLevel(job)))
				.collect(Collectors.toList());
		List<JobState> result = Lists.newArrayList(pc.getJobInfo());
		extended.forEach(derivedJob -> {
			int index = Iterables.indexOf(result, baseState -> baseState.getJob().equals(derivedJob.parent().get()));
			result.set(index, new JobState(derivedJob, pc.getLevel(derivedJob)));
		});
		
		return result;
	}
	
	private Optional<String> matchRetainer(Map<String, Retainer> retainers, List<Inventory> retainerInventory) {
		for (String key : retainers.keySet()) {
			if (compare(retainers.get(key).getInventories(), retainerInventory)) {
				return Optional.of(key);
			}
		}
		return Optional.empty();
	}

	private boolean compare(List<Inventory> oldSnapshot, List<Inventory> newSnapshot) {
		Inventories old = new Inventories(oldSnapshot);
		return newSnapshot.stream()
				.anyMatch(currentInv -> {
					return old.getByCode(currentInv.getCode())
							.map(preInv -> preInv.equals(currentInv))
							.orElse(false);
				});
	}

	@Override
	public List<Inventory> inventoryStateOf(String characterKey) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Multimap<String, Inventory> inventoryStateOf(Predicate<String> keyPredicate) {
		// TODO Auto-generated method stub
		return null;
	}

}
