package zc.games.ffxiv.tools.model;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;

import lombok.Value;

@Value
public class Retainer {
	
	public static final short MAX_PER_CHARACTER = 8;
	
	private String name;

	private List<Inventory> inventories;

	public Retainer(String name, Collection<Inventory> inventories) {
		this.name = name;
		this.inventories = ImmutableList.copyOf(inventories);
	}
}
