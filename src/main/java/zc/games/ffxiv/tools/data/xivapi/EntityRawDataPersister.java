package zc.games.ffxiv.tools.data.xivapi;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

import lombok.Value;
import zc.games.ffxiv.tools.data.DataException;

/**
 * 
 * @author Zhang Chi
 * @since 2020-02-27
 *
 */
class EntityRawDataPersister {
	
	private File rootDir;
	
	EntityRawDataPersister(File rootDir) {
		this.rootDir = rootDir;
		if (!this.rootDir.exists()) {
			boolean created = this.rootDir.mkdirs();
			if (!created) {
				throw new DataException("failed to create directory: " + rootDir);
			}
		}
	}
	
	EntityRawDataPersister(String rootDir) {
		this(new File(rootDir));
	}

	void save(EntityRawData data) {
		File dest = new File(this.rootDir, new EntityFilename(data.getContentName(), data.getContentId()).toString());
		try {
			Files.asCharSink(dest, Charsets.UTF_8).write(data.getJson());
		} catch (IOException e) {
			throw new DataException(String.format("failed to save game data %s: id=%s", data.getContentName(), data.getContentId()), e);
		}
	}
	
	Iterable<EntityRawData> load() {
		return Arrays.asList(this.rootDir.listFiles())
				.stream()
				.map(file -> {
					EntityFilename filename = EntityFilename.parse(file.getName());
					String json;
					try {
						json = Files.asCharSource(file, Charsets.UTF_8).read();
					} catch (IOException e) {
						throw new DataException("failed to read game content file: " + file, e);
					}
					return new EntityRawData(filename.getContentName(), filename.getContentId(), json);
				})
				.collect(Collectors.toList());
	}
	
	int entityCount() {
		return this.rootDir.listFiles().length;
	}

	@Value
	private static class EntityFilename {
		
		private static Splitter SPLITTER = Splitter.on(CharMatcher.anyOf("-."));
		
		private String contentName;
		
		private int contentId;
		
		private static EntityFilename parse(String filename) {
			List<String> parts = SPLITTER.splitToList(filename);
			return new EntityFilename(parts.get(0), Integer.parseInt(parts.get(1)));
		}
		
		public String toString() {
			return String.format("%s-%s.json", this.getContentName(), this.getContentId());
		}
	}

	@Value
	public static class EntityRawData {
		
		private String contentName;
		
		private int contentId;
		
		private String json;
	}
}
