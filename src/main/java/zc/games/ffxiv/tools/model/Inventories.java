package zc.games.ffxiv.tools.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import lombok.NonNull;
import lombok.Value;

@Value
public class Inventories implements Iterable<Inventory> {
	
	@NonNull
	private List<Inventory> listing;
	
	private transient Map<String, Inventory> codeIndex;

	public Inventories(@NonNull Collection<Inventory> listing) {
		this.listing = ImmutableList.copyOf(listing);
		this.codeIndex = this.listing.stream()
				.collect(Collectors.toMap(Inventory::getCode, it -> it));
	}
	
	public Optional<Inventory> getByCode(String code) {
		return Optional.ofNullable(this.codeIndex.get(code));
	}

	@Override
	public Iterator<Inventory> iterator() {
		return this.listing.iterator();
	}
}