package zc.games.ffxiv.tools.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import zc.games.ffxiv.tools.data.ActorSnapshot;
import zc.games.ffxiv.tools.data.service.ActorService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ActorControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ActorService service;
	
	@Autowired
	private ObjectMapper jsonMapper;

	@Test
	public void test() throws Exception {
//		ActorSnapshot actorSnapshot = new ActorSnapshot(Arrays.asList(
//				new ActorState("foo", ActorState.Type.MINION)));
		ActorSnapshot actorSnapshot = new ActorSnapshot(Collections.emptyList());
		this.mockMvc.perform(put("/api/actors")
				.content(this.jsonMapper.writeValueAsString(actorSnapshot))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(status().isOk());
		
		assertThat(this.service.retainer()).isNotPresent();
	}
	
	@TestConfiguration
	static class Config {
		
		@Bean
		ActorController controller(ActorService service) {
			return new ActorController(service);
		}
		
		@Bean
		ActorService service() {
			return new ActorService();
		}
	}
}
