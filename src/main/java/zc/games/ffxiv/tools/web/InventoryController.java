package zc.games.ffxiv.tools.web;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimaps;

import lombok.AllArgsConstructor;
import zc.games.ffxiv.tools.data.service.InventoryService;
import zc.games.ffxiv.tools.data.service.InventoryService.ItemStockInfo;

@RestController
@RequestMapping("/api/inventories")
@AllArgsConstructor
public final class InventoryController {
	
	private InventoryService service;
	
	@GetMapping(path = "/item-stock")
	public Map<String, Collection<ItemStockInfo>> locateItems(
			@RequestParam Collection<String> itemIds,
			@RequestParam Optional<String> pcKey) {
		
		return this.service.locateItems(itemIds, pcKey).stream()
				.collect(Multimaps.toMultimap(
						ItemStockInfo::getItemId, 
						stockInfo -> stockInfo, 
						ArrayListMultimap::create))
				.asMap();
	}
}
