package zc.games.ffxiv.tools.data.xivapi;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Consumer;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.CharStreams;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;

import feign.Response;
import feign.ResponseMapper;
import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.xivapi.EntityRawDataPersister.EntityRawData;

/**
 * 
 * @author Zhang Chi
 * @since 2020-02-27
 *
 */
class EntityRawDataExtractor implements ResponseMapper {
	
	private static Splitter URL_SPLITTER = Splitter.on(CharMatcher.anyOf("/?")).omitEmptyStrings();
	
	private Consumer<EntityRawData> consumer;
	
	public EntityRawDataExtractor consumer(Consumer<EntityRawData> consumer) {
		this.consumer = consumer;
		return this;
	}

	@Override
	public Response map(Response response, Type type) {
		if (this.consumer == null) {
			return response;
		}
		
		String url = response.request().url();
		List<String> urlParts = URL_SPLITTER.splitToList(url);
		String contentName = urlParts.get(2);
		int contentId = Integer.parseInt(urlParts.get(3));
		Charset charset = response.headers().get(HttpHeaders.CONTENT_TYPE)
				.stream()
				.findFirst()
				.map(MediaType::parse)
				.map(mediaType -> mediaType.charset().orNull())
				.orElse(Charsets.UTF_8);
		
		String responseBody;
		try (Reader responseReader = response.body().asReader(charset)) {
			responseBody = CharStreams.toString(responseReader);
		} catch (IOException e) {
			throw new DataException("failed to read response of data request", e);
		}
		
		this.consumer.accept(new EntityRawData(contentName, contentId, responseBody));
		return response.toBuilder()
				.body(responseBody, charset)
				.build();
	}
}
