package zc.games.ffxiv.tools.model;

import java.util.List;

import lombok.Value;

@Value
public class Chocobo {
	
	private String name = "chocobo";

	private List<Inventory> inventories;
}
