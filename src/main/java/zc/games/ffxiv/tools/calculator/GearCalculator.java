package zc.games.ffxiv.tools.calculator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.service.PlayerCharacterService;
import zc.games.ffxiv.tools.model.ConcreteItem;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Item.Category;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.PlayerCharacter;

@AllArgsConstructor
@Slf4j
public class GearCalculator {
	
	private final Ordering<Item> itemLevelOrdering = Ordering.natural().onResultOf(Item::getLevel);
	
	private PlayerCharacterService pcService;

	private ItemRepository items;
	
	private BaseData baseData;
	
	public Multimap<GearSlot, Item> upgradeSuggestion(PlayerCharacter pc, Optional<Job> job, Options options) {
		Job targetJob = job.orElse(pc.getCurrentJob());
		if (targetJob == null) {
			throw new DataMissingException(String.format(
					"job of gear upgrade for character '%s' cannot be determined", pc.getName()));
		}
		int jobLevel = pc.getLevel(targetJob);
		if (jobLevel == 0) {
			log.warn("job '{}' has no state info or is not learned yet", targetJob.getName());
			return Multimaps.forMap(Collections.emptyMap());
		}
		
		log.info("start calculating equipment upgrade suggestion for {} with job: {} Lv.{}", 
				pc.getName(), targetJob.getName(), jobLevel);
		
		Map<GearSlot, ConcreteItem> currentSuite = this.pcService.suiteOf(pc.getName(), targetJob);
		
		log.debug("current suite: {}", currentSuite.values().stream()
				.map(item -> item.getSpec().getName())
				.collect(Collectors.joining(",")));
		
		Map<Item.Category, ConcreteItem> currentGears = currentSuite.values().stream()
				.collect(Collectors.groupingBy(item -> item.getSpec().getCategory()))
				.entrySet().stream()
				.map(entry -> new Object[] {entry.getKey(), this.itemLevelOrdering.onResultOf(ConcreteItem::getSpec).max(entry.getValue())})
				.collect(Collectors.toMap(tuple -> (Item.Category) tuple[0], tuple -> (ConcreteItem) tuple[1]));
		
		Multimap<Item.Category, Item> candidates = FluentIterable.from(this.items.getAllGears())
//				.filter(item -> item.getCategory().getGroup().canEquip())
				.filter(item -> item.getJobs().contains(targetJob))
				.filter(item -> jobLevel >= item.getRequiredLevel())
				.filter(item -> {
					return Optional.ofNullable(currentGears.get(item.getCategory()))
							.map(equiped -> equiped.getSpec().getLevel() < item.getLevel() 
									|| equiped.isNq() && equiped.getSpec().getLevel() == item.getLevel())
							.orElse(true);
				})
				.filter(item -> options.craftable().map(craftable -> craftable ^ !item.isCraftable()).orElse(true))
				.index(Item::getCategory);
		
		log.debug("candidates info: {}", this.getInfoString(candidates.asMap()));
		
		Map<GearSlot, Collection<Item>> suggestion = Maps.toMap(Arrays.asList(GearSlot.VALUES), gs -> gs.itemCategoryIds()
				.stream()
				.map(this.baseData::getItemCategory)
				.flatMap(cat -> candidates.get(cat).stream())
				.collect(Collectors.toList()));
		
		Map<GearSlot, Collection<Item>> tailoredSuggestion = Maps.transformValues(suggestion, items -> {
			return this.itemLevelOrdering.greatestOf(items, 5);
		});
		Multimap<GearSlot, Item> result = ArrayListMultimap.create();
		tailoredSuggestion.forEach((gs, items) -> {
			if (!items.isEmpty()) {
				result.putAll(gs, items);
			}
		});
		return result;
	}
	
	public Map<GearSlot, Item> bestInSlots(PlayerCharacter pc, Job job) {
		// TODO
		throw new UnsupportedOperationException("not implemented");
	}

	private String getInfoString(Map<Category, Collection<Item>> map) {
		return map.entrySet().stream()
				.map(entry -> String.format("%s(%s)", 
						entry.getKey().getName(), 
						entry.getValue().size()))
				.collect(Collectors.joining(","));
	}
	
	@Value
	@Builder
	public static class Options {
		
		public static Options NONE = new Options(null);
		
		private Boolean craftable;
		
		public Optional<Boolean> craftable() {
			return Optional.ofNullable(this.craftable);
		}
	}
}
