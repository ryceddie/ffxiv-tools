package zc.games.ffxiv.tools.calculator;

import java.util.List;

import com.google.common.collect.Lists;

import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.Material;

public class BomBuilderFactory {
	
	private ItemRepository items;
	
	public BomBuilderFactory(ItemRepository itemRepo) {
		this.items = itemRepo;
	}
	
	public BomBuilder newBom() {
		return new BomBuilder();
	}
	
	public class BomBuilder {
		
		private List<Material> selection = Lists.newArrayList();
		
		public BomBuilder add(String name, int count) {
			this.selection.add(Material.builder()
					.itemId(items.findByName(name).get().getId())
					.itemName(name)
					.count(count)
					.build());
			return this;
		}
		
		public Bom build() {
			return new Bom(this.selection);
		}
	}
}
