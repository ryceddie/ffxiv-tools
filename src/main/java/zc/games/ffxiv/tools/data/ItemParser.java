package zc.games.ffxiv.tools.data;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;

import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Job;

public class ItemParser {
	
	private final String sourceVersion;
	
	private final ObjectMapper jsonMapper;
	
	private final BaseData baseData;
	
	private final Predicate<String> idOfFcCraftableItem;
	
	public ItemParser(BaseData baseData) {
		this(baseData, "v6");
	}
	
	private ItemParser(BaseData baseData, String version) {
		this.sourceVersion = version;
		this.jsonMapper = new ObjectMapper();
		this.baseData = baseData;
		Set<String> itemIds = FluentIterable.from(new FreeCompanyRecipeReader().read())
				.transform(fcRecipe -> fcRecipe.getProduct().getItemId())
				.toSet();
		this.idOfFcCraftableItem = itemIds::contains;
	}
	
	public Iterable<Item> parseLatest() {
		return this.parseV5();
	}
	
	public Iterable<Item> parse(String gameVersion) {
		if (gameVersion.startsWith("5.")) {
			return this.parseV5();
		}
		if (gameVersion.startsWith("4.")) {
			return this.parseV6();
		}
		throw new DataException("no data source for version: " + gameVersion);
	}
	
	@Deprecated
	public ItemParser useV5Data() {
		return new ItemParser(this.baseData, "v5");
	}
	
	@Deprecated
	public Iterable<Item> parse() {
		if (this.sourceVersion.equals("v6")) {
			return parseV6();
		}
		if (this.sourceVersion.equals("v5")) {
			return this.parseV5();
		}
		throw new DataException("unknown data source version: " + this.sourceVersion);
	}

	private Iterable<Item> parseV6() {
		Map<String, Map<String, Object>> info = new JsonDataV6Reader().read();
		return info.values().stream()
				.map(itemInfo -> {
					String id = String.valueOf(itemInfo.get("id"));

					String cname = Strings.emptyToNull((String) itemInfo.get("cname"));
					String ename = Strings.emptyToNull((String) itemInfo.get("ename"));
					String name = Optional.ofNullable(cname).orElse(ename);
					if (Strings.isNullOrEmpty(name)) {
						return null;
					}
					String category = String.valueOf(itemInfo.get("uc"));
					List<?> recipeIds = (List<?>) itemInfo.get("rid");
					int level = (int) itemInfo.get("lv_i");
					int requiredLevel = Optional.ofNullable((Integer) itemInfo.get("requiredLevel")).orElse(1);
					int jobCategoryCode = Optional.ofNullable((Integer) itemInfo.get("jobs")).orElse(0);
					Job.Category jobCategory = baseData.getJobCategory(jobCategoryCode);
					boolean craftable = !recipeIds.isEmpty() || this.idOfFcCraftableItem.test(id);
					Item item = Item.builder()
							.id(id)
							.name(name)
							.category(this.baseData.getItemCategory(category))
							.level(level)
							.requiredLevel(requiredLevel)
							.jobCategoryCode(jobCategoryCode)
							.jobs(jobCategory.getJobs())
							.craftable(craftable)
							.build();
					return item;
				})
				.filter(it -> it != null)
				.collect(Collectors.toList());
	}
	
	
	private Iterable<Item> parseV5() {
		Map<String, Map<String, Object>> info = new JsonDataV5Reader().read();
		return info.values().stream()
				.map(itemInfo -> {
					String id = String.valueOf(itemInfo.get("id"));
					
					List<?> names = (List<?>) itemInfo.get("lang");
					String cname = Strings.emptyToNull((String) names.get(2));
					String ename = Strings.emptyToNull((String) names.get(1));
					String name = Optional.ofNullable(cname).orElse(ename);
					if (Strings.isNullOrEmpty(name)) {
						return null;
					}
					
					String category = String.valueOf(itemInfo.get("uc"));
					List<?> recipeIds =  Optional.ofNullable((List<?>) itemInfo.get("rid"))
							.orElse(Collections.emptyList());
					int level = (int) itemInfo.get("ilv");
					int requiredLevel = Optional.ofNullable((Integer) itemInfo.get("elv")).orElse(1);
					int jobCategoryCode = Optional.ofNullable((Integer) itemInfo.get("jobs")).orElse(0);
					Job.Category jobCategory = baseData.getJobCategory(jobCategoryCode);
					boolean craftable = !recipeIds.isEmpty() || this.idOfFcCraftableItem.test(id);
					Item item = Item.builder()
							.id(id)
							.name(name)
							.category(this.baseData.getItemCategory(category))
							.level(level)
							.requiredLevel(requiredLevel)
							.jobCategoryCode(jobCategoryCode)
							.jobs(jobCategory.getJobs())
							.craftable(craftable)
							.build();
					return item;
				})
				.filter(it -> it != null)
				.collect(Collectors.toList());
	}
	
	/**
	 * Utility for reading json data from v6 app.
	 * 
	 * @author Zhang Chi
	 * @since 2019-12-10
	 *
	 */
	private class JsonDataV6Reader {
		
		private String infoPath = String.format("%s/itemsInfo_%s.json", DataSpecs.ROOT_PATH, DataSpecs.ITEM_VERSION);
		
		private String dataPath = String.format("%s/itemsData_%s.json", DataSpecs.ROOT_PATH, DataSpecs.ITEM_VERSION);
		
		Map<String, Map<String, Object>> read() {
			Map<String, Map<String, Object>> result = this.readInfo();
			Map<String, Map<String, Object>> extraData = this.readData();
			result.keySet().forEach(id -> {
				result.compute(id, (_k, itemInfo)-> {
//					if (!extraData.containsKey(id)) {
//					throw new IllegalArgumentException("item data is unavailable for item id: " + id);
//				}
					return ImmutableMap.<String, Object>builder()
						.putAll(itemInfo)
						.putAll(Optional.ofNullable(extraData.get(id))
								.orElse(Collections.emptyMap()))
						.build();
					});
			});
			return result;
		}
		
		Map<String, Map<String, Object>> readInfo() {
			Map<String, Object> items;
			try {
				items = jsonMapper.readValue(Resources.getResource(this.infoPath), 
						new TypeReference<Map<String, Object>>() {});
			} catch (IOException e) {
				throw new DataException("failed to read item data", e);
			}
			@SuppressWarnings("unchecked")
			Map<String, Map<String, Object>> data = (Map<String, Map<String, Object>>) items.get("data");
			return data;
		}

		@SuppressWarnings("unchecked")
		private Map<String, Map<String, Object>> readData() {
			Map<String, Object> root;
			try {
				root = jsonMapper.readValue(Resources.getResource(this.dataPath), 
						new TypeReference<Map<String, Object>>() {});
			} catch (IOException e) {
				throw new DataException("failed to read item data", e);
			}
			Map<String, List<Object>> data = (Map<String, List<Object>>) root.get("data");
			return Maps.transformValues(data, values -> {
				return ImmutableMap.<String, Object>builder()
						.put("requiredLevel", values.get(39))
						.put("jobs", values.get(42))
						.build();
			});
		}
	}

	/**
	 * Utility for reading json data from v5 app.
	 * 
	 * @author Zhang Chi
	 * @since 2019-12-10
	 *
	 */
	private class JsonDataV5Reader {
		
		private String resourcePath = "game-data/nuanbao5/item.json";
		
		/**
		 * 
		 * @return Map[item_id, Map[property_name, property_value]]
		 */
		Map<String, Map<String, Object>> read() {
			Map<String, Map<String, Object>> items;
			try {
				items = jsonMapper.readValue(Resources.getResource(resourcePath), 
						new TypeReference<Map<String, Object>>() {});
			} catch (IOException e) {
				throw new DataException("failed to read item data", e);
			}
			return items;
		}
	}
}
