package zc.games.ffxiv.tools.data;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.testng.annotations.Test;

import com.google.common.collect.Maps;

import zc.games.ffxiv.tools.data.init.BaseDataInitialization;
import zc.games.ffxiv.tools.model.Item;

@Test
public class ItemParserTest {

	public void test() {
		BaseDataInitialization.Config config = new BaseDataInitialization.Config("src/test/resources/xivapi");
		BaseData baseData = new BaseDataInitialization().init(config);
		Iterable<Item> items = new ItemParser(baseData).parseLatest();
		
		assertThat(items).hasSize(26239);
		
		Map<String, Item> index = Maps.uniqueIndex(items, Item::getId);
		
		assertThat(index.get("10338").getJobs()).isNotEmpty();
	}
}
