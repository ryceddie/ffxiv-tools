package zc.games.ffxiv.tools.data.xivapi;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.siebre.test.TestHelper;

import feign.Feign;
import feign.Logger.Level;
import feign.jackson.JacksonDecoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import okhttp3.Cache;
import zc.games.ffxiv.tools.data.xivapi.Models.ClassJob;
import zc.games.ffxiv.tools.data.xivapi.Models.ClassJobCategory;

public class GameContentApiTest {
	
	private GameContentApi api;
	
	private TestHelper testHelper = new TestHelper(this.getClass());
	
	@Before
	public void setup() {
		File cacheDir = new File(testHelper.getOutputTarget(), "cache");
		okhttp3.OkHttpClient delegate = new okhttp3.OkHttpClient.Builder()
				.cache(new Cache(cacheDir, 1L * 1024 * 1024))
				.build();
		EntityRawDataPersister gameContentPersister = new EntityRawDataPersister(
				new File(this.testHelper.getOutputTarget(), "game-content"));
		this.api = Feign.builder()
				.client(new OkHttpClient(delegate))
				.logger(new Slf4jLogger())
				.logLevel(Level.BASIC)
				.mapAndDecode(new EntityRawDataExtractor().consumer(gameContentPersister::save), new JacksonDecoder())
				.target(GameContentApi.class, "https://xivapi.com");
	}

	@Test
	public void test_classjob() {
		ClassJob job = this.api.getClassJob(0);
		
		assertThat(job.getId()).isEqualTo(0);
		assertThat(job.getCategoryId()).isEqualTo(30);
		
		ClassJob gla = this.api.getClassJob(1);
		
		assertThat(gla.getId()).isEqualTo(1);
		assertThat(gla.getCategoryId()).isEqualTo(30);
	}
	
	@Test
	public void test_classjob_category() {
		ClassJobCategory classJobCat = this.api.getClassJobCategory(1);
		
		assertThat(classJobCat.getIncludedClassJobs()).contains("BRD");
	}
}
