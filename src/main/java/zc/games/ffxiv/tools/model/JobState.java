package zc.games.ffxiv.tools.model;

import lombok.Value;

@Value
public class JobState {
	
	private Job job;

	private int level;
	
	public static JobState notLearned(Job job) {
		return new JobState(job, 0);
	}
}
