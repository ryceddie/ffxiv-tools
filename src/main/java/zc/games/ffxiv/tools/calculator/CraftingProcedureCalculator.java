package zc.games.ffxiv.tools.calculator;

import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import lombok.RequiredArgsConstructor;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.Recipe;

@RequiredArgsConstructor
public class CraftingProcedureCalculator {
	
	private final Function<String, Recipe> recipeResolver;
	
	List<CraftingStep> calculate(Bom bom) {
		List<CraftingAction> actions = toActions(bom);
		return this.calculate(actions);
	}
	
	List<CraftingStep> calculate(Collection<CraftingAction> actions) {
		Crafting crafter = new Crafting(actions);
		crafter.perform();
		return ImmutableList.copyOf(crafter.steps);
	}

	private List<CraftingAction> toActions(Bom bom) {
		List<CraftingAction> actions = bom.getMaterials().stream()
				.map(m -> {
					Recipe recipe = this.recipeResolver.apply(m.getItemId());
					return CraftingAction.builder()
							.product(m)
							.recipe(recipe)
							.dependencies(new HashSet<>())
							.dependents(new HashSet<>())
							.build();
				})
				.collect(Collectors.toList());
		Function<String, CraftingAction> itemId2Action = itemId -> {
			return actions.stream()
					.filter(action -> action.getProduct().getItemId().equals(itemId))
					.findAny().orElse(null);
		};
		actions.forEach(action -> {
			List<CraftingAction> dependencies = action.getRecipe().getBom().stream()
					.map(material -> itemId2Action.apply(material.getItemId()))
					.filter(dep -> dep != null) // drop materials those are ready for use.
					.collect(Collectors.toList());
			action.getDependencies().addAll(dependencies);
			dependencies.forEach(dependency -> dependency.getDependents().add(action));
		});
		return actions;
	}
	
	static class Crafting {
		
		private Collection<CraftingAction> actions;
		
		private Set<CraftingAction> unperformed;
		
		private Set<CraftingAction> performed = new LinkedHashSet<>();
		
		private Deque<CraftingAction> performStack = new LinkedList<>();
		
		private Deque<CraftingStep> stepStack = new LinkedList<>();
		
		private Set<CraftingStep> steps = new LinkedHashSet<>();
		
		Crafting(Collection<CraftingAction> actions) {
			this.actions = actions;
			this.unperformed = new HashSet<>(actions);
		}
		
		void perform() {
			List<CraftingAction> finalActions = this.actions.stream()
					.filter(action -> action.getDependents().isEmpty())
					.collect(Collectors.toList());
			finalActions.forEach(this::perform);
		}

		private void perform(CraftingAction action) {
			if (this.isPerformed(action)) {
				return;
			}
			if (this.isPerforming(action)) {
				throw new IllegalStateException("cyclic dependency found");
			}
			if (!this.canPerform(action)) {
				return;
			}
			this.performStack.push(action);
			
			try {
				// try get step from existing ones
				CraftingStep step = Optional.ofNullable(this.stepStack.peek())
						.filter(currentStep -> currentStep.getJob().equals(action.getJob()))
						.orElse(null);
				boolean startNewStep = step == null;
				if (startNewStep) {
					step = new CraftingStep(action.getJob());
					this.stepStack.push(step);
				}
				
				action.branchDependencies().forEach(this::perform);
				action.trunkDependencies().forEach(this::perform);
				// final check
				if (action.getDependencies().stream().allMatch(this::isPerformed)) {
					// all the green, let's do the action.
					this.unperformed.remove(action);
					this.performed.add(action);
					step.add(action);					
				}
				
				
				if (startNewStep) {					
					this.unperformed.stream()
					.filter(candidate -> candidate.getJob().equals(action.getJob()))
					.filter(actionInTrunk -> !this.isPerforming(actionInTrunk))
					.collect(Collectors.toList())//XXX required operation which is not obvious
					.forEach(this::perform);
					
					if (!step.getActions().isEmpty()) {
						this.steps.add(step);
					}
					this.stepStack.pop();
				}
			} finally {
				this.performStack.pop();
			}
		}

		boolean canPerform(CraftingAction action) {
			CraftingStep step = Optional.ofNullable(this.stepStack.peek())
					.filter(currentStep -> currentStep.getJob().equals(action.getJob()))
					.orElse(null);
			return action.getDependencies().stream()
					.allMatch(dep -> !this.isPerformed(dep) 
							|| step != null && step.getActions().contains(dep)
							|| this.stepStack.stream().allMatch(stepInStack -> !stepInStack.getActions().contains(dep)));
		}
		
		boolean isPerformed(CraftingAction action) {
			return this.performed.contains(action);
		}
		
		boolean isPerforming(CraftingAction action) {
			return this.performStack.contains(action);
		}
	}
}
