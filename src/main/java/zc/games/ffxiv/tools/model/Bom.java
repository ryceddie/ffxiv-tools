package zc.games.ffxiv.tools.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

import lombok.Value;

@Value
public class Bom implements Iterable<Material> {
	
	public static final Bom EMPTY = new Bom(Collections.emptyList());

	private List<Material> materials;
	
	@JsonIgnore
	private transient Map<String, Material> itemIndex;
	
	@JsonCreator
	public Bom(Iterable<Material> materials) {
		this.materials = ImmutableList.copyOf(normalize(materials));
		this.itemIndex = Maps.uniqueIndex(this.materials, Material::getItemId);
		this.validate();
	}
	
	private Bom(Map<String, Material> itemIndex) {
		this.itemIndex = ImmutableMap.copyOf(itemIndex);
		this.materials = ImmutableList.copyOf(itemIndex.values());
	}
	
	private void validate() {
		//XXX consider add sign check for all materials' count
	}

	private static Iterable<Material> normalize(Iterable<Material> materials) {
		return Multimaps.index(materials, Material::getItemId)
				.asMap().values().stream()
				.map(c -> c.stream().reduce((m1, m2) -> m1.plus(m2)).get())
				.collect(Collectors.toList());
	}

	public Bom add(Material material) {
		return this.plus(Collections.singleton(material));
	}

	public Optional<Material> get(String itemId) {
		return Optional.ofNullable(this.itemIndex.get(itemId));
	}
	
	public int getCount(String itemId) {
		return this.get(itemId).map(Material::getCount).orElse(0);
	}

	public boolean contains(Iterable<Material> materials) {
		return ImmutableList.copyOf(materials).stream()
				.allMatch(this::contains);
	}
	
	public boolean contains(Material material) {
		return this.get(material.getItemId())
				.map(m -> m.getCount() >= material.getCount())
				.orElse(false);
	}

	public Bom plus(Iterable<Material> addition) {
		return new Bom(Iterables.concat(this.materials, addition));
	}

	public Bom minus(Iterable<Material> reduction) {
		return this.plus(ImmutableList.copyOf(reduction).stream()
				.map(Material::negate)
				.collect(Collectors.toList()));
	}

	public Bom multiply(int multiplier) {
		List<Material> newMaterials = materials.stream()
				.map(m -> m.multiply(multiplier))
				.collect(Collectors.toList());
		return new Bom(newMaterials);
	}

	@Override
	public Iterator<Material> iterator() {
		return this.materials.iterator();
	}
	
	public List<Material> toList() {
		return this.materials;
	}
	
	public MutableBom toMutableBom() {
		return new MutableBom(this.itemIndex);
	}
	
	public String toString() {
		String bomRepr = this.materials.stream()
				.map(e -> String.format("- %s", e))
				.collect(Collectors.joining("\n"));
		return String.format("BOM:%n%s", bomRepr);
	}
	
	public static class MutableBom {
		
		private Map<String, Material> itemIndex;
		
		public MutableBom(Map<String, Material> itemIndex) {
			this.itemIndex = Maps.newLinkedHashMap(itemIndex);
		}
		
		public Material add(Material material) {
			Material contained = this.itemIndex.get(material.getItemId());
			if (contained == null) {
				contained = material.withZeroCount();
			}
			Material result = contained.plus(material);
			if (result.getCount() > 0) {
				this.itemIndex.put(material.getItemId(), result);
			}
			return result;
		}

		/**
		 * Remove specified material from this BOM as many as possible.
		 * 
		 * @param material
		 * @return Material can not be removed from this BOM.
		 */
		public Material remove(Material material) {
			Material contained = this.itemIndex.get(material.getItemId());
			if (contained == null) {
				return material;
			}
			if (contained.getCount() <= material.getCount()) {
				this.itemIndex.remove(material.getItemId());
				return material.minus(contained);
			}
			this.itemIndex.put(material.getItemId(), contained.minus(material));
			return material.withZeroCount();
		}
		
		public Bom fixed() {
			return new Bom(this.itemIndex);
		}
	}
}
