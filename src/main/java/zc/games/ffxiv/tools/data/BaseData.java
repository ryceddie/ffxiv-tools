package zc.games.ffxiv.tools.data;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import lombok.AllArgsConstructor;
import lombok.Data;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Item.Category;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.Recipe;
import zc.games.ffxiv.tools.model.Recipe.NotebookCategory;

public class BaseData {
	
	private Map<String, Job> jobIdIndex;
	private Map<String, Job> jobAbbrIndex;
	
	private Map<Integer, Job.Category> jobCategoryIndex;
	
	private Map<String, Item.Category> itemCategoryIndex;
	
	private Map<Integer, Recipe.Spec> recipeSpecIndex;
	
	private Collection<Recipe.NotebookCategory> craftingNotebookCategories;
	
	public BaseData(List<Job> jobs, List<Job.Category> jobCategories, List<Item.Category> itemCategories,
			Collection<Recipe.Spec> recipeSpecs,
			Collection<Recipe.NotebookCategory> craftingNotebookCategories) {
		this.jobIdIndex = jobs.stream()
				.collect(Collectors.toMap(Job::getId, job -> job));
		this.jobAbbrIndex = jobs.stream()
				.collect(Collectors.toMap(Job::getEnglishAbbr, job -> job));
		this.jobCategoryIndex = jobCategories.stream()
				.collect(Collectors.toMap(Job.Category::getCode, val -> val));
		this.itemCategoryIndex = itemCategories.stream()
				.collect(Collectors.toMap(Item.Category::getCode, c -> c));
		this.recipeSpecIndex = recipeSpecs.stream()
				.collect(Collectors.toMap(Recipe.Spec::getId, spec -> spec));
		this.craftingNotebookCategories = ImmutableList.copyOf(craftingNotebookCategories);
	}
	
	@Deprecated
	public static BaseData parseLatest() {
		return parse("5.x");
	}
	
	@Deprecated
	public static BaseData parse(String version) {
		if (version.startsWith("4.")) {
			return new BaseDataParser().parse().compose();
		}
		if (version.startsWith("5.")) {
			return new BaseDataParserV5()
					.parse()
					.withItemCategories(new BaseDataParser().parse().getItemCategories())
					.compose();
		}
		throw new DataException("no data source for version: " + version);
	}

	@Deprecated
	public static BaseData parse() {
		return new BaseDataParser().parse().compose();
	}

	public Job getJobById(String id) {
		return Optional.ofNullable(this.jobIdIndex.get(id))
				.orElseThrow(() -> new IllegalArgumentException(String.format("job with id '%s' is not found", id)));
	}
	
	public Job getJobByAbbr(String abbr) {
		Preconditions.checkNotNull(abbr, "abbr can not be null");
		return Optional.ofNullable(this.jobAbbrIndex.get(abbr))
				.orElseThrow(() -> new IllegalArgumentException(String.format("job with abbr '%s' is not found", abbr)));
	}
	
	public Item.Category getItemCategory(String code) {
		Preconditions.checkArgument(this.itemCategoryIndex.containsKey(code), 
				String.format("item category %s is not found", code));
		return this.itemCategoryIndex.get(code);
	}

	public Job.Category getJobCategory(int jobCategory) {
		Preconditions.checkArgument(this.jobCategoryIndex.containsKey(jobCategory), 
				String.format("job category %s is not found", jobCategory));
		return this.jobCategoryIndex.get(jobCategory);
	}

	public Recipe.Spec getRecipeSpec(int specId) {
		return Optional.ofNullable(this.recipeSpecIndex.get(specId))
				.orElseThrow(() -> new IllegalArgumentException("recipe spec is not found: id=" + specId));
	}

	public Recipe.NotebookCategory getCraftingNotebookCategory(int notebookId) {
		return findNotebookCategory(notebookId)
				.orElseThrow(() -> new IllegalArgumentException("failed to find category for notebook id: " + notebookId));
	}

	public Optional<NotebookCategory> findNotebookCategory(int notebookId) {
		return this.craftingNotebookCategories.stream()
				.filter(cat -> cat.includes(notebookId))
				.findAny();
	}
	
	@Data
	@AllArgsConstructor
	public static class Ingredients {
		List<Job> jobs;
		List<Job.Category> jobCategories;
		List<Item.Category> itemCategories;
		Collection<Recipe.Spec> recipeSpecs;
		Collection<Recipe.NotebookCategory> craftingNotebookCategories;
		
		public BaseData compose() {
			return new BaseData(jobs, jobCategories, itemCategories, recipeSpecs, craftingNotebookCategories);
		}

		public Ingredients withItemCategories(List<Category> itemCategories) {
			this.itemCategories = ImmutableList.copyOf(itemCategories);
			return this;
		}
	}
}
