package zc.games.ffxiv.tools.data;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.Value;
import zc.games.ffxiv.tools.model.ConcreteItem;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.InventoryCell;
import zc.games.ffxiv.tools.model.Item;

@Value
public class PlayerCharacterInventories {

	private String characterKey;
	
	private List<Inventory> listing;
	
	public List<Inventory> getOwns() {
		return this.listing.stream()
				.filter(inv -> inv.getType().isPlayers())
				.collect(Collectors.toList());
	}
	
	public List<Inventory> getRetainers() {
		return this.listing.stream()
				.filter(inv -> inv.getType().isRetainers())
				.collect(Collectors.toList());
	}
	
	public List<Inventory> getChocobos() {
		return this.listing.stream()
				.filter(inv -> inv.getType().isChocobos())
				.collect(Collectors.toList());
	}
	
	public Map<GearSlot, ConcreteItem> getCurrentEquipment() {
		return this.listing.stream()
				.filter(inv -> inv.getType().equals(Inventory.Type.EQUIPMENT))
				.flatMap(inv -> inv.getCells().stream())
				.collect(Collectors.toMap(
						cell -> GearSlot.VALUES[cell.getIndex()], 
						cell -> new ConcreteItem(cell.getItem(), cell.isHq())));
	}
	
	public Collection<Item> soulCrystals() {
		List<Item> result = this.listing.stream()
				.filter(inv -> inv.getCode().equals("AC_SOULS"))
				.flatMap(inv -> inv.getCells().stream())
				.map(InventoryCell::getItem)
				.collect(Collectors.toList());
		ConcreteItem equipped = this.getCurrentEquipment().get(GearSlot.SOUL);
		if (equipped != null) {
			result.add(equipped.getSpec());
		}
		
		return result;
	}
}
