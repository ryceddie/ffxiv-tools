package zc.games.ffxiv.tools.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import zc.games.ffxiv.tools.model.PlayerCharacter.JobStates;

public class PlayerCharacterTest {

	@Test
	public void job_unpresent_is_level_0() {
		Job job = Job.builder().id("1").build();
		PlayerCharacter pc = PlayerCharacter.builder()
				.currentJob(job)
				.build();
		
		assertThat(pc.getLevel(job)).isZero();
	}
	
	@Test
	public void derived_job_shares_level_with_base_class() {
		Job clazz = Job.builder().id("1").build();
		Job job = Job.builder().id("2").parent(clazz).build();
		PlayerCharacter pc = PlayerCharacter.builder()
				.currentJob(job)
				.jobStates(new JobStates(Arrays.asList(new JobState(clazz, 10)), Collections.emptyList()))
				.build();
		
		assertThat(pc.getLevel(job)).isEqualTo(10);
	}
}
