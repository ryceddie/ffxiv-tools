package zc.games.ffxiv.tools.data;

import java.util.Collections;
import java.util.Map;

import lombok.Builder;
import lombok.Value;
import zc.games.ffxiv.tools.model.Job;

@Value
@Builder
public class PlayerCharacterState {
	
	private String name;
	
	private Job currentJob;

	private Map<String, Integer> jobInfo;
	
	public static class PlayerCharacterStateBuilder {
		
		private Map<String, Integer> jobInfo = Collections.emptyMap();
	}
}
