package zc.games.ffxiv.tools.web;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.web.CraftingController.CraftingCalculationRequest;

@RunWith(SpringRunner.class)
@JsonTest
public class CraftingCalculationRequestTest {
	
	@Autowired
	private JacksonTester<CraftingCalculationRequest> jackson;

	@Test
	public void test() throws IOException {
		String jsonString = this.jackson.write(CraftingCalculationRequest.builder()
				.craft(Material.builder()
						.itemId("1")
						.itemName("mock item")
						.count(1)
						.build())
				.inventoryDisabled(false)
				.build()).getJson();
		this.jackson.parse(jsonString);
	}
}
