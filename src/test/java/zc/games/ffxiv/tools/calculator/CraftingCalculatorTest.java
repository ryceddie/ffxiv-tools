package zc.games.ffxiv.tools.calculator;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.ItemParser;
import zc.games.ffxiv.tools.data.RecipeParserV5;
import zc.games.ffxiv.tools.data.init.BaseDataInitialization;
import zc.games.ffxiv.tools.data.repository.FreeCompanyRecipeRepository;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.repository.RecipeRepository;
import zc.games.ffxiv.tools.data.service.ItemRecipeResolver;
import zc.games.ffxiv.tools.view.CraftingChecklistPrinter;

@Slf4j
public class CraftingCalculatorTest {
	
	private CraftingCalculator calc;
	
	private BomBuilderFactory bomFactory;
	
	private ItemRepository items;
	
	private CraftingChecklistPrinter checklistPrinter;
	
	@Before
	public void setup() {
		BaseDataInitialization.Config config = new BaseDataInitialization.Config("src/test/resources/xivapi");
		BaseData baseData = new BaseDataInitialization().init(config);
		this.items = new ItemRepository(
				new ItemParser(baseData).parseLatest());
		RecipeParserV5 recipeParser = new RecipeParserV5(baseData, 
				itemId -> this.items.getById(itemId).getName());
		RecipeRepository recipes = new RecipeRepository(recipeParser.parse(), this.items);
		this.calc = new CraftingCalculator(new ItemRecipeResolver(recipes, new FreeCompanyRecipeRepository()), recipes);
		this.bomFactory = new BomBuilderFactory(this.items);
		this.checklistPrinter = new CraftingChecklistPrinter(this.items);
	}

	@Test
	public void test1() {
		CraftingChecklist checklist = calc.calculate(this.bomFactory.newBom()
				.add("冬意毛衣", 2)
				.build(), this.bomFactory.newBom()
				.add("精纺毛纱", 12)
				.add("草原哔叽", 3)
				.build());
		log.info("{}", checklistPrinter.print(checklist));
	}
	
	@Test
	public void test2() {
		CraftingChecklist checklist = calc.calculate(this.bomFactory.newBom()
				.add("巨龙革游击腰带", 1)
				.build(), this.bomFactory.newBom()
				.add("蛋白石", 0)
				.add("硬银锭", 39)
				.add("白钛锭", 45)
				.add("祝圣栗木木材", 70)
				.add("巨龙革", 63)
				.add("1级巧力炼金溶剂", 1)
				.add("露线", 24)
				.add("棉线", 99)
				
				.add("暗栗木原木", 25)
				.add("榴弹怪的灰", 68)
				.add("双足飞龙的翼膜", 28)
				.add("冥鬼的翅膀", 7)
				.add("长颈驼的唾液", 25)
				.add("深瞳之泪", 94)
				.add("拉诺西亚岩盐", 37)
				.add("长颈驼的粗皮", 77)
				.add("灵银矿", 10)
				.add("灵银沙", 5)
				.add("秘银矿", 31)
				.add("铁矿", 99)
				.add("蛋白石原石", 39)
				.add("白云母", 63)
				.add("茴香", 38)
				.add("绵羊毛", 95)
				.build());
		log.info("{}", checklistPrinter.print(checklist));
	}
	
	@Test
	public void crafting_with_unacquirable_item() {
		CraftingCalculator.CraftingContext context = CraftingCalculator.CraftingContext.builder()
				.inventoryMaterials(this.bomFactory.newBom()
						.add("精纺毛纱", 5)
						.add("草原哔叽", 3)
						.add("曼殊师里的毛发", 2)
						.build())
				.unacquirableItems(Arrays.asList("曼殊师里的毛发").stream()
						.map(name -> this.items.findByName(name).get().getId())
						.collect(Collectors.toList()))
				.build();
		
		CraftingChecklist checklist = this.calc.calculate(this.bomFactory.newBom()
				.add("冬意毛衣", 2)
				.build(), context);
		log.info("{}", checklistPrinter.print(checklist));
	}
	
	@Test
	public void crafting_cycle_dependency_resolution() {
		CraftingCalculator.CraftingContext context = CraftingCalculator.CraftingContext.builder()
				.inventoryMaterials(this.bomFactory.newBom()
						.build())
				.build();
		
		CraftingChecklist checklist = this.calc.calculate(this.bomFactory.newBom()
				.add("灵银强袭头冠", 1)
				.add("古鸟革强袭手套", 1)
				.add("虹布强袭裙裤", 1)
				.add("古鸟革强袭腰带", 1)
				.add("虹布强袭外套", 1)
				.add("古鸟革强袭长靴", 1)
				.build(), context);
		log.info("{}", checklistPrinter.print(checklist));
	}
	
	@Test
	public void test_v5_creating() {
		CraftingCalculator.CraftingContext context = CraftingCalculator.CraftingContext.builder()
				.inventoryMaterials(this.bomFactory.newBom()
						.build())
				.build();
		CraftingChecklist checklist = this.calc.calculate(this.bomFactory.newBom()
				.add("油画布的修复素材", 6)
				.build(), context);
		log.info("*** v5 crafting ***");
		log.info("{}", checklistPrinter.print(checklist));
	}
}
