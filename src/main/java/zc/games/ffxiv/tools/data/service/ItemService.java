package zc.games.ffxiv.tools.data.service;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.Ordering;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.repository.RecipeRepository;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.util.BasicUtils;

@AllArgsConstructor
public class ItemService {
	
	private static final Pattern ID_PATTERN = Pattern.compile("\\d+");
	
	private ItemRepository items;
	
	private RecipeRepository recipes;

	public Iterable<Item> search(String key) {
		if (ID_PATTERN.matcher(key).matches()) {
			return BasicUtils.asSet(this.items.findById(key));
		}
		if (key.length() < 2) {
			throw new IllegalArgumentException("search key too short: " + key);
		}
		return FluentIterable.from(this.items.getAll())
				.filter(item -> item.getName().contains(key));
	}
	
	public Iterable<Item> search(String key, SearchCriteria criteria) {
		if (ID_PATTERN.matcher(key).matches()) {
			return BasicUtils.asSet(this.items.findById(key));
		}
		if (key.length() < 2) {
			throw new IllegalArgumentException("search key too short: " + key);
		}
		FluentIterable<Item> result = FluentIterable.from(this.items.getAll())
				.filter(item -> item.getName().contains(key));
		if (criteria.craftableOnly) {
			result.filter(Item::isCraftable);
		}
		return result;
	}

	public List<Item> findGlamourItems(GearSlot slot, SearchCriteria criteria) {
		List<Item> result = this.items.getGlamourItems(slot);
		if (criteria.isCraftableOnly()) {
			result = result.stream()
					.filter(Item::isCraftable)
					.collect(Collectors.toList());
			result = Ordering.natural()
					.onResultOf((Item item) -> recipes.byItem(item.getId()).getSpec().getLevel())
					.sortedCopy(result);
		}
		return result;
	}
	
	@Value
	@Builder
	public static class SearchCriteria {
		
		private boolean craftableOnly;
		
	}
}
