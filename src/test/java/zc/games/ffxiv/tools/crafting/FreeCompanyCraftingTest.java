package zc.games.ffxiv.tools.crafting;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import com.siebre.test.TestHelper;

import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.FreeCompanyRecipe;
import zc.games.ffxiv.tools.model.Material;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@AutoConfigureWebClient
public class FreeCompanyCraftingTest {
	
	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	@Autowired
	private ObjectMapper jsonMapper;
	
	private TestHelper testHelper = new TestHelper(getClass());

	private RestOperations restOps;
	
	private ItemRepository items;
	
	@Before
	public void setup() {
		this.restOps = this.restTemplateBuilder.build();
		this.items = new ItemRepository(BaseData.parse());
	}
	
	@Test
	public void placeholder() {
		
	}
	
//	@Test
	public void fetch_fc_recipes() throws IOException {
		Set<String> categoryScope = ImmutableSet.of(
				"66",
				"90", "91", "92", "93",
				"101", "102", "103", "104");
		List<FreeCompanyRecipe> fcRecipes = FluentIterable.from(items.getAll())
				.filter(item -> categoryScope.contains(item.getCategory().getCode()))
				.transform(item -> this.fetch_recipe(item.getId()))
				.toList();
		
		this.jsonMapper.writeValue(testHelper.getOutputFile("fcrecipies.json"), fcRecipes);
	}

	public FreeCompanyRecipe fetch_recipe(String itemId) {
//		String itemId = "15952";
		String url = String.format("http://www.garlandtools.org/db/doc/item/en/3/%s.json", itemId);
		RequestEntity<?> requestEntity = RequestEntity.get(URI.create(url))
				.header(HttpHeaders.HOST, "www.garlandtools.org")
				.header(HttpHeaders.REFERER, "https://www.garlandtools.org/db/")
				.header(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
				.build();
		GarlandToolsItemData response = this.restOps.exchange(requestEntity, GarlandToolsItemData.class).getBody();
		
		log.info("ingredients: {}", response.item.craft.get(0).getIngredients().size());
		
		Craft craft = response.item.craft.get(0);
		
		FreeCompanyRecipe recipe = FreeCompanyRecipe.builder()
				.id(craft.getId())
				.product(Material.builder()
						.itemId(String.valueOf(response.item.getId()))
						.itemName(items.findById(String.valueOf(response.item.getId())).get().getName())
						.count(1)
						.build())
				.bom(new Bom(craft.getIngredients().stream()
						.map(it -> Material.builder()
								.itemId(String.valueOf(it.getId()))
								.itemName(items.findById(String.valueOf(it.getId())).get().getName())
								.count(it.getAmount())
								.build())
						.collect(Collectors.toList())))
				.build();
		return recipe;
	}
	
	@Value
	private static class GarlandToolsItemData {
		
		private GarlandToolsItem item;

		@JsonCreator
		GarlandToolsItemData(GarlandToolsItem item) {
			this.item = item;
		}
	}
	
	@Value
	@Builder
	private static class FCRecipe {
		
		private String id;
		
		private int productItemId;
		
		private List<Material> bom;
	}
	
	@Value
	private static class GarlandToolsItem {
		
		private int id;
		
		private String name;
		
		private List<Craft> craft;
	}
	
	@Value
	private static class Craft {
		
		private String id;
		
		private List<Ingredient> ingredients;
	}
	
	@Value
	private static class Ingredient {
		
		private int id;
		
		private int amount;
		
		private String part;
		
		private int phase;
		
		private String stepid;
	}
}
