package zc.games.ffxiv.tools.data;

import java.util.function.Predicate;

import lombok.NonNull;
import lombok.Value;

@Value
public class ActorState {
	
	@NonNull
	private String ownerKey;

	@NonNull
	private String name;
	
	@NonNull
	private Type type;
	
	public Key getKey() {
		return new Key(ownerKey, type, name);
	}
	
	public enum Type {
		MONSTER,
		MINION,
		NPC,
		PC,
		RETAINER
	}

	@Value
	public static class Key {
		
		private String ownerKey;
		
		private Type type;

		private String name;

		public static Predicate<String> matcherFor(String ownerKey, Type type) {
			return key -> key.startsWith(String.format("%s-%s-", ownerKey, type.name().toLowerCase()));
		}
		
		public String toString() {
			return String.format("%s-%s-%s", this.ownerKey, this.type.name().toLowerCase(), this.name);
		}
	}
}
