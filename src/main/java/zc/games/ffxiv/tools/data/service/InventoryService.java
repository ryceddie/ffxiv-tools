package zc.games.ffxiv.tools.data.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.InventoryCell;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.model.PlayerCharacter;

@AllArgsConstructor
public class InventoryService {
	
	private PlayerCharacterService pcService;

	public List<Material> getAll() {
		return this.getAll(Collections.emptyList());
	}
	
	public List<Material> getAll(Iterable<? extends StockFilter> filters) {
		StockFilter filter = new AndStockFilter(filters);
		return this.pcService.getAll().stream()
				.filter(filter::filterPlayerCharacter)
				.flatMap(pc -> Lists.newArrayList(pc.allInventories()).stream())
				.filter(filter::filterInventory)
				.flatMap(inv -> inv.getCells().stream())
				.filter(filter::filterCell)
				.map(cell -> {
					return Material.builder()
							.itemId(cell.getItem().getId())
							.count(cell.getQuantity())
							.itemName(cell.getItem().getName())
							.build();
				})
				.collect(Collectors.toList());
	}

	public Collection<ItemStockInfo> locateItems(Collection<String> itemIds, Optional<String> pcKey) {
		List<PlayerCharacter> playerCharacters = pcKey
				.map(this.pcService::getPlayerCharacterByKey)
				.map(Collections::singletonList)
				.orElseGet(this.pcService::getAll);
		return playerCharacters.stream()
				.flatMap(pc -> {
					Stream<InventoryWrapper> selfs = pc.getInventories().stream()
							.map(inv -> new InventoryWrapper(inv, pc.getName(), pc.getName()));
					Stream<InventoryWrapper> retainers = pc.getRetainers().stream()
							.flatMap(retainer -> {
								return retainer.getInventories().stream()
										.map(inv -> new InventoryWrapper(inv, retainer.getName(), pc.getName()));
							});
					Stream<InventoryWrapper> chocobos = pc.chocobo()
							.map(Collections::singletonList)
							.orElse(Collections.emptyList())
							.stream()
							.flatMap(chocobo -> {
								return chocobo.getInventories().stream()
								.map(inv -> new InventoryWrapper(inv, chocobo.getName(), pc.getName()));
							});
					
					return Stream.of(selfs, retainers, chocobos)
							.reduce(Stream::concat).get();
				})
				.flatMap(inventory -> {
					return itemIds.stream()
							.flatMap(itemId -> {
								return inventory.search(itemId).stream();
							});
				})
				.collect(Collectors.toList());
	}
	
	public interface StockFilter {

		boolean filterPlayerCharacter(PlayerCharacter pc);
		boolean filterInventory(Inventory inventory);
		boolean filterCell(InventoryCell cell);
	}
	
	public static class StockFilterSupport implements StockFilter {

		@Override
		public boolean filterPlayerCharacter(PlayerCharacter pc) {
			return true;
		}

		@Override
		public boolean filterInventory(Inventory inventory) {
			return true;
		}

		@Override
		public boolean filterCell(InventoryCell cell) {
			return true;
		}
	}
	
	private static class AndStockFilter implements StockFilter {
		
		private List<StockFilter> delegates;

		public AndStockFilter(Iterable<? extends StockFilter> filters) {
			this.delegates = ImmutableList.copyOf(filters);
		}

		@Override
		public boolean filterPlayerCharacter(PlayerCharacter pc) {
			return this.delegates.stream()
					.allMatch(filter -> filter.filterPlayerCharacter(pc));
		}

		@Override
		public boolean filterInventory(Inventory inventory) {
			return this.delegates.stream()
					.allMatch(filter -> filter.filterInventory(inventory));
		}

		@Override
		public boolean filterCell(InventoryCell cell) {
			return this.delegates.stream()
					.allMatch(filter -> filter.filterCell(cell));
		}
		
	}
	
	public enum StockExclusion {
		ALL {
			@Override
			public StockFilter toFilter() {
				return new StockFilter() {

					@Override
					public boolean filterPlayerCharacter(PlayerCharacter pc) {
						return false;
					}

					@Override
					public boolean filterInventory(Inventory inventory) {
						return false;
					}

					@Override
					public boolean filterCell(InventoryCell cell) {
						return false;
					}
				};
			}
		},
		EQUIPMENT_INVENTORY {
			@Override
			public StockFilter toFilter() {
				return new StockFilterSupport() {

					@Override
					public boolean filterInventory(Inventory inventory) {
						return !inventory.getType().equals(Inventory.Type.EQUIPMENT);
					}
				};
			}
		},
		ARMS_INVENTORY {
			@Override
			public StockFilter toFilter() {
				return new StockFilterSupport() {

					@Override
					public boolean filterInventory(Inventory inventory) {
						return !inventory.getType().equals(Inventory.Type.ARMOR);
					}
				};
			}
		},
		HIGH_QUALITY {
			@Override
			public StockFilter toFilter() {
				return new StockFilterSupport() {

					@Override
					public boolean filterCell(InventoryCell cell) {
						return !cell.isHq();
					}
				};
			}
		};
		
		public abstract StockFilter toFilter();
	}
	
	@Value
	@Builder
	public static class ItemStockInfo {
		
		private String itemId;
		
		private boolean highQuality;
		
		private InventoryLocation location;
		
		private int quantity;
	}
	
	@Value
	@Builder
	public static class InventoryLocation {
		
		private String playerCharacterKey;
		
		private String inventoryOwnerName;
		
		private Inventory.Type inventoryType;
		
		private int cellIndex;		
	}

	@Value
	private static class InventoryWrapper {
		
		private Inventory inventory;
		
		private String ownerName;
		
		private String pcKey;

		Collection<ItemStockInfo> search(String itemId) {
			return this.inventory.search(itemId).stream()
					.map(cell -> ItemStockInfo.builder()
							.itemId(itemId)
							.quantity(cell.getQuantity())
							.highQuality(cell.isHq())
							.location(InventoryLocation.builder()
									.playerCharacterKey(this.pcKey)
									.inventoryType(this.inventory.getType())
									.inventoryOwnerName(this.ownerName)
									.cellIndex(cell.getIndex())
									.build())
							.build())
					.collect(Collectors.toList());
		}
	}
}
