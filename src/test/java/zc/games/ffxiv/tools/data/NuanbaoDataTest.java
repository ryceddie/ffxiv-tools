package zc.games.ffxiv.tools.data;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import com.siebre.test.TestHelper;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import zc.games.ffxiv.tools.data.DataSpecs;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.model.Recipe;

@Slf4j
public class NuanbaoDataTest {
	
	private OkHttpClient client = new OkHttpClient.Builder().build();
	
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	private TestHelper testHelper = new TestHelper(getClass());

//	@Test
	public void download_js() throws IOException {
		String[] names = {
				"staticData.exh.js",
				"Start_Data.js",
				"Start_Draw.js",
				"Start_Get.js",
				"sources.js",
				"sources_info_ja.js",
				"Start.js"};
		for (String name : names) {
			Request request = new Request.Builder()
					.url("http://v6.ffxiv.xin/data/" + name)
					.build();
			Response response = client.newCall(request).execute();
			Files.asByteSink(
					testHelper.getOutputFile(name))
			.write(
					response.body().bytes());
			//		String content = new String(response.body().bytes(), Charsets.UTF_8);
			//		log.info(content);
		}
	}
	
//	@Test
	public void download_data() throws IOException {
		Request request = new Request.Builder()
				.url("http://v6.ffxiv.xin/data.html?ver=6.19.2.19.2")
//				.header("Accept", "application/zip; charset=x-user-defined")
				.build();
		Response response = client.newCall(request).execute();
		Files.asByteSink(
				testHelper.getOutputFile("data.zip"))
		.write(response.body().bytes());
	}
	
//	@Test
	public void extract_json() throws IOException {
		String content = Resources.toString(Resources.getResource("crafting/data/sources.js"), Charsets.UTF_8);
		int index = content.indexOf('{');
		content = content.substring(index);
		Files.asCharSink(
				testHelper.getOutputFile("sources.json"), Charsets.UTF_8)
		.write(content);
	}
	
	@Test
	public void format_json() throws IOException {
		String folder = "nuanbao5/";
		String filename = "notebookCategory.json";
		
		String dataName = filename;
		int delimiterIndex = CharMatcher.anyOf("_.").indexIn(filename);;
		if (delimiterIndex != -1) {
			dataName = filename.substring(0, delimiterIndex);
		}
		if (folder.contains("nuanbao5")) {
			dataName += "v5";
		}
		JsonNode json = this.jsonMapper.readTree(
				Resources.getResource(folder + filename));
		String content = this.jsonMapper
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(json);
		
		Files.asCharSink(testHelper.getOutputFile(dataName + ".json"), Charsets.UTF_8)
		.write(content);
	}
	
	private Map<String, Item> itemIndex = Maps.newHashMap();
	
//	@Test
	public void parse() throws IOException {
		this.parse_items();
		this.parse_recipes();
	}
	
	public void parse_items() throws IOException {
		Map<String, Object> items = this.jsonMapper.readValue(Resources.getResource(DataSpecs.ITEM_INFO), Map.class);
		Map<String, Object> data = (Map<String, Object>) items.get("data");
		for (String itemId : data.keySet()) {
			Map<String, Object> itemData = (Map<String, Object>) data.get(itemId);
			String name = (String) itemData.get("cname");
			if (name.isEmpty()) {
				name = "#unknown";
			}
			String category = String.valueOf(itemData.get("uc"));
			List<String> recipeIds = (List<String>) itemData.get("rid");
			itemIndex.put(itemId, Item.builder()
					.id(itemId)
					.name(name)
					.category(new Item.Category(category, "mock-cat", Item.CategoryGroup.OTHER))
					.jobs(Collections.emptyList())
					.craftable(!recipeIds.isEmpty())
					.build());
		}
	}
	
	public void parse_recipes() throws IOException {
		Map<String, Object> recipes = this.jsonMapper.readValue(Resources.getResource(DataSpecs.RECIPE_DATA), Map.class);
		Map<String, Object> data = (Map<String, Object>) recipes.get("data");
		log.info("recipe count: {}", data.size());
		
		String recipeId = "1";
		List<Object> recipeData = (List<Object>) data.get(recipeId);
		int[] ingredientIndexes = {6, 8, 10, 12, 14, 16, 22, 24};
		List<Material> bom = Lists.newArrayList();
		for (int index : ingredientIndexes) {
			int itemNo = (int) recipeData.get(index);
			if (itemNo <= 0) {
				continue;
			}
			
			String itemId = String.valueOf(itemNo);
			String name = this.itemIndex.get(itemId).getName();
			int count = (int) recipeData.get(index + 1);
			bom.add(Material.builder()
					.itemId(itemId)
					.itemName(name)
					.count(count)
					.build());
		}
		int productNo = (int) recipeData.get(4);
		int productCount = (int) recipeData.get(5);
		String productId = String.valueOf(productNo);
		Recipe recipe = Recipe.builder()
				.product(Material.builder()
						.itemId(productId)
						.itemName(this.itemIndex.get(productId).getName())
						.count(productCount).build())
				.bom(bom)
				.build();
		log.info("{}: {}", recipeId, recipe);
	}
}
