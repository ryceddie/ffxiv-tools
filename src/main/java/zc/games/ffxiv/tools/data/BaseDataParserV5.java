package zc.games.ffxiv.tools.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;

import lombok.AllArgsConstructor;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.Job.Category;
import zc.games.ffxiv.tools.model.Recipe;
import zc.games.ffxiv.tools.model.Recipe.NotebookCategory;

/**
 * Parser for V5 raw data.
 * 
 * @author Zhang Chi
 * @since 2020-01-13
 *
 */
public class BaseDataParserV5 {
	
	private final BaseDataReader reader = new BaseDataReader();
	
	private Function<String, Integer> jobCategoryResolver;
	
	private Function<String, String> jobParentIdResolver;
	
	public BaseDataParserV5 withJobCategoryIdResolver(Function<String, Integer> jobCategoryIdResolver) {
		this.jobCategoryResolver = jobCategoryIdResolver;
		return this;
	}

	public BaseDataParserV5 withJobParentIdResolver(Function<String, String> jobParentIdResolver) {
		this.jobParentIdResolver = jobParentIdResolver;
		return this;
	}
	
	public BaseData.Ingredients parse() {
		List<Job> jobs = parseJobs();
		List<Job.Category> jobCategories = parseJobCategories(jobs);
		List<Item.Category> categories = parseItemCategories();
		Collection<Recipe.Spec> recipeSpecs = parseRecipeSpecs();
		Collection<Recipe.NotebookCategory> recipeNotebookCategories = parseRecipeNotebookCategories();
		return new BaseData.Ingredients(jobs, jobCategories, categories, recipeSpecs, recipeNotebookCategories);
	}

	private List<Job> parseJobs() {
		Map<String, Map<String, Object>> jobData = this.reader.readJobs();
		
		Stream<Job> stream = jobData.values().stream().map(this::buildJob);
		if (this.jobCategoryResolver != null) {
			stream = stream.map(job -> {
				int primaryCategoryId = this.jobCategoryResolver.apply(job.getId());
				return job.toBuilder()
						.primaryCategoryId(primaryCategoryId)
						.build();
			});
		}
		if (this.jobParentIdResolver != null) {
			Map<String, Job> indexed = stream.collect(HashMap::new, (index, job) -> {
				String parentJobId = this.jobParentIdResolver.apply(job.getId());
				job = job.toBuilder().parent(index.get(parentJobId)).build();
				index.put(job.getId(), job);
			}, HashMap::putAll);
			
			stream = indexed.values().stream();
		}
		
		return stream.collect(Collectors.toList());
	}
	
	private Job buildJob(Map<String, Object> properties) {
		Integer id = (Integer) properties.get("id");
		List<?> names = (List<?>) properties.get("lang");
		List<?> abbrs = (List<?>) properties.get("ab");
		return Job.builder()
				.id(String.valueOf(id))
				.name((String) names.get(2))
				.englishName((String) names.get(1))
				.englishAbbr((String) abbrs.get(1))
				.build();
	}
	
	private List<Category> parseJobCategories(List<Job> jobs) {
		Map<String, Map<String, Object>> data = this.reader.readJobCategories();
		JobCategoryBuilder builder = new JobCategoryBuilder(new JobCategoryDataResolver(jobs));
		return data.values().stream()
				.map(builder::buildJobCategory)
				.collect(Collectors.toList());
	}
	
	@AllArgsConstructor
	static class JobCategoryBuilder {
		
		private JobCategoryDataResolver jobCategoryDataResolver;
		
		Job.Category buildJobCategory(Map<String, Object> properties) {
			int id = (int) properties.get("id");
			List<?> names = (List<?>) properties.get("lang");
			
			String eLabel = (String) names.get(0);
			List<Job> jobs = this.jobCategoryDataResolver.resolve(id, null, eLabel);
			return Job.Category.builder()
					.code(id)
					.label((String) names.get(1))
					.englishLabel(eLabel)
					.jobs(jobs)
					.build();
		}
	}

	private List<Item.Category> parseItemCategories() {
		Map<String, Map<String, Object>> data = this.reader.readItemCategories();
		return data.values().stream()
				.map(this::buildItemCategory)
				.collect(Collectors.toList());
	}
	
	private Item.Category buildItemCategory(Map<String, Object> properties) {
		List<?> names = (List<?>) properties.get("lang");
		return new Item.Category(
				String.valueOf(properties.get("id")), 
				(String) names.get(2), 
				Item.CategoryGroup.UNKNOWN);
	}

	private Collection<NotebookCategory> parseRecipeNotebookCategories() {
		List<List<Object>> data = this.reader.readNotebookCategories();
		RecipeNotebookDataResolver recipeNotebookDataResolver = new RecipeNotebookDataResolver();
		List<NotebookCategory> specialCategory = data.stream()
				.map(values -> {
					int id = (int) values.get(1);
					Range<Integer> notebookIdRange = recipeNotebookDataResolver.resolveNotebookIdRange(id);
					return new NotebookCategory(id, (String) values.get(0), notebookIdRange);
				})
				.collect(Collectors.toList());
		return ImmutableList.<NotebookCategory>builder()
				.addAll(specialCategory)
				.add(new NotebookCategory(0, "1 - 80", Range.singleton(0)))
				.build();
	}

	private Collection<Recipe.Spec> parseRecipeSpecs() {
		Map<String, List<Integer>> data = this.reader.readRecipeSpecs();
		return data.keySet().stream()
				.map(id -> {
					List<Integer> values = data.get(id);
					return Recipe.Spec.builder()
							.id(Integer.parseInt(id))
							.level(values.get(0))
							.stars(values.get(1))
							//[2] recommended craft
							//[3] recommended control
							.difficulty(values.get(4))
							.quality(values.get(5))
							.durability(values.get(6))
							.build();
				})
				.collect(Collectors.toList());
	}
	
	private static class BaseDataReader {
		
		private JsonDataReader reader = new JsonDataReader();

		public Map<String, Map<String, Object>> readJobs() {
			String resourceName = "classJob";
			String dataDesc = "job";
			
			return this.reader.readIndexedObject(resourceName, dataDesc);
		}

		public Map<String, Map<String, Object>> readJobCategories() {
			return this.reader.readIndexedObject("classJobCategory", "job category");
		}

		public Map<String, Map<String, Object>> readItemCategories() {
			return this.reader.readIndexedObject("itemUC", "item category");
		}

		public Map<String, List<Integer>> readRecipeSpecs() {
			return this.reader.readMap("recipeLevel", "recipe spec", new TypeReference<List<Integer>>() {});
		}

		public List<List<Object>> readNotebookCategories() {
			return this.reader.readMap("notebookCategory", "notebook category", 
					new TypeReference<List<List<Object>>>() {}).get("job_note_special");
		}
	}
}
