package zc.games.ffxiv.tools.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.ImmutableMap;

import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiPlayerCharacter;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ModelConverter;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class ModelConverterTest {
	
	private ModelConverter converter;
	
	@Autowired
	private ItemRepository itemRepo;
	
	@Autowired
	private BaseData baseData;
	
	@Before
	public void setup() {
		this.converter = new ModelConverter(this.itemRepo, this.baseData);
	}

	@Test
	public void test() {
		ApiPlayerCharacter apiModel = new ApiPlayerCharacter("tinus", "BRD", ImmutableMap.of("BRD", 70));
		this.converter.toDomainModel(apiModel);
	}
	
	@Configuration
	static class Config {
		
		@Bean
		BaseData baseData() {
			return BaseData.parse();
		}
		
		@Bean
		ItemRepository itemRepo() {
			return new ItemRepository(baseData());
		}
	}
}
