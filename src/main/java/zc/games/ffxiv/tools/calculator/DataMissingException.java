package zc.games.ffxiv.tools.calculator;

public class DataMissingException extends RuntimeException {

	public DataMissingException(String message) {
		super(message);
	}
}
