package zc.games.ffxiv.tools.data.xivapi;

import java.util.Map;
import java.util.Optional;

import com.google.common.collect.Maps;

import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.xivapi.Models.ClassJob;

public class GameContent {
	
	private Map<Integer, ClassJob> classJobIndex;
	
	public GameContent(Iterable<ClassJob> classJobs) {
		this.classJobIndex = Maps.uniqueIndex(classJobs, ClassJob::getId);
	}

	public ClassJob getClassJob(int id) {
		return Optional.ofNullable(this.classJobIndex.get(id))
				.orElseThrow(() -> new DataException("can not find class-job: " + id));
	}
}
