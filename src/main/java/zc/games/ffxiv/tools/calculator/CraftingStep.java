package zc.games.ffxiv.tools.calculator;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.Bom.MutableBom;

@Value
public class CraftingStep {
	
	private String job;
	
	private List<CraftingAction> actions = new ArrayList<>();
	
	@Getter(AccessLevel.NONE)
	private MutableBom bom = Bom.EMPTY.toMutableBom();
	
	void add(CraftingAction action) {
		Preconditions.checkArgument(action.getJob().equals(this.job), 
				"can not accept action because job does not match: " + action);
		this.actions.add(action);
		this.bom.add(action.getProduct());
	}
	
//	public Bom getBom() {
//		return this.bom.fixed();
//	}
}