package zc.games.ffxiv.tools.data.repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.ItemParser;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.util.ItemUtils;

@Slf4j
public class ItemRepository {
	
	private final Map<String, Item> nameIndex;
	
	private final Map<String, Item> idIndex;
	
	private final Collection<Item> gears;
	
	public ItemRepository(Iterable<Item> items) {
		this.nameIndex = new HashMap<>();
		items.forEach(item -> {
			if (this.nameIndex.containsKey(item.getName())) {
				log.warn("item {} and {} have same name: {}, name index will ignore the latter", 
						this.nameIndex.get(item.getName()).getId(), item.getId(), item.getName());
			} else {				
				this.nameIndex.put(item.getName(), item);
			}
		});
		this.idIndex = Maps.uniqueIndex(items, Item::getId);
		this.gears = FluentIterable.from(items)
				.filter(item -> item.getCategory().isGear())
				.toList();
	}

	/**
	 * 
	 * @param baseData
	 * @deprecated use {@link #ItemRepository(Iterable)}
	 */
	@Deprecated
	public ItemRepository(BaseData baseData) {
		this(new ItemParser(baseData).parse());
	}

	public Optional<Item> findByName(String name) {
		return Optional.ofNullable(this.nameIndex.get(name));
	}
	
	public Optional<Item> findById(String id) {
		return Optional.ofNullable(this.idIndex.get(id));
	}
	
	public Item getById(String id) {
		return this.findById(id).orElseThrow(() -> new DataException("failed to get item with id: " + id));
	}
	
	public Iterable<Item> getAll() {
		return this.idIndex.values();
	}
	
	public Iterable<Item> getAllGears() {
		return this.gears;
	}

	public List<Item> getGlamourItems(GearSlot slot) {
		return this.gears.stream()
				.filter(gear -> gear.getCategory().getGearSlot().equals(slot))
				.filter(ItemUtils::isGlamourItem)
				.collect(Collectors.toList());
	}
}
