package zc.games.ffxiv.tools.data;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.model.Recipe;

/**
 * 
 * @author Zhang Chi
 * @since 2020-02-15
 *
 */
@AllArgsConstructor
public class RecipeParserV5 {
	
	private BaseData baseData;
	
	private Function<String, String> itemId2itemName;

	public Iterable<Recipe> parse() {
		Collection<Map<String, Object>> recipeData = new RecipeDataReader().read();
		return recipeData.stream()
				.map(this::buildRecipe)
				.collect(Collectors.toList());
	}
	
	private Recipe buildRecipe(Map<String, Object> properties) {
		int id = (int) properties.get("id");
		int jobId = (int) properties.get("job") + 8;
		List<Object> bp = ImmutableList.copyOf((List<?>) properties.get("bp"));
		String productId = String.valueOf(bp.get(0));
		Material product = Material.builder()
				.itemId(productId)
				.itemName(this.itemId2itemName.apply(productId))
				.count((int) bp.get(1))
				.build();
		List<?> materialsData = (List<?>) properties.get("m");
		List<?> crystalsData = (List<?>) properties.get("s");
		List<Material> bom = Flux.concat(
				Flux.fromIterable(materialsData), Flux.fromIterable(crystalsData))
				.cast(Integer.class)
				.buffer(2)
				.filter(data -> data.get(0) != -1)
				.map(data -> {
					String itemId = String.valueOf(data.get(0));
					return Material.builder()
							.itemId(itemId)
							.itemName(this.itemId2itemName.apply(itemId))
							.count(data.get(1))
							.build();			
				})
				.collectList().block();
		List<Object> sp3 = ImmutableList.copyOf((List<?>) properties.get("sp3"));
		int notebookId = (int) sp3.get(0);
		Recipe.NotebookCategory notebookCategory = this.baseData.findNotebookCategory(notebookId).orElse(null);

		return Recipe.builder()
				.id(String.valueOf(id))
				.job(this.baseData.getJobById(String.valueOf(jobId)).getName())
				.product(product)
				.spec(this.baseData.getRecipeSpec((int) properties.get("rlv")))
				//[sp1] percentage of difficulty/quality/durability
				.notebookId(notebookId)
				.notebookCategory(notebookCategory)
				.bom(bom)
				.build();
	}
	
	private static class RecipeDataReader {
		
		private JsonDataReader jsonReader = new JsonDataReader();
		
		private Collection<Map<String, Object>> read() {
			return this.jsonReader.readIndexedObject("recipe", "recipe").values();
		}
	}
}
