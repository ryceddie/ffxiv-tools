package zc.games.ffxiv.tools.data.xivapi;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import zc.games.ffxiv.tools.data.xivapi.Models.ClassJob;
import zc.games.ffxiv.tools.data.xivapi.Models.ClassJobCategory;

@Headers("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebkit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/.537.36")
public interface GameContentApi {

	@RequestLine("GET /ClassJob/{id}?columns=ID%2CClassJobCategoryTargetID%2CClassJobParentTargetID")
	ClassJob getClassJob(@Param("id") int id);
	
	@RequestLine("GET /ClassJobCategory/{id}")
	ClassJobCategory getClassJobCategory(@Param("id") int id);
}
