package zc.games.ffxiv.tools.data.init;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import zc.games.ffxiv.tools.data.BaseData;

public class BaseDataInitializationTest {

	@Test
	public void test() {
		BaseDataInitialization.Config config = new BaseDataInitialization.Config("src/test/resources/xivapi");
		BaseData baseData = new BaseDataInitialization().init(config);
		
		assertThat(baseData.getJobById("1")).isNotNull();
		assertThat(baseData.getJobById("19").getParent()).isNotNull();
		assertThat(baseData.getJobCategory(0)).isNotNull();
		
		assertThat(baseData.getRecipeSpec(1)).isNotNull();
		assertThat(baseData.getCraftingNotebookCategory(0)).isNotNull();
	}
}
