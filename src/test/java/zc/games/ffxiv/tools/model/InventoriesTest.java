package zc.games.ffxiv.tools.model;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import org.junit.Test;

public class InventoriesTest {
	
	@Test
	public void construction() {
		try {
			new Inventories(null);
			failBecauseExceptionWasNotThrown(NullPointerException.class);
		} catch (NullPointerException e) {
		}
	}
}
