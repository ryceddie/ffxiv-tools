package zc.games.ffxiv.tools.util;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public final class BasicUtils {

	public static <T> Set<T> asSet(Optional<T> optional) {
		return optional
				.map(it -> Collections.singleton(it))
				.orElse(Collections.emptySet());
	}
	
	public static <T> List<T> asList(Optional<T> optional) {
		return optional.map(it -> Collections.singletonList(it))
				.orElse(Collections.emptyList());
	}
}
