package zc.games.ffxiv.tools.model;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;
import zc.games.ffxiv.tools.data.PlayerCharacterInventories;

@Value
@ToString(of = {"name", "currentJob"})
public class PlayerCharacter {

	private String name;
	
	@NonNull
	private Job currentJob;
	
	@Getter(AccessLevel.PRIVATE)
	private JobStates jobStates;
	
	@NonNull
	private List<Inventory> inventories;
	
	private List<Retainer> retainers;
	
	private Chocobo chocobo;
	
	private PlayerCharacterInventories inventoryHelper;
	
	@Builder(toBuilder = true)
	public PlayerCharacter(String name, Job currentJob, JobStates jobStates,
			List<Inventory> inventories, List<Retainer> retainers, Chocobo chocobo) {
		this.name = name;
		this.currentJob = currentJob;
		this.jobStates = jobStates;
		this.inventories = inventories;
		this.retainers = retainers;
		this.chocobo = chocobo;
		
		this.inventoryHelper = new PlayerCharacterInventories(name, this.inventories);
	}
	
	public Collection<JobState> getJobInfo() {
		return this.jobStates.base();
	}
	
	public Collection<JobState> getAllJobStates() {
		return this.jobStates.extended();
	}

	public int getLevel(@NonNull Job job) {
		return this.jobStates.stateOf(job).getLevel();
	}
	
	public Map<GearSlot, ConcreteItem> getCurrentEquipment() {
		return this.inventoryHelper.getCurrentEquipment();
	}
	
	public Optional<Chocobo> chocobo() {
		return Optional.ofNullable(this.chocobo);
	}
	
	public Iterable<Inventory> allInventories() {
		return Iterables.concat(
				this.inventories,
				
				this.retainers.stream()
				.flatMap(retainer -> retainer.getInventories().stream())
				.collect(Collectors.toList()),
				
				this.chocobo()
				.map(Chocobo::getInventories)
				.orElse(Collections.emptyList()));
	}
	
	public Collection<Item> soulCrystals() {
		return this.inventoryHelper.soulCrystals();
	}

	public static class PlayerCharacterBuilder {
		
		private JobStates jobStates = new JobStates(Collections.emptyList(), Collections.emptyList());
		private List<Inventory> inventories = Collections.emptyList();
	}
	
	public static class JobStates {
		
		private List<JobState> baseStates;
		
		private List<JobState> extendedStates;
		
		public JobStates(List<JobState> jobStates, Collection<Job> derivedJobs) {
			Optional<Job> invalid = jobStates.stream()
					.map(JobState::getJob)
					.filter(job -> job.isDerived())
					.findAny();
			Preconditions.checkArgument(!invalid.isPresent(),
					"derived job is not allowed: " + invalid);
			this.baseStates = ImmutableList.copyOf(jobStates);
			this.extendedStates = resolveExtendedStates(this.baseStates, derivedJobs);
		}

		public List<JobState> base() {
			return this.baseStates;
		}
		
		public List<JobState> extended() {
			return this.extendedStates;
		}

		public JobState stateOf(@NonNull Job job) {
			Job jobToUse = job.parent().orElse(job);
			return this.baseStates.stream()
					.filter(state -> state.getJob().equals(jobToUse))
					.findAny()
					.orElse(JobState.notLearned(job));
		}

		private List<JobState> resolveExtendedStates(Collection<JobState> jobStates, Collection<Job> derivedJobs) {
			return jobStates.stream()
					.flatMap(baseState -> {
						List<JobState> matchingDerivedJobs = derivedJobs.stream()
								.filter(derivedJob -> baseState.getJob().equals(derivedJob.parent().get()))
								.map(derivedJob -> new JobState(derivedJob, baseState.getLevel()))
								.collect(Collectors.toList());
						if (matchingDerivedJobs.isEmpty()) {
							return Stream.of(baseState);
						}
						return matchingDerivedJobs.stream();
					})
					.collect(Collectors.toList());
		}
	}
}
