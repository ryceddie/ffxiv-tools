package zc.games.ffxiv.tools.data.repository;

import java.util.Map;
import java.util.Optional;

import com.google.common.collect.FluentIterable;

import zc.games.ffxiv.tools.data.FreeCompanyRecipeReader;
import zc.games.ffxiv.tools.model.FreeCompanyRecipe;

public class FreeCompanyRecipeRepository {
	
	private final Map<String, FreeCompanyRecipe> index;

	public FreeCompanyRecipeRepository() {
		this.index = FluentIterable.from(new FreeCompanyRecipeReader().read())
				.uniqueIndex(recipe -> recipe.getProduct().getItemId());
	}

	public Optional<FreeCompanyRecipe> findByItemId(String itemId) {
		return Optional.ofNullable(this.index.get(itemId));
	}

}
