package zc.games.ffxiv.tools.data;

public interface DataSpecs {
	
	public static final String FC_RECIPE_DATA = "game-data/garland-tools/fcrecipes.json";

	public static final String ROOT_PATH = "game-data/nuanbao";
	
	public static final String ITEM_VERSION = "2018031301";
	
	public static final String ITEM_INFO = String.format("%s/itemsInfo_%s.json", DataSpecs.ROOT_PATH, DataSpecs.ITEM_VERSION);
	
	public static final String RECIPE_VERSION = "2018013001";
	
	public static final String RECIPE_DATA = String.format("%s/recipe_%s.json", DataSpecs.ROOT_PATH, DataSpecs.RECIPE_VERSION);
}
