"use strict";
(function(){
var Bobo = {
	Start:function (){
		var F = this;
		F.loadConfig();//加载用户值
		F.DrawSeUc(); //绘制检索助手
		F.bindTree(); //绑定Tree列表
		F.setEvent(); //初始化事件
		F.DrawCalList();//绘制以添加的配方列表
		F.DrawHistory();//绘制历史
		F.initNoteBook();//绘制职业配方列表
		F.initLangs();
		$("#cal_div_loading").hide();//隐藏加载框
	},
	setEvent:function (){
		var F = this;
		//绑定职业 切换
		$(".class_job_"+F.Config.job).addClass("btn-primary");
		$(".btn-group").on("click",'button',function(e){
			$(".class_job_"+F.Config.job).removeClass("btn-primary");
			$(this).addClass("btn-primary");
			F.Config.job = this.getAttribute("arr");
			F.initNoteBook();
		});
		//绑定右侧菜单事件
		$("#left_meau").parent().parent().attr('id','left_Menu_Content');
		$("#left_Menu_Content").on("tap click",'.tager_left_meau',function(e){
			var data = JSON.parse(this.getAttribute("arr")),list = this.getAttribute("data-list");
			F.DrawItemList(data);
			$(".tager_left_meau").removeClass("active");
			$(this).addClass("active");
			if(!isNaN(list)){
				F.Config.jobSeleteId = list;
				F.saveConfig();
			}
		});
		//绑定中第二栏道具事件、debug模式不执行
		if(F.Config.lastOpen && !F.debug) F.DrawItem(F.Config.lastOpen);
		
		$('#main_div').on("tap click",'.tager_item_list',function (e){
			var id = this.getAttribute("arr"),TargetClass = e.target.classList;
			if(TargetClass.contains('lis_f')){
					F.DoBackCheck(id);
			}else if(TargetClass.contains('lis_add')){
					F.DoAddToMap(id);
			}
			if(F.ConfigData.ItemSeleteId && F.ConfigData.ItemSeleteId == this){
				return ;
			}else if(id){
				$(this).addClass("active");
				if(F.ConfigData.ItemSeleteId){
					$(F.ConfigData.ItemSeleteId).removeClass("active");
				}
				F.Config.lastOpen = id;
				F.ConfigData.ItemSeleteId = this;
				F.DrawItem(id);
				F.saveConfig(true);
			}
		});
		
		$(".my_icon").removeAttr('onclick').on('tap click',function (){
			F.DoAddToMap($("show_curItem").val());
		});
		//修改计算列表
		$('#left_item_list_2').on('change','.my_number',function (e){
			var Id = this.getAttribute("arr"),val = $(this).val(),thisObj = $(this);
			//console.log("#left_item_list_2').on('change =>");
			if(!Id) return ;
			if(isNaN(val)){
				$(this).val(F.Config.itemMap[Id]);
			}else{
				var num = parseInt(val);
					num = num >= 999 ? 999 : num;
				if(num == F.Config.itemMap[Id]) return;
				if(num == 0){
					thisObj.parent().remove()
					delete F.Config.itemMap[Id];
				}else{
					F.Config.itemMap[Id] = num;
					F.DrawCalList();
				}
				F.saveConfig();
			}
		});
		//修改 HTML残留原始事件
		$('#mySmModal_clear button').removeAttr('onclick').on('tap click',function (){
			var h = $(this).text();
			if(h=='确定'){
				F.Config.itemMap = {};
				F.saveConfig();
				F.DrawCalList();
				$("#mySmModal_clear").modal('hide');
			}
			return true;
		});
		$('#tab2Content1 .input-group button').removeAttr('onclick').removeClass('disabled').on('tap click',function (){
			var h = $(this).text();
			console.log("tab2Content1 h = ",h);
			if('清空' == h){
				//$("#mySmModal_clear").modal('show');
			}else if('统计报表' == h){
				$("#cal_div_main").show(300);
			}else if('计算' == h){
				F.showCalUI();
			}else if('保存' == h){
				F.saveItemList();
					var m = [];
					for(var i in F.Config.itemMap){
						m.push(i+':'+F.Config.itemMap[i]);
					}
					if(m.length==0) return ;
					m = m.join('|');
					if($('#saveKey').val()){
						m +='|'+$('#saveKey').val();
					}
					var href = location.href.split('#')[0]+'#'+m;
					$('#shareLink').val(href);
					$.post('http://nenge.net/api/tcn.php',
						{url:href.split('#')[0],'param':'#'+m},
						function (e){
							if(e&&e.url){
								$('#shareLink').val(e.url);
							}
						}
					);
			}
		});
		$('#tab2Content1 .input-group input').each(function (){
			var placeholder = this.getAttribute('placeholder','记录');
			if('ID' == placeholder){
				this.setAttribute('placeholder','名称');
				this.setAttribute('id','saveKey');
			}else if('name' == placeholder){
				this.setAttribute('placeholder','分享');
				this.setAttribute('id','shareLink');
			}
		});
		$('#tab2Content3').on('tap click','.History-btn',function (){
			var t = $(this).text(),k = $(this).parent().text().replace('加载删除','');
			console.log(F.Config.History[k]);
			if('加载' == t){
				if(F.Config.History[k]){
					F.Config.itemMap = F.Config.History[k];
					F.DrawCalList();
				}
			}else if('删除' == t){
				var m = {};
				for(var i in F.Config.History){
					if(k != i)m[i] = F.Config.History[i];
				}
				F.Config.History = m;
				F.saveConfig();
			}
		});
		
		$('#tab2Content4 input').on("tap click",function(){
			var a = $("#cfg_fistlang input[name='cfg_fistlang']:checked").val();
			var b = $("#cfg_selang input[name='cfg_selang']:checked").val();
			F.Config.langs.cfg_fistlang = a;
			F.Config.langs.cfg_selang   = b;
			F.saveConfig();
		})
		
		$('#cal_div_main div').removeAttr('onclick').each(function (i){
			if(i==0){
				$(this).on('tap click',function (){
					$("#cal_div_main").hide("slow");
				});
			}
		});
		$('#tab2Content5 input').removeAttr('onchange').on('change',function (){
			if(!F.Config.UcgData)F.Config.UcgData = {};
			var obj = $(this),id=obj.attr('id'),type=this.type,v;
			if('checkbox' == type){
				v = obj.prop("checked");
			}else if ('number' == type||'text' == type){
				v = parseInt(obj.val());
				if(id.indexOf('Lv2') !=-1){
					var path = id.replace('2','1');
					path = $('#'+path);
					var v1 = parseInt(path.val());
					if(v<v1){
						path.val(v);
						F.Config.UcgData[path.attr('id')] = v;
						obj.val(v1);
						v = v1;
					}
				}
			}
			F.Config.UcgData[id] = v;
			F.saveConfig(true);
		});
		$('#patch1,#patch2').change(function(e){
			var id = $(this).attr('id'),v = parseFloat($(this).val());
			if(!v) return ;
			if(!F.Config.UcgData)F.Config.UcgData = {};
			if(id=="patch2"){
				var v1 = parseFloat( $("#patch1").val() );
				if(v<v1){
					$("#patch1").val(v);
					$(this).val(v1);
					F.Config.UcgData['patch1'] = v;
					v = v1;
				}
			}
			F.Config.UcgData[id] = v;
			F.saveConfig(true);
		});;
		$("#tab2Content5 button:contains('Go')").removeAttr('onclick').on('tap click',function (e){
			F.getSuperse();
			return false;
		});
		$("#se_uc").chosen().on("change", function (evt, params) {
			F.Config.UcgData['se_uc'] = $(this).val();
			F.saveConfig(true);
		});
		$("#ucg_uc").chosen().on("change", function (evt, params) {
			F.Config.UcgData['ucg_uc'] = $(this).val();
			F.saveConfig(true);
		});
		for(var id in F.Config.UcgData){
			var ele = $("#"+id);
			if(ele[0]){
				var type = ele[0].type;
				if('number' == type||'text' == type){
					ele.val(F.Config.UcgData[id]);
				}else if('checkbox' == type){
					ele.prop('checked', F.Config.UcgData[id]);
				}else if('select-one' == type){
					ele.val(F.Config.UcgData[id]);
					if(id == "se_uc" || id == "ucg_uc"){
						ele.trigger('chosen:updated');
					}
				}else{
					console.log(ele);
				}
			}
		}
		

	},
	initNoteBook:function (Job){
		var html = "";
		var html1 = "";
		var num = 0;
		var num1 = 0;
		var F = this,
			Job = Job || F.Config.job,
			JobNoteHash = F.getStaticData('jobNoteHash'),
			JobNoteTitle = F.getStaticData('jobNoteTitle'),
			HashJob = F.getConfigData('HashJob'),
			HashLi = F.getConfigData('HashLi'),
			recipeNote = F.getCacheData('recipeNote'),
			template = {
				0:'<li arr={2} class="tager_left_meau" data-list={3}><a href="javascript:void(0)"  >{0} <span class="label label-badge label-success my_lab_barg">{1}</span></a></li>',
				1:'<li><a class="dropdown-toggle" data-toggle="dropdown" href="#">1-70非秘籍 <span class="label label-badge label-success my_lab_barg">{1}</span><span class="caret"></span></a>{0}</li>',
				3:'<ul class="dropdown-menu">{0}</ul>',
				4:''
			};
		F.saveConfig();
		for(var fileid in JobNoteHash) {
			var hashs = JobNoteHash[fileid][HashJob[Job]];
			if(fileid == 0 ) {
				var tem01 = "";
				for(var j = 0; j < hashs.length; j++) {
					var rnh = recipeNote[hashs[j]]
					//console.log("rnh = " , rnh);
					//console.log(FFconfig.hash_li[j] +" == "+rnh[1]);
					tem01 = template[0].format(HashLi[j],rnh[1],JSON.stringify(rnh),j)+tem01;
					num += rnh[1];
					//console.log("num = "+num);
				}
				//html += html_1.format(html_3.format(tem01),num)
				html += tem01;
				//console.log("num = "+num);
			} else {
				var rnh = recipeNote[hashs[0]]
				if(rnh != undefined){
					html1 += template[0].format(JobNoteTitle[fileid][0], rnh[1], JSON.stringify(rnh),'');
					num1 += rnh[1];
				}
			}
		}
		var leftMenuLi = $("#left_meau").html(html).find('li');
		var clickId = 0,jobSeleteId = F.Config.jobSeleteId;
		if(jobSeleteId){
			clickId = leftMenuLi.length - jobSeleteId-1;
		}
		if(leftMenuLi[clickId])leftMenuLi[clickId].click();
		else leftMenuLi[0].click();
		$("#txt_leve").html(num);
		$("#left_meau1").html(html1);
		$("#txt_master").html(num1);
		//F.EVENT['leftMenu'].click();
	},
	initLangs:function(){
		var F = this, Config = F.Config;
		if(Config["langs"] == undefined){
			Config["langs"] == F.ConfigData["langs"];
			F.Config["langs"] == F.ConfigData["langs"];
			F.saveConfig();
		}
		//$("#tab2Content4:radio").prop("checked", false);
		var lang1 = Config.langs.cfg_fistlang, lang2 = Config.langs.cfg_selang;
		$("#cfg_fistlang_"+lang1).prop("checked", true);
		$("#cfg_selang_"+lang2).prop("checked", true);
	},
	/**
	 * 添加函数  事件处理
	 * @param {Object} Id
	 * @param {Object} Num
	 */
	DoAddToMap:function (Id,Num){
		var F = this,itemsInfo = F.getCacheData('itemsInfo'),Id = Id?Id:$("#show_curItem").val();
		var recipeid = itemsInfo[Id]["rid"];
		
		if(recipeid.length > 0){
			console.log("存在配方，继续操作")
		}else{
			console.log("没有配方，放弃操作")
			return;
		}
		F.PushToTem(Id,Num);
	},
	/**
	 * 添加函数 加入计算
	 * @param {Object} Id
	 * @param {Object} Num
	 */
	PushToTem:function (Id,Num){
		var F = this;
		if(!F.Config.itemMap)F.Config.itemMap =  {};
		if (F.Config.itemMap[Id]) {
			F.Config.itemMap[Id] += parseInt(isNaN(Num)?1:Num);
			//更新显示数量
			$('.my_number[arr="'+Id+'"]').val(F.Config.itemMap[Id]);
		}else {
			F.Config.itemMap[Id] = 1;
			//绘制
			F.DrawCalList();
		}
		F.saveConfig();
	},
	/**
	 * 反查函数
	 * @param {Object} id
	 */
	DoBackCheck:function (id,back){
		var F = this,
			recipeData = F.getCacheData('recipeData');
		console.log("反查 id=",id)
		var out  = [0,0];
		var hash = [6, 8, 10, 12, 14, 16];
		
		for(var i in recipeData){
			var tem = recipeData[i];
			for(var j = 0; j<hash.length;j++){
				if(tem[hash[j]] == id)
					out.push(tem[0]);
			}
		}
		if(back) return out;
		
		var name = F.getItemNamesById(id)[0];
		if(out.length == 2){
			new $.zui.Messager('提示:【{0}】不是其他配方的素材'.format(name), {  type: 'danger'}).show();
			return;
		}else{
			new $.zui.Messager('提示:反查【{0}】找到了{1}个配方'.format(name,out.length - 2), {  type: 'success'}).show();
		}
		F.DrawItemList(out);
	},
	bindTree:function (){
		var F = this;
		F.Tree={};
		F.Tree[1] = $('#myTree').tree().data('zui.tree');
		F.Tree[2] = $('#myTree2').tree().data('zui.tree');
		F.Tree[3] = $('#myTree3').tree().data('zui.tree');
		F.Tree[4] = $('#myTree4').tree().data('zui.tree');
		F.Tree[5] = $('#myTree5').tree().data('zui.tree');
		F.Tree[6] = $('#myTree6').tree().data('zui.tree');
		F.Tree[7] = $('#myTree7').tree().data('zui.tree');
		F.Tree[8] = $('#myTree_sm').tree().data('zui.tree');
		F.Tree[9] = $('#myTree_so').tree().data('zui.tree');
	},
	showCalUI:function () {
		var F = this;
		$("#cal_div_main").show(300);
		if(JSON.stringify(F.Config.itemMap) === F.ConfigData.tempItemMap) return console.log('已经生成');
		F.ConfigData.tempItemMap = JSON.stringify(F.Config.itemMap);
		F.ConfigData.SumMapData = {
				list:{},//队列
				lv1:{},//直接材料
				lv2:{},//2级材料
				lv3:{},//3级材料
				lv4:{},//4级材料
				lv0:{},//成品材料
				base:{},//基础素材统计
			};
		var lv = 1;
		for(var i in F.Config.itemMap){
			//Cal.calHelper(i,lv,_temMap[i]);
			F.CalItem(i,lv,F.Config.itemMap[i]);
			//console.log(F.ConfigData.SumMapData);
		}
		F.DrawSum()
	},
	CalItem:function(id, lv, count,sumMap){
		var F = this,	
			itemsData = F.getCacheData('itemsData'),
			itemsInfo = F.getCacheData('itemsInfo'),
			recipeData = F.getCacheData('recipeData'),
			//recipLeve = F.getStaticData('recipLeve'),
			itemInfo = itemsInfo[id],
			ItemNames = F.getItemNamesById(id),
			SumMapData = F.getConfigData('SumMapData');
		if(itemInfo == undefined){
			console.log("结算异常 >",id, lv, count)
			return ;
		}
		if(lv == 1){
			SumMapData["list"][id] = {id:id,pc:0,mkc:0,need:count,name:ItemNames[0],t:"l"};
		}
	var recipeids = itemInfo["rid"];
	var recipe = recipeids.length == 0 ? null : recipeData[recipeids[0]];

	if(recipe == null){
		console.log("基础素材",count,itemInfo["id"],ItemNames[0])
		//基础素材统计 - 不是水晶并且没有配方
		if(lv >1){
			if(SumMapData["lv"+(lv -1) ][id]["t"] == "p"){
				if(SumMapData["base"][id] == undefined){
					SumMapData["base"][id] = {id:id,pc:0,mkc:0,need:count,name:ItemNames[0],t:"p"};
				}else{
					SumMapData["base"][id]["need"] = SumMapData["base"][id]["need"]+count;
				}
			}
		}
		return null;
	}else{
		var product_count = recipe[5];
		var _mkc = parseInt(count / product_count);
		 	_mkc += (count % product_count) > 0 ? 1 : 0;
		count = _mkc;
		//console.log("半成品素材 制作次数=",count," id=",itemInfo["id"]," name=",ItemNames[0]," pc=",product_count," mkc=",_mkc)
		//console.log("recipe",recipe)
	}
	var reArr = [22, 24, 6, 8, 10, 12, 14, 16];
	var myRecipeData = [];
	var shard = F.MkShardArr(recipe);
	var material = F.MkMateArr(recipe);
	//console.log("shard",shard);
	//console.log("material",material);
	for(var i in shard){
		var old_need = SumMapData["lv"+lv][i];
			_itemInfo = itemsInfo[i],
			_ItemNames = F.getItemNamesById(i);
		if(old_need == undefined){
			SumMapData["lv"+lv][i] = {id:i,pc:0,mkc:0,need:0,name:_ItemNames[0],t:"s"};
			old_need = SumMapData["lv"+lv][i];
		}
		var re_need = old_need["need"] + shard[i] * Number(count);
		SumMapData["lv"+lv][i]["need"] = re_need; 
		SumMapData["lv"+lv][i]["mkc"] = 0;
	}
	for(var i in material){
		var re_product_need = Number(material[i]),
			re_product_count = Number(F.getProductCount(i)),
			old_need = SumMapData["lv"+lv][i],
			_itemInfo = itemsInfo[i],
			_ItemNames = F.getItemNamesById(i);
		if(old_need == undefined){
			SumMapData["lv"+lv][i] = {id:i,pc:re_product_count,mkc:0,need:0,name:_ItemNames,t:"p"};
			old_need = SumMapData["lv"+lv][i];
		}
		let re_need = Number(old_need["need"]) + re_product_need * count;//总需求数量 = 旧需求数量 + 本素材需求数量 * 制作次数
		
		var re_mkc = parseInt(re_need / re_product_count);
		re_mkc += (re_need % re_product_count) > 0 ? 1 : 0;
		
		re_mkc = isNaN(re_mkc) ? 0 : re_mkc;
		//if("柿漆" == ItemNames[0])
		//console.log(ItemNames[0],">>",_ItemNames[0]," 总需求数量 = ",re_need," 旧需求数量=",Number(old_need["need"]) ," 本素材需求数量=", re_product_need ,"制作次数", count,"re_mkc",re_mkc,"re_product_count",re_product_count)
		SumMapData["lv"+lv][i]["need"] = re_need; 
		SumMapData["lv"+lv][i]["mkc"] = re_mkc;
		
		if(re_product_count > 0){
			F.CalItem(i,lv+1, parseInt(re_product_need) * count );
		}else{
			var baseNeed = count * re_product_need;
			if(SumMapData["base"][i] == undefined){
				SumMapData["base"][i] = {id:id,pc:0,mkc:0,need: "",name:_ItemNames,t:"p"};
			}else{
				//SumMapData["base"][i]["need"] = SumMapData["base"][i]["need"] + baseNeed;
			}
		}
	}
},
MkShardArr:function (re){
	var out = {};
	out[re[22]] = re[23]
	if(re[24] != 0 && re[24] != -1)
	out[re[24]] = re[25]
	return out;
},
MkMateArr:function (re){
	var out = {};
	if(re[6] != 0 && re[6] != -1)
		out[re[6]] = re[7]
	if(re[8] != 0 && re[8] != -1)
		out[re[8]] = re[9]
	if(re[10] != 0 && re[10] != -1)
		out[re[10]] = re[11]
	if(re[12] != 0 && re[12] != -1)
		out[re[12]] = re[13]
	if(re[14] != 0 && re[14] != -1)
		out[re[14]] = re[15]
	if(re[16] != 0 && re[16] != -1)
		out[re[16]] = re[17]
	return out;
}

}
$.extend(window.Bobo,Bobo);
})()
