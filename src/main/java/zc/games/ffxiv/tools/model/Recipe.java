package zc.games.ffxiv.tools.model;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Range;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Value
@Builder
public class Recipe implements ItemRecipe {
	
	private String id;
	
	private Spec spec;
	
	private String job;
	
	private int notebookId;
	
	private NotebookCategory notebookCategory;
	
	private Material product;
	
	//TODO change type to Bom
	private List<Material> bom;
	
	public String toString() {
		String bomRepr = bom.stream()
				.map(e -> String.format("\t- %s", e))
				.collect(Collectors.joining("\n"));
		return String.format("Recipe: %s%n%s", this.product, bomRepr);
	}

	@Value
	@Builder
	public static class Spec {
		
		private int id;
		
		private int level;
		
		private int stars;
		
		private int durability;
		
		private int difficulty;
		
		private int quality;
	}
	
	@Value
	public static class NotebookCategory {
		
		private int id;
		
		private String name;
		
		@Getter(AccessLevel.NONE)
		private Range<Integer> notebookIdRange;

		public boolean includes(int notebookId) {
			return this.notebookIdRange.contains(notebookId);
		}
	}
}
