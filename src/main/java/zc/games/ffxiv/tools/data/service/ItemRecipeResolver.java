package zc.games.ffxiv.tools.data.service;

import java.util.Optional;

import lombok.AllArgsConstructor;
import zc.games.ffxiv.tools.data.repository.FreeCompanyRecipeRepository;
import zc.games.ffxiv.tools.data.repository.RecipeRepository;
import zc.games.ffxiv.tools.model.ItemRecipe;

@AllArgsConstructor
public class ItemRecipeResolver {

	private RecipeRepository recipes;

	private FreeCompanyRecipeRepository fcRecipes;

	public Optional<ItemRecipe> resolveByItemId(String itemId) {
		Optional<ItemRecipe> result = Optional.ofNullable(this.recipes.findByItemId(itemId).orElse(null));
		if (!result.isPresent()) {
			result = Optional.ofNullable(this.fcRecipes.findByItemId(itemId).orElse(null));
		}
		return result;
	}
}
