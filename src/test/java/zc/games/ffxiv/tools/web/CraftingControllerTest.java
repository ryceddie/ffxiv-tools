package zc.games.ffxiv.tools.web;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import zc.games.ffxiv.tools.calculator.CraftingCalculator;
import zc.games.ffxiv.tools.calculator.CraftingChecklist;
import zc.games.ffxiv.tools.calculator.CraftingChecklist.CraftTable;
import zc.games.ffxiv.tools.data.service.InventoryService;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.web.CraftingController.CraftingCalculationRequest;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CraftingControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper jsonMapper;
	
	@MockBean
	private CraftingCalculator calculator;
	
	@MockBean
	private InventoryService inventoryService;

	@Test
	public void test() throws Exception {
		CraftingCalculationRequest request = CraftingCalculationRequest.builder()
				.craft(Material.builder().itemId("1").itemName("product1").count(1).build())
				.build();
		
		Bom raw = new Bom(Arrays.asList(
				Material.builder()
				.itemId("2")
				.itemName("raw1")
				.count(2)
				.build(),
				Material.builder()
				.itemId("3")
				.itemName("raw2")
				.count(3)
				.build()));
		Bom step1 = new Bom(Arrays.asList(
				Material.builder()
				.itemId("4")
				.itemName("intermediate")
				.count(1)
				.build()));
		when(this.calculator.calculate(Mockito.anyIterable(), Mockito.anyIterable()))
		.thenReturn(CraftingChecklist.builder()
				.fromInventory(Bom.EMPTY)
				.raw(raw)
				.craftTable(CraftTable.from(
						Collections.singletonList(step1), 
						id -> "test-job"))
				.redundant(Bom.EMPTY)
				.build());
		this.mockMvc.perform(post("/api/crafting/calculation")
				.accept(MediaType.APPLICATION_JSON)
				.content(this.jsonMapper.writeValueAsString(request))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@TestConfiguration
	static class ContextConfig {
		
		@Bean
		CraftingController controller(CraftingCalculator calculator, InventoryService inventoryService) {
			return new CraftingController(calculator, inventoryService);
		}
	}
}
