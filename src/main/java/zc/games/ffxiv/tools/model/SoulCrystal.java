package zc.games.ffxiv.tools.model;

import com.google.common.base.Preconditions;

import lombok.Value;

@Value
public class SoulCrystal {

	private Item item;
	
	private Job job;
	
	public static SoulCrystal from(Item item) {
		// TODO add cache
		return new SoulCrystal(item);
	}

	public SoulCrystal(Item item) {
		Preconditions.checkArgument(
				GearSlot.SOUL.itemCategoryIds().contains(item.getCategory().getCode()),
				item + " is not soul crystal");
		this.item = item;
		if (item.getJobs().size() == 1) {
			this.job = item.getJobs().get(0);
		} else {			
			this.job = item.getJobs().stream()
					.filter(job -> item.getName().startsWith(job.getName()))
					.findAny()
					.orElseThrow(() -> new IllegalArgumentException(String
							.format("invalid soul crystal: %s, can not resolve it's job from job list: %s", item, item.getJobs())));
		}
	}

	public Job job() {
		return this.job;
	}
}
