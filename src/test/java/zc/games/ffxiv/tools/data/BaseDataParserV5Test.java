package zc.games.ffxiv.tools.data;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import zc.games.ffxiv.tools.data.BaseData.Ingredients;

public class BaseDataParserV5Test {
	
	@Test
	public void test() {
		Ingredients parseResult = new BaseDataParserV5()
				.withJobCategoryIdResolver(classJob -> 30)
				.parse();
		
		BaseData baseData = parseResult.compose();
		
		assertThat(baseData.getJobById("1")).isNotNull();
		assertThat(baseData.getJobById("1").getPrimaryCategoryId()).isEqualTo(30);
		assertThat(baseData.getJobCategory(0)).isNotNull();
		
		assertThat(baseData.getRecipeSpec(1)).isNotNull();
		assertThat(baseData.getCraftingNotebookCategory(0)).isNotNull();
	}
}
