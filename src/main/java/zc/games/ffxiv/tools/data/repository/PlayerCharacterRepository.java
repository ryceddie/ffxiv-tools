package zc.games.ffxiv.tools.data.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.google.common.collect.Multimap;

import zc.games.ffxiv.tools.data.ActorState;
import zc.games.ffxiv.tools.data.PlayerCharacterState;
import zc.games.ffxiv.tools.model.Inventory;

//TODO rename to CharacterRepository
public interface PlayerCharacterRepository {

	void updateState(PlayerCharacterState state);
	
	void updateActorState(ActorState state);

	void updateInventoryData(String characterKey, List<Inventory> inventoryData);
	
	Optional<PlayerCharacterState> stateOf(String characterKey);
	
	Collection<PlayerCharacterState> allStates();
	
	Collection<ActorState> actorStateOf(Predicate<String> keyPredicate);
	
	List<Inventory> inventoryStateOf(String characterKey);

	Multimap<String, Inventory> inventoryStateOf(Predicate<String> keyPredicate);
}