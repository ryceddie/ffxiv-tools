package zc.games.ffxiv.tools.data.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import zc.games.ffxiv.tools.data.ActorSnapshot;
import zc.games.ffxiv.tools.data.ActorState;

public class ActorService {
	
	private Collection<ActorState> states = Collections.emptyList();

	public void updateStates(ActorSnapshot snapshot) {
		this.states = snapshot.getListing();
	}
	
	public Optional<ActorState> retainer() {
		return this.states.stream()
				.filter(state -> state.getType().equals(ActorState.Type.RETAINER))
				.findAny();
	}
}
