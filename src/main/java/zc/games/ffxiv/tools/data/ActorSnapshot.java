package zc.games.ffxiv.tools.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;

import lombok.NonNull;
import lombok.Value;

@Value
public class ActorSnapshot {

	@NonNull
	private List<ActorState> listing;

	@JsonCreator
	public ActorSnapshot(List<ActorState> listing) {
		this.listing = listing;
	}
}
