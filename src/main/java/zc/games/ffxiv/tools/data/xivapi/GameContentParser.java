package zc.games.ffxiv.tools.data.xivapi;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.FluentIterable;

import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.xivapi.EntityRawDataPersister.EntityRawData;
import zc.games.ffxiv.tools.data.xivapi.Models.ClassJob;

/**
 * 
 * @author Zhang Chi
 * @since 2020-02-29
 *
 */
class GameContentParser {

	private ObjectMapper jsonMapper = new ObjectMapper();
	
	GameContent parse(Iterable<EntityRawData> rawData) {
		
		Iterable<ClassJob> classJobs = FluentIterable.from(rawData)
				.filter(raw -> raw.getContentName().equals("ClassJob"))
				.transform(EntityRawData::getJson)
				.transform(json -> {
					try {
						return jsonMapper.readValue(json, ClassJob.class);
					} catch (IOException e) {
						throw new DataException("failed to parse json data", e);
					}
				});
		return new GameContent(classJobs);
	}
}
