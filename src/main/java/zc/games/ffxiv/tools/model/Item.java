package zc.games.ffxiv.tools.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(of = {"id", "name"})
@ToString(of = {"id", "name", "level"})
public class Item {

	private String id;
	
	private String name;
	
	@NonNull
	private Category category;
	
	private int level;
	
	private int jobCategoryCode;
	
	/**
	 * TODO mark deprecated
	 */
	private List<Job> jobs;
	
	private int requiredLevel;
	
	private boolean craftable;

	@Builder
	public Item(String id, String name, @NonNull Category category, int level,
			int jobCategoryCode, @NonNull List<Job> jobs, int requiredLevel,
			boolean craftable) {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(id), "id can not be blank");
		Preconditions.checkArgument(!Strings.isNullOrEmpty(name), String.format("item %s name is blank", id));
		this.id = id;
		this.name = name;
		this.category = category;
		this.level = level;
		this.jobCategoryCode = jobCategoryCode;
		this.jobs = ImmutableList.copyOf(jobs);
		this.requiredLevel = requiredLevel;
		this.craftable = craftable;
	}
	
	public static Item unknown(String id) {
		Item result = Item.builder()
				.id(id)
				.name("!unknown")
				.category(Category.UNKNOWN)
				.craftable(false)
				.jobs(Collections.emptyList())
				.build();
		return result;
	}
	
	@Value
	public static class Category {
		
		public static final Category UNKNOWN = new Category("unknown", "!unknown", CategoryGroup.UNKNOWN);
		
		private static Category SOUL_CRYSTAL = null;
		
		private String code;
		
		private String name;
		
		private CategoryGroup group;
		
		private GearSlot gearSlot;
		
		public static Category soulCrystal() {
			Preconditions.checkState(SOUL_CRYSTAL != null, "this category is not resolved yet");
			return SOUL_CRYSTAL;
		}
		
		public static void resolveSoulCrystalTo(Category category) {
			SOUL_CRYSTAL = category;
		}

		public Category(String code, String name, CategoryGroup group) {
			this.code = code;
			this.name = name;
			this.group = group;
			this.gearSlot = GearSlot.forCategory(this.code).orElse(null);
			if (this.group.canEquip && this.gearSlot == null) {
				throw new RuntimeException("failed to resolve gear slot for item category: " + name);
			}
		}
		
		public Optional<GearSlot> gearSlot() {
			return Optional.ofNullable(this.gearSlot);
		}

		public boolean isGear() {
			return this.group.canEquip();
		}
	}
	
	public enum CategoryGroup {
		
		UNKNOWN(false),
		ARM(true),
		TOOL(true),
		ARMOR(true),
		ACCESSORY(true),
		CONSUMABLE(false),
		MATERIAL(false),
		OTHER(false);
		
		public static final CategoryGroup[] VALUES = values();
		
		private int code;
		
		private boolean canEquip;

		private CategoryGroup(boolean canEquip) {
			this.canEquip = canEquip;
			this.code = this.ordinal();
		}
		
		public CategoryGroup ofCode(int code) {
			return Arrays.asList(values())
					.stream()
					.filter(g -> g.code == code)
					.findFirst()
					.get();
		}

		public boolean canEquip() {
			return this.canEquip;
		}

		public void setCode(int code) {
			this.code = code;
		}
	}
}
