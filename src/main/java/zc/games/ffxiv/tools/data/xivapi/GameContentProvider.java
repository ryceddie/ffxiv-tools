package zc.games.ffxiv.tools.data.xivapi;

import java.io.File;

import feign.Feign;
import feign.Logger.Level;
import feign.jackson.JacksonDecoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import lombok.AllArgsConstructor;
import lombok.Builder;
import reactor.core.publisher.Flux;

/**
 * 
 * @author Zhang Chi
 * @since 2020-02-29
 *
 */
@AllArgsConstructor
@Builder
public class GameContentProvider {
	
	private EntityRawDataPersister persister;
	
	private GameContentApi api;
	
	public GameContent provide() {
		if (this.persister.entityCount() == 0) {
			Flux.range(1, 38).subscribe(this.api::getClassJob);
		}
		return new GameContentParser().parse(persister.load());
	}
	
	public static class GameContentProviderBuilder {
		
		private EntityRawDataExtractor rawDataExtractor = new EntityRawDataExtractor();
		
		public GameContentProviderBuilder cachingAt(File dataDir) {
			this.persister = new EntityRawDataPersister(dataDir);
			this.rawDataExtractor.consumer(this.persister::save);
			return this;
		}
		
		public GameContentProviderBuilder defaultClient() {
			this.api = Feign.builder()
					.logger(new Slf4jLogger())
					.logLevel(Level.BASIC)
					.client(new OkHttpClient())
					.mapAndDecode(this.rawDataExtractor, new JacksonDecoder())
					.target(GameContentApi.class, "https://xivapi.com");
			return this;
		}
	}
}
