package zc.games.ffxiv.tools.view;

import com.google.common.base.Function;

import zc.games.ffxiv.tools.calculator.CraftingChecklist;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.model.Material;

public class CraftingChecklistPrinter {
	
	private ItemRepository items;

	public CraftingChecklistPrinter(ItemRepository items) {
		this.items = items;
	}

	public String print(CraftingChecklist checklist) {
		Function<Material, String> grouper = m -> {
			String itemId = m.getItemId();
			return items.findById(itemId).get().getCategory().getName();
		};
		CompositeBom fromInventory = CompositeBom.from(checklist.getFromInventory(), grouper);
		CompositeBom raw = CompositeBom.from(checklist.getRaw(), grouper);
		return String.format("crafting checklist:%nfrom-inventory %s%nraw %s%n%s%nprocedure: %s%nredundant %s", 
				fromInventory, raw, checklist.getCraftTable(), checklist.getProcedure(), checklist.getRedundant());
	}
}
