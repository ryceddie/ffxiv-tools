"use strict";
(function (){
var Bobo = {
getStaticData:function (str){
	var F = this,D = F.staticData;
	if(!D[str]) console.log('Bobo.staticData.'+str+' is 不存在');
	else return D[str];
},
getConfigData:function (str){
	var F = this,D = F.ConfigData; 
	if(!D[str])console.log('Bobo.ConfigData.'+str+' is 不存在');
	else return D[str];
},
getCacheData:function (str){
	var F = this,D = F.cacheData;
	if(!D[str])console.log('Bobo.cacheData.'+str+' is 不存在');
	else return D[str];
},
checkImgPath:function (icon_id, net){
	return this.ConfigData.ImgPath + icon_id.toString().substring(0, 2) + "000/0" + icon_id + ".tex.png";
},
getImgPath:function (icon_id, recipeData){
	var F = this,
		//recipeData = F.getCacheData('recipeData'),
		hq = recipeData && recipeData[39] ? "/hq/" : "",
		icon_id = icon_id + "";
	if (icon_id.length == 4)
		icon_id = "0" + icon_id;
	if (icon_id.length == 3)
		icon_id = "00" + icon_id;
	if (icon_id.length == 2)
		icon_id = "000" + icon_id;
	if (icon_id.length == 1)
		icon_id = "0000" + icon_id;
	return this.ConfigData.ImgPath+"{0}000/{2}0{1}.tex.png".format(icon_id.substring(0, 2), icon_id, hq);
},
loadConfig:function (){
	var Config = this.get('Config');
	if(Config) $.extend(this.Config,Config);
	
	var k = location.hash.substr(1).split('|'), d;
	for(var i=0;i<k.length;i++){
		var v = k[i].split(':');
		if(!isNaN(v[0])&&!isNaN(v[1])){
			if(!d)d={};
			d[v[0]] = v[1];
		}else if(d&&i==k.length-1){
			$('#savekey').val(k[i]);
		}
	}
	if(d){
		F.saveItemList();
		location.hash = "";
		this.Config.itemMap = d;
	}
	
	if(F.Config["UcgData"] == undefined){
		F.Config["UcgData"] == {},
		F.saveConfig();
	}
},
saveConfig:function (unDraw){
	this.set('Config',this.Config);
	if(!unDraw)F.DrawHistory();
	
},
saveItemList:function (){
	var itemMap = this.Config.itemMap;
	var v = $('#savekey').val()|| (new Date()).toLocaleString();
	if(!this.Config.History) this.Config.History = {};
	this.Config.History[v] = itemMap;
	this.saveConfig();
},
getLang:function(){
	var F = this, langs = F.Config["langs"], names = {zh:"cname", ja:"jname", en:"ename"}, re = ["cname","cname"];
	if(langs == undefined)
		return re;
	re[0] = names[langs.cfg_fistlang];
	re[1] = names[langs.cfg_selang];
	return re;
},
getItemNameById:function (Id){
	var F = this,
		itemsInfo = F.getCacheData('itemsInfo'),
		itemInfo = Id && isNaN(Id) ? Id : itemsInfo[Id];
	var lang = F.getLang();
	//console.log("getItemNameById",Id,itemInfo);
	var name = itemInfo[lang[0]] != "" ? itemInfo[lang[0]] : itemInfo[lang[1]];
	return name;
},
getItemNamesById:function (Id){
	var F = this,
		itemsInfo = F.getCacheData('itemsInfo'),
		itemInfo = Id && isNaN(Id) ? Id : itemsInfo[Id],
		NameKey = F.ConfigData.NameKey,
		Names = [];
	for(var i =0;i<NameKey.length;i++){
		if(itemInfo[NameKey[i]])Names.push(itemInfo[NameKey[i]]);
	}
	return Names;
},

getCountInRecipe : function(id,re){
	var count = 0,reArr = [6, 8, 10, 12, 14, 16];
	for(var i = 0; i < reArr.length; i++) {
		if(re[reArr[i]] == id)
			count = re[reArr[i] + 1];
	}
	//console.log("getCountInRecipe",id,count,re);
	return count;
},
getRecipeTree:function(id, _open, _cnum, _jobid) {
	var F = this,
		//itemsData = F.getCacheData('itemsData'),
		itemsInfo 	= F.getCacheData('itemsInfo'),
		recipeData 	= F.getCacheData('recipeData'),
		template 	= '<img class="my_img3 my_pull_left" src="{0}" title="{1}" /><lable class="tag_recipe_tree_node" title="{1}">{2}</lable>{3}',
		recipeid 	= itemsInfo[id]["rid"],
		jobindex	= _jobid || 0, 
		recipe 		= recipeid.length == 0 ? null : recipeData[recipeid[0]],
		isopen 		= _open || true,
		cnum 		= _cnum || 1,
		reArr 		= [22, 24, 6, 8, 10, 12, 14, 16],
		myRecipeData = [];
	if(recipe == null) return null;
	for(var i = 0; i < reArr.length; i++) {
		var ItemId = recipe[reArr[i]];
		if(ItemId == 0) continue;
		if(ItemId == -1) continue;
		var Nemes 		= F.getItemNamesById(ItemId),
			showName 	= F.getItemNameById(ItemId),
			IconId 		= F.getImgPath(itemsInfo[ItemId]["icon"], recipe),
			re_need 	= parseInt(recipe[reArr[i] + 1]),
			re_product_count = recipe[5], //产量
			re_mkc 		= parseInt(re_need / re_product_count)+(re_need % re_product_count) > 0 ? 1 : 0,
			mkinfo 		= " (" + re_product_count +")(+"+re_mkc+")",
			ChildRen 	= F.getRecipeTree(ItemId, false),
			re_need_str = re_need > 0 ? " x"+re_need :"",
			re_pc_str 	= re_product_count == 1 || ChildRen == null ? "" : mkinfo;
		myRecipeData.push({
			//html: template.format(IconId,Nemes.join('\n'))+ Nemes[0] + "</lable> x" + re_need + re_pc_str,
			html: template.format(IconId,Nemes.join('\n'),showName,re_need_str,re_pc_str),
			itid: ItemId,
			children: ChildRen,
			icon: IconId,
			open: isopen
		});
	}
	return myRecipeData;
},
getLodestoneUrl:function (id,hq,lang){
	var F = this,itemIdPatch = F.getCacheData('itemIdPatch');
	if(itemIdPatch[id] == undefined) return "";
	var lsid = itemIdPatch[id]["lodestone_id"];
	if(lsid != undefined){
		return "http://{0}.finalfantasyxiv.com/lodestone/playguide/db/item/{1}/?hq={2}".format(lang||'jp',itemIdPatch[id]["lodestone_id"],!isNaN(hq)?hq:1);
	}
},
getXivdbUrl:function (id,hq,lang){
	return 'http://xivdb.com/item/'+id;
},
getWikiUrl:function (id,hq,lang){
	var F = this,Names = F.getItemNamesById(id);
	return "https://ff14.huijiwiki.com/"+(Names.length==3?'wiki/物品:'+Names[0]:'index.php?search='+Names[0]+'&title=Special%3ASearch');
},
getGarlandtoolsUrl:function (id,hq,lang){
	return 'http://www.garlandtools.org/db/#item/'+id;
},
getTreeFromArray:function(datas) {
	var F = this,
		//itemsData = F.getCacheData('itemsData'),
		itemsInfo = F.getCacheData('itemsInfo'),
		recipeData = F.getCacheData('recipeData'),
		out = [],
		//template = '<img class="my_img3 my_pull_left" src="{0}" title="{1}" />';
		template 	= '<img class="my_img3 my_pull_left" src="{0}" title="{1}" /><lable class="tag_recipe_tree_node" title="{1}">{2}</lable>{3}';
	
		for(var id in datas){
			var tem = datas[id];
			var itemInfo = itemsInfo[id];
			var recipeid = itemInfo["rid"];
			var recipe   = recipeid.length == 0 ? null : recipeData[recipeid[0]];
			var ItemNames = F.getItemNamesById(id);
			var showName  = F.getItemNameById(id);
			//var img = template.format(F.getImgPath(itemsInfo[id]["icon"], recipe),ItemNames.join('\n'));
			
			var re_need = tem["need"],
				re_need_Str = re_need > 1 ? " x"+re_need : "";
			//var tem_html = img +"<lable class='tag_recipe_tree_node' title='"+ItemNames.join('\n')+"'>"+ showName + "</lable> x" + re_need ;//+ re_mkc_str ;
			var tem_html = template.format(F.getImgPath(itemsInfo[id]["icon"], recipe),ItemNames.join('\n'),showName,re_need_Str,"");
			var children = F.getRecipeTree(id, false);
			
			out.push({
				html: tem_html,
				itid: id,
				children: children,
				icon: itemsInfo[id]["icon"],
				open: false
			})
		}
	return out;
},
getProductCount:function(id){
	var F = this,	
		//itemsData = F.getCacheData('itemsData'),
		itemsInfo = F.getCacheData('itemsInfo'),
		recipeData = F.getCacheData('recipeData'),
		itemInfo = itemsInfo[id];
	if(itemInfo == undefined){
		return 0;
	}
	var recipeids = itemInfo["rid"];
	var recipe = recipeids.length == 0 ? null : recipeData[recipeids[0]];
	if(recipeids.length != 0 ) return recipeData[recipeids[0]][5];
	else return 0;
},
getSuperse:function() {
	var F = this,	
	itemsData  = F.getCacheData('itemsData'),
	itemsInfo  = F.getCacheData('itemsInfo'),
	recipeData = F.getCacheData('recipeData'),
	recipLeve  = F.getStaticData('recipLeve');
	console.log("开始检索");
	var out = [];
	var prm_ucg  = $("#input_ucg_uc").prop("checked");
	var prm_ucg1 = $("#ucg_uc").val();
	var prm_id1  = $("#input_id1").val().trim();
	var prm_id   = prm_id1.length > 0;
	var prm_uc   = $("#input_se_uc").prop("checked");
	var prm_uc1  = $("#se_uc").val();
	var prm_ucg  = $("#input_ucg_uc").prop("checked");
	var prm_ucg1 = $("#ucg_uc").val();
	var prm_ilv  = $("#input_iLv").prop("checked");
	var prm_ilv1 = $("#input_iLv1").val();
	var prm_ilv2 = $("#input_iLv2").val();
	var prm_lv   = $("#input_Lv").prop("checked");
	var prm_lv1  = $("#input_Lv1").val();
	var prm_lv2  = $("#input_Lv2").val();
	var prm_p    = $("#input_p").prop("checked");
	var prm_p1   = $("#patch1").val();
	var prm_p2   = $("#patch2").val();
	var prm_no   = 0;
	//console.log("--prm_ilv=",prm_ilv,"prm_lv=",prm_lv,"prm_ucg=",prm_ucg,"prm_uc=",prm_uc)
	if(prm_id) prm_no ++;
	if(prm_uc) prm_no ++;
	if(prm_ucg)prm_no ++;
	if(prm_ilv)prm_no ++;
	if(prm_lv) prm_no ++;	
	if(prm_p) prm_no  ++;
	if(prm_no == 0){
		new $.zui.Messager('提示:无效的检索条件'.format(name), {  type: 'danger'}).show();
		console.log("无效的检索条件")
		return;
	}
	if(prm_id && !parseInt(prm_id1)){
		new $.zui.Messager('提示:无效的ID'.format(name), {  type: 'danger'}).show();
		return;
	}
	
	if (prm_id && prm_id1 !="" ){
		console.log("id = "+prm_id1)
		out.push(parseInt(prm_id1));
	}else{
	console.log("已输入的检索条件:\nprm_ilv=",prm_ilv,prm_ilv1,prm_ilv2,"\nprm_lv=",prm_lv,prm_lv1,prm_lv1,"\nprm_uc=",prm_uc,prm_uc1,"\nprm_ucg=",prm_ucg,prm_ucg1)
	for(var k in itemsData){
		var itemData = itemsData[k],
		tem_iLv = itemData[12],
		tem_Lv  = itemData[39],
		tem_ucg = itemData[16],
		tem_uc  = itemData[42];
		
		var b_ilv = prm_ilv ? (tem_iLv >= prm_ilv1 && tem_iLv <= prm_ilv2)  : true;
		var b_lv  = prm_lv  ? (tem_Lv >= prm_lv1  && tem_Lv  <= prm_lv2 )  : true;
		var b_ucg = (prm_ucg && prm_ucg1 !="" ) ? prm_ucg1 == tem_ucg : true;
		var b_uc  = (prm_uc && prm_uc1 !="" ) ? prm_uc1 == tem_uc : true;
		
		var b_id  = (prm_id && prm_id1 !="" ) ? prm_id1 == itemData[0] : false;
		var b_p   = prm_p ? F.CheckInput(k,prm_p1,prm_p2) : true;
		
		if(b_ilv && b_lv && b_ucg && b_uc && b_p){
			//try{
				console.log("检索助手  k = ",k)
				if(itemsInfo[k]["jname"] != "")
					out.push(itemData[0]);
			//}catch(e){
			//}
			
		}
	}
}

	if(out.length == 0){
		new $.zui.Messager('提示:当前检索条件未找到物品', {  type: 'danger'}).show();
		console.log("无效的检索条件2");
		return;
	}
	if(out.length > 3000){
		new $.zui.Messager('提示:通过筛选找到了【{0}】个物品<br/>列表大于3000,检索条件没有意义,放弃列举'.format(out.length), {  type: 'danger'}).show();
		console.log("无效的检索条件3");
		return;
	}
	var succStr = "";
	if(prm_id && prm_id1 !="" )
		succStr = "提示:查找id为【{0}】的物品".format(prm_id1);
	else
		succStr = '提示:通过筛选找到了【{0}】个物品'.format(out.length);
	
	new $.zui.Messager(succStr, {  type: 'success'}).show();
	F.DrawItemList(out,true)
	},
	CheckInput:function(id,p1,p2){
		var F = this,
			itemIdPatch = F.getCacheData('itemIdPatch');
		var itemId = itemIdPatch[id];
		if(itemId == undefined) return true;
		var startp = parseInt(p1);
		var endp = parseInt(p2);
		
		if(startp == 0 && itemId["patch"] <= endp) return true;
		if(endp == 0 && itemId["patch"] >= startp) return true;
		if(itemId["patch"] >= startp && itemId["patch"] <= endp) return true;
	}
}
$.extend(window.Bobo,Bobo);
})()
