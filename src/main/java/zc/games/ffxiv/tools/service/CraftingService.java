package zc.games.ffxiv.tools.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.FluentIterable;

import zc.games.ffxiv.tools.calculator.CraftingCalculator;
import zc.games.ffxiv.tools.calculator.CraftingChecklist;
import zc.games.ffxiv.tools.data.repository.FreeCompanyRecipeRepository;
import zc.games.ffxiv.tools.data.service.InventoryService;
import zc.games.ffxiv.tools.data.service.InventoryService.StockExclusion;
import zc.games.ffxiv.tools.data.service.InventoryService.StockFilter;
import zc.games.ffxiv.tools.model.Material;

public class CraftingService {
	
	private CraftingCalculator calculator;
	
	private InventoryService inventoryService;
	
	private FreeCompanyRecipeRepository fcRecipes;

	public CraftingChecklist makePlan(Iterable<Material> craftingList, List<StockExclusion> stockExclusions) {
		craftingList = FluentIterable.from(craftingList)
				.transformAndConcat(m -> {
					return this.fcRecipes.findByItemId(m.getItemId())
							.map(recipe -> recipe.getBom().multiply(m.getCount()).getMaterials())
							.orElse(Collections.singletonList(m));
				});
		List<StockFilter> filters = stockExclusions.stream()
				.map(StockExclusion::toFilter).collect(Collectors.toList());
		List<Material> inventoryMaterials = this.inventoryService.getAll(filters);
		return this.calculator.calculate(craftingList, inventoryMaterials);		
	}
}
