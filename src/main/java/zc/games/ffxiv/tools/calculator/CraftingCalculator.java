package zc.games.ffxiv.tools.calculator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Value;
import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.repository.RecipeRepository;
import zc.games.ffxiv.tools.data.service.ItemRecipeResolver;
import zc.games.ffxiv.tools.model.Bom;
import zc.games.ffxiv.tools.model.Bom.MutableBom;
import zc.games.ffxiv.tools.model.ItemRecipe;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.model.Recipe;

public class CraftingCalculator {
	
	private ItemRecipeResolver recipeResolver;

	private CraftingProcedureCalculator procedureCalculator;
	
	//TODO get rid of dependency to RecipeRepository 
	public CraftingCalculator(ItemRecipeResolver recipeResolver, RecipeRepository recipes) {
		this.recipeResolver = recipeResolver;
		this.procedureCalculator = new CraftingProcedureCalculator(recipes::byItem);
	}
	
	public CraftingChecklist calculate(Iterable<Material> craftingListing) {
		return this.calculate(craftingListing, Bom.EMPTY);
	}
	
	public CraftingChecklist calculate(Iterable<Material> craftingListing, Iterable<Material> inventoryMaterials) {
		return this.calculate(craftingListing, new CraftingContext(inventoryMaterials, Collections.emptyList()));
	}
	
	public CraftingChecklist calculate(Iterable<Material> craftingListing, CraftingContext context) {
		MaterialCraftTreeVisitor visitor = new MaterialCraftTreeVisitor(context);
		visitor.simulate(craftingListing);
		
		// TODO merge inventory materials with redundant
		Bom fullCraftBom = new Bom(FluentIterable.from(visitor.getCrafted())
				.filter(m -> this.recipeResolver.resolveByItemId(m.getItemId()).get() instanceof Recipe));
		List<CraftingStep> procedure = this.procedureCalculator.calculate(fullCraftBom);
		return CraftingChecklist.builder()
				.fromInventory(visitor.getBomFromInventory())
				.raw(visitor.getRaw())
				.procedure(procedure)
				.redundant(visitor.getRedundant())
				.build();
	}
	
	public List<Bom> splitByCraftSequence(Bom target, Bom origin) {
		return new CraftingProcedure(target, origin).run();
	}
	
	public CraftingChecklist minus(CraftingChecklist original, Iterable<Material> available) {
		return null;
	}
	
	@Value
	@Builder
	public static class CraftingContext {
		
		private Iterable<Material> inventoryMaterials;

		@Default()
		private Iterable<String> unacquirableItems = Collections.emptyList();
	}

	private class MaterialCraftTreeVisitor {
		
		private Set<String> unacquirableItems;
		
		private MutableBom inventory;
		
		private MutableBom fromInventory;
		
		// rename to collecting
		private MutableBom raw;
		
		private MutableBom crafted;
		
		private MutableBom redundant;
		
		private List<Function<Material, Integer>> resolvers = Arrays.asList(
				m -> m.getCount() - this.redundant.remove(m).getCount(),
				m -> {
					Material taken = m.minus(this.inventory.remove(m));
					this.fromInventory.add(taken);
					return taken.getCount();
				},
				m -> {
					return recipeResolver.resolveByItemId(m.getItemId())
							.map(r -> this.craft(m, r))
							.orElse(0);
				},
				m -> {
					if (this.unacquirableItems.contains(m.getItemId())) {
						return 0;
					}
					this.raw.add(m);
					return m.getCount();
				});
		
		private MaterialCraftTreeVisitor(CraftingContext context) {
			this.inventory = new Bom(context.getInventoryMaterials()).toMutableBom();
			this.unacquirableItems = Sets.newHashSet(context.getUnacquirableItems());
			
			this.fromInventory = Bom.EMPTY.toMutableBom();
			this.raw = Bom.EMPTY.toMutableBom();
			this.crafted = Bom.EMPTY.toMutableBom();
			this.redundant = Bom.EMPTY.toMutableBom();
		}
		
		@Deprecated
		private MaterialCraftTreeVisitor(Bom inventory) {
			this(new CraftingContext(inventory, Collections.emptyList()));
		}
		
		void simulate(Iterable<Material> products) {
//			this.visit(FluentIterable.from(products)
//					.transformAndConcat(product -> {
//						return recipeResolver.resolveByItemId(product.getItemId())
//								.map(recipe -> recipe.getBom(product.getCount()))
//								.orElse(Bom.EMPTY);
//					}));
			products.forEach(p -> {
				ItemRecipe recipe = recipeResolver.resolveByItemId(p.getItemId())
						.orElseThrow(() -> new DataException("no recipe found for " + p));
				this.craft(p, recipe);
				//TODO check result and handle uncraftable products.
			});
		}
		
		int resolve(Material material) {
//			Material unresolved = resolvers.stream()
//					.reduce(material, (m, resolver) -> m.minus(resolver.apply(m)), Material::plus);
			Material unresolved = material;
			for (Function<Material, Integer> resolver : resolvers) {
				unresolved = unresolved.minus(resolver.apply(unresolved));
				if (unresolved.getCount() <= 0) {
					break;
				}
			}
			
			return material.getCount() - Math.max(0, unresolved.getCount());
		}

		private int craft(Material target, ItemRecipe recipe) {
			Preconditions.checkArgument(recipe.getProduct().getItemId().equals(target.getItemId()));
			if (target.getCount() == 0) {
				return 0;
			}
			
			int craftingCount = recipe.countFor(target.getCount());
			Bom rawBom = recipe.getBom(1);
			int craftedCount = this.tryCraft(rawBom, craftingCount);
			if (craftedCount < craftingCount) {
				
				craftingCount = craftedCount;
				craftedCount = this.tryCraft(rawBom, craftingCount);
				if (craftedCount != craftingCount) {
					throw new RuntimeException("unexpected");
				}
			}
			Material product = recipe.getProduct().multiply(craftedCount);
			if (product.getCount() > target.getCount()) {
				this.redundant.add(product.minus(target));
			}
			this.crafted.add(product);
			return product.getCount();
//			this.visit(new Bom(recipe.getBom()).multiply(craftingCount));
		}

		/**
		 * A composite operation: do or just calculate
		 * <ol>
		 * <li>calculate craft raw bom
		 * <li>resolve all materials in the bom
		 * <li>calculate craftable count according to resolved material count
		 * <li>rollback crafting state if craftable count is less than specified count.
		 * <li>return the calculated craftable count
		 * </ol>
		 * 
		 * @param originalBom
		 * @param craftingCount
		 * @return
		 */
		private int tryCraft(Bom originalBom, int craftingCount) {
			//take snapshot
			Bom inventorySnapshot = this.inventory.fixed();
			Bom fromInventorySnapshot = this.getBomFromInventory();
			Bom collectingSnapshot = this.getRaw();
			Bom craftedSnapshot = this.getCrafted();
			Bom redundantSnapshot = this.getRedundant();
			
			Bom rawBom = originalBom.multiply(craftingCount);
			Integer craftableCount = rawBom.toList().stream()
					.map(m -> this.resolve(m) / originalBom.getCount(m.getItemId()))
					.min(Comparator.naturalOrder())
					.get();
			if (craftableCount < craftingCount) {
				//restore snapshot
				this.inventory = inventorySnapshot.toMutableBom();
				this.fromInventory = fromInventorySnapshot.toMutableBom();
				this.raw = collectingSnapshot.toMutableBom();
				this.crafted = craftedSnapshot.toMutableBom();
				this.redundant = redundantSnapshot.toMutableBom();
			}
			return craftableCount;
		}

		public Bom getBomFromInventory() {
			return this.fromInventory.fixed();
		}
		
		Bom getRaw() {
			return this.raw.fixed();
		}
		
		Bom getCrafted() {
			return this.crafted.fixed();
		}
		
		Bom getRedundant() {
			return this.redundant.fixed();
		}

		@Deprecated
		boolean visit(Iterable<Material> materials) {
			boolean resolved = true;
			
			for (Material material : materials) {
				Material application = this.redundant.remove(material);
				if (application.getCount() == 0) {
					continue;
				}
				
				Material shortage = this.inventory.remove(application);
				this.fromInventory.add(application.minus(shortage));
				
				if (shortage.getCount() == 0) {
					continue;
				}
				
				Optional<ItemRecipe> recipe = recipeResolver.resolveByItemId(material.getItemId());
				if (recipe.isPresent()) {
					this.craft(shortage, recipe.get());
				} else {
					if (this.unacquirableItems.contains(shortage.getItemId())) {
						resolved = false;
					} else {
						this.raw.add(shortage);
					}
				}
			}
			
			return resolved;
		}
	}
	
	private class CraftingProcedure {
		
		private Collection<Material> targetList;
		
		private Bom materials;
		
		private CraftingProcedure(Bom crafted, Bom materials) {
			this.targetList = Sets.newHashSet(crafted.toList());
			this.materials = materials;
		}

		public List<Bom> run() {
			List<Bom> sequence = Lists.newArrayList();
			
			while (!this.targetList.isEmpty()) {
				
				List<Material> crafted = this.targetList.stream()
						.filter(this::tryCraft)
						.collect(Collectors.toList());
				this.targetList.removeAll(crafted);
				this.materials = this.materials.plus(crafted);
				sequence.add(new Bom(crafted));
			}
			
			return sequence;
		}
		
		private boolean tryCraft(Material material) {
			ItemRecipe recipe = recipeResolver.resolveByItemId(material.getItemId()).get();
			Bom directMaterials = new Bom(recipe.getBom()).multiply(recipe.countFor(material.getCount()));
			if (this.materials.contains(directMaterials)) {
				this.materials = this.materials.minus(directMaterials);
				return true;
			}
			return false;
		}
	}
}
