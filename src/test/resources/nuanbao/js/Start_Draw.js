"use strict";
(function (){
	var Bobo = {


DrawItemList:function (data,type){
		var F = this,
			itemsInfo = F.getCacheData('itemsInfo'),
			itemsData = F.getCacheData('itemsData'),
			recipeData = F.getCacheData('recipeData'),
			itemIdPatch = F.getCacheData('itemIdPatch'),
			Patch405 = F.getCacheData('Patch405'),
			ItemUiCategory = F.getStaticData('item_ui_category'),
			template = {
				0:'<li class="tager_item_list it_uc_{3}" arr={4}><i class="icon icon-search lis_t lis_f" title="反查" arr={4}></i><i class="icon icon-share lis_t lis_add" title="加入队列" arr={4}></i><a href="javascript:void(0)">{2}<span class="name" title="{5}">{0}</span><span class="icons">{1}</span></a></li>',
				1:'<img class="my_img my_pull_left" src="{0}" title="{1}" />',
				2:'<img class="my_img3 my_pull_left" src="{0}" title="{1}" />',
				3:'<i class="icon icon-circle-thin item_dye"  title="可染色" ></i>',
			};
		var html = "";
		var ucs = {};
		for(var i = 0; i < data.length; i++) {
				if(!type && i <2){
					continue;
				}
				var recipe ;//= type ? : ;
				var ItemInf ;//= type ? itemsInfo[data[i]] : itemsInfo[recipe[4]];
				var ItemInd ;//= type ? itemsData[data[i]] : itemsData[recipe[4]];
				//console.log(recipe);
				if(type){
					ItemInf = itemsInfo[data[i]];
					recipe = ItemInf["rid"].length > 0 ? recipeData[ItemInf["rid"][0]] : undefined;
					ItemInd =  itemsData[data[i]] ;
				}else{
					recipe = recipeData[data[i]];
					ItemInf = itemsInfo[recipe[4]];
					ItemInd = itemsData[recipe[4]];
				}
				
				//console.log("ItemInd=",ItemInf);
				var canDye = ItemInd[28];
				var ItemNames = F.getItemNamesById(ItemInf);
				var showName  = F.getItemNameById(ItemInf);
				var icon_id = ItemInf["icon"];
				var ItemCategory = ItemUiCategory[ItemInf["uc"]];
				var it_patch = itemIdPatch[ItemInf["id"]]
				var it_patch_obj = it_patch == undefined ? "?" : Patch405[it_patch["patch"]];
				var it_patch_str = it_patch == undefined ? "?" : it_patch_obj["number"]
				
				var itemIlv = itemsData[ItemInf["id"]][12];
				
				var ttt1 = template[2].format(F.checkImgPath(ItemCategory[2]),ItemCategory[1])+" .iLv"+itemIlv+" p"+it_patch_str;
				
				//var ttt2 = template[2].format(F.getImgPath(it_2_g[2])) +" "+it_2_g[1];
				var uc_num = ucs[ItemCategory[0]] == undefined ? 0 : ucs[ItemCategory[0]][3];
				//console.log(uc_num)
				ucs[ItemCategory[0]] = [ItemCategory[0],ItemCategory[1],ItemCategory[2],uc_num+1];
				//var img = template[1].format(F.getImgPath(icon_id,true,ItemInf["id"]));
				
				var img = template[1].format(F.getImgPath(icon_id,recipe),ItemNames.join('\n')) + (canDye ? template[3] : "");
				
				var ttt2 = " uc = "+ItemInf["uc"]+ " sc = "+ItemInf["sc"] ;//JSON.stringify(ItemCategory);
				html += template[0].format(showName,ttt1,img,ItemCategory[0],ItemInf["id"],ItemNames.join('\n'));
			
		}
		$("#left_item_list_1").html(html);
		F.DrawUc(ucs,"_1");

	},

DrawUc:function(data,tags){
		var F = this,
			template = {
				0:'<option value="{0}">{1} ({2})</option>',
				1:'<select data-placeholder="选择一个类型..." id="sc_uc{1}"  class="chosen-select form-control" style="position: absolute; z-index: 5;" >{0}</select>',
			};
		var html = "";
		var _tags = tags || "_1"
	
	html += '<option value=""> </option>';
	for(var i in data){
		html += template[0].format(data[i][0],data[i][1],data[i][3]);
	}
	$("#sc_uc_div"+_tags).html(template[1].format(html,tags));
	//console.log(template[1].format(html))
	$('#sc_uc'+_tags).chosen({
		allow_single_deselect: true,
		placeholder_text:"",
		search_contains: true,
		width: '100%'
	});
	$('#sc_uc'+_tags).on('change', function(e){
		console.log($("#sc_uc"+_tags).val())
		var id = $("#sc_uc"+_tags).val();
		if(id == ""){
		   		$("#left_item_list"+_tags+" .tager_item_list").removeClass("hidden");
		   	}else{
		   		$("#left_item_list"+_tags+" .tager_item_list").addClass("hidden");
		   		$("#left_item_list"+_tags+" .it_uc_"+id).removeClass("hidden");
		   	}
		   
	});
},

DrawItem:function (id){
	window.localStorage.removeItem("page_/");
	window.localStorage.removeItem("page_/cal/");
		var F = this,
		itemsData  = F.getCacheData('itemsData'),
		itemsInfo  = F.getCacheData('itemsInfo'),
		recipeData = F.getCacheData('recipeData'),
		itemInfo   = itemsInfo[id],
		recipeId   = itemsInfo[id]["rid"],
		recipe     = recipeId.length == 0 ? null : recipeData[recipeId[0]];
		
		var showName  = F.getItemNameById(itemInfo);
			showName  = showName.length > 14 ? showName.substr(0,14) + "..." : showName;
		var showTitle = F.getItemNamesById(itemInfo).join("\n");
		
		$("#item_pram").html(F.DrawPrams(id,showName,showTitle));
		
		var tree = F.getRecipeTree(id, true, 1);
		F.Tree[1].reload(tree);
		F.Tree[1].collapse();
		$('#myTree').show();
		
		if(tree != null)
			$("#item_pram1").html("<span class='label label-primary' title='{1}'>配方  ({0})</span><br>".format(showName, showTitle));
		else{
			$("#item_pram1").html("");
			$('#myTree').empty();
			$('#myTree').hide();
		}
		
		var desc_sm = F.DrawSm(id,itemsInfo);
		F.Tree[8].reload(desc_sm)
		F.Tree[8].collapse();
		$('#myTree_sm').show();
		if(desc_sm != null){
			var tt = "\n注意，对于游戏本身而言：\n1.物品类型一致情况下主模型一致就是同模。\n2.武器、工具、盾类两个主模型一致也算是同模（装饰物不一样）"
			var sm = "<span class='label label-primary' title='{1}{2}'>同模装备 ({0})</span><br>".format(showName, showTitle, tt);
			$("#item_pram_sm").html(sm);
		}else{
			$("#item_pram_sm").html("");
			$('#myTree_sm').empty();
			$('#myTree_sm').hide();
		}
		
		var desc_so = F.DrawSources(id,recipeId,itemsData,itemsInfo,recipeData);
		F.Tree[9].reload(desc_so)
		F.Tree[9].collapse();
		$('#myTree_so').show();
		if(desc_so != null && desc_so.length > 0){
			var so = "<span class='label label-primary'  title='{1}' >获得 or 消耗  ({0})</span><br>".format(showName, showTitle);
			$("#item_pram_so").html(so);
		}else{
			$("#item_pram_so").html("");
			$('#myTree_so').empty();
			$('#myTree_so').hide();
		}
		
		$("#item_pram2").html(F.getRecipePram(id,showName,showTitle));
		$("#item_pram3").html(F.DrawLinks(id,F.isweb));	
	},
	
DrawSources : function(id,recipeId,itemsData,itemsInfo,recipeData){
	var F = this,re = [],
		template = {
			0 : '{0}<span class="{1}" title="{2}">{3} {4}</span>',
			1 : '<img class="my_img3" src="{0}" title="{1}" />'
		}
			
	if(F.Sources == undefined){
		console.log("F.Sources == undefined return");
		return;
	} 
	
	re = F.mk_recipe(id,itemsInfo,recipeData);
	
	var tem_so = F.Sources[id];
	if(tem_so == undefined){
		console.log("tem_so == undefined return");
		return re;
	}
	var price = itemsData[id][25];
	console.log("tem_so",tem_so)
	for(var i in tem_so){
		var souConf = F.ConfigData.Sources[i], icon_id = souConf[0];
		var img = template[1].format(F.getImgPath(icon_id),"");
		var tem = tem_so[i], 
			chi = [],
			sum = 0,
			sumStr = 0,
			title  = souConf[1] +"\n" + i;
		if(tem.length != undefined){
			sum = tem.length;
		}else {
			for(var j in tem){
				sum ++;
			}
		}
		chi = F.mk_so(i,tem,price);
		sumStr = " ({0})".format(sum)
		if (i == "satisfaction" || i == "masterpiece") sumStr = "";
		re.push({
			html: template[0].format(img, "", title, souConf[1],sumStr),
			children: chi,
			open: false
		})
	}
	return re;	
},

mk_recipe :function(id,itemsInfo,recipeData){
	console.log("mk_recipe",id)
	var F = this,
		chi  = [],
		chi2 = [], 
		cfg  = F.ConfigData.Sources["ricipe"],
		template = {
			0 : '<img class="my_img3" src="{0}" title="{1}" />',
			1 : '{0}<span class="{1}" title="{2}">{3} ({4})</span>'
		};
	var data = F.DoBackCheck(id,true);
	if(data.length <= 2)
		return [];
	for(var i = 0; i < data.length; i++) {
		if(i <2){
			continue;
		}
		var recipe = recipeData[data[i]],
			itemID = recipe[4],
			itemIc = itemsInfo[itemID]["icon"],
			count  = F.getCountInRecipe(id,recipe),
			names  = F.getItemNamesById(itemID),
			showN  = F.getItemNameById(itemID),
			img    = template[0].format(F.getImgPath(itemIc), names.join('\n')),
			chi3   = F.getRecipeTree(itemID, false);
		
		chi2.push({
			html : template[1].format(img,"",names.join('\n'),showN, count ),
			children : chi3,
		})
	}
	var img   = template[0].format(F.getImgPath(cfg[0]), cfg[1]);
	chi.push({
		html : template[1].format(img,"",cfg[1],cfg[1],data.length - 2),
		children : chi2,
		open : false
	})
	return chi;
},

mk_so : function(type,obj,peice){
	var F = this,
		chi   = [], 
		template = {
			0 : '<img class="my_img3" src="{0}" title="{1}" />',
			1 : '<span class="{0}" title=\'{2}\'>{1}{3} </span>',
			2 : '{0}{1} = {2}{3} + {4}{5} + {6}{7}',
			3 : '{0}{1} = {2}{3} + {4}{5}'
		};
	if(type == "tradeSources" || type == "tradeUses" ){
		for(var i in obj){
			var npcObj = F.mk_npc(i,obj[i].length);
			var chi2 = []
			for(var j = 0; j < obj[i].length; j++){
				var item = obj[i][j]["item"];
				var curr = obj[i][j]["currency"];
				var it_txt = [];
				var cu_txt = [];
				for(var k = 0; k <item.length; k++){
					var showName = type == "tradeUses" && item.length ==1;
					it_txt.push(F.mk_it_txt(item[k], showName));
				}
				for(var k = 0; k <curr.length; k++){
					var showName = type == "tradeSources" && curr.length ==1;
					cu_txt.push(F.mk_it_txt(curr[k], showName));
				}
				var txt =  cu_txt.join(" + ") +" = " + it_txt.join(" + ");
				chi2.push({html:template[1].format("","","",txt,"")})
			}
			chi.push({
				html:template[1].format("","",npcObj["txt"],npcObj["name"],""),
				children: chi2,
				open: false
			})
		}
	}
	else if(type == "vendors"){
		for(var i = 0; i <obj.length; i++){
			var npcObj = F.mk_npc(obj[i],"");
			chi.push({
				html:template[1].format("","",npcObj["txt"],npcObj["name"] + "[ "+peice+"G ]")
			})
		}
	}
	//采集点
	else if(type == "nodes"){
		
	}
	//蓝票、黄票、红票、罗薇娜显赫商会 
	else if(type == "masterpiece" ){
		var hash1 = {10309:3,17833:4}
			imgs  = [
				template[0].format(F.getImgPath(1572),"价值"),
				template[0].format(F.getImgPath(65042),""),
				template[0].format(F.getImgPath(65002),"金币"),
				template[0].format(F.getImgPath(65031),"红票"),
				template[0].format(F.getImgPath(65044),"黄票"),
				template[0].format(F.getImgPath(65010),"经验"),
				template[0].format(F.getImgPath(65011),"加星")
		];
		var k = hash1[obj["reward"]];
		var chi2 = [], chi3 = [];
		
		for(var i = 0;i < obj["rating"].length; i++){
			var tem2 = template[3].format(imgs[0], obj["rating"][i],
					//imgs[2], obj["gil"][i],
					imgs[k], obj["rewardAmount"][i],
					imgs[5], obj["xp"][i] );
			var tem3 = template[3].format(imgs[0], obj["rating"][i],
					//imgs[2], obj["gil"][i],
					imgs[k], Math.floor(1.2 * obj["rewardAmount"][i]),
					imgs[5], Math.floor(1.2 * obj["xp"][i]) );
				chi2.push({html:tem2});
				chi3.push({html:tem3});
		}
		chi.push({
			html:template[1].format("","","收藏品交易员","收藏品交易员",""),
			open: true,
			children: chi2
		})
		chi.push({
			html:template[1].format("","","收藏品交易员","收藏品交易员"+imgs[6],""),
			open: true,
			children: chi3
		})
	}
	////蓝票、黄票、红票、老主顾
	else if(type == "satisfaction" ){
		var chi2   = [],
			imgs   = [
				template[0].format(F.getImgPath(1572),"价值"),
				template[0].format(F.getImgPath(65042),""),
				template[0].format(F.getImgPath(65002),"金币"),
				template[0].format(F.getImgPath(65031),"红票"),
				template[0].format(F.getImgPath(65044),"黄票"),
				template[0].format(F.getImgPath(65010),"经验")
			],
			npcObj = F.mk_npc(obj["npc"],0);
		var noIts  = obj["items"];
		for(var i = 0;i < obj["rating"].length; i++){
			var tem = template[2].format(imgs[0], obj["rating"][i],
					imgs[2], obj["gil"][i],
					imgs[3], obj["items"][0]["amount"][i],
					imgs[4], obj["items"][1]["amount"][i] )
			chi2.push({html:tem});
		}
		
		chi.push({
			html:template[1].format("","",npcObj["txt"],npcObj["name"],""),
			open: true,
			children: chi2
		})
	}
	else{
		//var SoInfo = F.Sources_info[type + "_" + obj];
		//if(SoInfo == undefined) return chi;
		console.log("mk_so else....",type,obj);
		var hash = {
			"leves":"leve",
			"drops": "mob",
			"instances": "instance",
			"nodes": "node",
			"quests": "quest",
		}
		if(obj.length != undefined){
			for(var i = 0; i<obj.length; i++){
				var SoInfo = F.Sources_info[hash[type] + "_" + obj[i]];
				//console.log("mk_so else.... SoInfo",type,SoInfo);
				if(SoInfo == undefined) continue;
				var tt = JSON.stringify(SoInfo),
					nn = SoInfo["obj"]["n"];
				if(SoInfo["obj"]["p"] != undefined) nn += " [{0}]".format(F.mk_loction[SoInfo["obj"]["p"]]) ;
				if(SoInfo["obj"]["l"] != undefined && SoInfo["obj"]["p"] == undefined){
					var ll = F.mk_loction[SoInfo["obj"]["l"]];
					if(ll != undefined) 
						nn += " [{0}]".format(ll);
//					console.log("mk_so SoInfo.l ",type,SoInfo)
				} 
				
				chi.push({html:template[1].format("", "", tt, nn, "")});
			}
		}
	}
	return chi;
},

mk_loction : function(F,id) {
	if(F.ConfigData.locationIndex[id] == undefined)
		return id;
	else
		return F.ConfigData.locationIndex[id]["name"];
},

mk_npc : function(id,no,nc) {
	var F = this;
	//"npc_1016261":{"type":"npc","id":"1016261","obj":{"i":1016261,"n":"スポイル取引窓口","l":2301,"s":6,"r":1,"a":2323,"c":[14.75,11]}},
	if(F.Sources_info["npc_"+id] == undefined)
		return {name:"???npc id = {0} ({1})".format(i,no),txt:""}
	var reStr  = no != "" ? "{0}{1} ({2})" : "{0}{1}";
	var txtStr = "npc id = {0} name = {1}\nlocation id = {2} name = {3}";
	var npc = F.Sources_info["npc_"+id]["obj"];
	//if(nc && npc["l"] == undefined) return undefined;
	var wh1 = "";
	var wh2 = "";
	if(F.ConfigData.locationIndex[npc["l"]] != undefined){
		if(npc["c"] != undefined ){
			wh2 = "({0})".format(npc["c"].join(","));
		}
		wh1 =  "[{0}{1}]".format(F.ConfigData.locationIndex[npc["l"]]["name"],wh2);
	}
	else
		console.log(id,npc["l"]);
	return {name:reStr.format(npc["n"],wh1,no,id),txt:txtStr.format(id,npc["n"],F.ConfigData.locationIndex[npc["l"]],wh1)}
},

mk_it_txt : function(idobj,showName){
	var F = this,
		template = {
			0 : '<img class="my_img3" src="{0}" title="{1}" />',
			1 : '<span class="{0}" title="{2}">{1}{3} </span>'
		};
	
	var itemInfo = F.getCacheData('itemsInfo')[idobj["id"]];
	if(itemInfo == undefined)
		return "??"+idobj["id"];
	var icon_id = itemInfo["icon"];
	var title  = F.getItemNamesById(itemInfo).join("\n");
	var name   = F.getItemNameById(itemInfo);
		name   = showName ? name.length > 6 ? name.substr(0, 6) + "..." : name  : "";
	var isHQ   = idobj["hq"] != undefined ;
	var temre  = template[0].format(F.getImgPath(icon_id,null,isHQ),title);
	var amount = idobj["amount"] > 1 ? "x"+idobj["amount"] : "";
		temre += template[1].format("",name,title,amount);
	return temre;
},

DrawPrams:function (id,showName,showTitle){
		var F = this,itemsInfo = F.getCacheData('itemsInfo'),itemInfo = itemsInfo[id];
		if(itemInfo == undefined) return "";
		var itemsData = F.getCacheData('itemsData'),
			recipeData = F.getCacheData('recipeData'),
			itemIdPatch = F.getCacheData('itemIdPatch'),
			Patch405 = F.getCacheData('Patch405'),
			itemAction = F.getCacheData('itemAction'),
			itemFood = F.getCacheData('itemFood'),
			ItemUiCategory = F.getStaticData('item_ui_category'),
			ClassJobCategory = F.getStaticData('classjobcategory'),
			baseparamChs = F.getStaticData('baseparam_chs'),
			itemData = itemsData[id],
			ItemNames = F.getItemNamesById(id),
			ItemPatch = itemIdPatch[id],
			CssClass = "label label-badge label-info",
			canDye = itemData[28],
			template = {
				0:'<span class="{0}">{1}</span>',
				1:'<div class="my_style1 my_style_pram" style="padding: 3px" >{0}</div>',
				2:'<img class="my_img my_pull_left" src="{0}" title="{1}" />',
				3:'<img class="my_img3 " src="{0}" />',
				4:'<i class="icon icon-circle-thin item_dye"  title="可染色" ></i>'
			};
		var names = template[2].format(F.getImgPath(itemInfo["icon"]),ItemNames.join('\n')) + (canDye ? template[4] : "");
			for(var i=0;i<ItemNames.length;i++){
				names += template[0].format(CssClass,ItemNames[i]);
				if((ItemNames.length == 2 && i == 0) || (ItemNames.length == 3 && i == 1))
					names += "<br/>"
			}
			
		var ItemPatchobj = ItemPatch && ItemPatch["patch"] ? Patch405[ItemPatch["patch"]] : undefined,
			patchStr = "patch" + (ItemPatchobj ? ItemPatchobj["number"] : "?"),
			lsidStr  = ItemPatch && ItemPatch["lodestone_id"] ? ItemPatch["lodestone_id"] : "?";
		var re = '<input type="hidden" id="show_curItem" value="'+id+'" />'+template[1].format(names),
			desc = template[0].format("label label-dot label-success","")
		 	 + template[0].format(CssClass,id) + " "
		 	 + template[0].format(CssClass, lsidStr) + " "
		 	 + template[0].format(CssClass, patchStr)
		 	 +"<br>"
		 	 + template[0].format("label label-dot label-success","")
		 	 + template[0].format(CssClass,ItemUiCategory[itemData[16]][1])+" "
		 	 + template[0].format(CssClass,"品级 "+ itemData[12])
		 	 + "<br>";
		if(itemData[42] >0){
			var cjc = ClassJobCategory[itemData[42]];
			desc += template[0].format("label label-dot label-success itemData_42-1","")
			+ template[0].format(CssClass,itemData[39]+"级以上")
			+ "<br>"
			+ template[0].format("label label-dot label-success itemData_42-2","")
			+ "<span class='label label-badge label-danger itemData_42-3'>{0}</span>".format(cjc[1])
			+ "<span class='label label-badge label-danger itemData_42-4'>{0}</span><br>".format(cjc[2]);
		}
		
		var desc2 = "";
		if(itemInfo["cdec"].length > 0){
			desc2 += template[0].format("label label-dot label-success cdec","")
			+ itemInfo["cdec"].replace(/<[^>]+>/g,"") +"<br/>";
		}
		
		if(itemInfo["jdec"].length > 0){
			desc2 += template[0].format("label label-dot label-success jdec","")
			+ itemInfo["jdec"].replace(/<[^>]+>/g,"") +"<br/>";
		}
		
		if(itemInfo["edec"].length > 0 && itemInfo["cdec"].length == 0){
			desc2 += template[0].format("label label-dot label-success edec","")
			+ itemInfo["edec"].replace(/<[^>]+>/g,"") +"<br/>";
		}
		
		//
		if(itemData[85] > 0){
			var tem_m = "";
			for(var i = 0;i < itemData[85];i++){
				tem_m += '<i class="icon icon-circle-blank my_icon_gren"></i>'
			}
			if(itemData[86]){
				for(var i = itemData[85];i < 5;i++){
					tem_m += '<i class="icon icon-circle-blank my_icon_red"></i>'
				}
			}
			desc += template[0].format("label label-dot label-success","");
			desc += template[0].format(CssClass,"魔晶石工艺");
			desc += tem_m +"<br/>";
		}
		
		if(desc){
			re += "<span class='label label-primary' title='{1}'>描述 ({0})</span><br>".format(showName,showTitle);
			re += template[1].format(desc + desc2);
		}

		
		var hazHq = itemData[27];
		
		var re20 = "";
		// 50 物理基本性能
		// 51 魔法基本性能
		// 52 攻击间隔
		// 54，55 格挡发动力、性能
		//
			if(itemData[50] > itemData[51]){
				re20 += template[0].format("label label-dot label-success","") + " " + template[0].format(CssClass , "物理基本性能 ："+itemData[50]) + " ";  
				if(hazHq)
					re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[50] + itemData[73]));  
				re20 += "<br>";
			}
			if(itemData[51] > itemData[50]){
				re20 += template[0].format("label label-dot label-success","") + " " + template[0].format(CssClass , "魔法基本性能 ："+(itemData[51])) + " ";  
				if(hazHq)
					re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[51] + itemData[75]));  
				re20 += "<br>";
			}
			if(itemData[50] > 0){
				var teeee = (itemData[50]/(itemData[52]/1000)) * 167.27 ;
				var teeee1 = ((itemData[50] + itemData[73])/(itemData[52]/1000)) * 167.27;
				re20 += template[0].format("label label-dot label-success","") + " " + template[0].format(CssClass , "物理自动攻击："+  Math.floor(teeee - 1)/100) + " ";  
				if(hazHq)
					re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+  Math.floor(teeee1 - 1)/100);  
				re20 += "<br>";
			}
			if(itemData[52] != 0){
				re20 += template[0].format("label label-dot label-success","") + " " + template[0].format(CssClass , "攻击间隔 ："+(itemData[52]/1000)) + " ";  
				if(hazHq)
					//re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[54] + itemData[73]));  
				re20 += "<br>";
			}
			if(itemData[54] != 0){
				re20 += template[0].format("label label-dot label-success","") + " " + template[0].format(CssClass , "格挡发动力 ："+itemData[54]) + " ";  
				if(hazHq)
					re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[54] + itemData[73]));  
				re20 += "<br>";
			}
			if(itemData[55] != 0){
				re20 += template[0].format("label label-dot label-success","") + " " + template[0].format(CssClass , "格挡性能："+itemData[55]) + " ";  
				if(hazHq)
					re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[55] + itemData[75]));  
				re20 += "<br>";
			}
			
			/*for(var i = 54; i < 56;i+=1){
				if(itemData[i] != 0){
					re20 += template[0].format("label label-dot label-success","") + " ";
					re20 += template[0].format(CssClass ,i +"："+itemData[i]) + " ";  
					//if(i<66 && hazHq)
					//	re20 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[i+1] + itemData[i+19]));  
					re20 += "<br>";
				}
			}*/
		
		if(re20.length > 0){
			re += "<span class='label label-primary' title='{1}'>基本属性 ({0})</span><br>".format(showName,showTitle);
			re += template[1].format(re20);
		}
		
		var re2 = "";
		for(var i = 58; i < 70;i+=2){
			if(itemData[i] != 0){
				re2 += template[0].format("label label-dot label-success","") + " ";
				re2 += template[0].format(CssClass ,baseparamChs[itemData[i]][2] +"："+itemData[i+1]) + " ";  
				if(i<66 && hazHq)
					re2 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> "+ (itemData[i+1] + itemData[i+19]));  
				re2 += "<br>";
			}
		}
		
		if(re2.length > 0){
			re += "<span class='label label-primary' title='{1}'>特殊属性 ({0})</span><br>".format(showName,showTitle);
			re += template[1].format(re2);
		}
		
		if(itemData[17] == 57){
			var out = [0,0];
			var re3 = "";
			for(var i =0;i<staticData.Materia.length;i++){
				var tem = staticData.Materia[i];
				for(var j = 1;j<=6;j++){
					if(tem[j] == id){
						out[0] = tem[11];
						out[1] = tem[j+11];
					}
				}
			}
			if(out[0] == 0){
				re3 = "";
			}else{
				re3 += template[0].format("label label-dot label-success","") + " ";
				re3 += template[0].format(CssClass ,baseparamChs[out[0]][2] +" +"+out[1]) + "<br/>";  
				re += template[0].format("label label-primary","魔晶石属性") +"<br/>";
				re += template[1].format(re3);
			}
		}
		
		//食物属性  
		//name = 洋葱距奶油汤 itemData[30] = 714
		//action 
		//714,0,true,true,false,845,48,212,1800,0,0,0,0,0,0,48,212,1800,0,0,0,0,0,0
		//itemfood
		//212,3,11,true,10,38,13,48,70,true,4,22,5,28,0,false,0,0,0,0
		
		//炖番茄 itemData[30] = 718 cp+13% up50  加工+5% up31
		//718,0,true,true,false,845,48,216,1800,0,0,0,0,0,0,48,216,1800,0,0,0,0,0,0
		//216,3,11,true,12,40,15,50,71,true,4,25,5,31,0,false,0,0,0,0
		/*216,3,
		11,true ,12,40,15,50,
		71,true , 4,25,5 ,31,
		 0,false, 0, 0,0 , 0*/
		
		//刚力仙药
		if(itemData[17] == 45 || itemData[17] == 43){
			var re4 = "";
			var act = itemAction[itemData[30]];
			var food = itemFood[act[7]];
			
			if(food[2] != 0){
				re4 += template[0].format("label label-dot label-success","") + " ";
				re4 += template[0].format(CssClass ,baseparamChs[food[2]][2] +" +"+food[4] +"% 上限"+food[5]); 
				if(food[3]){
					re4 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> +"+food[6] +"% 上限"+food[7]); 
				}
				re4 += "<br/>";
			}
			
			if(food[8] != 0){
				re4 += template[0].format("label label-dot label-success","") + " ";
				re4 += template[0].format(CssClass ,baseparamChs[food[8]][2] +" +"+food[10] +"% 上限"+food[11]); 
				if(food[9]){
					re4 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> +"+food[12] +"% 上限"+food[13]); 
				}
				re4 += "<br/>";
			}
			
			if(food[14] != 0){
				re4 += template[0].format("label label-dot label-success","") + " ";
				re4 += template[0].format(CssClass ,baseparamChs[food[14]][2] +" +"+food[16] +"% 上限"+food[17]); 
				if(food[15]){
					re4 += template[0].format("label label-badge label-danger" ,"<i class='icon icon-hq'></i> +"+food[18] +"% 上限"+food[19]); 
				}
				re4 += "<br/>";
			}
			if(re4.length > 0){ 
				re += "<span class='label label-primary' title='{1}'>特殊效果 ({0})</span><br>".format(showName,showTitle);
				re += template[1].format(re4);
			}
		}
		
		return re;
	},
	
DrawSm : function(id,itemsInfo){
	var F = this, sharedModels = F.getCacheData('sharedModels');
	if(sharedModels[id] == undefined) return null;
	var sm = sharedModels[id], re = [];
	for(var i = 0; i < sm.length; i++){
		if(i<4){
			re.push({
				html: F.mk_sm(F,sm[i],itemsInfo)
			})
		}else{
			var child = [];
			var more = sm.length - i + 1;
			var tem = "<lable class='tag_recipe_tree_node'>more ({0})</lable>".format(more);
			for(i; i < sm.length; i++){
				child.push({
					html: F.mk_sm(F,sm[i],itemsInfo)
				})
			}
			re.push({
				html: tem,
				children: child,
				open: false
			})
			continue;
		}
	}
	return re;
},

mk_sm :function (F,tem_id,itemsInfo){
	var html_img1 = '<img class="my_img3" src="{0}" title="{1}" />';
	var html1   = '<span class="{0}" title="{2}">{1}</span>';
	var itemInfo = itemsInfo[tem_id];
	if(itemInfo == undefined)
		return "??"+tem_id;
	var icon_id = itemInfo["icon"];
	var title = F.getItemNamesById(tem_id).join("\n");// itemInfo["jname"] + "\n" + itemInfo["ename"] + "\n" + itemInfo["cname"];
	var name  = F.getItemNameById(tem_id); // itemInfo["cname"] == "" ? itemInfo["jname"] : itemInfo["cname"];
	var temre = html_img1.format(F.getImgPath(icon_id),title);
		temre += html1.format("",name,title);
	return temre;
},

getRecipePram:function(id,showName,showTitle) {
		var F = this,
			//itemsData = F.getCacheData('itemsData'),
			itemsInfo = F.getCacheData('itemsInfo'),
			recipeData = F.getCacheData('recipeData'),
			//itemIdPatch = F.getCacheData('itemIdPatch'),
			//itemAction = F.getCacheData('itemAction'),
			//itemFood = F.getCacheData('itemFood'),
			//ItemUiCategory = F.getStaticData('item_ui_category'),
			//ClassJobCategory = F.getStaticData('classjobcategory'),
			//baseparamChs = F.getStaticData('baseparam_chs'),
			JobChs = F.getStaticData('job_chs'),
			recipLeve = F.getStaticData('recipLeve'),
			ElementMap = F.getConfigData('ElementMap'),
			HashJob = F.getConfigData('HashJob'),
			recipeid = itemsInfo[id]["rid"],
			CssClass = "label label-badge label-info",
			template = {
				0:'<span class="{0}" title = "{2}">{1}</span>',
				1:'<div class="my_style1" style="padding: 3px" >{0}</div>'
			}
		if(recipeid.length == 0) return "";
		var recipes = recipeData[recipeid[0]];
		for(var i = 0; i < recipeid.length; i++) {
			if(recipeData[recipeid[i]][2] == HashJob[F.Config.job]){
				recipes = recipeData[recipeid[i]];
				break;
			}
		}
		var job = JobChs[recipes[2] + 8];
		//等級id,等級，星級，難度，品質，耐久
		//250,60,4,1968,13187,70
		var leve = recipLeve[recipes[3]]
		var start = ["", "★", "★★", "★★★", "★★★★"];
		var re = template[0].format("label label-primary", "制作情报 ({0})".format(showName),showTitle) + "<br/>";
		var re2 = ""
		re2 += template[0].format("label label-dot label-success", "","") + " ";
		re2 += template[0].format("label label-badge label-danger", "rid： " + recipeid,"rl： " + JSON.stringify(leve)) + "<br/>";
		
		re2 += template[0].format("label label-dot label-success", "","") + " ";
		re2 += template[0].format(CssClass, job[1] + "(" + job[2] + ") Lv" + leve[1] + start[leve[2]]) + " ";
		//re2 += template[0].format("label label-dot label-success","") + " ";
		re2 += template[0].format("label label-badge label-danger", "产量： " + recipes[5],"") + "<br/>";
		//re += "产量： "+recipes[5] +" ";
		var nandu = Number(recipes[29]) / 100;
		var pingzhi = Number(recipes[30]) / 100;
		var naijiu = Number(recipes[31]) / 100;
		//console.log("leve = ", leve);
		//console.log("nandu = " + nandu + " pingzhi = " + pingzhi + " naijiu = " + naijiu);
		re2 += template[0].format("label label-dot label-success", "","") + " ";
		re2 += template[0].format(CssClass, "耐久：" + Math.floor(leve[5] * naijiu),"耐久 = ("+recipes[31]+" / 100) * "+leve[5]) + " ";
		//re2 += template[0].format("label label-dot label-success","") + " ";
		re2 += template[0].format(CssClass, "难度：" + Math.floor(leve[3] * nandu),"难度 = ("+recipes[29]+" / 100) * "+leve[3]) + " ";
		//re2 += template[0].format("label label-dot label-success","") + " ";
		re2 += template[0].format(CssClass, "品质：" + Math.floor(leve[4] * pingzhi),"品质 = ("+recipes[30]+" / 100) * "+leve[4]) + " ";
		re2 += "<br/>"
		if(recipes[26] != 0) {
			re2 += template[0].format("label label-dot label-success", "","") + " ";
			re2 += template[0].format("label label-badge label-warning", ElementMap[recipes[26]][0],"") + "<br>";
		}
		//re += "HQ制作 "+recipes[39] +"<br>";
		//re += "簡易制作 "+recipes[38] +"<br>";
		re2 += template[0].format("label label-dot label-success", "","") + " ";
		re2 += template[0].format("label label-badge label-warning", "HQ制作 " + (recipes[39] ? "○" : "x"),"") + "<br>";
		re2 += template[0].format("label label-dot label-success", "","") + " ";
		re2 += template[0].format("label label-badge label-warning", "简易制作 " + (recipes[38] ? "○" : "x"),"") + "<br>";
		if(Number(recipes[33]) > 0) {
			re2 += template[0].format("label label-dot label-success", "","") + " ";
			re2 += template[0].format("label label-badge label-danger", "制作条件：作业精度 " + recipes[33],"") + "<br>";
		}
		if(Number(recipes[34]) > 0) {
			re2 += template[0].format("label label-dot label-success", "","") + " ";
			re2 += template[0].format("label label-badge label-danger", "制作条件：加工精度 " + recipes[34],"") + "<br>";
		}
		if(Number(recipes[35]) > 0) {
			re2 += template[0].format("label label-dot label-success", "","") + " ";
			re2 += template[0].format("label label-badge label-danger", "简易制作条件：作业 " + recipes[35],"") + "<br>";
		}
		if(Number(recipes[36]) > 0) {
			re2 += template[0].format("label label-dot label-success", "","") + " ";
			re2 += template[0].format("label label-badge label-danger", "简易制作条件：加工 " + recipes[36],"") + "<br>";
		}
//		if(Number(recipes[40]) > 0) {
//			re2 += template[0].format("label label-dot label-success", "") + " ";
//			re2 += template[0].format("label label-badge label-danger", "制作条件：buff id = " + recipes[40]) + "<br>";
//		}
//		if(Number(recipes[41]) > 0) {
//			re2 += template[0].format("label label-dot label-success", "") + " ";
//			re2 += template[0].format("label label-badge label-danger", "制作条件：装备 id = " + recipes[41]) + "<br>";
//		}
		if(recipes[43]) {
			re2 += template[0].format("label label-dot label-success", "","") + " ";
			re2 += template[0].format("label label-badge label-danger", "制作条件：专家 ","") + "<br>";
		}
		re += template[1].format(re2);
		//console.log(re)
		return re;
	},
	
DrawLinks:function (id,isWeb){
		var F = this,
		data={
			"LodeStone":F.getLodestoneUrl(id),
			"XivDb":F.getXivdbUrl(id),
			"Garlandtools":F.getGarlandtoolsUrl(id),
			"灰机wiki":F.getWikiUrl(id)
		},
		CssClass = "label label-badge label-info",
		re = '<span class="label label-primary">链接</span><br>';
		for(var i in data){
			if(data[i]){
				re += '<i class="icon icon-link"></i>';
				if(isWeb) re += '<span class="'+CssClass+'">'+i+'</span><button class="btn btn-mini mybtn_mini" type="button" id="'+data[i]+'" onclick="copy(this.id)">复制</button><br>';
				else re +='<a href="'+data[i]+'" target="blank"><span class="'+CssClass+'">点击前往'+i+'查看</span></a><br>';
			}
		}
		return re;
	},

DrawCalList:function (){
	var F = this,	
		itemsData = F.getCacheData('itemsData'),
		itemsInfo = F.getCacheData('itemsInfo'),
		recipeData = F.getCacheData('recipeData'),
		recipLeve = F.getStaticData('recipLeve'),
		//JobChs = F.getStaticData('job_chs'),
		//itemIdPatch = F.getCacheData('itemIdPatch'),
		//Patch405 = F.getCacheData('Patch405'),
		Start = F.getConfigData('Start'),
		//itemAction = F.getCacheData('itemAction'),
		//itemFood = F.getCacheData('itemFood'),
		ItemUiCategory = F.getStaticData('item_ui_category'),
		
		JobGj = F.getStaticData('job_gj'),
		//ClassJobCategory = F.getStaticData('classjobcategory'),
		//baseparamChs = F.getStaticData('baseparam_chs');
	template = {
		0:'<li class="tager_item_list it_uc_{3}" arr={4}><input class="my_number" type="number" value="{6}" arr={4} min="0" max="999"><a href="javascript:void(0)">{2}<span class="name" title="{5}">{0} </span><span class="icons">{1}</span></a></li>',
		1:'<img class="my_img my_pull_left" src="{0}" title="{1}" />',
		2:'<img class="my_img3 my_pull_left" src="{0}" title="{1}" />',
		3:'<i class="icon icon-circle-thin item_dye"  title="可染色" ></i>'
			};
	var dye_html = '<i class="icon icon-circle-thin item_dye"  title="可染色" ></i>';
	var html = "";
	var ucs = {};
	for(var id in F.Config.itemMap) {
		var count = F.Config.itemMap[id];
		var itemInfo = itemsInfo[id];
		var itemData = itemsData[id];
		var canDye = itemData[28];
		var recipe = recipeData[itemInfo["rid"][0]];
		var  leve = recipLeve[recipe[3]];
		var ItemNames = F.getItemNamesById(id);
		var showName  = F.getItemNameById(id);
		var ItemCategory = ItemUiCategory[itemInfo["uc"]];
		var JobData = JobGj[recipe[2]];
			ucs[ItemCategory[0]] = [ItemCategory[0],ItemCategory[1],ItemCategory[2],(ucs[ItemCategory[0]] == undefined ? 0 : ucs[ItemCategory[0]][3])+1];
			html += '<li class="tager_item_list it_uc_'+ItemCategory[0]+'" arr="'+itemInfo["id"]+'">'
				 +  '<input class="my_number" type="number" value="'+count+'" arr="'+id+'" min="0" max="999">'
				 +  '<a href="javascript:void(0)">'
				 +  '<img class="my_img my_pull_left" src="'+F.getImgPath(itemInfo["icon"],recipe)+'"/>'
				 +  '<span class="name" title="'+itemInfo["id"]+'">'+showName+' </span>'
				 +  '<span class="icons">'
				 +  '<img class="my_img3 my_pull_left" src="'+F.checkImgPath(JobData[5])+'"/>'+JobData[1] + "("+JobData[2] +") lv" + leve[1] + Start[leve[2]]
				 +  '</span></a></li>';
		}
		$("#left_item_list_2").html(html);
		F.DrawUc(ucs,"_2");
	},
	
DrawHistory:function (){
		var HTML = "";
		for(var i in this.Config.History){
			HTML += '<li>'
			+i+'<span class="btn btn-sm History-btn">加载</span><span class="btn btn-sm History-btn">删除</span>'
			+'</li>';
		}
		$('#tab2Content3').html('<ul>'+HTML+'</ul>');
	},
	
DrawSum:function(){
	var F = this,
		SumMap = F.getConfigData('SumMap'),
		SumMapData = F.getConfigData('SumMapData');
	for(var i=0;i<SumMap.length;i++){
		F.Tree[i+2].reload(F.getTreeFromArray(SumMapData[SumMap[i]]));
	}
},
	
DoSe : function(e){
		if(e.keyCode == "13")
			this.DrawSe();
	},
	
DrawSe : function(e){
	var F = this,itemsInfo = F.getCacheData('itemsInfo');
	
	console.log("开始绘制搜索");
	var inputstr = document.getElementById("input_se").value.trim();
	console.log("inputstr = ",inputstr);
	if(inputstr.length <= 0){
		new $.zui.Messager('错误:无效的关键字', {  type: 'danger'}).show();
		return;
	}
	var out = [];
	for(var k in itemsInfo){
		var i = itemsInfo[k];
		if(i["cname"].indexOf(inputstr) >=0 || i["ename"].toLowerCase().indexOf(inputstr.toLowerCase()) >=0 || i["jname"].indexOf(inputstr) >=0){
				out.push(i["id"])
		}
	}
	if(out.length > 3000){
		new $.zui.Messager('提示:通过筛选找到了【{0}】个物品<br/>列表大于3000,检索条件没有意义,放弃列举'.format(out.length), {  type: 'danger'}).show();
		console.log("无效的检索条件3");
		return;
	}
		
	var succStr = "";
	if(out.length == 0){
		succStr = '提示:当前检索条件未找到物品'
	}else
	succStr = '提示:找到了【{0}】个物品'.format(out.length);
		
	new $.zui.Messager(succStr, {  type: 'success'}).show();
		F.DrawItemList(out,true);
},
	
DrawSeUc:function (){
		var F = this,
			ClassJobCategory = F.getStaticData('classjobcategory'),
			html = "",
			template = {
				0:'<option value="{0}">{1}</option>',
				1:'<select data-placeholder="选择职业..." id="se_uc"  class="chosen-select form-control" style="position: absolute; z-index: 5;" >{0}</select>'
			};
		html += '<option value=""> </option>';
		for(var i in ClassJobCategory){
			if(ClassJobCategory[i][1] != "")
			html += template[0].format(ClassJobCategory[i][0],ClassJobCategory[i][1]);
		}
		$("#se_uc_div").html(template[1].format(html));
		$('#se_uc').chosen({
			allow_single_deselect: true,
			placeholder_text:"",
			search_contains: true,
			width: '100%'
		});
		F.DrawUcgUc();
		F.DrawUcgPatch();
	},
	
DrawUcgUc:function(){
		var F = this,
			ItemUiCategory = F.getStaticData('item_ui_category'),
			html = '<option value=""> </option>',
			template={
				0:'<option value="{0}">{1}</option>',
				1:'<select data-placeholder="选择物品类型..." id="ucg_uc"  class="chosen-select form-control" style="position: absolute; z-index: 5;" >{0}</select>'
			};
		for(var i in ItemUiCategory){
			if(ItemUiCategory[i][1] != "")
			html += template[0].format(ItemUiCategory[i][0],ItemUiCategory[i][1]);
		}
		$("#ucg_uc_div").html(template[1].format(html));
		//console.log(html1.format(html))
		$('#ucg_uc').chosen({
			allow_single_deselect: true,
			placeholder_text:"",
			search_contains: true,
			width: '100%'
		});
	
	},
	
DrawUcgPatch:function(){
		var F = this,
			Patch405 = F.getCacheData('Patch405'),
			out = "<option value='0' title=''>所有版本</option>",
			patchData = [],
			template = {  
				0:"<option value='{0}' title='{2}'>{1}</option>",
			};
		for(var i in Patch405){
			patchData.push(Patch405[i]);
		}
		patchData.sort(function(a,b){
			return b.patch - a.patch;
		})
		for(var i = 0; i < patchData.length;i++){
			var tem = patchData[i];
			out += template[0].format(tem["patch"],tem["number"],tem["name"]);
		}
		$("#patch1").html(out);
		$("#patch2").html(out);
	}

}
	$.extend(window.Bobo,Bobo);
})()
