package zc.games.ffxiv.tools.model;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "name", "parent"})
public class Job {
	
	private static final Set<Job> CLASSES = Sets.newHashSet();

	private String id;
	
	private String name;
	
	private String englishName;
	
	private String englishAbbr;
	
	private Job parent;
	
	private int primaryCategoryId;

	@Builder(toBuilder = true)
	public Job(@NonNull String id, String name, String englishName, String englishAbbr, 
			Job parent, int primaryCategoryId) {
		this.id = id;
		this.name = name;
		this.englishName = englishName;
		this.englishAbbr = englishAbbr;
		this.parent = parent;
		this.primaryCategoryId = primaryCategoryId;
		
		if (this.parent != null) {
			CLASSES.add(this.parent);
		}
	}
	
	public boolean isClass() {
		return CLASSES.contains(this);
	}
	
	public boolean isDerived() {
		return this.parent != null;
	}
	
	public Optional<Job> parent() {
		return Optional.ofNullable(this.parent);
	}

	@Value
	public static class Category {
		
		public static final int DoH_JOB_COUNT = 8;
		
		public static Category ALL;
		
		public static Category DoH;
		
		private int code;
		
		private String label;
		
		private String englishLabel;
		
		private List<Job> jobs;

		private void bindToStatic() {
			if (this.englishLabel.equals("Disciple of the Hand")) {
				DoH = this;
			}
			if (this.englishLabel.equals("All Classes")) {
				ALL = this;
			}
		}

		@Builder
		public Category(int code, String label, String englishLabel, List<Job> jobs) {
			this.code = code;
			this.label = label;
			this.englishLabel = englishLabel;
			this.jobs = ImmutableList.copyOf(jobs);
			
			bindToStatic();
		}
	}
}
