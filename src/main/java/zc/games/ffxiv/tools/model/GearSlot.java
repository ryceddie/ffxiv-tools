package zc.games.ffxiv.tools.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableList;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum GearSlot {

	MAIN_HAND(ImmutableList.<String>builder()
			.addAll(Predefined.ARMS_2_0)
			.addAll(Predefined.TOOLS_MAIN_HAND)
			.add("84", "87", "88", "89", "96", "97", "98", "105", "106", "107")
			.build()),
	OFF_HAND(ImmutableList.<String>builder()
			.add("11")
			.addAll(Predefined.TOOLS_OFF_HAND)
			.add("99")
			.build()),
	HEAD(Collections.singletonList("34")),
	BODY(Collections.singletonList("35")),
	HANDS(Collections.singletonList("37")),
	WAIST(Collections.singletonList("39")),
	LEGS(Collections.singletonList("36")),
	FOOTS(Collections.singletonList("38")),
	EARS(Collections.singletonList("41")),
	NECK(Collections.singletonList("40")),
	WRIST(Collections.singletonList("42")),
	FINGER1(Collections.singletonList("43")),
	FINGER2(Collections.singletonList("43")),
	SOUL(Collections.singletonList("62"));
	
	public static final GearSlot[] VALUES = values();
	
	public static Optional<GearSlot> forCategory(String categoryCode) {
		return Arrays.asList(VALUES).stream()
				.filter(slot -> slot.itemCategoryIds().contains(categoryCode))
				.findFirst();
	}
	
	private List<String> itemCategoryIds;
	
	public List<String> itemCategoryIds() {
		return this.itemCategoryIds;
	}
	
	interface Predefined {
		
		static final List<String> ARMS_2_0 = IntStream.rangeClosed(1, 10)
				.boxed().map(String::valueOf).collect(Collectors.toList());
		
		static final List<String> TOOLS_MAIN_HAND = IntStream.rangeClosed(12, 32)
				.filter(it -> it % 2 == 0)
				.boxed().map(String::valueOf).collect(Collectors.toList());
		
		static final List<String> TOOLS_OFF_HAND = IntStream.rangeClosed(12, 32)
				.filter(it -> it % 2 == 1)
				.boxed()
				.map(String::valueOf)
				.collect(Collectors.toList());
	}
}
