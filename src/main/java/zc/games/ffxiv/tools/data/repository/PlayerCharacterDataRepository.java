package zc.games.ffxiv.tools.data.repository;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import lombok.Value;
import zc.games.ffxiv.tools.data.ActorState;
import zc.games.ffxiv.tools.data.DataException;
import zc.games.ffxiv.tools.data.PlayerCharacterState;
import zc.games.ffxiv.tools.model.Inventory;

public class PlayerCharacterDataRepository implements PlayerCharacterRepository {
	
	private ConcurrentMap<String, PlayerCharacterState> stateIndex;
	
	private ConcurrentMap<String, ActorState> actorStateIndex;
	
	private ConcurrentMap<String, CharacterInventories> inventoryDataIndex;
	
	private Persister persister;
	
	public PlayerCharacterDataRepository(String dataDirPath) {
		this(new FilePersister(dataDirPath));
	}

	public PlayerCharacterDataRepository(Persister persister) {
		this.stateIndex = Maps.newConcurrentMap();
		this.inventoryDataIndex = Maps.newConcurrentMap();
		this.actorStateIndex = Maps.newConcurrentMap();
		this.persister = persister;
	}

	@PostConstruct
	public void init() {
		this.persister.init();
		this.persister.loadAll(PlayerCharacterState.class)
				.forEach(state -> this.stateIndex.put(state.getName(), state));
		this.persister.loadAll(ActorState.class)
				.forEach(state -> this.actorStateIndex.putIfAbsent(state.getKey().toString(), state));
		this.persister.loadAll(CharacterInventories.class)
				.forEach(inv -> this.inventoryDataIndex.put(inv.getCharacterKey(), inv));
	}
	
	@Override
	public void updateState(PlayerCharacterState state) {
		PlayerCharacterState oldState = this.stateIndex.put(state.getName(), state);
		if (!state.equals(oldState)) {
			this.persister.persist(state.getName(), state);
		}
//		PlayerCharacterData pcData = PlayerCharacterData.builder().state(state).build();
//		this.index.merge(state.getName(), pcData, (oldData, newData) -> {
//			return oldData.toBuilder().state(state).build();
//		});
	}
	
	@Override
	public void updateActorState(ActorState state) {
		ActorState oldState = this.actorStateIndex.put(state.getKey().toString(), state);
		if (!state.equals(oldState)) {
			this.persister.persist(state.getKey().toString(), state);
		}
	}

	@Override
	public void updateInventoryData(String characterKey, List<Inventory> inventoryData) {
		CharacterInventories newData = new CharacterInventories(characterKey, inventoryData);
		CharacterInventories oldData = this.inventoryDataIndex.get(characterKey);
		if (newData.size() == 0 && oldData != null && oldData.size() > 5) {
			//XXX Empty data is ambiguous for now and will be ignored in most cases.
			return;
		}
		this.inventoryDataIndex.put(characterKey, newData);
		if (!newData.equals(oldData)) {
			this.persister.persist(characterKey, newData);
		}
//		PlayerCharacterData pcData = PlayerCharacterData.builder()
//				.inventoryData(inventoryData)
//				.build();
//		this.index.merge(characterKey, pcData, (oldData, newData) -> {
//			return oldData.toBuilder().inventoryData(inventoryData).build();
//		});
	}

	@Override
	public Optional<PlayerCharacterState> stateOf(String characterKey) {
		return Optional.ofNullable(this.stateIndex.get(characterKey));
	}

	@Override
	public Collection<PlayerCharacterState> allStates() {
		return this.stateIndex.values();
	}

	@Override
	public Collection<ActorState> actorStateOf(Predicate<String> keyPredicate) {
		return this.actorStateIndex.keySet().stream()
				.filter(keyPredicate)
				.map(this.actorStateIndex::get)
				.collect(Collectors.toList());
	}

	@Override
	public List<Inventory> inventoryStateOf(String characterKey) {
		return Optional.ofNullable(this.inventoryDataIndex.get(characterKey))
				.map(CharacterInventories::getListing)
				.orElse(Collections.emptyList());
	}

	@Override
	public Multimap<String, Inventory> inventoryStateOf(Predicate<String> keyPredicate) {
		return this.inventoryDataIndex.keySet().stream()
				.filter(keyPredicate)
				.collect(Multimaps.flatteningToMultimap(
						key -> key, 
						key -> this.inventoryDataIndex.get(key).getListing().stream(), 
						ArrayListMultimap::create));
	}

	@Value
	private static class CharacterInventories {
		
		private String characterKey;
		
		private List<Inventory> listing;
		
		int size() {
			return this.getListing().stream().collect(Collectors.summingInt(Inventory::size));
		}
	}
	
	public interface Persister {
		
		void init();

		void persist(String key, Object data);

		<T> Iterable<T> loadAll(Class<T> type);
		
		<T> Optional<T> load(String key, Class<T> type);		
	}
	
	private static class FilePersister implements Persister {
		
		private final String rootPath;
		
		private final ObjectMapper jsonMapper;
		
		FilePersister(String rootPath) {
			Preconditions.checkArgument(new File(rootPath).isDirectory(), "root path is not directory: " + rootPath);
			this.rootPath = rootPath;
			this.jsonMapper = new ObjectMapper()
					.registerModule(new ParameterNamesModule())
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);
		}

		public void init() {
			File rootDir = new File(this.rootPath);
			if (!rootDir.exists()) {
				rootDir.mkdirs();
			}
		}
		
		public void persist(String key, Object data) {
			File resultFile = new File(this.pathOf(key, data.getClass()));
			if (!resultFile.getParentFile().exists()) {
				resultFile.getParentFile().mkdirs();
			}
			try {
				// XXX how to control character encoding?
				this.jsonMapper.writeValue(resultFile, data);
			} catch (IOException e) {
				throw new DataException("failed to write json data to " + resultFile, e);
			}
		}
		
		public <T> Iterable<T> loadAll(Class<T> type) {
			File[] entries = new File(this.rootPath).listFiles();
			return FluentIterable.from(Arrays.asList(entries))
					.transform(file -> this.load(file.getName(), type).orElse(null))
					.filter(Predicates.notNull());
		}
		
		public <T> Optional<T> load(String key, Class<T> type) {
			File sourceFile = new File(this.pathOf(key, type));
			if (!sourceFile.exists()) {
				return Optional.empty();
			}
			try {
				return Optional.of(this.jsonMapper.readValue(sourceFile, type));
			} catch (IOException e) {
				throw new DataException("failed to read json data from " + sourceFile, e);
			}
		}
		
		private String pathOf(String characterKey, Class<?> dataType) {
			Preconditions.checkArgument(!Strings.isNullOrEmpty(dataType.getSimpleName()), "invalid data type: " + dataType);
			return String.format("%s/%s/%s.json", this.rootPath, characterKey, dataType.getSimpleName());
		}
	}
}
