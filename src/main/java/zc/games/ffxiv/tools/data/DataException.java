package zc.games.ffxiv.tools.data;

public class DataException extends RuntimeException {

	public DataException(String message) {
		super(message);
	}

	public DataException(String message, Throwable cause) {
		super(message, cause);
	}
}
