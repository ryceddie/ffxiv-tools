package zc.games.ffxiv.tools.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.io.Resources;

import lombok.extern.slf4j.Slf4j;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiInventories;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiInventory;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiInventoryCell;

@RunWith(SpringRunner.class)
@JsonTest
@Slf4j
public class ApiInventoriesTest {
	
	@Autowired
	private JacksonTester<ApiInventories> jackson;

	@Test
	public void json() throws IOException {
		ApiInventories inventories = new ApiInventories(Arrays.asList(
				new ApiInventory("test", Arrays.asList(
						new ApiInventoryCell(0, "1", true, 10),
						new ApiInventoryCell(1, "2", false, 10)))));
		String jsonString = this.jackson.write(inventories).getJson();
		log.info("json: {}", jsonString);
		this.jackson.parseObject(jsonString);
		
		ApiInventories parsed = this.jackson.parseObject(
				Resources.toByteArray(Resources.getResource("json/inventories.json")));
		
		assertThat(parsed.getListing()).hasSize(34);
	}
}
