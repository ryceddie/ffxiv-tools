package zc.games.ffxiv.tools.data.xivapi;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.CharMatcher;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;

public interface Models {

	@Getter
	@Setter
	public class ClassJob {
		
		private int id;
		
		private int categoryId;
		
		@JsonProperty("ClassJobParentTargetID")
		private int parentId;
		
		@JsonProperty("ID")
		public int getId() {
			return this.id;
		}

		@JsonProperty("ClassJobCategoryTargetID")
		public int getCategoryId() {
			return categoryId;
		}
	}
	
	public class ClassJobCategory {
		
		private static final CharMatcher UPPER_LETTER = CharMatcher.inRange('A', 'Z');
		
		@JsonAnySetter
		private Map<String, Object> dynamicProperties = Maps.newHashMap();

		public List<String> getIncludedClassJobs() {
			return this.dynamicProperties.entrySet()
					.stream()
					.filter(entry -> isClassJobProperty(entry.getKey(), entry.getValue()))
					.filter(entry -> entry.getValue().equals(1))
					.map(Map.Entry::getKey)
					.collect(Collectors.toList());
		}

		private boolean isClassJobProperty(String name, Object value) {
			return name.length() == 3 
					&& UPPER_LETTER.matchesAllOf(name) 
					&& value instanceof Integer;
		}
	}
}
