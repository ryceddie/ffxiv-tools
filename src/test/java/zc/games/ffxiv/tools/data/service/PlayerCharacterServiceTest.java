package zc.games.ffxiv.tools.data.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableMap;
import com.siebre.test.TestHelper;

import zc.games.ffxiv.tools.data.ActorState;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.PlayerCharacterInventories;
import zc.games.ffxiv.tools.data.PlayerCharacterState;
import zc.games.ffxiv.tools.data.repository.PlayerCharacterDataRepository;
import zc.games.ffxiv.tools.model.ConcreteItem;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.InventoryCell;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.PlayerCharacter;

public class PlayerCharacterServiceTest {
	
	private PlayerCharacterService service;
	
	private TestHelper testHelper = new TestHelper(this.getClass());
	
	private BaseData baseData = mock(BaseData.class);
	
	private ActorService actorService = mock(ActorService.class);
	
	@Before
	public void setup() {
//		Persister persister = mock(Persister.class);
		File dataDir = testHelper.getOutputTarget();
		
		PlayerCharacterDataRepository repository = new PlayerCharacterDataRepository(dataDir.getPath());
		repository.init();
		this.service = new PlayerCharacterService(baseData, repository, this.actorService);
	}
	
	@After
	public void reset() {
		Mockito.reset(this.actorService);
		testHelper.getOutputFile("tinus/CharacterInventories.json").delete();
	}

	@Test
	public void update_pc() {
		// setup
		Job job1 = Job.builder().id("job1").build();
		Job job2 = Job.builder().id("job2").build();
		when(this.baseData.getJobByAbbr(job1.getId())).thenReturn(job1);
		when(this.baseData.getJobByAbbr(job2.getId())).thenReturn(job2);
		
		// when
		this.service.update("tinus", PlayerCharacterState.builder()
				.name("tinus")
				.currentJob(job1)
				.jobInfo(ImmutableMap.of(job1.getId(), 10))
				.build());
		
		// expect
		PlayerCharacter pc = this.service.playerCharacterByKey("tinus").get();
		
		assertThat(pc.getName()).isEqualTo("tinus");
		assertThat(pc.getCurrentJob()).isEqualTo(job1);
		assertThat(pc.getLevel(job1)).isEqualTo(10);
		
		// when
		this.service.update("tinus", PlayerCharacterState.builder()
				.name("tinus")
				.currentJob(job2)
				.jobInfo(ImmutableMap.of(job2.getId(), 15))
				.build());
		
		// expect
		pc = this.service.playerCharacterByKey("tinus").get();
		
		assertThat(pc.getName()).isEqualTo("tinus");
		assertThat(pc.getCurrentJob()).isEqualTo(job2);
		assertThat(pc.getLevel(job1)).isEqualTo(0);		
		assertThat(pc.getLevel(job2)).isEqualTo(15);
	}
	
	@Test
	public void update_inventories() {
		// setup
		when(this.actorService.retainer())
		.thenReturn(Optional.of(new ActorState("tinus", "foo", ActorState.Type.RETAINER)));
		this.service.update("tinus", PlayerCharacterState.builder()
				.name("tinus")
				.currentJob(Job.builder().id("job01").build())
				.build());
		
		// given
		this.service.update(new PlayerCharacterInventories("tinus", Arrays.asList(
				Inventory.builder()
				.code("test")
				.type(Inventory.Type.PRIMARY)
				.build())));
		
		// expect
		PlayerCharacter pc = this.service.playerCharacterByKey("tinus").get();
		
		assertThat(pc.getInventories()).hasSize(1);
	}
	
	@Test
	public void update_retainer_inventories() {
		// setup
		when(this.actorService.retainer())
		.thenReturn(Optional.of(new ActorState("tinus", "foo", ActorState.Type.RETAINER)));
		this.service.update("tinus", PlayerCharacterState.builder()
				.name("tinus")
				.currentJob(Job.builder().id("job01").build())
				.build());
		
		// given
		this.service.update(new PlayerCharacterInventories("tinus", Arrays.asList(
				Inventory.builder()
				.code("test")
				.type(Inventory.Type.PRIMARY)
				.build(),
				
				Inventory.builder()
				.code("test")
				.type(Inventory.Type.RETAINER)
				.build())));
		
		// expect
		PlayerCharacter pc = this.service.playerCharacterByKey("tinus").get();
		
		assertThat(pc.getInventories()).hasSize(1);
		assertThat(pc.getRetainers()).hasSize(1);
		assertThat(pc.getRetainers().get(0).getName()).isEqualTo("foo");
		assertThat(pc.getRetainers().get(0).getInventories()).hasSize(1);
	}
	
	@Test
	public void cache_gear_suite() {
		Job job = Job.builder().id("job01").build();
		this.service.update("tinus", PlayerCharacterState.builder()
				.name("tinus")
				.currentJob(job)
				.build());
		Item weapon = Item.builder()
				.id("item01")
				.name("test-item")
				.category(Item.Category.UNKNOWN)
				.level(50)
				.jobs(Arrays.asList(job))
				.build();
		this.service.update(new PlayerCharacterInventories("tinus", Arrays.asList(
				Inventory.builder()
				.code("test")
				.type(Inventory.Type.EQUIPMENT)
				.cells(Arrays.asList(
						InventoryCell.builder()
						.index(0)
						.item(weapon)
						.build()))
				.build())));
		
		Map<GearSlot, ConcreteItem> suite = this.service.suiteOf("tinus", job);
		
		assertThat(suite).hasSize(1);
		assertThat(this.service.suiteOf("tinus", Job.builder().id("job02").build())).isEmpty();
		
		Item weaponLow = Item.builder()
				.id("item02")
				.name("test-item")
				.category(Item.Category.UNKNOWN)
				.level(40)
				.jobs(Arrays.asList(job))
				.build();
		
		this.service.update(new PlayerCharacterInventories("tinus", Arrays.asList(
				Inventory.builder()
				.code("test")
				.type(Inventory.Type.EQUIPMENT)
				.cells(Arrays.asList(
						InventoryCell.builder()
						.index(0)
						.item(weaponLow)
						.build()))
				.build())));
		
		suite = this.service.suiteOf("tinus", job);
		
		assertThat(suite.get(GearSlot.MAIN_HAND).getSpec()).isEqualTo(weapon);
		
		Item weaponHigh = Item.builder()
				.id("item03")
				.name("test-item")
				.category(Item.Category.UNKNOWN)
				.level(60)
				.jobs(Arrays.asList(job))
				.build();
		
		this.service.update(new PlayerCharacterInventories("tinus", Arrays.asList(
				Inventory.builder()
				.code("test")
				.type(Inventory.Type.EQUIPMENT)
				.cells(Arrays.asList(
						InventoryCell.builder()
						.index(0)
						.item(weaponHigh)
						.build()))
				.build())));
		
		suite = this.service.suiteOf("tinus", job);
		
		assertThat(suite.get(GearSlot.MAIN_HAND).getSpec()).isEqualTo(weaponHigh);
	}
}
