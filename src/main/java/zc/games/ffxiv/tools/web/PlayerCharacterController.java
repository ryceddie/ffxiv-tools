package zc.games.ffxiv.tools.web;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.collect.ImmutableList;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.PlayerCharacterInventories;
import zc.games.ffxiv.tools.data.PlayerCharacterState;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.service.PlayerCharacterMissingException;
import zc.games.ffxiv.tools.data.service.PlayerCharacterService;
import zc.games.ffxiv.tools.model.Inventories;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.Inventory.Type;
import zc.games.ffxiv.tools.model.InventoryCell;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.PlayerCharacter;

@RestController
@RequestMapping("/api/player-characters")
@AllArgsConstructor
public final class PlayerCharacterController {
	
	private PlayerCharacterService pcDataService;
	
	private ModelConverter converter;
	
	public PlayerCharacterController(
			PlayerCharacterService pcDataService, ItemRepository items, BaseData baseData) {
		this.pcDataService = pcDataService;
		this.converter = new ModelConverter(items, baseData);
	}
	
	@PutMapping(path = "/{characterKey}")
	public void updatePlayerState(@PathVariable String characterKey, @RequestBody ApiPlayerCharacter pcData) {
		this.pcDataService.update(characterKey, this.converter.toDomainModel(pcData));
	}

	@PutMapping("/{characterKey}/inventories")
	public void updateInventoryState(@PathVariable String characterKey, @RequestBody ApiInventories inventoryData) {
		List<Inventory> inventories = inventoryData.getListing().stream()
				.map(this.converter::toDomainModel)
				.collect(Collectors.toList());
		this.pcDataService.update(new PlayerCharacterInventories(characterKey, inventories));
	}
	
	@GetMapping
	public ApiPlayerCharacters getPlayerCharacterInfo() {
		return new ApiPlayerCharacters(this.pcDataService.playerCharacterKeys());
	}
	
	@GetMapping(path = "/{characterKey}")
	public PlayerCharacter getPlayerCharacter(@PathVariable String characterKey) {
		return this.pcDataService.playerCharacterByKey(characterKey)
				.orElseThrow(() -> new PlayerCharacterMissingException(characterKey));
	}
	
	@GetMapping("/{characterKey}/inventories")
	public Inventories getInventories(@PathVariable String characterKey) {
		List<Inventory> list = this.pcDataService.playerCharacterByKey(characterKey)
				.map(PlayerCharacter::getInventories)
				.map(it -> (List<Inventory>) ImmutableList.copyOf(it))
				.orElse(Collections.emptyList());
		return new Inventories(list);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleException(PlayerCharacterMissingException exception) {
	}
	
	@Value
	public static class ApiPlayerCharacter {
		
		private String name;
		
		private String jobAbbr;
		
		private Map<String, Integer> jobLevels;
	}
	
	@Value
	public static class ApiPlayerCharacters {
		
		private Collection<String> names;
	}

	@Value
	@Builder
	public static class ApiInventoryCell {
		
		private int index;
		
		private String id;
		
		private boolean hq;
		
		private int quantity;
	}
	
	@Value
	public static class ApiInventory {
		
		private String type;
		
		private List<ApiInventoryCell> cells;
	}
	
	@Value
	public static class ApiInventories {
		
		private List<ApiInventory> listing;

		@JsonCreator
		public ApiInventories(List<ApiInventory> listing) {
			this.listing = listing;
		}
	}
	
	@AllArgsConstructor
	public static class ModelConverter {
		
		private ItemRepository items;
		
		private BaseData baseData;
		
		public Inventory toDomainModel(ApiInventory inv) {
			return Inventory.builder()
					.code(inv.getType())
					.type(parseInventoryType(inv.getType()))
					.cells(inv.getCells().stream()
							.map(cell -> {
								return InventoryCell.builder()
										.index(cell.getIndex())
										.item(
												items.findById(cell.getId())
												.orElse(Item.unknown(cell.getId())))
										.hq(cell.isHq())
										.quantity(cell.getQuantity())
										.build();
							})
							.collect(Collectors.toList()))
					.build();
		}
		
		public PlayerCharacterState toDomainModel(ApiPlayerCharacter apiModel) {
			return PlayerCharacterState.builder()
					.name(apiModel.getName())
					.currentJob(this.baseData.getJobByAbbr(apiModel.getJobAbbr()))
					.jobInfo(apiModel.jobLevels)
					.build();
		}

		private Type parseInventoryType(String code) {
			Type result = null;
			if (code.startsWith("INVENTORY_")) {
				result = Type.PRIMARY;
			} else if (code.equals("CRYSTALS")) {
				result = Type.CRYSTAL;
			} else if (code.equals("QUESTS_KI")) {
				result = Type.QUEST;
			} else if (code.equals("CURRENT_EQ")) {
				result = Type.EQUIPMENT;
			} else if (code.startsWith("AC_")) {
				result = Type.ARMOR;
			} else if (code.startsWith("HIRE_")) {
				result = Type.RETAINER;
			} else if (code.startsWith("CHOCOBO")) {
				result = Type.CHOCOBO;
			} else {
				result = Type.UNKNOWN;
			}
			return result;
		}
	}
}
