package zc.games.ffxiv.tools.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class InventoryCell {
	
	private int index;
	
	@NonNull
	private Item item;
	
	private int quantity;
	
	private boolean hq;
}