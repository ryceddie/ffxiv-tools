package zc.games.ffxiv.tools.web;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import zc.games.ffxiv.tools.data.PlayerCharacterInventories;
import zc.games.ffxiv.tools.data.service.PlayerCharacterService;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.Inventory.Type;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.PlayerCharacter;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiInventories;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiInventory;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ApiInventoryCell;
import zc.games.ffxiv.tools.web.PlayerCharacterController.ModelConverter;

@RunWith(SpringRunner.class)
@WebMvcTest(PlayerCharacterController.class)
public class PlayerCharacterControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper jsonMapper;
	
	@MockBean
	private PlayerCharacterService pcService;
	
	@MockBean
	private ModelConverter converter;

	@Test
	public void update_inventory_data() throws Exception {
		Inventory mockInventory = Inventory.builder()
				.code("test")
				.type(Type.PRIMARY)
				.build();
		when(this.converter.toDomainModel(Mockito.any(ApiInventory.class)))
		.thenReturn(mockInventory);
		
		ApiInventories inventoryData = new ApiInventories(Arrays.<ApiInventory>asList(
				new ApiInventory("primary", Arrays.asList(ApiInventoryCell.builder()
						.index(0)
						.quantity(1)
						.hq(false)
						.build())),
				new ApiInventory("chocobo", Arrays.asList(ApiInventoryCell.builder()
						.index(0)
						.quantity(10)
						.hq(true)
						.build()))));
		this.mockMvc.perform(put("/api/player-characters/{key}/inventories", "Tinus")
				.accept(MediaType.APPLICATION_JSON)
				.content(this.jsonMapper.writeValueAsString(inventoryData))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(status().isOk());
		
		verify(this.pcService).update(new PlayerCharacterInventories("Tinus", Collections.nCopies(2, mockInventory)));
	}
	
	@Test
	public void get_pc() throws Exception {
		Job job = Job.builder()
				.id("job-test")
				.build();
		when(this.pcService.playerCharacterByKey("Tinus"))
		.thenReturn(Optional.of(PlayerCharacter.builder()
				.name("Tinus")
				.currentJob(job)
//				.jobStates(new JobStates(Arrays.asList(new JobState(job, 1)), Collections.emptyList()))
				.build()));
		
		this.mockMvc.perform(get("/api/player-characters/{key}", "Tinus"))
		.andExpect(status().isOk());
	}
	
	@TestConfiguration
	static class ContextConfig {
		
		@Bean
		PlayerCharacterController pcController(PlayerCharacterService pcService, ModelConverter converter) {
			return new PlayerCharacterController(pcService, converter);
		}
	}
}
