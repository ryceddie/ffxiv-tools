package zc.games.ffxiv.tools.calculator;

import java.util.Collection;
import java.util.stream.Collectors;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.model.Recipe;

@Value
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "product")
@ToString(of = {"product", "recipe"})
public class CraftingAction {
	
	private Material product;
	
	private Recipe recipe;
	
	private transient Collection<CraftingAction> dependents;
	
	private transient Collection<CraftingAction> dependencies;
	
	String getJob() {
		return this.recipe.getJob();
	}

	Collection<CraftingAction> trunkDependencies() {
		return this.dependencies.stream()
				.filter(dep -> dep.getJob().equals(this.getJob()))
				.collect(Collectors.toList());
	}
	
	Collection<CraftingAction> branchDependencies() {
		return this.dependencies.stream()
				.filter(dep -> !dep.getJob().equals(this.getJob()))
				.collect(Collectors.toList());
	}

	Collection<CraftingAction> trunkDependents() {
		return this.dependents.stream()
				.filter(dep -> dep.getJob().equals(this.getJob()))
				.collect(Collectors.toList());
	}
}