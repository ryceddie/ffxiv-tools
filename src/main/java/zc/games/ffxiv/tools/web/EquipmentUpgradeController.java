package zc.games.ffxiv.tools.web;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.Builder;
import zc.games.ffxiv.tools.calculator.GearCalculator;
import zc.games.ffxiv.tools.calculator.GearCalculator.Options;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.service.PlayerCharacterMissingException;
import zc.games.ffxiv.tools.data.service.PlayerCharacterService;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.PlayerCharacter;

@RestController
@Builder
public class EquipmentUpgradeController {
	
	private GearCalculator calculator;
	
	private PlayerCharacterService pcService;
	
	private BaseData baseData;

	@GetMapping("/api/tools/equipment-upgrade")
	public Map<GearSlot, Collection<Item>> getUpgradeSuggestion(
			@RequestParam String cname, @RequestParam Optional<String> jobId,
			@RequestParam(required = false) Boolean craftable) {
		PlayerCharacter pc = this.pcService.playerCharacterByKey(cname)
				.orElseThrow(() -> new PlayerCharacterMissingException(cname));
		
		Options options = GearCalculator.Options.builder()
				.craftable(craftable)
				.build();
		return this.calculator.upgradeSuggestion(pc, jobId.map(this.baseData::getJobById), options).asMap();
	}
}
