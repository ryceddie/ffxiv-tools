package zc.games.ffxiv.tools.model;

import com.google.common.base.Preconditions;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Material {
	
	private String itemId;
	
	private String itemName;
	
	private int count;
	
	public String toString() {
		return String.format("%s X%s", this.itemName, this.count);
	}

	public Material plus(int count) {
		return new Material(this.itemId, this.itemName, this.count + count);
	}
	
	public Material plus(Material another) {
		Preconditions.checkArgument(another.getItemId().equals(this.getItemId()), "operands must have same type");
		return this.plus(another.getCount());
	}

	public Material multiply(int multiplier) {
		return new Material(this.itemId, this.itemName, this.count * multiplier);
	}
	
	public Material minus(int count) {
		return new Material(this.itemId, this.itemName, this.count - count);
	}

	public Material minus(Material another) {
		Preconditions.checkArgument(another.getItemId().equals(this.getItemId()), "operands must have same type");
//		Preconditions.checkArgument(another.getCount() <= this.getCount());
		return this.minus(another.getCount());
	}
	
	public Material negate() {
		return new Material(this.itemId, this.itemName, -this.count);
	}

	public Material withZeroCount() {
		return new Material(this.itemId, this.itemName, 0);
	}
}