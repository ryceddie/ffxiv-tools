package zc.games.ffxiv.tools.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class FreeCompanyRecipe implements ItemRecipe {

	private String id;
	
	private Material product;
	
	private Bom bom;
}
