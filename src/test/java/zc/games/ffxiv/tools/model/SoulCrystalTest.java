package zc.games.ffxiv.tools.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.Test; 

public class SoulCrystalTest {

	@Test
	public void job_sc() {
		Job job = Job.builder().id("job").name("test").build();
		SoulCrystal sc = SoulCrystal.from(Item.builder()
				.id("test")
				.name("test")
				.category(new Item.Category("62", "sc", Item.CategoryGroup.ACCESSORY))
				.jobs(Arrays.asList(job))
				.build());
		
		assertThat(sc.job()).isEqualTo(job);
	}
	
	@Test
	public void derived_job_sc() {
		Job clazz = Job.builder().id("class").name("class").build();
		Job job = Job.builder().id("job").name("test").parent(clazz).build();
		SoulCrystal sc = SoulCrystal.from(Item.builder()
				.id("test")
				.name("test")
				.category(new Item.Category("62", "sc", Item.CategoryGroup.ACCESSORY))
				.jobs(Arrays.asList(clazz, job))
				.build());
		
		assertThat(sc.job()).isEqualTo(job);		
	}
}
