package zc.games.ffxiv.tools.data;

import com.google.common.collect.Range;

import zc.games.ffxiv.tools.model.Job;

public class RecipeNotebookDataResolver {
	
	private static final int LEVELING_NOTEBOOK_CAT_ID = 0;
	
	private static final int FIRST_STAR_NOTEBOOK_CAT_ID = 1000;
	
	private static final int DoH_JOB_COUNT = Job.Category.DoH_JOB_COUNT;
	
	public Range<Integer> resolveNotebookIdRange(int notebookCategoryId) {
		if (notebookCategoryId == LEVELING_NOTEBOOK_CAT_ID) {
			// normal notebooks is treated as a singular, difference between job or level is useless.
//			return Range.closed(0, DoH_JOB_COUNT * 40 - 1);
			return Range.singleton(0);
		}
		int starCategoryOrdinal = notebookCategoryId - FIRST_STAR_NOTEBOOK_CAT_ID;
		int start = FIRST_STAR_NOTEBOOK_CAT_ID + starCategoryOrdinal * DoH_JOB_COUNT;
		int end = start + DoH_JOB_COUNT - 1;
		return Range.closed(start, end);
	}

	//XXX not necessary when normal notebook is singular
//	public int calcNormalNotebookId(int jobId, int recipeLevel) {
//		return (recipeLevel - 1) / 5 + jobId * 40;
//	}
}
