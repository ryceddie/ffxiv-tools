package zc.games.ffxiv.tools.data;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;

import lombok.extern.slf4j.Slf4j;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.model.Material;
import zc.games.ffxiv.tools.model.Recipe;

@Slf4j
public class RecipeParser {
	
	private ObjectMapper jsonMapper;
	
	private ItemRepository items;
	
	private BaseData baseData;
	
	private int[] ingredientIndexes = {6, 8, 10, 12, 14, 16, 22, 24};

	public RecipeParser(ItemRepository items, BaseData baseData) {
		this.jsonMapper = new ObjectMapper();
		this.items = items;
		this.baseData = baseData;
	}

	public Iterable<Recipe> parse() {
		List<Recipe> result = Lists.newArrayList();
		Map<String, Object> recipes;
		try {
			String resourcePath = String.format("%s/recipe_%s.json", DataSpecs.ROOT_PATH, DataSpecs.RECIPE_VERSION);
			recipes = this.jsonMapper.readValue(Resources.getResource(resourcePath), Map.class);
		} catch (IOException e) {
			throw new DataException("failed to read recipe data", e);
		}
		Map<String, Object> data = (Map<String, Object>) recipes.get("data");
		log.info("recipe count: {}", data.size());
		
		for (String recipeId : data.keySet()) {
			
			List<Object> recipeData = (List<Object>) data.get(recipeId);
			try {
				buildRecipe(recipeData).ifPresent(r -> {
					result.add(r);
					log.trace("{}: {}", recipeId, r);
				});
			} catch (RuntimeException e) {
				throw new DataException("failed to build recipe: " + recipeId, e);
			}
		}
		
		return result;
	}

	private Optional<Recipe> buildRecipe(List<Object> recipeData) {
		int productNo = (int) recipeData.get(4);
		if (productNo == 0) {
			return Optional.empty();
		}
		List<Material> bom = Lists.newArrayList();
		for (int index : ingredientIndexes) {
			int itemNo = (int) recipeData.get(index);
			if (itemNo <= 0) {
				continue;
			}
			
			String itemId = String.valueOf(itemNo);
			String name = this.items.findById(itemId).get().getName();
			int count = (int) recipeData.get(index + 1);
			bom.add(Material.builder()
					.itemId(itemId)
					.itemName(name)
					.count(count)
					.build());
		}
		
		int recipeId = (int) recipeData.get(0);
		int jobId = (int) recipeData.get(2);
		int specId = (int) recipeData.get(3);
		Recipe.Spec spec = this.baseData.getRecipeSpec(specId);
		String job = this.baseData.getJobById(String.valueOf(jobId + 8)).getName();
		int productCount = (int) recipeData.get(5);
		String productId = String.valueOf(productNo);
		int notebookId = (int) recipeData.get(27);
		Recipe.NotebookCategory notebookCategory = this.baseData.getCraftingNotebookCategory(notebookId);
		Recipe recipe = Recipe.builder()
				.id(String.valueOf(recipeId))
				.spec(spec)
				.job(String.valueOf(job))
				.notebookId(notebookId)
				.notebookCategory(notebookCategory)
				.product(Material.builder()
						.itemId(productId)
						.itemName(this.items.findById(productId)
								.orElseThrow(() -> new DataException("can not find item: " + productId))
								.getName())
						.count(productCount).build())
				.bom(bom)
				.build();
		return Optional.of(recipe);
	}
}
