package zc.games.ffxiv.tools.model;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
public class Inventory {

	@NonNull
	private String code;
	
	@NonNull
	private Type type;
	
	private List<InventoryCell> cells;
	
	private transient Multimap<String, InventoryCell> index;

	@Builder
	public Inventory(String code, Type type, 
			List<InventoryCell> cells) {
		this.code = code;
		this.type = type;
		this.cells = Optional.ofNullable(cells).orElse(Collections.emptyList());
		this.index = this.cells.stream()
				.collect(Multimaps.toMultimap(
						cell -> cell.getItem().getId(), 
						cell -> cell, 
						ArrayListMultimap::create));

//		if (code.equals("AC_SOULS") && !cells.isEmpty()) {			
//			Item.Category.resolveSoulCrystalTo(this.cells.get(0).getItem().getCategory());
//		}
	}
	
	public int size() {
		return this.cells.size();
	}
	
	public Collection<InventoryCell> search(String itemId) {
		return this.index.get(itemId);
	}
	
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("code", this.code)
				.add("type", this.type)
				.add("cell-num", this.cells.size())
				.toString();
	}
	
	public static enum Type {
		PRIMARY,
		CRYSTAL,
		QUEST,
		ARMOR,
		EQUIPMENT,
		RETAINER,
		CHOCOBO,
		UNKNOWN;

		public boolean isPlayers() {
			return this != RETAINER && this != CHOCOBO;
		}

		public boolean isRetainers() {
			return this == RETAINER;
		}
		
		public boolean isChocobos() {
			return this == CHOCOBO;
		}
	}
}