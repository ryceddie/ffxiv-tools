package zc.games.ffxiv.tools.data;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;

import zc.games.ffxiv.tools.model.Job;

class JobCategoryDataResolver {
	
	private static final Splitter JOB_CATEGORY_LABEL_SPLITTER = 
			Splitter.on(CharMatcher.whitespace().or(CharMatcher.is(','))).omitEmptyStrings();
	
	private List<Job> jobs;
	
	private ListMultimap<Integer, Job> idIndex;

	public JobCategoryDataResolver(List<Job> jobs) {
		this.jobs = jobs;
		this.idIndex = ArrayListMultimap.create();
	}

	List<Job> resolve(int id, String jLabel, String eLabel) {
		List<Job> result = doResolve(id, eLabel);
		this.idIndex.putAll(id, result);
		return result;
	}

	private List<Job> doResolve(int id, String eLabel) {
		// All
		if (id == 1) {
			return this.jobs;
		}
		// Disciple categories
		if (id >= 30 && id <= 33) {
			return this.jobs.stream()
					.filter(job -> job.getPrimaryCategoryId() == id)
					.collect(Collectors.toList());
		}
		// Disciple composition
		if (id == 34) {
			return ImmutableList.<Job>builder()
					.addAll(this.idIndex.get(30))
					.addAll(this.idIndex.get(31))
					.build();
		}
		// Disciple composition
		if (id == 35) {
			return ImmutableList.<Job>builder()
					.addAll(this.idIndex.get(32))
					.addAll(this.idIndex.get(33))
					.build();
		}
		if (id == 36) {
			return this.idIndex.get(34).stream()
					.filter(job -> !job.getId().equals("1"))
					.collect(Collectors.toList());
					
		}
		if (id == 70) {
			return this.idIndex.get(33).stream()
					.filter(job -> !job.getId().equals("15"))
					.collect(Collectors.toList());			
		}
		if (id >= 74 && id <= 83 || id == 104) {
			// return dummy values for these useless categories.
			return this.jobs;
		}
		if (id == 108) {
			return ImmutableList.copyOf(this.idIndex.get(34));
		}
		if (id == 109) {
			// return dummy values for these useless categories.
			return this.jobs;
		}
		// Jobs of the Disciples of War or Magic
		if (id == 110) {
			return this.idIndex.get(34).stream()
					.filter(job -> !job.isClass())
					.collect(Collectors.toList());
		}
		// All classes and jobs (excluding limited jobs)
		if (id == 130) {
			return this.jobs.stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		// Any Disciple of War or Magic (excluding limited jobs)
		if (id == 142) {
			return this.idIndex.get(34).stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		// Disciples of War (excluding limited jobs)
		if (id == 143) {
			return this.idIndex.get(30).stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		// Any Disciple of Magic (excluding limited jobs)
		if (id == 144) {
			return this.idIndex.get(31).stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		// Any job of the Disciples of War or Magic (excluding limited jobs)
		if (id == 146) {
			return this.idIndex.get(110).stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		// Tank (excluding limited jobs)
		if (id == 156) {
			return this.idIndex.get(59).stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		// Healer (excluding limited jobs)
		if (id == 157) {
			return this.idIndex.get(64);
		}
		// Physical DPS (excluding limited jobs)
		if (id == 158) {
			return this.idIndex.get(118);
		}
		// Magical DPS (excluding limited jobs)
		if (id == 159) {
			return this.idIndex.get(116).stream()
					.filter(job -> !job.getId().equals("129"))
					.collect(Collectors.toList());
		}
		if (eLabel.contains("Facet of")) {
			eLabel = Splitter.on(CharMatcher.anyOf("()")).splitToList(eLabel).get(1);
		}
		try {
			return JOB_CATEGORY_LABEL_SPLITTER.splitToList(eLabel).stream()
					.map(this::resolveJobByAbbr)
					.collect(Collectors.toList());
		} catch (DataException e) {
			throw new DataException(String.format("failed to resolve job: id=%s, e-label=%s", id, eLabel), e);
		}
	}
	
	private Job resolveJobByAbbr(String abbr) {
		return this.jobs.stream()
				.filter(job -> job.getEnglishAbbr().equals(abbr))
				.findFirst()
				.orElseThrow(() -> new DataException(String.format("failed to resolve job abbr: '%s'", abbr)));
	}
}
