package zc.games.ffxiv.tools.data.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import zc.games.ffxiv.tools.data.ActorState;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.PlayerCharacterInventories;
import zc.games.ffxiv.tools.data.PlayerCharacterState;
import zc.games.ffxiv.tools.data.repository.PlayerCharacterRepository;
import zc.games.ffxiv.tools.model.Chocobo;
import zc.games.ffxiv.tools.model.ConcreteItem;
import zc.games.ffxiv.tools.model.GearSlot;
import zc.games.ffxiv.tools.model.Inventory;
import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Job;
import zc.games.ffxiv.tools.model.JobState;
import zc.games.ffxiv.tools.model.PlayerCharacter;
import zc.games.ffxiv.tools.model.PlayerCharacter.JobStates;
import zc.games.ffxiv.tools.model.Retainer;
import zc.games.ffxiv.tools.model.SoulCrystal;

@AllArgsConstructor
public class PlayerCharacterService {
	
	@NonNull
	private BaseData baseData;
	
	@NonNull
	private PlayerCharacterRepository repo;
	
	private ActorService actorService;
	
	//TODO persist suite cache
	private final Map<String, Map<Job, Map<GearSlot, ConcreteItem>>> suiteCache = Maps.newHashMap();

	public void update(String characterKey, PlayerCharacterState newSnapshot) {
		this.repo.updateState(newSnapshot);
	}
	
	public void update(PlayerCharacterInventories inventories) {
		String characterKey = inventories.getCharacterKey();
		Preconditions.checkState(this.repo.stateOf(characterKey).isPresent(), 
			String.format("can not find character state for '%s'", characterKey));

		this.repo.updateInventoryData(characterKey, inventories.getOwns());

		this.actorService.retainer().ifPresent(retainerState -> {
			this.repo.updateActorState(retainerState);
			this.repo.updateInventoryData(retainerState.getKey().toString(), inventories.getRetainers());
		});
		
//		Optional<String> retainerKey = this.resolveRetainerKey(characterKey, inventories.getRetainers());
//		retainerKey.ifPresent(key -> {			
//			this.repo.updateInventoryData(key, inventories.getRetainers());
//		});
		
		this.repo.updateInventoryData(characterKey + "-chocobo", inventories.getChocobos());
		
		this.updateSuiteCache(
				characterKey, this.repo.stateOf(characterKey).get().getCurrentJob(), inventories.getCurrentEquipment());
	}

//	private Optional<String> resolveRetainerKey(String playerCharacterKey, List<Inventory> retainerInventories) {
//		if (retainerInventories.isEmpty()) {
//			return Optional.empty();
//		}
//		
//		Multimap<String, Inventory> inventoryState = this.repo
//				.inventoryStateOf(RetainerKey.keyMatcherFor(playerCharacterKey));
//		String retainerKey = inventoryState.asMap().entrySet().stream()
//				.filter(entry -> compare(entry.getValue(), retainerInventories))
//				.findFirst()
//				.map(entry -> entry.getKey())
//				.orElse(RetainerKey.of(playerCharacterKey, inventoryState.keySet().size() + 1));
//		return Optional.of(retainerKey);
//	}

//	private boolean compare(Collection<Inventory> oldSnapshot, Collection<Inventory> newSnapshot) {
//		Inventories old = new Inventories(oldSnapshot);
//		return newSnapshot.stream()
//				.anyMatch(currentInv -> {
//					return old.getByCode(currentInv.getCode())
//							.map(preInv -> preInv.equals(currentInv))
//							.orElse(false);
//				});
//	}

	@Deprecated
	public void update(String characterKey, List<Inventory> inventoryData) {
		this.repo.updateInventoryData(characterKey, inventoryData);
	}

	public List<PlayerCharacter> getAll() {
		return this.repo.allStates().stream()
				.map(this::toDomainModel)
				.collect(Collectors.toList());
	}
	
	public Collection<String> playerCharacterKeys() {
		return this.repo.allStates().stream()
				.map(PlayerCharacterState::getName)
				.collect(Collectors.toList());
	}

	public Optional<PlayerCharacter> playerCharacterByKey(String characterKey) {
		return this.repo.stateOf(characterKey).map(this::toDomainModel);
	}
	
	public PlayerCharacter getPlayerCharacterByKey(String characterKey) {
		return this.playerCharacterByKey(characterKey)
				.orElseThrow(() -> new PlayerCharacterMissingException(characterKey));
	}
	
	public Map<GearSlot, ConcreteItem> suiteOf(String characterKey, Job job) {
		Preconditions.checkArgument(playerCharacterByKey(characterKey).isPresent(), "unknown character key: " + characterKey);
		return Optional.ofNullable(this.suiteCache.get(characterKey))
				.flatMap(index -> Optional.ofNullable(index.get(job)))
				.orElse(Collections.emptyMap());
	}
	
	private PlayerCharacter toDomainModel(PlayerCharacterState state) {
		String playerCharacterKey = state.getName();
		List<JobState> jobStates = state.getJobInfo().entrySet().stream()
				.map(entry -> new JobState(this.baseData.getJobByAbbr(entry.getKey()), entry.getValue()))
				.collect(Collectors.toList());
		List<Inventory> playerInventoryList = this.repo.inventoryStateOf(playerCharacterKey);
		Collection<Job> derivedJobs = this.resolveDerivedJobs(new PlayerCharacterInventories(state.getName(), playerInventoryList));
		return PlayerCharacter.builder()
				.name(playerCharacterKey)
				.currentJob(state.getCurrentJob())
				.jobStates(new JobStates(jobStates, derivedJobs))
				.inventories(playerInventoryList)
				.retainers(this.repo.actorStateOf(ActorState.Key.matcherFor(playerCharacterKey, ActorState.Type.RETAINER))
						.stream()
						.map(retainerState -> {
							Collection<Inventory> inventories = this.repo.inventoryStateOf(retainerState.getKey().toString());
							return new Retainer(retainerState.getName(), inventories);
						})
						.collect(Collectors.toList()))
//				.retainers(this.repo.inventoryStateOf(RetainerKey.keyMatcherFor(playerCharacterKey))
//						.asMap().entrySet().stream()
//						.map(entry -> new Retainer(entry.getKey(), entry.getValue()))
//						.collect(Collectors.toList()))
				.chocobo(new Chocobo(this.repo.inventoryStateOf(ChocoboKey.of(playerCharacterKey))))
				.build();
	}
	
	private Collection<Job> resolveDerivedJobs(PlayerCharacterInventories playerCharacterInventories) {
		List<Job> extended = playerCharacterInventories.soulCrystals().stream()
				.map(SoulCrystal::from)
				.filter(sc -> sc.job().parent().isPresent())
				.map(SoulCrystal::job)
				.collect(Collectors.toList());
		return extended;
	}

	private void updateSuiteCache(String characterKey, Job job, Map<GearSlot, ConcreteItem> suite) {
		Map<Job, Map<GearSlot, ConcreteItem>> suiteIndex = this.suiteCache.computeIfAbsent(characterKey, key -> Maps.newHashMap());
		suiteIndex.merge(job, suite, (cachedSuite, newSuite) -> {
			return cachedSuite.entrySet().stream().anyMatch(this.betterThan(newSuite)) ? cachedSuite : newSuite;
		});
	}

	private Predicate<Entry<GearSlot, ConcreteItem>> betterThan(Map<GearSlot, ConcreteItem> newSuite) {
		return equip -> {
			GearSlot slot = equip.getKey();
			ConcreteItem gear = equip.getValue();
			return Optional.ofNullable(gear)
					.map(currentGear -> {
						return Optional.ofNullable(newSuite.get(slot))
								.map(newGear -> currentGear.getSpec().getLevel() >= newGear.getSpec().getLevel() 
									&& (currentGear.isHq() || newGear.isNq()))
								.orElse(true);
					})
					.orElse(false);
		};
	}
	
	@Deprecated
	private static class RetainerKey {
		
		static String of(String playerCharacterKey, String retainerName) {
			return String.format("%s-retainer-%s", playerCharacterKey, retainerName);
		}
		
		@Deprecated
		static String of(String playerCharacterKey, int ordinal) {
			return String.format("%s-retainer#%s", playerCharacterKey, ordinal);
		}
		
		static Predicate<String> keyMatcherFor(String playerCharacterKey) {
//			return retainerKey -> retainerKey.startsWith(playerCharacterKey + "-retainer#");
			return retainerKey -> retainerKey.startsWith(playerCharacterKey + "-retainer-");
		}
	}
	
	private static class ChocoboKey {
		
		static String of(String playerCharacterKey) {
			return playerCharacterKey + "-chocobo";
		}
	}
}
