package zc.games.ffxiv.tools.util;

import zc.games.ffxiv.tools.model.Item;
import zc.games.ffxiv.tools.model.Job;

public class ItemUtils {

	public static boolean isGlamourItem(Item item) {
		return item.getLevel() == 1 && item.getRequiredLevel() == 1 
				&& item.getJobCategoryCode() == Job.Category.ALL.getCode();
	}
}
